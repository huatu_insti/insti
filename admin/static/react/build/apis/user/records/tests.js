if ( !window.apis ) window.apis = {};
if ( !window.apis.user ) window.apis.user = {};
if ( !window.apis.user.records ) window.apis.user.records = {};

apis.user.records.tests = function (){
    if ( "tests" in this ) throw new Error("需要先实例化 apis.user.records.tests 类( 伪 Class )");
    this.name = "tests api";
    this.version = "0.1";
};

apis.user.records.tests.prototype.fetch = function (opts, xmen){
    // 获取 用户 练习记录
    if ( !xmen ) var xmen = function (){};
    if ( !opts ) var opts = {};
    if ( !opts.size ) opts.size = 10;
    if ( !opts.page || parseInt(opts.page) < 0 ) opts.page = 0;
    var url = "/apis/whipapp/user/records?";
    /*
        练习类型（ttype） :
            1: 快速练习
            2: 专项练习
            3: 错题重练
        练习完成状态（status） :
            0: 未完成
            1: 完成
        {
            "secretid": 2,
            "records": [
                {
                    "tid": 31,
                    "tname": "测试",
                    "ttype": 1,
                    "status": 0,
                    "time": 1427880618
                }
            ],
            "total": 1,
            "updtime": 1427880618
        }
    */
    url += "page=" + opts.page + "&size=" + opts.size;
    requests.get(url, {"async": true, "callback": xmen});
};

apis.user.records.tests.prototype.unfinished = function (opts, xmen){
    // 获取用户 未完成练习记录
    if ( !xmen ) var xmen = function (){};
    if ( !opts ) var opts = {};
    if ( !opts.size ) opts.size = 10;
    if ( !opts.page || parseInt(opts.page) < 0 ) opts.page = 0;
    var url = "/apis/whipapp/user/unfinished/records?";
    /*
        练习类型（ttype） :
            1: 快速练习
            2: 专项练习
            3: 错题重练
        练习完成状态（status） :
            0: 未完成
            1: 完成
        {
            "secretid": 2,
            "records": [
                {
                    "tid": 31,
                    "tname": "测试",
                    "ttype": 1,
                    "status": 0,
                    "time": 1427880618
                }
            ],
            "total": 1,
            "updtime": 1427880618
        }
    */
    url += "page=" + opts.page + "&size=" + opts.size;
    requests.get(url, {"async": true, "callback": xmen});
};
