
if ( !window.apis ) window.apis = {};

apis.exams = function (){
    if ( "exams" in this ) throw new Error("需要先实例化 apis.exams 类( 伪 Class )");
    this.name = "exams api";
    this.version = "0.1";
};

apis.exams.prototype.fetch = function (page, size, xmen){
    // undone.
    // if (!hookid ) var hookid = 0;
    if ( !page ) var page = 1;
    if ( !size ) var size = 20;
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/exams?page=#page#&size=#size#"
              //.replace("#hookid#", hookid)
              .replace("#page#", parseInt(page) -1 )
              .replace("#size#", size);
    requests.get(url, {"async": true, "callback": xmen});
};

apis.exams.prototype.make = function (exam, xmen){
    if (!exam ) throw new Error("Error .");
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/exams";
    requests.put(url, exam, {"async": true, "callback": xmen});
};

apis.exams.prototype.pull = function (eid, xmen){
    if ( !eid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/exams/" + eid;
    requests.get(url, {"async": true, "callback": xmen});
};

apis.exams.prototype.remove = function (eid, xmen){
    if ( !eid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/exams/" + eid;
    requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
};
