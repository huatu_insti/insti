if ( !window.apis ) window.apis = {};
if ( !window.apis.user ) window.apis.user = {};
if ( !window.apis.user.records ) window.apis.user.records = {};

apis.user.records.favors = function (){
    if ( "favors" in this ) throw new Error("需要先实例化 apis.user.records.favors 类( 伪 Class )");
    this.name = "favors api";
    this.version = "0.1";
};
apis.user.records.favors.prototype.fetch_old = function (opts, xmen){
    if (!opts ) var opts = { "size": 20, "page": 1 };
    if ( !opts.size ) opts.size = 20;
    if ( !opts.page ) opts.page = 1;
    
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/whipapp/user/favors";
    /*
        {
            "favors": [
                {
                    "qid": 1,
                    "time": 1426650285
                },
                {
                    "qid": 2,
                    "time": 1426650285
                }
            ],
            "favorsnum": 2,
            "inittime": 1426650186,
            "updtime": 1426650285,
            "tombstone": 0,
            "secretid": 2
        }
    */
    requests.get(url, {"async": true, "callback": xmen});
};
apis.user.records.favors.prototype.fetch = function (node, xmen){
    // 根据节点获取收藏本当中的标签树
    if ( parseInt(node) < 0 ) return false;
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/whipapp/user/favors/catalog/"+parseInt(node).toString();
    /*
        {
            "catalogs": [
                {
                    "superCid": 1195
                    "cid": 1234,
                    "cname": "me",
                    "totalNum": 1,
                    "rightNum": 0,
                    "rightRate": 0.0
                }
            ]
        }
    */
    requests.get(url, {"async": true, "callback": xmen});
};

apis.user.records.favors.prototype.fetchByCatalog = function (cid, xmen){
    // 获取收藏在该标签下面的试题
    if ( !cid || parseInt(cid) < 1 ) throw new Error ("error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/whipapp/user/favors/questions/" + cid.toString();
    /*
        {
            "questions": [
                123, 456, 789
            ]
        }
    */
    requests.get(url, {"async": true, "callback": xmen});
};
apis.user.records.favors.prototype.remove = function (favors, xmen){
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/whipapp/user/favors";
    if ( Object.keys(favors).length == 0 ) {
        // 清空用户所有收藏记录
        requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
    } else {
        // 删除某个收藏记录
        // favors : {"action":"REMOVE","favors":[{"qid":1,"time":1426650285}]}
        requests.put(url, {"action": "REMOVE", "favors": favors}, {"async": true, "callback": xmen});
    }
};

apis.user.records.favors.prototype.append = function (favors, xmen){
    // 新增用户收藏记录
    if ( !xmen ) var xmen = function (){};
    // favors: {"action":"ADD","favors":[{"qid":1,"time":1426650285}]}
    var url = "/apis/whipapp/user/favors";
    requests.put(url, {"action": "ADD", "favors": favors }, {"async": true, "callback": xmen});
};
apis.user.records.favors.prototype.hit = function (qid, xmen){
    // 查询用户是否收藏此题
    if ( !xmen ) var xmen = function (){};
    // favors: {"action":"ADD","favors":[{"qid":1,"time":1426650285}]}
    var url = "/apis/whipapp/user/favors/" + qid;
    requests.get(url, {"async": true, "callback": xmen});
};
