if ( !window.components ) window.components = {};
if ( !window.components.papers ) window.components.papers = {};

window.components.papers.list =  React.createClass({
    displayName: "PaperList",
    name: "试卷列表",
    papers: [],
    page: 1,
    area: -9,
    size: 20,
    api: new apis.papers(),
    dialog: null,
    getInitialState: function (){
        if ( !this.props.page || !this.props.size ) throw new Error("window.components.papers.list init fail.");
        this.page = this.props.page;
        // this.area = this.props.area;
        this.size = this.props.size;
        window.paper_list = this;
        return {"papers": [], "count": 0};
    },
    componentDidMount: function (){
        this.fetch();
    },
    fetch: function (){
        // 提取试卷列表
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "papers" in body != true ) body.papers = [];
                if ( "count" in body != true ) body.count = 0;
                //body.args = { "page": self.state.args.page, "area": self.state.args.area, "size": self.state.args.size };
                self.replaceState(body);
                self.forceUpdate();
            }catch(e){
                console.warn(e);
            }
        };
        this.api.fetch(parseInt(this.page), parseInt(this.size), callback);
    },
    setReviewStatus: function (pid, status){
        // 设置 试卷的 审核状态.
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                JSON.parse(response.body);
                self.fetch();
            }catch(e){
                console.warn(e);
            }
        };
        var action_dict = {"0": "reset", "1": "update", "2": "update", "3": "update", "4": "update"};
        this.api.updateStatus(pid, action_dict[status.toString()], callback );
    },
    removePaper: function (pid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                JSON.parse(response.body);
                self.fetch();
                // $("tr[data-eid='#pid#']".replace("#pid#", pid)).remove();
            }catch(e){
                alert("操作失败！");
                console.warn(e);
            }
        };
        this.api.remove(pid, callback);
    },
    hit_the_kp: function (cid, cname){
        // console.log("hit the kp.");
        // console.log("cid: " + cid + "   name: " + cname);
        var obj = $(".exam_tag");
        if ( obj.length == 0 ) {
            console.warn("DOM组件不完整"); return false;
        }
        $(obj).val(cname).data("cid", cid);
    },
    hit_the_kps: function (kps){
        var self = this;
        kps.map(function (kp, i){
            self.hit_the_kp(kp.cid, kp.name);
        });
    },
    mktree: function (){
        var self = this;
        var node = $('<div class="ktree_dialog"></a>');
        node.dialog({"title": "选择试卷标签"});
        var destroy = function (){
            $(node).dialog("close");
        };
        React.render(
            React.createElement( window.components.catalogs.exams.ktree_dialog, {'ok': self.hit_the_kps, 'destroy': destroy } ), 
            node[0]
            // document.getElementById("dialog") 
        );
    },
    addExam: function (pid, opts){
        // 添加考试
        if ( !opts ) var opts = {};
        var self = this;
        var paper;

        this.state.papers.map(function(_paper, index){
            if ( _paper.pid.toString() == pid.toString() ) paper = _paper;
        });

        var question_total = 0;
        var score = 0;

        var year=0, area=-9;
        if ( "attrs" in paper != true ) paper.attrs = [];
        if ( "struct" in paper != true ) paper.struct = [];

        paper.attrs.map(function(attr, i){
            if ( attr.name == "year" ) year = attr.value;
            if ( attr.name == "area" ) area = attr.value;
        });

        var compute_total = function (elem){
            if ( "struct" in elem != true )  elem.struct = [];
            question_total += elem.struct.length;
            var i = 0;
            for (i=0; i<elem.struct.length; i++ ) {
                if ( "struct" in elem.struct[i] == true) {
                    compute_total(elem.struct[i]);
                }
            }
        };
        compute_total(paper);

        var form = $( ("<div class=\"form1\" data-method=\"post\">"

                           + "<div class=\"add_int\">"
                            + "<label>试卷名称<\/label><input class=\"add_inp\" type=\"text\" name=\"test_name\" value=\"#NAME#\" />"
                           + "<\/div>"

                           + "<div class=\"add_int\">"
                            + "<div class=\"add_int_num\"><label>试卷题量<\/label><input class=\"add_inp_num\" type=\"text\" name=\"test_num\" value=\"#TOTAL#\" /><\/div>"
                            + "<div class=\"add_int_score\"><label>试卷分数<\/label><input class=\"add_inp_score\" type=\"text\" name=\"test_score\" value=\"#SCORE#\" /><\/div>"
                           + "<\/div>"

                           + "<div class=\"add_int\">"
                            + "<div class=\"add_int_year\"><label>试卷年份<\/label><input class=\"add_inp_year\" type=\"text\" name=\"test_num\" value=\"#YEAR#\" /><\/div>"
                            + "<div class=\"add_int_area\"><label>试卷地区<\/label><input class=\"add_inp_area\" type=\"text\" name=\"test_score\" value=\"#AREA#\" /><\/div>"
                           + "<\/div>"

                           + "<div class=\"add_int\">"
                            + "<label>考试时长<\/label><input class=\"add_inp_time\" type=\"text\" name=\"test_name\" value=\"#DURATION#\" />"
                           + "<\/div>"

                           + "<div class=\"add_int\"><label>选择标签<\/label><a class=\"add_int_choose\">点击选择标签<\/a><\/div>"
                           + '<div class=\"add_int\"><label>考试标签<\/label><input class=\"add_inp_time exam_tag\" type=\"text\" name=\"test_name\" value="" /><\/div>'
                           + "<div class=\"add_int\"><label>答题报告<\/label><input class=\"add_inp_check\" type=\"checkbox\" /><span>交卷后立即查看<\/span><\/div>"
                           + "<div class=\"add_int add_timeto\">"
                            + "<div class=\"add_int_endtime\">"
                                + "<label>最晚入场时间<\/label>"
                                + "<input class=\"add_inp_endtime\" type=\"datetime-local\" name=\"test_name\" value=\"1428254458\" />"
                            + "<\/div>"

                            + "<div class=\"add_int_to\">"
                                + "<label>至<\/label>"
                                + "<input class=\"add_inp_totime\" type=\"datetime-local\" name=\"test_name\" value=\"1428254500\" />"
                            + "<\/div>"
                       + "<\/div>")
                       .replace( "#NAME#", paper.name )
                        .replace("#TOTAL#", question_total)
                        .replace("#SCORE#", score)
                        .replace("#YEAR#", year)
                        .replace("#AREA#", area)
                        .replace("#DURATION#", 20) );

        var dialog = $('<div class=\"add_test\"></div>').append(
            $('<div class=\"add_test_title\"><span>添加考试<\/span><img src=\"\/static\/images\/add_test_close.png\"><\/div>')
        ).append(
            form
        ).append(
            $('<div class=\"add_btn\">  <\/div>').append(
                $("<a class=\"add_canal\">取消<\/a>").on("click", function(){
                    console.log("cancel add exam.");
                })
            ).append(
                $("<a class=\"add_new\">添加<\/a>")
            )
        );

        var callback = function (cid, elems, dialog1, response){
            if ( response.type == 'progress' ) return 'loading ...';

            console.info("catalog.exam.bind.");
            console.log("cid: " + cid + "  Elems: " + JSON.stringify(elems) );
            console.log(response);
            if ( response.code != 200 ) {
                console.warn(':: function papers.addExam fail.'); return false;
            }

            try{
                var body = JSON.parse(response.body);
            }catch (e){
                console.warn(e); return
            }
            var eid = body.eid;
            alert("添加考试成功");
            var api = new apis.catalogs.exams();
            elems[0].id = eid;
            
            api.updateRef("append", cid, elems);
            $(dialog1).dialog("close");

        };
        var init_exam = function (){
            /*
                "pid" : paper.pid,
                "name" : paper.name, 
                "uppertime" : time.time() + 1000,
                "lowertime" : time.time() + 960000,
                "duration" : 120*60
            */
            var obj = $(this).parent().parent();
            var duration = $(obj).find(".add_inp_time").val();
            var spend_report = $(obj).find(".add_inp_check").val();
            var uppertime = parseInt($(obj).find(".add_inp_endtime").val());
            var lowertime = parseInt($(obj).find(".add_inp_totime").val());
            var exam_tag = { "cid": parseInt($(obj).find(".exam_tag").data("cid")), "name": $(obj).find(".exam_tag").val() };
            if ( isNaN(uppertime) || isNaN(lowertime) || uppertime < 1 || lowertime < 1 ) {
                uppertime = time.time();
                lowertime = time.time() + 6000;
            } 
            var data = {
                "pid" : parseInt(paper.pid),
                "name" : paper.name, 
                // "tag": JSON.stringify(exam_tag),
                "uppertime" : parseInt(uppertime),
                "lowertime" : parseInt(lowertime),
                "duration" : parseInt(duration)
            };
            // alert( JSON.stringify(data) );
            console.log(data);
            var api = new apis.exams();
            api.make(
                    data, 
                    callback.bind(
                            self, 
                            parseInt($(obj).find(".exam_tag").data("cid")), 
                            [{"name": paper.name}], 
                            dialog 
                    ) 
            );
        };
        $(dialog).find(".add_int_choose").on("click", self.mktree);
        $(dialog).find(".add_canal").on("click", function(){ $(dialog).dialog("close"); } );
        $(dialog).find(".add_new").on("click", init_exam );

        $(dialog).dialog({
            title: '发布考试',
            width: 700,
            height: 550,
            closed: true,
            cache: false,
            modal: true
        });

    },
    onSelect: function (e, rid){
        var self = this;
        // var action = parseInt(e.target.selectedOptions[0].value);
        var action = parseInt($(e.target).val());
        // var pid = parseInt(e.target.dataset['pid']);
        var pid = parseInt( $(e.target).data('pid') );
        var status = parseInt( $(e.target).data('status') );
        if ( action == 1 ) {
            // 审核通过
            this.setReviewStatus(pid, 2);
            return;
            if ( status == 1 ) this.setReviewStatus(pid, 2);
            else alert("该试卷还在编辑当中，请先提交审核吧 ...");
        } else if ( action == 2 ) {
            // 审核不通过
            if ( status == 1 ) this.setReviewStatus(pid, 0);
            else alert("该试卷还在编辑当中，请先提交审核吧 ...");
        } else if ( action == 3 ) {
            // 编辑试卷
            if ( status ==  0) window.views.papers.edit(pid);
            else alert("该试卷已经锁定，无法再进行编辑!");
        } else if ( action == 4 ) {
            // 删除试卷
            this.removePaper(pid);
        } else if ( action == 5 ) {
            // 添加考试
            this.addExam(pid);
            return;
            if ( status == 3 ) this.addExam(pid);
            else alert("只有审核通过的试卷才可以发布考试哦");
        } else {
            console.warn("unknow action type.");
        }
    },
    onSetPage: function (page, e){
        var self = this;

        if ( ! page || (parseInt(page) < 1  && page != "goto" ) ) return false;
        if ( page == "goto" ) page = parseInt($(e.target).val());
        if ( parseInt(page) == parseInt(this.page) ) return true;
        if ( parseInt(page) > 0 && parseInt(page) < Math.ceil(this.state.count/this.size)+1 ){
            this.page = parseInt(page);
            this.fetch();
        }
    },
    render: function (){
        // 渲染模板
        var self = this;
        var status_table = {"0": "编辑中", "1": "等待审核", "2": "审核通过", "3": "发布"};
        if ( "papers" in this.state ) var papers = this.state.papers;
        else var papers = [];

        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "试卷列表"), 
                    React.createElement("div", {className: "input_test"}, React.createElement("a", {href: "javascript:views.papers.init();"}, "录入试卷"))
                ), 

                React.createElement("div", {className: "paper_sort_main"}, 
                    React.createElement("div", {className: "paper_sort_title"}, 
                        React.createElement("span", null, "试卷名称"), 
                        React.createElement("input", {type: "text", placeholder: "请输入试卷名称", className: "inp_search"}), 
                        React.createElement("a", {className: "sort_search", href: "javascript:;"}, "搜索")
                    ), 
                    React.createElement("table", {className: "tab_01", bordercolor: "#ddd", border: "1px", align: "left", cellSpacing: "0", cellPadding: "0", width: "100%"}, 
                       React.createElement("thead", null, 
                            React.createElement("tr", null, 
                                React.createElement("th", null, "序号"), 
                                React.createElement("th", null, "试卷名称"), 
                                React.createElement("th", null, "创建时间"), 
                                React.createElement("th", null, "审核状态"), 
                                React.createElement("th", {className: "sort_operate"}, "操作")
                           )
                        ), 
                        React.createElement("tbody", {id: "tid", align: "center"}, 
                             papers.map(function (paper, index){
                                return (React.createElement("tr", {"data-pid": paper.pid, key: paper.pid}, 
                                    React.createElement("td", null, paper.pid), 
                                    React.createElement("td", null, paper.name), 
                                    React.createElement("td", null, window.time.ctime(paper.inittime)), 
                                    React.createElement("td", null, status_table[paper.status.toString()]), 
                                    React.createElement("td", {className: "sort_operate"}, 
                                        React.createElement("a", {onClick: views.papers.preview.bind(self, paper.pid)}, "查看"), 
                                        React.createElement("select", {className: "sort_select", "data-pid": paper.pid, "data-status": paper.status, onChange: self.onSelect}, 
                                            React.createElement("option", {value: "0"}, "更多"), 
                                            React.createElement("option", {value: "1"}, "审核通过"), 
                                            React.createElement("option", {value: "2"}, "审核不通过"), 
                                            React.createElement("option", {value: "3"}, "编辑试卷"), 
                                            React.createElement("option", {value: "4"}, "删除试卷"), 
                                            React.createElement("option", {value: "5"}, "添加考试")
                                        )
                                    )
                                ));
                            }), 
                            
                            React.createElement("tr", {key: "paper_list_nav___"}, 
                                React.createElement("td", {colSpan: "5"}, 
                                    React.createElement("div", {className: "page_left"}, React.createElement("span", null, "共", this.state.count, "条记录，每页", this.size, "条，共", Math.ceil(this.state.count/this.size), "页")), 
                                    React.createElement("div", {className: "page_right"}, 
                                        React.createElement("span", {className: "page_sp"}, 
                                            "跳转至第          ", 
                                            React.createElement("input", {type: "text", className: "page_num", 
                                                        placeholder: this.page+1, 
                                                        onChange: this.onSetPage.bind(self, "goto") }), "页，" + ' ' +
                                            "页数 ", this.page, " / ", Math.ceil(this.state.count/this.size)
                                        ), 
                                        React.createElement("a", {className: "page_top", onClick: this.onSetPage.bind(self, this.page-1) }, "<"), 
                                        React.createElement("a", {className: "page_down", onClick: this.onSetPage.bind(self, this.page+1) }, ">")
                                    )
                                )
                            )
                       )
                  )
                )
            )
            );
    },
});
