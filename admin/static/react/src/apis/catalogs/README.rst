标签操作
====================


:Author: 吴楚楠

.. contents::

获取单个标签
-------------------
**Operation**

::

    GET /catalogs/questions/:cid
    GET /catalogs/papers/:cid
    GET /catalogs/exams/:cid

**Request Sample**

    curl "http://115.159.4.84:9123/catalogs/questions/1"

删除单个标签
--------------------

**Operation**
::

    DELETE /catalogs/questions/:cid
    DELETE /catalogs/papers/:cid
    DELETE /catalogs/exams/:cid

**Request Sample**

    curl -X DELETE "http://115.159.4.84:9123/catalogs/questions/1"

插入入新标签
---------------------

**Operation**
::

    PUT /catalogs/questions
    PUT /catalogs/papers
    PUT /catalogs/exams

**Request Sample**

.. code:: sh

    curl -X PUT --header "Content-type:application/json"\
        -d '{"name" : "言言语理解","hookid" : 1}'\
        'http://115.159.4.84:9123/catalogs/questions'

更新单个试题标签
----------------------------

**Operation**
::

    PATCH /catalogs/questions/:cid
    PATCH /catalogs/papers/:cid
    PATCH /catalogs/exams/:cid

**Request Sample**

.. code:: sh

    curl -X PATCH --header "Content-type:application/json"\
        -d '{"name" : "言言语理解改","hook" : 2}'\
        'http://115.159.4.84:9123/catalogs/questions/2'

获取指定标签及其子子标签的关联试题并集
------------------------------------------------------------
**Operation**
::

    PATCH /catalogs/questions/:cid/links
    PATCH /catalogs/papers/:cid/links
    PATCH /catalogs/exams/:cid/links

**Request Sample**
    
    curl "http://115.159.4.84:9123 /catalogs/questions/1/links"
    
建立立新的标签与试题的关联
--------------------------------------------------

**Operation**
::

    PATCH /catalogs/questions/:cid/links
    PATCH /catalogs/papers/:cid/links
    PATCH /catalogs/exams/:cid/links

**Request Sample**

.. code:: sh

    curl
        -X PATCH
        --header "Content-type:application/json"
        -d '[1,2,3,4]'
        "http://115.159.4.84:9123/catalogs/questions/2/links"

移除一一个标签下指定的关联试题
----------------------------------------------------

**Operation**

::

    PATCH /catalogs/questions/:cid/links
    PATCH /catalogs/papers/:cid/links
    PATCH /catalogs/exams/:cid/links

**Request Sample**

.. code:: sh

    curl
        -X PATCH
        --header "Content-type:application/json"
        -d '[2,3]'
        "http://115.159.4.84:9123/catalogs/questions/2/links/garbage"

清空一一个标签下的所有关联试题
----------------------------------------------------

**Operation**
::

    DELETE /catalogs/questions/:cid/links
    DELETE /catalogs/papers/:cid/links
    DELETE /catalogs/exams/:cid/links

**Request Sample**

    curl -X DELETE "http://115.159.4.84:9123/catalogs/questions/2/links"

获取一一个标签的所有子子标签
---------------------------------------------------

**Operation**
::

    GET /catalogs/questions/:cid/links
    GET /catalogs/papers/:cid/links
    GET /catalogs/exams/:cid/links

**Request Sample**

    curl "http://115.159.4.84:9123/catalogs/questions/2/children"
