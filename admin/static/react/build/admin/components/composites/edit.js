if ( !window.components ) window.components = {};
if ( !window.components.composites ) window.components.composites = {};
components.composites.edit = React.createClass({displayName: "edit",
    name: "分组",
    tixu: 0,
    elements: [],
    api: new apis.composites(),
    getInitialState: function(){
        if ( !this.props.cqid ||  parseInt(this.props.cqid < 0 ) ) throw new Error("components.composites.edit react init fail.");        
        this.tixu = this.props.index.join("-");
        return {
                "cqid": parseInt(this.props.cqid),
                "content": "",
                "tombstone":0,
                "updtime":time.time(),
                "inittime":time.time(),
                "struct": []
            };
    },
    // 组织 主体 ( question body )
    init: function (){
        var self = this;
        var composite = {
                "cqid": parseInt(this.props.cqid),
                "content": "",
                "tombstone":0,
                "updtime":time.time(),
                "inittime":time.time(),
                "struct": []
        };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function composite.init() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                self.props.paper.elements["cqid"+body.cqid] = self;
                self.replaceState(body);
            }catch(e){
                print(":: composite.init fail. json decode fail.");
                print(response);
            }
        };
        this.api.init(composite, callback);
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function composite.pull() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                
            }catch(e){
                print(":: composite.pull fail. json decode fail.");
                print(response);
            }
            self.props.paper.elements["cqid"+body.cqid] = self;
            self.replaceState(body);
        };
        this.api.pull(this.props.cqid, callback);
    },
    componentDidMount: function (){
        if ( parseInt(this.props.cqid) == 0 ){
            this.init();
        } else if ( parseInt(this.props.cqid) > 0 ) {
            this.pull();
        }
    },
    remove: function (){
        var self = this;
        var elem = {"cqid": parseInt(this.state.cqid)};
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function paper.remove() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "parent" in this.props ) {
                    // 当前元素为子元素，刷新父级元素的结构即可，不用刷新整套试卷结构
                    delete self.props.paper.elements["cqid"+self.state.cqid];
                    self.props.parent.pull();
                } else {
                    self.replaceState(body);
                }
                // self.replaceState(body);
            }catch(e){
                print(":: paper.remove fail. json decode fail.");
                print(response);
            }
        };
        if ( "paper" in this.props ){
            this.props.paper.updateStruct( "remove", elem, callback);
        } else {
            throw new Error("remove composite element from paper struct fail.\npaper object required.");
        }
    },
    updateStruct: function (action, elem){
        if ( !action || !elem ) throw new Error("composite.updateStruct fail.");
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function composite.updateStruct() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                self.setState({"struct": []});
                self.pull();
                // self.replaceState(body);
            }catch(e){
                print(":: composite.updateStruct fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        this.api.updateStruct(this.state.cqid, action, elem ,callback);
    },
    addQuestion: function (){
        var self = this;
        var api = new apis.questions();
        var question = {
            "tombstone":0,
            "updtime":time.time(),
            "inittime":time.time(),
            "qid":0,
            "score": 0,
            "content": "",
            "ans": "",
            "ansnum": 0,
            "choices": [],
            "attrs": []
        };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.init() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "qid" in body ) var elem = {"qid": body.qid};
                else if ( "cqid" in body ) var elem = {"cqid": body.cqid};
                self.updateStruct("append", elem);
            }catch(e){
                print(":: question.init fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        api.init(question, callback);
    },
    // body update
    setContent: function (e, rid){
        var url, callback, self, obj, composite;
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var composite = JSON.parse(JSON.stringify(self.state));
        composite.content = $(obj).html();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function composite.setContent() fail.');
                print(response);
                return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                // 回滚之前的 试题主体
                print(":: composite.setContent fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        // delete composite['struct'];
        this.api.updateBody( this.state.cqid, composite, callback );
    },
    handleDragStart: function (e, rid){
        // 拖放开始
        if ( $(e.target).hasClass('element') ){
            var tixu = $(e.target).find(".tixu").text();
            print("composite: 正在拖动元素 " + tixu);
            window._dragevent = {"src": tixu, "dest": null};
        }
    },
    handleDragEnter: function (e, rid){
        if ( $(e.target).hasClass('element') ){
            // 正确的位置
            var tixu = $(e.target).find(".tixu").text();
            print("composite: 拖动元素至 " + tixu);
            window._dragevent.dest = tixu;
        } else if ($(e.target).parent().hasClass('element') ) {
            var tixu = $(e.target).parent().find(".tixu").text();
            print("composite: 拖动元素至 " + tixu);
            window._dragevent.dest = tixu;
        }
    },
    handleDragEnd: function (e, rid){
        var self = this;
        var api = {"question": this.api, "composite": new apis.composites() };

        print(window._dragevent);
        if ( window._dragevent.dest == null || window._dragevent.src == null ) {
            print("composite: 指令无法执行");
            return false;
        }

        if ( $(e.target).hasClass('element') ){
            // DEBUG.
            print(window._dragevent);

            var dest = window._dragevent.dest.split("-");
            var src = window._dragevent.src.split("-");

            var src_element = null;
            var dest_element = null;
            var dest_dom = $( 'dt[data-tixu="#tixu#"]'.replace("#tixu#", dest.join("-") ) );
            var elements = self.props.paper.state.struct; // paper struct

            if ( dest.length < 1 ) throw new Error("drag fail.");

            if ( !this.props.parent ) {
                // paper struct
                
                if ( dest_dom.data("cqid") != undefined ){
                    dest_element = self.props.paper.elements["cqid"+$(dest_dom).data("cqid")];
                } else {
                    dest_element = self.props.paper.elements["qid"+$(dest_dom).data("qid")];
                }
                if ( !dest_element.props.parent ){
                    // paper
                    if ( "cqid" in dest_element.state ){
                        // 
                        self.props.paper.updateStruct("remove", {"cqid": self.state.cqid} );
                        dest_element.updateStruct( "append", {"cqid": self.state.cqid });
                    } else if ( "qid" in dest_element.state ) {
                        // self.props.paper.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(0, 1)) });
                        console.warn("组合元素不可以挂载在试题元素上面");
                    }
                } else {
                    // composite
                    if ( "cqid" in dest_element.state ){
                        self.props.paper.updateStruct("remove", {"cqid": self.state.cqid} );
                        dest_element.updateStruct( "append", {"cqid": self.state.cqid });
                    } else if ( "qid" in dest_element.state ) {
                        self.props.paper.updateStruct("remove", {"cqid": self.state.cqid} );
                        dest_element.props.parent.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(-1)) });
                        // console.warn("组合元素不可以挂载在试题元素上面");
                    }
                }
            } else {
                // composite struct
                if ( dest_dom.data("cqid") != undefined ){
                    dest_element = self.props.paper.elements["cqid"+$(dest_dom).data("cqid")];
                } else {
                    dest_element = self.props.paper.elements["qid"+$(dest_dom).data("qid")];
                }
                if ( !dest_element.props.parent ){
                    // paper
                    if ( "cqid" in dest_element.state ){
                        self.props.parent.updateStruct("remove", {"cqid": self.state.cqid} );
                        dest_element.updateStruct( "append", {"cqid": self.state.cqid });
                    } else if ( "qid" in dest_element.state ) {
                        // self.props.paper.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(0, 1)) });
                        console.warn("组合元素不可以挂载在试题元素上面");
                    }
                } else {
                    // composite
                    if ( "cqid" in dest_element.state ){
                        // 
                        self.props.parent.updateStruct("remove", {"cqid": self.state.cqid} );
                        dest_element.updateStruct( "append", {"cqid": self.state.cqid });
                    } else if ( "qid" in dest_element.state ) {
                        // dest_element.props.parent.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(-1)) });
                        console.warn("组合元素不可以挂载在试题元素上面");
                    }
                }
            }

        }
    },
    
    render: function (){
        var self = this;
        // draggable onDragOver onDragStart onDragEnd
        return (
            React.createElement("dt", {className: "element", 
                "data-tixu": this.tixu, 
                "data-cqid": this.state.cqid, 
                draggable: "true", 
                onDragStart: this.handleDragStart, 
                onDragEnter: this.handleDragEnter, 
                onDragEnd: this.handleDragEnd}, 
                
                React.createElement("div", {className: "paper_item_slide clearfix composite"}, 
                    React.createElement("div", {className: "paper_drag_item none"}), 
                    React.createElement("div", {className: "paper_item_slide_tit", title: "题序"}, 
                        React.createElement("span", {className: "tixu", contentEditable: "true"}, this.tixu), 
                        "（", React.createElement("span", {className: "flag"}, this.name), "）"
                    ), 
                    React.createElement("div", {className: "paper_item_title", contentEditable: "true", onBlur: this.setContent}, this.state.content), 
                    React.createElement("div", {className: "paper_item_del", onClick: this.remove}), 
                    React.createElement("div", {className: "paper_item_toggle paper_item_hide none"})
                ), 

                React.createElement("div", {className: "paper_item_substance_composite"}, 
                     this.state.struct.map(function(elem, index){
                        var _index = JSON.parse(JSON.stringify(self.props.index));
                        _index.push(index+1);
                        if ( "qid" in elem ){
                            return React.createElement(window.components.questions.edit, {
                                        key: _index.join("-") + ":qid"+elem.qid.toString(), 
                                        qid: elem.qid.toString(), 
                                        paper: self.props.paper, 
                                        parent: self, 
                                        index: _index})
                        } else if ( "cqid" in elem ){
                            return React.createElement(window.components.composites.edit, {
                                        key: _index.join("-") + ":cqid"+elem.cqid.toString(), 
                                        cqid: elem.cqid.toString(), 
                                        paper: self.props.paper, 
                                        parent: self, 
                                        index: _index})
                        } else {
                            print("unknow element type.");
                            return React.createElement("div", {className: "element"}, "Sync Element Fail.");
                        }
                    }) 
                )
            )
            );
    }
});


/*





*/
