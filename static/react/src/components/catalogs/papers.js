if ( !window.components ) window.components = {};
if ( !window.components.catalogs ) window.components.catalogs = {};
if ( !window.components.catalogs.papers ) window.components.catalogs.papers = {};

window.components.catalogs.papers.list =  React.createClass({
    displayName: "PaperCatalogs",
    name: "试卷标签列表",
    page: 1,
    area: -9,
    size: 20,
    api: new apis.catalogs.papers(),
    getInitialState: function (){
        this.fetch();
        return { "list": [], "num": 0 };
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs fetch fail.");
                console.warn(response);
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
                self.replaceState(body);
            }catch(e){
                console.warn(e);
            }
        };
        /*
            list: [
                0: {name: "二级", hookid: 0, updtime: 1426655982, inittime: 1426655982, tombstone: 0, cid: 3},
                1: {name: "Food", hookid: 0, updtime: 1423122459, inittime: 1423122459, tombstone: 0, cid: 1},
            ],
            num: 5
        */
        this.api.root('0', callback);
    },
    addTag: function (cid, hookid, name){
        var self = this;
        if ( !name ) {
            // 交互
            var name = prompt();
        }
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs.add fail.");
                console.warn(response);
            }
            self.fetch();
        };
        if ( cid == '0' && hookid == '0' ) this.api.push({"name": name, "hookid": "0" }, callback );
        else this.api.push({"name": name, "hookid": cid.toString() }, callback );
    },
    updateTag: function (cid, hookid, name){
        if ( !name ) {
            // 交互
            var name = prompt();
        }
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs.update fail.");
                console.warn(response); return ;
            }
            self.fetch();
        };
        this.api.update(cid, {"name": name, "hookid": hookid }, callback );
    },
    removeTag: function (cid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs.remove fail.");
                console.warn(response); return ;
            }
            self.fetch();
        };
        this.api.remove(cid, callback);
    },
    render: function (){
        var self = this;
        var catalog_elems_count = function (catalog){
            if ( "elems" in catalog ) {
                return catalog.elems.length;
            } else {
                return 0;
            }
        };
        return (
            <table className="table">
                <thead>
                    <th>试卷标签名称</th>
                    <th>试卷数量</th>
                    <th>试卷排序</th>
                    <th>操作</th>
                </thead>
                <tbody>
                    { this.state.list.map(function(catalog, index){
                        return (
                            <tr data-cid={catalog.cid} data-hookid={catalog.hookid} data-elems={ catalog_elems_count(catalog) } data-index={index}>
                                <td>{catalog.name}</td>
                                <td>{ catalog_elems_count(catalog) }</td>
                                <td>未知资料</td>
                                <td>
                                    <div className="btn-group">
                                        <button type="button" className="btn btn-default" onClick={self.addTag.bind(self, catalog.cid.toString() ,catalog.hookid.toString(), null )}>添加子标签</button>
                                        <button type="button" className="btn btn-primary" onClick={self.updateTag.bind(self, catalog.cid.toString(), catalog.hookid.toString(), null )} >编辑标签</button>
                                        <button type="button" className="btn btn-danger" onClick={self.removeTag.bind(self, catalog.cid.toString() )} >删除标签</button>
                                    </div>
                                </td>
                            </tr>
                        );
                    }) }
                </tbody>
                <button type="button" className="btn btn-primary" onClick={this.addTag.bind(this, '0' ,'0', null )} >添加一级标签</button>
            </table>
            );
    }
});