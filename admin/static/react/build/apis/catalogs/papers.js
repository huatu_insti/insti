
if ( !window.apis ) window.apis = {};
if ( !window.apis.catalogs ) window.apis.catalogs = {};

apis.catalogs.papers = function (){
    if ( "papers" in this || "catalogs" in this ) throw new Error("需要先实例化 apis.catalogs.papers 类( 伪 Class )");
    this.name = "catalogs.papers api";
    this.version = "0.1";
};

/*
    @            cid: 标签编号
            hookid: 父级标签

*/
apis.catalogs.papers.prototype.root = function (cid, xmen){
    // 提取试卷标签的 根标签
    if ( !xmen ) var xmen = function (){};
    if ( !cid ) var cid = 0;
    
    if ( parseInt(cid) == 0 ) var url = "/instiApis/whipbase/catalogs/papers";
    else var url = "/instiApis/whipbase/catalogs/papers/" + cid + "/children";

    requests.get(url, {'"async': true, "callback": xmen});
};
apis.catalogs.papers.prototype.fetch = function (cid, xmen){
    // 提取该标签的 子标签
    if ( !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    if ( parseInt(cid) == 0 ) var url = "/instiApis/whipbase/catalogs/papers";
    else var url = "/instiApis/whipbase/catalogs/papers/" + cid + "/children";

    requests.get(url, {'"async': true, "callback": xmen});
};

apis.catalogs.papers.prototype.pull = function (cid, xmen){
    if ( !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/catalogs/papers/" + cid ;
    requests.get(url, {'"async': true, "callback": xmen});
};
apis.catalogs.papers.prototype.push = function (catalog, xmen){
    if ( !catalog ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    // catalog format: { "name":"常识与判断", "hookid":"0","elems":[ {"id":2,"name": "hhh"} ] }
    // Note: 这里的 hookid: String
    var url = "/instiApis/whipbase/catalogs/papers";
    requests.put(url, catalog, {'"async': true, "callback": xmen});
};
apis.catalogs.papers.prototype.update = function (cid, catalog, xmen){
    if ( !catalog || !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    // catalog format: {"name" : "言言语理解", "hookid" : 1}
    // Note: Hookid: Int
    var url = "/instiApis/whipbase/catalogs/papers/" + cid;
    requests.patch(url, catalog, {'"async': true, "callback": xmen});
};
apis.catalogs.papers.prototype.remove = function (cid, xmen){
    if ( !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/catalogs/papers/" + cid ;
    requests.METHOD_DELETE(url, {'"async': true, "callback": xmen});
};
