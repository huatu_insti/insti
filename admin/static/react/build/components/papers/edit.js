//  paper.js

if ( !window.components ) window.components = {};
if ( !window.components.papers ) window.components.papers = {};

// React.unmountComponentAtNode(document.getElementById('content'));


window.components.papers.tixu_card =  React.createClass({
    displayName: "components.papers.edit",
    name: "试卷编辑-题序卡",
    tixu: "0",
    getInitialState: function (){
        if ( !this.props.elem ) throw new Error("window.components.papers.tixu_card react init fail.");
        if ( !this.props.index ) throw new Error("window.components.papers.tixu_card react init fail.");
        if ( !this.props.paper ) throw new Error("window.components.papers.tixu_card react init fail.");

        this.tixu = this.props.index.join("-");
        var elem = this.props.elem;
        if ( "qid" in elem ) elem.struct = [];
        return elem;
    },
    render: function (){
        var self = this;
        if ( "qid" in this.state ) {
            return (
                React.createElement("li", {
                    "data-qid": this.state.qid, 
                    "data-index": this.props.index[this.props.index.length-1], 
                    "data-tixu": this.tixu}, 
                    React.createElement("a", {href: "javascript:;"}, this.tixu)
                )
            );
        } else if ( "cqid" in this.state ){
            return (
                React.createElement("li", null, 
                    this.tixu, 
                    React.createElement("ul", {
                        "data-cqid": this.state.cqid, 
                        "data-index": this.props.index[this.props.index.length-1], 
                        "data-tixu": this.tixu}
                    )
                )
            );
        }
    }
});

/*

{ this.state.struct.map(function(elem, index){
                            return <window.components.papers.tixu_card 
                                        key={"elemID-"+elem.qid}
                                        index={self.props.index.join(index+1)} 
                                        elem={elem} />;
                        }) }

*/


window.components.papers.edit =  React.createClass({
    displayName: "components.papers.edit",
    name: "试卷编辑",
    api: new apis.papers(),
    elements: {},
    reload: 0,
    getInitialState: function (){
        if ( !this.props.pid ||  parseInt(this.props.pid < 0 ) ) throw new Error("window.components.papers.edit react init fail.");
        
        window.paper_edit = this;

        return {
                "pid": parseInt(this.props.pid),
                "status": 0,
                "name": "",
                "inittime": time.time(),
                "updtime": time.time(),
                "attrs": [ ],
                "tombstone": 0,
                "struct": []
            };
    },
    pull: function (){
        // pull paper data from server.
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(":: paper.pull fail");
                print(response);
            }
            try{
                var body = JSON.parse(response.body);
                if ( "status" in body == true && parseInt(body.status) == 0 ){
                    // 试卷当前状态为编辑中
                    self.replaceState(body);
                } else {
                    // 该试卷不再允许编辑
                    alert("该试卷不再允许进行编辑。");
                    throw new Error("试卷状态不为0，禁止编辑修改。");
                }
                
            }catch(e){
                print(e);
            }
        };
        this.api.pull(this.props.pid, callback);
    },
    componentDidMount: function() {   
        // this.setInterval(this.tick, 1000); // Call a method on the mixin
        if (  parseInt(this.props.pid) >  0 ) this.pull();
        else if ( parseInt(this.props.pid) ==  0 ) throw new Error("pid required");
    },
    // Paper Struct.
    updateStruct: function (action, elem){
        if ( !action || !elem ) throw new Error("paper.updateStruct fail.");
        var self = this;
        print(":: paper.updateStruct [#action#] ".replace("#action#", action) );
        print(elem);
        print(self);
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function paper.updateStruct() fail.');
                print(response);
                return false;
            }
            try{
                print(self);
                var body = JSON.parse(response.body);
                print("json decode done.");
                // self.replaceState({"pid": 0, "name": "", "struct": [], "attrs": []});
                // self.replaceState(body);
                self.setState({"struct": []});
                self.pull();
            }catch(e){
                print(":: paper.updateStruct fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        this.api.updateStruct(this.state.pid, action, [elem] ,callback);
    },
    addQuestion: function (e, rid, qid){
        if ( !qid ) var qid = "0";
        var self = this;
        var api = new apis.questions();
        var question = {
            "tombstone":0,
            "updtime":time.time(),
            "inittime":time.time(),
            "qid":0,
            "score": 0,
            "content": "",
            "ans": "",
            "ansnum": 0,
            "choices": [],
            "attrs": []
        };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.init() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                // 更新试卷 元素结构 ( paper.updateStruct )
                if ( "qid" in body ) var elem = {"qid": body.qid};
                else if ( "cqid" in body ) var elem = {"cqid": body.cqid};
                self.updateStruct("append", elem);
            }catch(e){
                print(":: question.init fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        api.init(question, callback);
    },
    addComposite: function (e, rid){
        var self = this;
        var api = new apis.composites();
        var composite = {
                "cqid": 0,
                "content": "",
                "tombstone":0,
                "updtime":time.time(),
                "inittime":time.time(),
                "struct": []
        };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function composite.init() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                // 更新试卷 元素结构 ( paper.updateStruct )
                if ( "qid" in body ) var elem = {"qid": body.qid};
                else if ( "cqid" in body ) var elem = {"cqid": body.cqid};
                self.updateStruct("append", elem);
            }catch(e){
                print(":: composite.init fail. json decode fail.");
                print(response);
            }
        };
        api.init(composite, callback);
    },
    tick: function() {
        this.setState({seconds: this.state.seconds + 1});
    },
    componentWillUpdate: function (){
        // print("paper will update.");
    },
    build: function (){
        var elements = [];
        var self = this;
        this.elements = {};
        this.state.struct.map(function(elem, index){
            if ( "qid" in elem ){
                elements.push(React.createElement(window.components.questions.edit, {
                            key: "qid"+elem.qid.toString(), 
                            qid: elem.qid.toString(), 
                            paper: self, 
                            reload: time.time(), 
                            index: [index+1]}));
            } else if ( "cqid" in elem ){
                elements.push(React.createElement(window.components.composites.edit, {
                            key: "cqid"+elem.cqid.toString(), 
                            cqid: elem.cqid.toString(), 
                            paper: self, 
                            reload: time.time(), 
                            index: [index+1]}));
            } else {
                print("unknow element type.");
            }
        });
        print(elements);
        return elements;
    },
    render: function() {
        // 渲染数据至DOM
        var self = this;
        var question_total = 0;    // 试卷试题总数
        var score = 0;             // 试卷总分
        this.reload++;
        var i, ii;
        for ( i in this.state.struct ) {
            if ( "qid" in this.state.struct[i] ) question_total += 1;
            else if ( "cqid" in this.state.struct[i]  && "struct" in this.state.struct[i] && this.state.struct[i].struct.length > 0 ){
                for ( ii in  this.state.struct[i].struct ){
                    if ( "qid" in this.state.struct[i].struct[ii] ) {
                        question_total += 1;
                        score += parseFloat(this.state.struct[i].struct[ii].score);
                    }
                }
            }
        }
        
        return (
            React.createElement("div", {className: "container-fluid paper_enter"}, 
              React.createElement("div", {className: "menu"}, 
                React.createElement("div", {className: "record_paper"}, "录入试卷"), 
                React.createElement("ul", {className: "creatte_ul clearfix"}, 
                    React.createElement("li", {className: "cre_f1"}, "1.创建试卷"), 
                    React.createElement("li", {className: "on"}, "2.录入试卷"), 
                    React.createElement("li", {className: "cre_f1"}, "3.预览试卷"), 
                    React.createElement("li", {className: "cre_f1"}, "4.提交审核")
                    
                ), 
                React.createElement("div", {className: "return_list"}, "返回上一步"), 
                React.createElement("div", {className: "input_test", onClick: window.views.papers.preview.bind(this, this.state.pid)}, "试卷预览")
              ), 
              React.createElement("div", {className: "clearfix"}), 
              
              React.createElement("div", {className: "main"}, 
                  React.createElement("div", {className: "import_word fl"}, 
                    React.createElement("a", {href: "javascript:;", className: "import_word_btn"}, "导入试卷文档"), 
                    React.createElement("p", null, "仅支持office2007及以上版本")
                  ), 
                  React.createElement("div", {className: "enter_paper fr"}, 

                    React.createElement("div", {className: "paper_info"}, 
                        React.createElement("div", {className: "paper_title fl"}, React.createElement("span", {className: "name"}, this.state.name)), 
                        React.createElement("div", {className: "paper_sroce fr"}, "分数: ", React.createElement("span", {className: "score"}, score), "分"), 
                        React.createElement("div", {className: "paper_num fr"}, " 题量: ", React.createElement("span", {className: "total"}, question_total), "道"), 
                        React.createElement("div", {className: "clearfix"})
                    ), 

                    React.createElement("div", {className: "paper_item"}, 
                        React.createElement("div", {className: "paper_item_edit fl"}, 
                            React.createElement("dl", {className: "paper_item_dl"}, 
                                 this.state.struct.map(function(elem, index){
                                    if ( "qid" in elem ){
                                        return React.createElement(window.components.questions.edit, {
                                                    key: [index+1].join("-") + ":qid"+elem.qid.toString(), 
                                                    qid: elem.qid.toString(), 
                                                    paper: self, 
                                                    reload: self.reload, 
                                                    index: [index+1]});
                                    } else if ( "cqid" in elem ){
                                        return React.createElement(window.components.composites.edit, {
                                                    key: [index+1].join("-") + ":cqid"+elem.cqid.toString(), 
                                                    cqid: elem.cqid.toString(), 
                                                    paper: self, 
                                                    reload: self.reload, 
                                                    index: [index+1]});
                                    } else {
                                        print("unknow element type.");
                                    }
                                }) 
                            )
                        ), 

                        React.createElement("div", {className: "paper_item_group fr"}, 
                            React.createElement("div", {className: "paper_group_view"}, "试题一览"), 
                            React.createElement("div", {className: "paper_group_name"}, 
                                React.createElement("ul", null, 
                                     this.state.struct.map(function(elem, index){
                                    if ( "qid" in elem ){
                                        return React.createElement(window.components.papers.tixu_card, {
                                                    key: "qid"+elem.qid, 
                                                    elem: elem, 
                                                    paper: self, 
                                                    index: [index+1]})
                                    } else if ( "cqid" in elem ){
                                        return React.createElement(window.components.papers.tixu_card, {
                                                    key: "cqid"+elem.cqid, 
                                                    elem: elem, 
                                                    paper: self, 
                                                    index: [index+1]})
                                    } else {
                                        print("unknow element type.");
                                        return React.createElement("div", {className: "element"}, "Sync Element Fail.");
                                    }
                                }) 
                                )
                            )
                        ), 
                        React.createElement("div", {className: "clearfix"}), 
                        React.createElement("div", {className: "paper_add"}, 
                            React.createElement("a", {className: "add_single", onClick: this.addQuestion}, "新增普通题"), 
                            React.createElement("a", {className: "add_group", onClick: this.addComposite}, "新增复合题")
                        )
                    )
                  ), 
                  React.createElement("div", {className: "clearfix"})
              )
            )
        );
    },

});