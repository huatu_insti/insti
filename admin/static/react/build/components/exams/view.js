

if ( !window.components ) window.components = {};
if ( !window.components.exams ) window.components.exams = {};

window.components.exams.view =  React.createClass({
    displayName: "ExamsView",
    name: "考试预览",
    api: new window.apis.exams(),
    questions: {},
    composites: {},
    elements: { "element": null, "component": null, "tixu": "0", "index": [1] },

    getInitialState: function (){
        if ( !this.props.eid || parseInt(this.props.eid) < 1 ) throw new Error("window.components.exams.list react init error.");
        return { "name": "", "eid": this.props.eid, "struct": [], "attrs": [] };
    },
    componentDidMount: function (){
        window.exams_preview = this;
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                console.warn(e); return false;
            }

        };
        this.api.pull(this.props.eid, callback);
    },
    total: function (t){
        var self = this;
        if ( t == "elements" ) {
            return Object.keys(self.questions).length;
        } else if ( t == "score" ) {
            var score = 0;
            Object.keys(self.questions).map(function(qid, index){
                score += parseFloat(self.questions[qid].element.score);
            });
            return score;
        }
    },
    mk_tixu_card: function (){
        // 题序卡
        var self = this;
        var tixu_card = [];

        var parse_question = function (question){
            if ( parseInt(question.qid) in self.questions ) {
                tixu_card.push(self.questions[parseInt(question.qid)] );
            }
        };
        var parse_composite = function (composite){
            if ( composite.cqid in self.composites ) {
                tixu_card.push(self.composites[parseInt(composite.cqid)] );
            }
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };

        var i = 0;
        for (i=0; i<this.state.struct.length; i++ ) {
            if ( "qid" in this.state.struct[i] == true) {
                parse_question(this.state.struct[i]);
            } else if ( "cqid" in this.state.struct[i] == true) {
                parse_composite(this.state.struct[i]);
            } 
        }
        return tixu_card;
    },
    showDetail: function (e, target){
        var target = $(e.target).parent().parent().find(".pre_detail_info");
        if ( target.hasClass("none") ) {
            target.removeClass("none");
        } else {
            target.addClass("none");
        }
    },
    goTo: function (event){
        var tixu = $(event.target).data("kaxu");
        var t = $('[data-tixu="#TIXU#"]'.replace("#TIXU#", tixu)).position().top;
        var paperScrollTop = $("#paper").scrollTop(); 
        t = t+paperScrollTop;
        $('#paper').animate({scrollTop:t},700)
    },
    handleCard:function(){
        $(".fastBottom").toggle()
    },

        
      
     
      computeQuestion:function(element){
        var pos = 0;
        var num = 0;
        var self = this;
        var parse_question = function(question){
             num += 1; 
             if( "qid" in element == true && parseInt(question.qid) == parseInt(element.qid)){ pos = num;}
        }
        var parse_composite = function( composite ){
            
            if ( "cqid" in element == true && parseInt(element.cqid) == parseInt(composite.cqid) ) pos = num+1;

            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }
        }
        var paperStruct = self.state.struct;
        for( var i=0;i<paperStruct.length;i++){
             if("qid" in paperStruct[i] ) {
                  parse_question(paperStruct[i]);
             }else if("cqid" in paperStruct[i]){
                  parse_composite(paperStruct[i]);
             }
        }
    
       return pos;

    },
    backToList:function(){
       var self = this;
       self.props.root.setState({"content":React.createElement(window.components.exams.list, {page: "1", size: "20", root: self.props.root})})
    },
    remove:function(){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            } else {
                self.backToList();
            }
        };
        // callback({"type": "hehe", "code": 500});
        $("<div class=\"confirm_exam_del\">"
            +"<div class=\"message\">确定撤出考试吗?</div>"
            +"<div class=\"add_btn\">"
                +" <a class=\"add_canal\">取消<\/a>"
                +" <a class=\"add_new\">确定<\/a>"
            +"</div></div>").dialog({
               title:"撤出考试",
               close:true,
               modal:true,
               width:"400px"
        })
        $(".confirm_exam_del").find(".add_canal").on("click",function(){ $(".confirm_exam_del").dialog("close")})
                              .end().find(".add_new").on("click",function(){ 
                                     self.api.remove(self.props.eid, callback);$(".confirm_exam_del").dialog("close");})
    },
    render: function (){
        // 渲染模板
        var self = this;
        var tixu_card = this.mk_tixu_card();
        console.log(tixu_card);
        /*
            {
                "name":"p1",
                "struct":[
                    {"qid":554},{"qid":555},{"cqid":129,"struct":[]}
                ],
                "attrs":[
                    {"name":"year","value":"2002"},{"name":"area","value":"21"}
                ],
                "uppertime":0,
                "lowertime":0,
                "duration":7200,
                "tombstone":0,
                "inittime":1427426836,
                "updtime":1427426836,
                "eid":29
            }
        */
        return ( React.createElement("div", null, 
            React.createElement("div", {style: {"overflow-y":"scroll","height":"701px","position":"relative"}, id: "paper"}, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "试卷预览"), 
                    React.createElement("ul", {className: "clearfix"}, " "), 
                    React.createElement("div", {className: "return_list", onClick: self.remove}, React.createElement("a", null, "撤出考试")), 
                    React.createElement("div", {className: "input_test", onClick: self.backToList}, React.createElement("a", null, "返回列表"))
                ), 
                React.createElement("div", {className: "pre_title"}, 
                    React.createElement("h3", {dangerouslySetInnerHTML: {__html: "试卷名称：" + this.state.name}}), 
                    React.createElement("a", {className: "pre_shouqi", onClick: this.showDetail}, "查看考试信息")
                ), 
                   
                   
                React.createElement("div", {className: "pre_detail_info none", style: {"fontSize":"18px","fontWeight":"bolder"}}, 
                    React.createElement("div", {className: "pre_detail"}, 
                        React.createElement("span", {dangerouslySetInnerHTML: {__html: "试卷题量：" + this.total("elements")}}), 
                        React.createElement("div", null), 
                        this.state.attrs.map(function (attr, index){
                            if ( (attr.name == "area" || attr.name == "地区") && attr.value.length > 1 ) {
                                var areas = attr.value.split(",").map(function (a, i){
                                    return a.replace(/\d+:/, "");
                                });
                                return React.createElement("span", null, "试卷地区：", areas.join(", "))
                            }
                        })
                    ), 
                    React.createElement("div", {className: "pre_detail"}, 
                        React.createElement("span", {dangerouslySetInnerHTML: {__html: "试卷总分：" + this.total("score")}}), 
                        React.createElement("div", null), 
                        React.createElement("span", null, "考试标签：定期模考")
                    ), 
                    React.createElement("div", {className: "pre_detail"}, 
                        this.state.attrs.map(function (attr, index){
                            if ( attr.name == "year" || attr.name == "年份" ) return React.createElement("span", null, "试卷年份：", attr.value, "年")
                        }), 
                        React.createElement("div", null), React.createElement("span", null, "答题报告：考试结束后")
                    ), 
                    React.createElement("div", {className: "pre_detail"}, 
                        React.createElement("span", null, "考试时长：", this.state.duration, "分钟"), React.createElement("br", null), 
                        React.createElement("span", null, "最晚入场时间：", time.ctime(this.state.uppertime), "至", time.ctime(this.state.lowertime))
                    )
                  ), 

                React.createElement("div", {className: "pre_type"}, 
                    React.createElement("ul", {className: "clearfix"}, 
                         tixu_card.map(function(obj, index){
                            return null;
                            if ( "qid" in obj.element ) {
                                return (
                                    React.createElement("li", {
                                        key: "tixucard_q_" + obj.element.qid, 
                                        "data-qid": obj.element.qid, 
                                        "data-tixu": obj.tixu}, 
                                        React.createElement("a", {href: "javascript:;"}, obj.tixu)
                                    )
                                );
                            } else if ( "cqid" in obj.element ) {
                                return (
                                    React.createElement("li", {
                                        key: "tixucard_c_" + obj.element.cqid, 
                                        "data-cqid": obj.element.cqid, 
                                        "data-tixu": obj.tixu}, 
                                        React.createElement("a", {href: "javascript:;"}, obj.tixu)
                                    )
                                );
                            }
                        }) 
                    )
                ), 
                React.createElement("div", {className: "elements"}, 
                    this.state.struct.map(function(elem, index){
                        if ( "qid" in elem ){

                            return React.createElement(window.components.questions.preview, {
                                        key: "preview_question_" + elem.qid.toString(), 
                                        qid: elem.qid.toString(), 
                                        paper: self, 
                                        tixu: self.computeQuestion(elem), 
                                        index: [index+1]})
                        } else if ( "cqid" in elem ){
                            return React.createElement(window.components.composites.preview, {
                                        key: "preview_composite_" + elem.cqid.toString(), 
                                        cqid: elem.cqid.toString(), 
                                        paper: self, 
                                        index: [index+1].concat(index+elem.struct.length)})
                                        
                        } else {
                            console.warn("unknow element type.");
                            return React.createElement("div", {className: "element"}, "Sync Element Fail.");
                        }
                    }) 
                )
             
                
           
         ), 
            React.createElement("div", {className: "fast_card"}, 
                    React.createElement("div", {className: "fastBtn"}, 
                        React.createElement("div", {className: "fastShowBtn"}, 
                            React.createElement("div", {className: "fastInfo", onClick: self.handleCard}, "试题卡(共计", React.createElement("span", null, this.total("elements")), "道题)")
                    ), 
                    React.createElement("div", {className: "fastLine"}), 
                    React.createElement("div", {className: "fastBottom"}, 
                        React.createElement("ul", null, 
                        tixu_card.map(function(element,index){
                               if(typeof element.tixu == 'number'){
                                   if(element.tixu % 5 == 0 ){
                                       return React.createElement("li", {style: {"marginRight":"30px"}}, 
                                                React.createElement("a", {"data-kaxu": element.tixu, onClick: self.goTo}, element.tixu))

                                   }else{
                                       return React.createElement("li", {"data-kaxu": element.tixu}, 
                                                 React.createElement("a", {"data-kaxu": element.tixu, onClick: self.goTo}, element.tixu))
                                   }
                                
                                 
                               }
                         })
                        )
                    )
                  )
                      
                      

                           
                             
                                       
                                      
                                     
                       
                )
              )
         
   
        );
    }

});