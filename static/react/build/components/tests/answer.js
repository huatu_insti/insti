
if ( !window.components ) window.components = {};
if ( !window.components.tests ) window.components.tests = {};
if ( !window.components.tests.answer ) window.components.tests.answer = {};

window.components.tests.answer.test = React.createClass({
    displayName: "components.tests.answer.test",
    name: "作答",
    api: new apis.tests(),
    tkeeper: [],
    answers: [],
    elements: { "composites": {}, "questions": {} },
    tixu_card: [],
    test_source: {'1': 'test', '2': 'catalog', '3': 'errors'},
    tips: true,   // 多选题 是否自动跳题提示
    getInitialState: function (){
        // props: tid / root
        self.elements = { "composites": {}, "questions": {} };
        self.tixu_card = [];
        self.tkeeper = [];
        
        self.clock = self.tock;
        return {
            "secretid": 0,
            "eid": 0,
            "name": "",
            "struct": [],
            "answers": [],
            "status": 0,
            "ttype": 1, // 测试生成来源
            "duration": 0,
            "events": [],
            "now": time.time(),
            "inittime": 1427781503,
            "updtime": 1427857455,
            "tombstone": 0,
            "tid": this.props.tid
        };
    },
    componentDidMount: function (){
        // 加载完后触发的函数
        var self = this;
        this.pull();

        // 答题操作块悬浮
        $('.ti_paper_con_fr').data({ "top": $('.ti_paper_con_fr').offset().top, "left":$('.ti_paper_con_fr').offset().left});
        window.onscroll = function (){
            // 悬浮效果
            var handle = $('.ti_paper_con_fr');
            var Ostop=$(window).scrollTop();
            var overTop=handle.offset().top;
            var width = handle.width();
            // side fixed
            if(Ostop>overTop){
                handle.css({position:'fixed',top:'0px',left:handle.data('left')}).width(width); 
            } else if(Ostop<handle.data('top')) {
                handle.css({position:'static'});
            }

            // 答题卡悬浮
            var dh = $(document).height();
            var p = dh - ( $('.ti_paper_sheet_fixed').offset().top+ $('.ti_paper_sheet_fixed').height() );
            if ( p < $(".ti_footer").height() ){
                $('.ti_paper_sheet_fixed').css("bottom", $(".ti_footer").height()+$('.ti_paper_sheet_top').height() + $('.ti_paper_sheet_bottom').height() + 10+"px");
                if ( $('.ti_paper_sheet_bottom').css("display") == "none" ) $('.ti_paper_sheet_bottom').show();
                // $('.ti_paper_sheet_fixed').css("margin-bottom", $(".ti_footer").height()+10+"px");
                // $(".ti_footer").css("margin-top", $('.ti_paper_sheet_fixed').height()+ 40 + "px");
            } else {
                // $('.ti_paper_sheet_fixed').css("margin-bottom", "0px");
                $('.ti_paper_sheet_fixed').css("bottom", "0px");
                $(".ti_footer").css("margin-top", $('.ti_paper_sheet_top').height() + $('.ti_paper_sheet_bottom').height() + 20 + "px");
            }
        };

        /////////
        var beforeunload = function (){
            // 保存练习进度
            // 上传中断解除信息
            self.api.addEvents(self.props.tid, [{"event": "PAUSE", "time": time.time() }]);
            return ("测试进度已保存，你可以在我的练习当中恢复测试进度。");
        };
        $(window).bind('beforeunload', beforeunload);
    },
    componentWillUnmount: function (){
        // 将要卸载时候触发的函数
        var self = this;
        console.info(" :: 答题组卷即将 卸载 ...... ");
        self.tkeeper.map(function (t, i){
            clearInterval(t);
        });
        self.tkeeper = [];

        window.onscroll = function (){};
        if ( self.issubmit != true ) {
            self.api.addEvents(self.props.tid, [{"event": "PAUSE", "time": time.time() }]);
            setalert("测试进度已保存，你可以在我的练习当中恢复测试进度。");
        }
        $(window).unbind("beforeunload");
        self = undefined;
    },
    init: function (){
        
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body); 
                if ( "answers" in body != true ) body.answers = {};
                if ( "duration" in body != true ) body.duration = 0;
                if ( "events" in body != true ) body.events = [];
            }catch(e){
                console.warn(e); return;
            }
            if ( parseInt(body.status) == 1 ) {
                // 已经交卷，无法再继续答题
                alert("该试卷已经锁定，无法再继续答题～～");
                console.warn(":: 试卷状态已经锁定，无法再继续答题～～");
                self.props.root.setState({"content": React.createElement(window.components.tests.report.test, {tid: self.props.tid, tab: "report", root: self.props.root})});
                return false;
            }
            self.answers = body.answers;
            
            if ( ['test', "catalog", "errors", "catalogs", "error"].indexOf( self.test_source[body.ttype] ) != -1 ){
                // 如果是测试 / 专项练习, 则使用正计时
                console.info(":: 正计时...");
                self.clock = self.tock;
                var duration = 0;
                var i=0;
                var pt = 0;    // 暂停时间
                for ( i=0;i<body.events.length; i+=2 ) {
                    if ( (i+1) < body.events.length  ) {
                        pt += body.events[i+1].time - body.events[i].time;
                    } else {
                        body.now = body.events[i].time; // 最后一次暂停时间
                    }
                }
                body.duration = body.now - body.inittime - pt;
            } else {
                // 倒计时
                console.info(":: 倒计时...");
                self.clock = self.tick;
                var duration = parseInt(body.duration);
                var i=0;
                var pt = 0;    // 暂停时间
                for ( i=0;i<body.events.length; i+=2 ) {
                    if ( (i+1) < body.events.length  ) {
                        pt += body.events[i+1].time - body.events[i].time;
                    } else {
                        body.now = body.events[i].time; // 最后一次暂停时间
                    }
                }
                body.duration = duration - (body.now - body.inittime - pt);
            }
            self.elements = { "composites": {}, "questions": {} };
            self.tixu_card = [];

            self.replaceState(body);
            // 启动计时器
            if ( self.tkeeper.length > 0 ) {
                self.tkeeper.map(function (t, i){
                    clearInterval(t);
                });
                self.tkeeper = [];
            }
            self.tkeeper.push( 
                setInterval( self.clock, 1000)
            );

            if ( body.events.length > 0 && body.events.slice(-1)[0].event == "PAUSE" ) {
                // 恢复该试卷最后的状态 ...
                self.pause(null, null, true);
            }
        };
        this.api.pull(this.props.tid, callback);
    },
    clock: function (){
        // 计时器

    },
    tock: function (){
        // 正计时
        var self = this;
        if ( isNaN(self.state.duration) || parseInt(self.state.duration) < 0 ) return ;
        // if ( self.state.duration == 5 ) alert("请抓紧时间答题。本次考试将在5分钟后结束。");
        // if ( self.state.duration < 1 ) {
        //     alert("本次考试已经结束！");
        //     self.tkeeper.map(function (t, i){
        //         clearInterval(t);
        //     });
        //     self.tkeeper = [];
        //     return;
        // }
        self.setState( {"duration": self.state.duration+1} );
    },
    tick: function (){
        // 倒计时 Tick-Tock
        var self = this;
        if ( isNaN(self.state.duration) || parseInt(self.state.duration) < 1 ) return ;
        if ( self.state.duration == 5 ) alert("请抓紧时间答题。本次考试将在5分钟后结束。");
        if ( self.state.duration < 1 ) {
            alert("本次考试已经结束！");
            self.tkeeper.map(function (t, i){
                clearInterval(t);
            });
            self.tkeeper = [];
            return;
        }
        // console.log( self.state.duration-1 );
        self.setState( {"duration": self.state.duration-1} );
    },
    pause: function (e, rid, noupload){
        // 答题中断事件
        console.info("试卷 事件");
        var self = this;
        var callback = function (response){

        };
        if ( noupload == true  ) {
            // 恢复试卷暂停状态
            self.tkeeper.map(function (t, i){
                clearInterval(t);
            });
            self.tkeeper = [];
            // 背景画布
            html = $('<div class="ti_dati">'
                        + '<a href="javascript:;" class="ti_continue">继续答题</a>'
                        + '</div>');
            $(html).find("a").on("click", self.pause );
            dialog({ content: html, id: "pause" }).showModal();
            return;
        }

        if ( self.tkeeper.length == 0 && noupload != true  ) {
            // 恢复答题
            if ( noupload == undefined ) {
                // 上传中断解除信息
                self.api.addEvents(self.props.tid, [{"event": "PLAY", "time": time.time() }], callback );
            }
            self.tkeeper.push( 
                setInterval( self.clock, 1000)
            );
            try{
                dialog.get("pause").remove();
            }catch(e){

            }
        } else if ( self.tkeeper.length > 0 && noupload != true ) {
            // 暂停答题
            if ( noupload == undefined ) {
                // 上传中断信息
                self.api.addEvents(self.props.tid, [{"event": "PAUSE", "time": time.time() }], callback );
            }
            self.tkeeper.map(function (t, i){
                clearInterval(t);
            });
            self.tkeeper = [];
            // 背景画布
            html = $('<div class="ti_dati">'
                        + '<a href="javascript:;" class="ti_continue">继续答题</a>'
                        + '</div>');
            $(html).find("a").on("click", self.pause );
            dialog({ content: html, id: "pause" }).showModal();
        }
    },
    save: function (){
        // 下次再做

    },
    submit: function (){
        // 设置该测试状态为结束状态 （最终交卷）
        var self = this; 
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return;
            }
            self.issubmit = true;
            self.props.root.setState({"content": React.createElement(window.components.tests.report.test, {tid: self.props.tid, tab: "report", root: self.props.root, indexbox: self.props.root})});
            dialog.get('submit').remove();
            $("#dialog").remove();
            // window.location.href="/index.php?s=/exam/report/testid/" + self.props.tid;
            return true;
        };
        var undo = Object.keys(self.elements.questions).length - Object.keys(self.state.answers).length;
        if ( undo > 0 ) {
            var html = $('<div class="ti_confirm">'
                            +        '<img src="static/images/tk_special_project_cancel.png" />'
                            +        '<div class="ti_confirm_txt">'
                            +             '还有 '+undo+' 道题未答完,确定交卷吗?'
                            +        '</div>' 
                            +        '<div class="ti_confirm_btn clearfix">'
                            +             '<a class="ti_confirm_submit fl">确定交卷</a>'
                            +             '<a class="ti_confirm_continue fr">谢谢提醒，继续</a>'
                            +        '</div>'         
                            +     '</div>');
            var ok = function (){
                self.api.markEnd(self.props.tid, callback);
            };
            var cancel = function (){
                dialog.get('submit').remove();
            };
            $(html).find(".ti_confirm_submit").on( "click",  ok);
            $(html).find(".ti_confirm_continue").on( "click",  cancel);
            $(html).find("img").on( "click",  cancel);
            
            dialog({ content: html, id: 'submit' }).showModal();
        } else {
            self.api.markEnd(self.props.tid, callback);
        }
    },
    mk_tixu_card: function (){
        var self = this;
        // this.tixu_card = [];
        var tixu_card = [];

        var parse_question = function (question){
            if ( parseInt(question.qid) in self.elements.questions ) {
                tixu_card.push(self.elements.questions[parseInt(question.qid)] );
            }
        };
        var parse_composite = function (composite){
            if ( composite.cqid in self.elements.composites ) {
                // tixu_card.push(self.elements.composites[parseInt(composite.cqid)] );
            }
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };
        var i = 0;
        for (i=0; i<this.state.struct.length; i++ ) {
            if ( "qid" in this.state.struct[i] == true) {
                parse_question(this.state.struct[i]);
            } else if ( "cqid" in this.state.struct[i] == true) {
                parse_composite(this.state.struct[i]);
            } 
        }
        return tixu_card;
        this.tixu_card = tixu_card;
    },
    compute_questions: function (action, elem){
        var num = 0;
        var pos = 0;
        var parse_question = function (question){
            if ( "qid" in elem == true && parseInt(elem.qid) == parseInt(question.qid) ) pos = num;
            num += 1;
        };
        var parse_composite = function (composite){
            if ( "cqid" in elem == true && parseInt(elem.cqid) == parseInt(composite.cqid) ) pos = num;
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }
        };

        var i = 0;
        var target;
        if ( action == "children" ) target = elem;
        else if ( action == "pos" ) target = this.state;

        if ( typeof target == 'undefined' ) {
            console.warn(" target 没有 struct.");
            return 1;
        }
        for (i=0; i<target.struct.length; i++ ) {
            if ( "qid" in target.struct[i] == true) {
                parse_question(target.struct[i]);
            } else if ( "cqid" in target.struct[i] == true) {
                parse_composite(target.struct[i]);
            } 
        }
        if ( action == "children" ) return num;
        else if ( action == "pos" ) return pos;
    },
    goto: function (e, rid){
        var tixu = $(e.target).data("tixu");
        $("body, html").scrollTop( $('.element[data-tixu="#TIXU#"]'.replace("#TIXU#", tixu)).offset().top-55 );
    },
    showTCard: function (e, rid){
        // 开关答题卡
        var target = $(e.target).parent().find('.ti_paper_sheet_bottom');
        var isshow = target.css("display");
        if ( isshow == "none" ) {
            target.css("display", "block");
        } else {
            target.css("display", "none");
        }
    },
    render: function (){
        var self = this;

        this.tixu_card = this.mk_tixu_card();
        var f_time = function (_s){
            // 时间解析器
            if ( !_s || parseInt(_s) < 0 || isNaN(_s) ) return false;
            if ( self.test_source[self.state.stamp] == 'test' ){
                // 正计时 解析器
                var t, m=0, h=0, s=parseInt(_s);
                // 计算分钟
                if (s>60&&(s%60) == 0) { m += s/60; s = 0; }
                else if (s>60&&(s%60) != 0) {
                    m += parseInt(s/60); 
                    s = parseInt(s%60);
                    // s = parseFloat('0.'+parseInt((s/60+'').replace(/\d+./,'')) )*60;
                }
                // 计算小时
                if (m>60&&(m%60) == 0) { h += m/60; m = 0; }
                else if (m>60&&(m%60) != 0) { 
                    h += parseInt(m/60); 
                    m = parseInt(m%60);
                    // m =parseInt(parseFloat('0.'+parseInt((m/60+'').replace(/\d+./,'')) )*60);
                }
                // //
                if(s>0&&(s%60) == 0) { m += 1; s = 0; }
                if(m>0&&(m%60) == 0) { h += 1; m = 0; }
                t = (h <= 9 ?"0" + h : h) + ":" + (m <= 9 ?"0" + m : m) + ":" + (s <= 9 ?"0" + s : s); 
                return t;
            } else {
                // 倒计时 解析器
                var fillZero = function (num,n){
                    var str = ''+num;
                    while(str.length<n){
                        str = '0'+str;
                    }
                    return str;
                };
                var iRemain = parseInt(_s);
                var iHour = fillZero( parseInt(iRemain/3600), 2); //剩余小时
                iRemain%=3600;  //剩下的秒数
                var iMin = fillZero(parseInt(iRemain/60), 2); //剩余分钟
                iRemain%=60;
                var iSec = fillZero(parseInt(iRemain), 2); //剩余秒
                return iHour+':'+iMin+':'+iSec;
            }
        };
        return (
            React.createElement("div", {className: "test"}, 
                React.createElement("div", {className: "ti_paper_info"}, this.state.name), 
                React.createElement("div", {className: "ti_paper_con clearfix"}, 
                    React.createElement("div", {className: "ti_paper_con_fl fl elements"}, 
                         this.state.struct.map(function (elem, index){
                                if ( "qid" in elem == true ) {
                                    return ( 
                                        React.createElement(window.components.tests.answer.question, {
                                                    key: "question_"+elem.qid+"_"+index+"_", 
                                                    parent: self, 
                                                    test: self, 
                                                    qid: elem.qid, 
                                                    elem: elem, 
                                                    index: [index+1], 
                                                    tixu: self.compute_questions( "pos", elem ) + 1}) 
                                    );
                                } else if ( "cqid" in elem  == true ) {
                                    var tixu = [ self.compute_questions("pos", elem)+1 , self.compute_questions("pos", elem) + self.compute_questions("children", elem) ] .join("-");
                                    return ( 
                                        React.createElement(window.components.tests.answer.composite, {
                                                    key: "composite_"+elem.cqid+"_"+index+"_", 
                                                    parent: self, 
                                                    test: self, 
                                                    cqid: elem.cqid, 
                                                    elem: elem, 
                                                    index: [index+1], 
                                                    tixu: tixu})
                                    );
                                } else {
                                    console.warn("Unknow Element Type.");
                                }
                            }) 
                    ), 
                    React.createElement("div", {id: "fixed_div"}), 
                    React.createElement("div", {className: "ti_paper_sheet_div"}, 
                        React.createElement("div", {className: "ti_paper_sheet ti_paper_sheet_fixed"}, 
                            React.createElement("div", {className: "ti_paper_sheet_top ti_paper_sheet_top_a", onClick: self.showTCard}, 
                                "答题卡（剩下", React.createElement("span", null, Object.keys(self.elements.questions).length - Object.keys(self.state.answers).length), "道题）"
                            ), 
                            React.createElement("div", {className: "ti_paper_sheet_bottom"}, 
                                React.createElement("ul", {className: "clearfix"}, 
                                     this.tixu_card.map(function (atom, index){
                                        if ( self.answers[atom.element.qid] != undefined && self.answers[atom.element.qid].uans.length > 0 ) var _act_class="active";
                                        else var _act_class="";
                                        if ( (index +1) % 5 == 0 ) {
                                            return (
                                                React.createElement("li", {
                                                    key: "tixu_card_key_"+(index+1).toString() + "_", 
                                                    className: "ti_num " + _act_class, 
                                                    "data-tixu": atom.tixu, 
                                                    "data-qid": atom.element.qid, 
                                                    onClick: self.goto}, 
                                                    atom.tixu2
                                                )
                                            );
                                        } else {
                                            return (
                                                React.createElement("li", {
                                                    key: "tixu_card_key_"+(index+1).toString() + "_", 
                                                    className: _act_class, 
                                                    "data-tixu": atom.tixu, 
                                                    "data-qid": atom.element.qid, 
                                                    onClick: self.goto}, 
                                                    atom.tixu2
                                                )
                                            );
                                        }
                                    }) 
                                )
                            )
                        )
                    ), 

                    React.createElement("div", {className: "ti_paper_con_fr fr"}, 
                        React.createElement("div", {className: "ti_paper_dashboards_div"}, 
                            React.createElement("div", {className: "ti_paper_dashboards"}, 
                                React.createElement("div", {className: "ti_paper_clock"}, 
                                    React.createElement("span", null, "时间 "), 
                                    React.createElement("span", {id: "time"}, f_time(this.state.duration))
                                ), 
                                React.createElement("div", {className: "ti_paper_dashboards_btn"}, 
                                    React.createElement("ul", {className: "clearfix"}, 
                                        React.createElement("li", {key: "button_pause", className: "ti_paper_pasue", onClick: this.pause}, "暂停"), 
                                        React.createElement("li", {key: "button_submit", className: "ti_paper_submit", onClick: this.submit}, "交卷")
                                    )
                                )
                            )
                        )
                    )

                )
            )
        );
    // <li className="ti_paper_next">下次做</li>
    }
});

window.components.tests.answer.question = React.createClass({
    displayName: "components.tests.answer.question",
    name: "单选题",
    tixu: 1,
    tixu2: "1",
    api: new apis.questions(),
    tips: true,   // 多选题 是否自动跳题提示
    showTips: false,
    getInitialState: function (){
        this.tixu = this.props.index.join("-");
        this.tixu2 = this.props.tixu;
        return {
            "content": "",
            "choices": [],
            "attrs": [],
            "ans": "",
            "ansnum": 1,
            "score": 0,
            "tombstone": 0,
            "updtime": 1427183577,
            "inittime": 1427183577,
            "qid": this.props.qid
        };
    },
    componentDidMount: function (){
        this.pull();
        this.props.test.elements.questions[this.props.qid] = { "tixu": this.tixu, "tixu2": this.tixu2, "component": this, "element": this.state };
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "content" in body != true ) body.content = "";
                if ( "attrs" in body != true ) body.attrs = [];
                if ( "choices" in body != true ) body.choices = [];
                if ( parseInt(body.ansnum) > 1 ) self.tips = true;
                else self.tips = false;
            }catch(e){
                console.warn(e); return;
            }
            if ( parseInt(body.ansnum) > 1 ) self.name = "多选题";
            self.replaceState(body);
        };
        this.api.pull(this.props.qid, callback);
    },
    updateChoice: function (e, rid){
        // 更新试题答案选择
        var self = this;
        var card = $(e.target).find(".key").text();
          
        var callback = function (response){  
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                
                var body = JSON.parse(response.body);
                // body: "{"answers":[{"qid":45694,"qans":"D","uans":"C","time":1446102824}],"tid":591}"
            }catch(e){
                console.warn(e); return;
            }
            
            if ( parseInt(self.state.ansnum) > 1 ) {
                // show GoOn tips. 
                if (self.tips == true && self.props.test.tips == true) self.showTips = true;
                else self.showTips = false;
            } else {
                self.goto();
            }
            self.props.test.setState( { "answers": self.props.test.answers } );
        };

        if ( self.props.test.answers[self.props.qid] != undefined ){
            // 已作答
            if ( parseInt(self.state.ansnum) > 1 ) {
                // 多选题
                if ( self.props.test.answers[self.props.qid].uans.indexOf( card.toUpperCase() ) != -1 ) {
                    self.props.test.answers[self.props.qid].uans = self.props.test.answers[self.props.qid].uans.replace(card, "");
                   

                } else {
                    
                    self.props.test.answers[self.props.qid].uans += card.toUpperCase();
                     var anr = self.props.test.answers[self.props.qid].uans.split(""); 
                    var afterSort = anr.sort(); 
                    self.props.test.answers[self.props.qid].uans = afterSort.join("");
                }
            } else if ( parseInt(self.state.ansnum) == 1 ) {
                // 单选题
                if ( self.props.test.answers[self.props.qid].uans.indexOf(card) != -1 ) {
                        
                          self.props.test.answers[self.props.qid].uans = '';
                } else {
                    self.props.test.answers[self.props.qid].uans = card;
                }
            } else {
                // 未知试题类型
                console.warn(":: 未知的试题类型，试题合法答案数量无法确定");
            }
            
        } else {
            // 未作答
            self.props.test.answers[self.props.qid] = { "qid": self.props.qid, "qans": self.state.ans, "uans": card, "time": time.time() };
        }
          // console.log(self.props.test.answers);
        self.props.test.api.uploadAns( self.props.test.props.tid, self.props.test.answers[self.props.qid], callback );
    },
    in_Ans: function (card){
        // 当前选项是否在已选择数据列表当中。
        var self = this;
        if (  self.props.test.answers[self.props.qid] != undefined ){
            if ( self.props.test.answers[self.props.qid].uans.indexOf( card.toUpperCase() ) != -1 ) {
                return "active";
            } else {
                return "";
            }
        } else {
            // 未作答
            return "";
        }
    },
    goto: function (){
        // var tixu = $(e.target).data("tixu");
        var self = this;
        var tixu = $('[data-tixu="#TIXU#"]'.replace("#TIXU#", self.tixu ) ).next().data("tixu");
        if ( tixu == undefined || tixu.toString().length < 1 ) return false;
        $("body, html").scrollTop( $('.element[data-tixu="#TIXU#"]'.replace("#TIXU#", tixu)).offset().top-55 );
    },
    shut_tips: function (area){
        var self = this;
        if ( area == "test" ) {
            self.showTips = false;
            self.props.test.tips = false;
        } else if ( area == "this") {
            self.showTips = false;
        } else {
            console.warn("Unknow Area.");
        }
        this.forceUpdate();
    },
    render: function (){
        var self = this;
        var question_class = "ti_radio_question";
        if ( this.state.ansnum > 1 )  question_class = "ti_check_question";

        var show_tips = function (){
            if ( self.showTips != true ) return [];
            // if ( parseInt(self.state.ansnum) < 2 ) return [];

            return ([
                React.createElement("div", {className: "ti_confirm_info"}, 
                    React.createElement("p", null, "温馨提示：本题是多选题，不会帮您自动下滑到下一题哦！ "), 
                    React.createElement("div", {className: "ti_info_btn clearfix"}, 
                        React.createElement("a", {onClick: self.shut_tips.bind(self, "test"), className: "ti_confirm_ok fl"}, "本页不再提示"), 
                        React.createElement("a", {onClick: self.shut_tips.bind(self, "this"), className: "ti_confirm_cancle fr"}, "关闭")
                    )
                )
            ]);
        };
        var show_skip = function (){
            // if ( self.showTips != true ) return [];
            try{
                if ( self.state.ansnum < 2  ) return [];
                if ( self.props.test.answers[self.props.qid].uans.length < 1 ) return [];
            }catch(e){
                return [];
            }
            return ([
                React.createElement("a", {className: "mchoice_skip", onClick: self.goto}, "确认，跳到下一题")
            ]);
            
        };
        return (
            React.createElement("div", {className: "element", "data-qid": this.state.qid, "data-tixu": this.tixu}, 
                React.createElement("div", {className:  question_class + " ti_question"}, 
                    React.createElement("div", {className: "question_content clearfix"}, 
                        React.createElement("div", null, this.tixu), 
                        
                        React.createElement("p", null, 
                            React.createElement("span", null, "（ ", this.name, " ）"), 
                            React.createElement("span", {dangerouslySetInnerHTML: {__html: this.state.content}})
                        )
                    ), 
                    React.createElement("div", {className: "question_choice"}, 
                        React.createElement("ul", null, 
                            this.state.choices.map(function (choice, index){
                                if ( choice == "{%NULL%}" || choice == "{%NULL%} " ) return undefined;
                                return (
                                    React.createElement("li", {
                                        className: self.in_Ans(chr(65+index)), 
                                        key: "Question_"+self.props.qid+"_choices_"+index+"_", 
                                        onClick: self.updateChoice}, 
                                        React.createElement("span", {className: "key"}, chr(65+index)), 
                                        React.createElement("span", {className: "value", dangerouslySetInnerHTML: {__html: choice}})
                                    )
                                );
                            }) 
                        )
                    ), 
                    React.createElement("div", {className: "question_btn_box"}, 
                        React.createElement("div", {className: "question_btn"}, 
                            React.createElement("dl", {className: "clearfix"}, 
                                this.state.choices.map(function (choice, index){
                                    return (
                                        React.createElement("dt", {
                                            key: "Question_"+self.props.qid+"_choices_card_"+index+"_", 
                                            className: self.in_Ans(chr(65+index)), 
                                            onClick: self.updateChoice}, 
                                            React.createElement("span", {className: "key"}, chr(65+index))
                                        )
                                    );
                                }) 
                            ), 
                            show_skip()
                        )
                    ), 
                    show_tips()
                    
                )
            )
        );
    }
});

window.components.tests.answer.composite = React.createClass({
    displayName: "components.tests.answer.composite",
    name: "复合题",
    tixu: 1,
    tixu2: "1",
    api: new apis.composites(),
    getInitialState: function (){
        this.tixu = this.props.index.join("-");
        this.tixu2 = this.props.tixu;
        return {
            "content": "",
            "struct": [],
            "tombstone": 0,
            "updtime": 1427185277,
            "inittime": 1427185277,
            "cqid": this.props.cqid
        };
    },
    componentDidMount: function (){
        this.pull();
        this.props.test.elements.composite[this.props.cqid] = { "tixu": this.tixu, "tixu2": this.tixu2, "component": this, "element": this.state };
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "content" in body != true ) body.content = "";
                if ( "struct" in body != true ) body.struct = [];
            }catch(e){
                console.warn(e); return;
            }
            if ( parseInt(body.ansnum) > 1 ) self.name = "多选题";
            self.replaceState(body);
        };
        this.api.pull(this.props.cqid, callback);
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "element"}, 
                React.createElement("div", {className: "ti_composites_content clearfix"}, 
                    React.createElement("div", {className: "ti_composites_content_fl fl"}, this.name), 
                    React.createElement("div", {className: "ti_composites_content_fr fl"}, 
                        "阅读下列材料，回答下面的题目。", React.createElement("br", null), 
                        React.createElement("p", {dangerouslySetInnerHTML: {__html: this.state.content}}), 
                        React.createElement("img", {src: "static/images/test.png"}), 
                        React.createElement("div", {style:  {"height": 100, "background": "#F00"} })
                    )
                ), 
                React.createElement("div", {className: "elements"}, 
                    this.state.struct.map(function (elem, index){
                            if ( "qid" in elem == true ) {
                                return ( 
                                    React.createElement(window.components.tests.answer.question, {
                                                key: "question_"+elem.qid+"_"+index+"_", 
                                                parent: self, 
                                                test: self.props.test, 
                                                qid: elem.qid, 
                                                elem: elem, 
                                                index: self.props.index.concat(index+1), 
                                                tixu: self.props.test.compute_questions( "pos", elem ) + 1})
                                );
                            } else if ( "cqid" in elem  == true ) {
                                var tixu = [ self.props.test.compute_questions("pos", elem)+1 , self.props.test.compute_questions("pos", elem) + self.props.test.compute_questions("children", elem) ] .join("-");
                                return ( 
                                    React.createElement(window.components.tests.answer.composite, {
                                                key: "composite_"+elem.cqid+"_"+index+"_", 
                                                parent: self, 
                                                test: self.props.test, 
                                                cqid: elem.cqid, 
                                                elem: elem, 
                                                index: self.props.index.concat(index+1), 
                                                tixu: tixu}) 
                                );
                            } else {
                                console.warn("Unknow Element Type.");
                            }
                        })
                )
            )
        );
    }
});