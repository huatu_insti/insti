
/*

1.试题操作
    获取单个试题的详细信息
    URL:    http://115.159.4.84:9321/questions/:qid
    e.g:    http://115.159.4.84:9321/questions/521?token=eyJhcHBpZCI6NiwiaW5pdHRpbWUiOjE0MjcxNjcyOTcsImV4cGlyZWluIjoxNDI3NDI2NDk3fQ==.eXwKrU3jMDebfRe2wTZQesh96bg=
    Method: GET
    Parameter:  qid
    Descrp: 
    返回json:
    {
        "content": "tset",
        "choices": "test",
        "ans": "A",
        "ansnum": 4,
        "score": 1,
        "tombstone": 0,
        "updtime": 1427183577,
        "inittime": 1427183577,
        "qid": 521
    }
    
    删除单个试题
    URL:    http://115.159.4.84:9321/questions/:qid
    e.g:    http://115.159.4.84:9321/questions/521?token=eyJhcHBpZ...
    Method: DETELE
    Parameter:  qid
    Descrp: 
    返回json:
    {
        "content": "tset",
        "choices": "test",
        "ans": "A",
        "ansnum": 4,
        "score": 1,
        "tombstone": 1,
        "updtime": 1427183577,
        "inittime": 1427183577,
        "qid": 521
    }
    
    新建单个试题
    URL:    http://115.159.4.84:9321/questions/
    e.g:    http://115.159.4.84:9321/questions?token=eyJhcHBpZC...
    Method: PUT
    Parameter:{ "content" : "tset", "choices" : "test", "attrs" : [], "ans" : "A", "ansnum" : 4, "score" : 1.0 } 
    Descrp: content:试题主干，choices：试题选项，attrs：试题扩展，ans：试题答案，ansnum：试题答案个数，score：试题分数
    返回json:
    {
        "content": "tset",
        "choices": "test",
        "ans": "A",
        "ansnum": 4,
        "score": 1,
        "tombstone": 1,
        "updtime": 1427183577,
        "inittime": 1427183577,
        "qid": 521
    }
    
    修改单个试题
    URL:    http://115.159.4.84:9321/questions
    e.g:    http://115.159.4.84:9321/questions?token=eyJhcHBpZC...
    Method: PATCH
    Parameter:
    Descrp: 
    返回json:
    {
        "content": "tset",
        "choices": "A,B,C,D",
        "ans": "A",
        "ansnum": 4,
        "score": 1,
        "tombstone": 0,
        "updtime": 1427184493,
        "qid": 522
    }
*/

if ( !window.apis ) window.apis = {};

apis.questions = function (){
    if ( "questions" in this ) throw new Error("需要先实例化 apis.questions 类( 伪 Class )");
    this.name = "question api";
    this.version = "0.1";
};

apis.questions.prototype.init = function (body, xmen){
    if ( !body ){
        var body = {
                "attrs":[],   // "attrs":[{"name":"comment","value":"test"}]
                "ans":"",
                "ansnum": 0,
                "score":0,
                "tombstone":0,
                "updtime":time.time(),
                "inittime":time.time(),
                "qid": 0
            };
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/questions";
    requests.put(url, body, {"async": true, "callback": xmen});
};
apis.questions.prototype.pull = function (qid, xmen){
    if ( !qid || parseInt(qid) < 1 ){
        throw new Error("apis.questions.pull fail");
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/questions/" + qid;
    requests.get(url, {"async": true, "callback": xmen});
};

apis.questions.prototype.updateBody = function (qid, body, xmen){
    if ( !qid || parseInt(qid) < 1 || !body ){
        throw new Error("apis.questions.updateBody fail");
    }
    /*

        {
            "content": "tset",
            "choices": ["A,B,C,D"],
            "ans": "A",
            "ansnum": 4,
            "score": 1,
            "tombstone": 0,
            "updtime": 1427184493,
            "qid": 522
        }

    */
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/questions";
    requests.patch(url, body, {"async": true, "callback": xmen});
};
apis.questions.prototype.updateAttrs = function (qid, action, attr, xmen){
    /*
        Action ( handler ):
            append: question.attrs.append
            remove: question.attrs.remove
            clean:  question.attrs.clean

        Method: PATCH

        Http Body: { "handler" : "question.attrs.append", "attrs" : [{"cid":1,"cname":"test","year":2015}] }

        Response:
            {
                "content": "tset",
                "choices": ["A,B,C,D"],
                "ans": "A",
                "ansnum": 4,
                "score": 1,
                "tombstone": 0,
                "updtime": 1427273968,
                "inittime": 1427183836,
                "qid": 522
            }
        Note:
            cid: catlog id.
            cname: catalog name.
            attrs: [ {"cid":1,"cname":"test"}, {"year":2015} ]
    */
    if ( !qid || parseInt(qid) < 1 || !action || !attr ){
        throw new Error("apis.questions.updateAttrs fail");
    }
    if ( !xmen ) var xmen = function (){};
    var dict = {"append": "question.attrs.append", "remove": "question.attrs.remove", "clean": "question.attrs.clean" };
    var url = "/instiApis/whipbase/questions/" + qid + "/attrs";
    var attrs = { "handler" : dict[action], "attrs" : [ attr ] };
    requests.patch(url, attrs, {"async": true, "callback": xmen});
};

apis.questions.prototype.remove = function (qid, xmen){
    if ( !qid || parseInt(qid) < 1 ){
        throw new Error("apis.questions.delete fail");
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/questions/" + qid;
    requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
};

