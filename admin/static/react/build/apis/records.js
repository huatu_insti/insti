/*

2.2.9 用用户练习记录操作

获取用用户练习记录
Operation
	
	GET /records/:secretid?page=:page&size=:size

Request Sample
	
	curl “http://115.159.4.84:9123/records/1?page=0&size=10”


*/


if ( !window.apis ) window.apis = {};

apis.records = function (){
    if ( "exams" in this ) throw new Error("需要先实例化 apis.records 类( 伪 Class )");
    this.name = "records api";
    this.version = "0.1";

};
apis.records.prototype.fetch = function (secretid, page, size, xmen){
    if (!secretid ) throw new Error("Error .");
    if ( !page ) var page = 0;
    if ( !size ) var size = 20;
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipbase/records/#secretid#?page=#page#&size=#size#"
    		  .replace("#secretid#", secretid)
    		  .replace("#page#", page)
    		  .replace("#size#", size);
    requests.get(url, {"async": true, "callback": xmen});
};