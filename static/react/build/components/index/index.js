if ( !window.components ) window.components = {};
if ( !window.components.index ) window.components.index = {};

window.components.error = React.createClass({displayName: "error",
  getInitialState: function (){
    var self = this;
    return {"code": this.props.code, "title": this.props.title, "msg": this.props.msg};
  },
  render: function (){
    var self = this;
    return (
        React.createElement("div", {className: "ERROR"}, 
            React.createElement("div", {className: "title"}, this.state.title), 
            React.createElement("div", {className: "msg"}, this.state.msg), 
            React.createElement("div", {className: "code"}, this.state.code)
        )
    );
  }
});



window.components.index.index = React.createClass({
    displayName: "components.index.index",
    name: "首页",
    api: new apis.tests(),
    getInitialState: function (){
        var self = this;
        return { 
            "content": React.createElement(window.components.index.boxs, {
                                    root: self, 
                                    indexbox: self}),
             showTop:true
        };
    },
    componentDidMount: function (){
          
    },
    componentWillUpdate: function (){

    },
    pull: function (){
        
    },
    render: function (){
        var self = this;
    
        return (
            React.createElement("div", null, 
               [1].map(function(){
                  if(self.state.showTop != false){ 
                      return  React.createElement(window.components.share.header, {root: self, indexbox: self, updateEvent: time.time()})
                 }
               

               }), 
               
                this.state.content, 
                React.createElement(window.components.share.footer, {root: self, indexbox: self})
            )
        );
    }
});

window.components.index.boxs =  React.createClass({
    displayName: "components.index.boxs",
    name: "九宫格功能树",
    api: new apis.tests(),
    getInitialState: function (){
        // props: 
        return { };
    },
    componentDidMount: function() {
       
    },
    // exam/errors/catalog/test
    mk_fast_test: function (){
        // 快速练习
        var self = this;
        loading.show('正在组卷');
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return;
            }
            loading.hide();
            self.props.root.setState({
                    "content": React.createElement(window.components.tests.answer.test, {
                                            tid: body.tid, 
                                            root: self.props.root})
            });
        };
        this.api.init("test", { "num" : 20, "duration" : 0, "year": [], "extent": [] }, callback);
    },
    mk_catalog_test: function (){
        // 专项练习
        var self = this;

        var node = $('<div class="ktree_dialog"></a>');
        // node.dialog({"title": "选择知识点", "width": 500, "height": 600});
        dialog({ content: node, id: 'ktree_dialog' }).showModal();

        var callback = function (response){
            // 根据试题标签制作一份测试
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                console.log(body); 
            }catch(e){
                console.warn(e); return;
            }
            loading.hide();
            self.props.root.setState({
                "content": React.createElement(window.components.tests.answer.test, {
                                        tid: body.tid, 
                                        root: self.props.root})
            });
        };
        var destroy = function (){
            dialog.get('ktree_dialog').remove();
        };

        var hit_the_kps = function (kps){
            // console.info(" kps .... ");
            // console.log(kps);
            // kps[0].cid = 56;
            loading.show('正在组卷');
               //TODO
            self.api.init("catalog", {"num" : 20, "duration" : 0 ,"cid": kps[0].cid, "year": auth.years, "extent": auth.extent }, callback);
        };
        React.render(
            React.createElement( window.components.index.ktree, {'ok': hit_the_kps, 'destroy': destroy } ), 
            node[0]
            // document.getElementById("dialog")
        );
    },
    mk_exam_test: function (cid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return;
            }
            self.props.root.setState({
                    "content": React.createElement(window.components.tests.answer.test, {
                                                tid: body.tid, 
                                                root: self.props.root})
            });
        };
        // this.api.init("exams", { "num" : 15, "duration" : 20*60}, callback);
    },
    mk_errors_test: function (){
        var self = this;
        if ( parseInt(cid) < 1 ) return ;
        var cid = 0;


        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return;
            }
            loading.hide();
            self.props.root.setState({
                    "content": React.createElement(window.components.tests.answer.test, {
                                            tid: body.tid, 
                                            root: self.props.root})
            });
        };

        var node = $('<div class="ktree_for_errors"></a>');
        dialog({ "content": node, id: 'ktree_for_errors' }).showModal();

        var destroy = function (){
            dialog.get('ktree_for_errors').remove();
        };

        var etree = (React.createElement(window.components.index.etree, {
                                        indexbox: self.props.root, 
                                        parent: self, 
                                        destroy: destroy}));
        React.render( etree, node[0] );

        // this.api.init("errors", { "num" : 15, "duration" : 0, "cid": parseInt(cid) }, callback);
    },
    mk_zhenti_test:function(){
        var self = this;
        var node = $('<div class="zhenti_dialog"></div>');
            dialog({"content":node,"id":"zhenti_dialog"}).showModal();
        var callback = function (response){
           
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                console.log(body); 
            }catch(e){
                console.warn(e); return;
            }
            loading.hide();
            self.props.root.setState({
                "content": React.createElement(window.components.tests.answer.test, {
                                        tid: body.tid, 
                                        root: self.props.root})
            });
        };
        var destroy = function(){
            dialog.get('zhenti_dialog').remove();
        }

        var hit_answer = function (exam){
            loading.show('正在组卷');
            self.api.init("exam", {"name":exam['name'],"eid":exam['eid'] }, callback);
        };
        React.render(React.createElement(window.components.index.zhentilist, {"destroy":destroy,
                                                          "ok":hit_answer }),node[0]);
           
            
       

    },
    render: function (){
        var self = this;
        // <a className="test_bottom" onClick = "exam.show_errorlabel();">错题重练</a>
        // <!--<p className="fr">2014年真题卷权威解析，标准分值，精确预测成绩</p>-->
        return (
            React.createElement("div", {className: "ti_container"}, 
                React.createElement("div", {className: "ti_content clearfix"}, 
                    React.createElement("div", {className: "con_test"}, 
                        React.createElement("div", {className: "test_top"}, 
                            React.createElement("img", {src: "static/images/icon_list_01.png"}), 
                            React.createElement("div", {className: "test_top_right"}, 
                                React.createElement("h3", null, "智能推送"), 
                                React.createElement("p", null, "不个性，无高分！通过练习智能判断你的掌握程度，" + ' ' +
                                "实时评估个人能力，建立你的专属答题模型，" + ' ' +
                                "提升综合能力")
                            )
                        ), 
                        React.createElement("a", {className: "test_bottom", onClick: self.mk_fast_test}, "来 20 道")
                    ), 
                    React.createElement("div", {className: "con_test"}, 
                        React.createElement("div", {className: "test_top"}, 
                            React.createElement("img", {src: "static/images/icon_list_02.png"}), 
                            React.createElement("div", {className: "test_top_right"}, 
                                React.createElement("h3", null, "考点直击"), 
                                React.createElement("p", null, "自主选择专项或具体考点，各个击破!")
                            )
                        ), 
                        React.createElement("a", {className: "test_bottom", onClick: self.mk_catalog_test}, "选择考点")
                    ), 
                    React.createElement("div", {className: "con_test"}, 
                        React.createElement("div", {className: "test_top"}, 
                            React.createElement("img", {src: "static/images/icon_list_03.png"}), 
                            React.createElement("div", {className: "test_top_right"}, 
                                React.createElement("h3", null, "错题重练"), 
                                React.createElement("p", null, "进一步清扫错题本中的错题，做到无死角备考!")
                            )
                        ), 
                        React.createElement("a", {className: "test_bottom", onClick: self.mk_errors_test}, "错题重练")
                    ), 
                    React.createElement("div", {className: "con_test"}, 
                        React.createElement("div", {className: "test_top"}, 
                            React.createElement("img", {src: "static/images/icon_list_04.png"}), 
                            React.createElement("div", {className: "test_top_right"}, 
                                React.createElement("h3", {className: "fr_h"}, "真题演练"), 
                                React.createElement("p", null, "近12年国考，联考，省考及事业单位考试真题，")
                            )
                        ), 
                        React.createElement("a", {className: "test_bottom", onClick: self.mk_zhenti_test}, "选择试卷")
                    ), 
                    React.createElement("div", {className: "con_test"}, 
                        React.createElement("div", {className: "test_top"}, 
                            React.createElement("img", {src: "static/images/icon_list_05.png"}), 
                            React.createElement("div", {className: "test_top_right"}, 
                                React.createElement("h3", {className: "fr_h"}, "定期模考"), 
                                React.createElement("p", null, "定期举行全国模考，百万考生同台比拼。")
                            )
                        ), 
                        React.createElement("a", {href: "javascript:;", className: "test_bottom test_qidai"}, "敬请期待")
                    ), 
                    React.createElement("div", {className: "con_test"}, 
                        React.createElement("div", {className: "test_top"}, 
                            React.createElement("img", {src: "static/images/icon_list_06.png"}), 
                            React.createElement("div", {className: "test_top_right"}, 
                                React.createElement("h3", {className: "fr_h"}, "神秘功能"), 
                                React.createElement("p", null, "这是神秘功能")
                            )
                        ), 
                        React.createElement("a", {href: "javascript:;", className: "test_bottom test_qidai"}, "敬请期待")
                    )
                )
            )
        );
    }
});


/**
    @   错题标签树

**/
window.components.index.etree = React.createClass({
    displayName: "components.index.etree",
    name: "我的错题记录标签树",
    api: new apis.user.records.errors(),
    size: 20,
    page: 1,
    getInitialState: function (){
        // props: parent / indexbox / destroy
        return { "catalogs": [], "total": 0 };
    },
    componentDidMount: function (){
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
                if ( "total" in body != true ) body.total = 0;
            }catch(e){
                console.warn(e); return false;
            }
            self.replaceState(body);
        };
        this.api.fetch( "0", callback );
    },
    destroy: function (){
        var self = this;
        self.props.destroy();
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "tk_cover_bg"}), 
                React.createElement("div", {className: "ti_zujuan"}, 
                    React.createElement("img", {src: "static/images/loading_zujuan.gif"}), 
                    React.createElement("div", {className: "loading_info"})
                ), 
                React.createElement("div", {className: "tk_special_project"}, 
                    React.createElement("div", {className: "tk_special_project_top clearfix"}, 
                        React.createElement("div", {className: "tk_special_project_cancel clearfix"}, 
                            React.createElement("a", {onClick: this.destroy, className: "fr"}, 
                                React.createElement("img", {src: "static/images/tk_special_project_cancel.png"})
                            )
                        ), 
                        React.createElement("div", {className: "tk_special_project_tit"}, "请选择知识点")
                    ), 
                    React.createElement("div", {className: "tk_special_project_bottom"}, 
                        React.createElement("ul", null, 
                             this.state.catalogs.map(function(catalog, index){
                                // if ( this.props.index.length == 3 ) return null;
                                return React.createElement(window.components.index.eleaf, {
                                                    key: "tree_of_errors_cid_"+catalog.cid+"_index_"+index+"_", 
                                                    index: [index+1], 
                                                    root: self, 
                                                    parent: self, 
                                                    catalog: catalog, 
                                                    indexbox: self.props.indexbox});
                            }) 
                        )
                    )
                )
            )
        );
    }
});


window.components.index.eleaf = React.createClass({
    displayName: "components.index.eleaf",
    name: "叶子",
    api: new apis.user.records.errors(),
    size: 20,
    page: 1,
    getInitialState: function (){
        // props: parent / root / catalog
        var catalog = this.props.catalog;
        if ( "catalogs" in catalog != true ) catalog.catalogs =  [];
        return catalog;
    },
    componentDidMount: function (){
        // this.fetch();
    },
    fetch: function (e, rid){
        var self = this;
        var target = $(e.target);

        if ( self.props.index.length == 3 ) {
            $(target).removeClass("noa");
            return null;
        }

        
        if ( $(e.target).hasClass("noa") ){
            // $(e.target).removeClass("noa").addClass("a");
        } else {
            $(e.target).removeClass("a").addClass("noa");
            self.setState({"catalogs": [] });
            return;
        }

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
                // if ( "total" in body != true ) body.total = 0;
            }catch(ee){
                console.warn(ee); return false;
            }
            $(target).removeClass("noa").addClass("a");
            self.setState({"catalogs": body.catalogs});
        };
        this.api.fetch( this.state.cid, callback );
    },
    mk_redo_error: function (){
        var self = this;
        loading.show("正在组卷 ... ");
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                loading.hide();
                console.warn(e); return;
            }
            loading.hide();
            self.props.indexbox.setState({
                    "content": React.createElement(window.components.tests.answer.test, {
                                            tid: body.tid, 
                                            root: self.props.indexbox})
            });
            self.props.root.destroy();
        };
        var api = new apis.tests();
        api.init("errors", { "num" : 20, "duration" : 0, "cid": parseInt(this.state.cid) }, callback);
    },
    render: function (){
        var self = this;
        var sync_ico = "noa";
        if ( self.props.index.length == 3 ) sync_ico = "";

        return (
            React.createElement("li", {
                "data-cid": this.state.cid}, 
                React.createElement("div", {className: "tk_special_project_tab_li clearfix"}, 
                    React.createElement("div", {
                        className: "tk_special_project_li_fl fl " + sync_ico, 
                        onClick: self.fetch, 
                        dangerouslySetInnerHTML: {__html: [this.state.cname, "（共", this.state.totalNum, "道题）"].join("")}}), 
                    React.createElement("div", {className: "tk_special_project_li_fr fr"}, 
                        React.createElement("span", {
                            onClick: self.mk_redo_error}, 
                            "错题重练"
                        )
                    )
                ), 
                React.createElement("ul", null, 
                     this.state.catalogs.map(function (catalog, index){
                            if ( self.props.index.length >= 3 ) return null;
                            return React.createElement(window.components.index.eleaf, {
                                            key: "tree_of_errors_cid_"+catalog.cid+"_index_"+index+"_", 
                                            index: self.props.index.concat(index+1), 
                                            parent: self, 
                                            catalog: catalog, 
                                            root: self.props.root, 
                                            indexbox: self.props.indexbox})
                        }) 
                )
            )
        );
    }
});

/**
    @   专项练习
    
**/


window.components.index.kpoint =  React.createClass({
    displayName: "试题知识点树",
    name: "试题知识点树",
    api: new apis.catalogs.atoms(),
    children: [],         // 子节点
    num: 0,              // 子节点元素总量
    sync_status: 0,   // 同步状态
    open_status: 0,  // 树打开状态
    is_hit: 0,             // 是否选择
    getInitialState: function (){
        if ( !this.props.knowledge ) throw new Error("kpoint init fail.");
        return { "knowledge": this.props.knowledge };
    },
    componentDidMount: function() {
        var self = this;
        // self.fetch(self.props.knowledge.cid);
    },
    fetch: function (cid, e, rid){
        var self = this; 
        // console.info(self.props);
        if ( self.props.index.length >= 3 ) return null;
        if ( parseInt(self.sync_status) == 1 ) {
            if ( parseInt(self.open_status) == 1 ) {
                self.open_status = 0;
                self.children_copy = self.children;
                self.children = [];
                self.forceUpdate();
                return ":: use cache ...";
            }
            if ( parseInt(self.open_status) == 0 ) {
                self.open_status = 1;
                self.children = self.children_copy;
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body); 
                if ( "list" in body ) body = body.list;
                if ( "num" in body) self.num = body.num;
            }catch(e){
                console.warn(e); return;
            }
            self.sync_status = 1;
            self.open_status = 1;
            self.children = body;
            self.forceUpdate();
        };
        this.api.root(cid, callback);
    },
     comeWithTwenty: function(cid, name){
        // console.info("** hit_the_kp ... ");
        // console.log("cid: " + cid + "  name: " + name);
        var self = this; 
        if ( parseInt(self.sync_status) != 1 ) {
             self.fetch(cid); 

            
        }
        if ( parseInt(self.children.length) != null ) {
            if ( parseInt(self.is_hit) == 1) {
                self.is_hit = 0;
                delete self.props.root.kps[parseInt(self.props.knowledge.cid)];
            } else {
                self.is_hit = 1;
                self.props.root.kps[parseInt(self.props.knowledge.cid)] = self.props.knowledge;
            }
            self.props.root.ok(cid);
            self.forceUpdate();
        } else {
            // 不可以选择

        }
        // onClick={this.fetch.bind(self, this.props.knowledge.cid)
    },
    getChildren:function(){
           var self = this;
           self.fetch(self.props.knowledge.cid);
         
    },
    render: function (){
        var self = this;
        var is_top_node = function (hookid){
            if ( hookid.toString() == "0" ) { 
                return "order_first";
            } else { 
                return "order_usually";
            }
        };
        var sync_ico, hit_classname;
        if ( self.open_status == 1 ) sync_ico = "a";  // 树打开状态.
        else sync_ico = "noa";
        if ( self.props.index.length == 3 ) sync_ico = "";

        if ( self.is_hit == 1 ) hit_classname = "c_on"; // 选中状态
        else hit_classname = "c_ban";

        return (
            React.createElement("li", {"data-hookid": this.props.knowledge.hookid, 
                key: this.props.key, 
                "data-cid": this.props.knowledge.cid, 
                "data-index": this.props.index}, 
                React.createElement("div", {className: "tk_special_project_tab_li clearfix"}, 
                    React.createElement("div", {
                        className: "tk_special_project_li_fl fl " + sync_ico, 
                        onClick: self.getChildren}, 
                        this.props.knowledge.name
                    ), 
                    React.createElement("div", {className: "tk_special_project_li_fr fr"}, 
                        React.createElement("span", {
                            onClick: self.comeWithTwenty.bind(self, self.props.knowledge.cid, self.props.knowledge.name) }, 
                            "来20道"
                        )
                    )
                ), 
                React.createElement("ul", null, 
                     this.children.map(function (k, i){ 
                            if ( self.props.index.length >= 3 ) return null;
                            return React.createElement(window.components.index.kpoint, {
                                            key: "ktree_dialog_cids_" + k.cid.toString() + "_index_" + i.toString(), 
                                            parent: self, 
                                            root: self.props.root, 
                                            knowledge: k, 
                                            index: self.props.index.concat(i+1)})
                        }) 
                )
            )
        );
    }
});


window.components.index.ktree =  React.createClass({
    displayName: "components.index.ktree",
    name: "知识点树",
    api: new apis.catalogs.atoms(),
    catalogs: [],
    kps: {},           // 已选择的知识点
    getInitialState: function (){
        if ( !this.props.destroy ) this.destroy = function(){};
        else this.destroy = this.props.destroy;
        // if ( !this.props.ok ) this.ok = function(){};
        // else this.ok = this.props.ok;
        return { "list": [], "num": 0 };
    },
    componentDidMount: function() {
        this.fetch(); 
    },
    fetch: function (){
        var self = this; 
        var callback = function (response){ 
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
            }catch(e){
                console.warn(e); return ;
            }
            self.catalogs = body.list;
            self.forceUpdate();
        };
        this.api.root('0', callback);
    },
    ok: function (){
        // if ( !this.props.ok ) return;
        var self = this;
        var kps = [];
        Object.keys(this.kps).map(function (k, i){
            kps.push(self.kps[k]);
        });
        this.props.ok(kps);
        this.destroy();
    },
    destroy: function (){ 
        // if ( !this.props.dialog ) return;
        // $(this.props.dialog).dialog("close");
        console.log(this.props);
        // this.props.destroy();
    },
    deleteDialog:function(){
        
          this.props.destroy();
          $(".ktree_dialog").remove();

    },
    render: function (){
        var self = this;

        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "tk_cover_bg"}), 
                React.createElement("div", {className: "ti_zujuan"}, 
                    React.createElement("img", {src: "static/images/loading_zujuan.gif"}), 
                    React.createElement("div", {className: "loading_info"})
                ), 
                React.createElement("div", {className: "tk_special_project"}, 
                    React.createElement("div", {className: "tk_special_project_top clearfix"}, 
                        React.createElement("div", {className: "tk_special_project_cancel clearfix"}, 
                            React.createElement("a", {className: "fr"}, 
                                React.createElement("img", {src: "static/images/tk_special_project_cancel.png", onClick: self.deleteDialog})
                            )
                        ), 
                        React.createElement("div", {className: "tk_special_project_tit"}, "请选择知识点")
                    ), 
                    React.createElement("div", {className: "tk_special_project_bottom"}, 
                        React.createElement("ul", null, 
                             this.catalogs.map(function(k, i){ 
                               
                                // if ( this.props.index.length == 3 ) return null;
                                return React.createElement(window.components.index.kpoint, {
                                                    key: "ktree_dialog_cids_" + k.cid.toString() + "_index_" + i.toString(), 
                                                    parent: self, 
                                                    root: self, 
                                                    knowledge: k, 
                                                    index: [i+1]});
                            }) 
                        )
                    )
                )
            )
        );
    }
});

window.components.index.zhentilist = React.createClass({displayName: "zhentilist",
      exams: new apis.exams(),
      getInitialState:function(){ return { exams:[]}
        // return {"exams":[
        //                {"name":"5月29日全国事业单位万人公基模考",
        //                "uppertime":1428254458,
        //                "lowertime":1428254500,
        //                "duration":20,
        //                "tombstone":0,
        //                "inittime":1436406067,
        //                "updtime":1436406067,
        //                "eid":45
        //                 },
        //                 {"name":"ceshi",
        //                 "uppertime":1436335295,
        //                 "lowertime":1436341295,
        //                 "duration":20,
        //                 "tombstone":0,
        //                 "inittime":1436335297,
        //                 "updtime":1436335297,
        //                 "eid":44
        //                  }
        //                  ],
        //           "count":2} 
      },
      componentDidMount:function(){
         this.fetch();

      },
      fetch:function(){
        var self = this
        var callback = function(response){
             if( response.type === "progress") { return false;}
             if( response.code !== 200){ return false;}
             var body = JSON.parse( response.body);
             self.setState(body);
          
        }
        self.exams.fetch(1,10,callback);

      },
      startAnswer:function(event){
        var self = this;
        var eid = $(event.target).data("eid");
        var name = $(event.target).data("name");
        self.props.destroy();
        self.props.ok({"name":name,"eid":eid});
      },
      deleteDialog:function(){
        var self = this;
        self.props.destroy();
      },
      render:function(){
        var self = this;
        return React.createElement("div", {className: "tk_special_project zhenti"}, 
                   React.createElement("div", {className: "tk_special_project_top clearfix"}, 
                        React.createElement("div", {className: "tk_special_project_cancel clearfix"}, 
                            React.createElement("a", {className: "fr"}, 
                                React.createElement("img", {src: "static/images/tk_special_project_cancel.png", onClick: self.deleteDialog})
                            )
                        ), 
                        React.createElement("div", {className: "tk_special_project_tit"})
                    ), 
                    React.createElement("div", {className: "tk_special_project_bottom"}, 
                        React.createElement("ul", null, 
                           
                             self.state.exams.map(function(item){
                                 return  React.createElement("li", null, 
                                            React.createElement("span", {className: "paper_name"}, item.name), 
                                            React.createElement("span", {className: "start_answer", 
                                                  "data-eid":  item.eid, 
                                                  "data-name":  item.name, 
                                                  onClick: self.startAnswer
                                                   }, "开始")
                                         )  
                            })
                        )
                    )
               ) 
        
                
      }
})