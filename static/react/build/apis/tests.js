
if ( !window.apis ) window.apis = {};

apis.tests = function (){
    if ( "tests" in this ) throw new Error("需要先实例化 apis.tests 类( 伪 Class )");
    this.name = "tests api";
    this.version = "0.1";
};

apis.tests.prototype.init = function (factory, body, xmen){
    // factory: exam/errors/catalog/test
    // body:
    /**
        { "name" : "测试", "eid" : 28 }
        { "name" : "错题重练", "num" : 1, "duration" : 3000}
        { "name" : "专项训练", "num" : 1, "duration" : 3000 ,"cid":28}
        {}
    **/

    // if ( factory == "test" || factory == "errors" || factory == "error" ) body = {};
    // else {
    //     // if ( parseInt(body.eid) < 1 || body.name.length < 1 ) throw new Error("error.");
    // }
    if ( !body ) throw new Error("error."); // { "name" : "测试", "eid" : 28 }
    if ( !xmen ) var xmen = function (){};
    
    var factory_table = {"exam": "exam", "errors": "errors", "error": "errors", "catalog": "catalog", "test": "test"};
    if ( Object.keys(factory_table).indexOf(factory) == -1 ) throw new Error("factory error.");
    

    var url = "/instiApis/whipapp/tests/factory/" + factory.toLowerCase();

    // var url = "/apis/whipapp/tests/factory/" + factory.toLowerCase();
   

    requests.put(url, body, {"async": true, "callback": xmen});
};

apis.tests.prototype.get = function (tid, xmen){
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    

    var url = "/instiApis/whipapp/tests/" + tid;

    // var url = "/apis/whipapp/tests/" + tid;

    requests.get(url, {"async": true, "callback": xmen});
};

apis.tests.prototype.pull = function (tid, xmen){
    return this.get(tid, xmen);
};


apis.tests.prototype.uploadAns = function (tid, answer, xmen){
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    /*
        {
            "answers": [
                {
                    "qid": 8,
                    "qans": "B",
                    "uans": "C",
                    "time": 1427852130
                }
            ],
            "time": 1427852136
        }
    */
    answer.time = parseInt((new Date()).getTime()/1000);
    var url = "/instiApis/whipapp/tests/answers/" + tid;
    requests.put(url, {"answers": [ answer], "time": parseInt((new Date()).getTime()/1000) }, {"async": true, "callback": xmen});
};

apis.tests.prototype.downloadAns = function (tid, xmen){
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    var url = "/instiApis/whipapp/tests/answers/" + tid;
    requests.get(url, {"async": true, "callback": xmen});
};

apis.tests.prototype.markEnd = function (tid, xmen){
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    // var url = "/instiApis/whipapp/tests/" + tid + "/end";
    // status:  0: 答题中 1: 交卷 2: 出报告
    var url = "/instiApis/whipapp/tests/" + tid + "/1/status";
    requests.patch(url, {},{"async": true, "callback": xmen});
};

apis.tests.prototype.remove = function (tid, xmen){
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    var url = "/instiApis/whipapp/tests/" + tid;
    requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
};

/**
 * 
 *  该测试涉及的标签树操作
 * */

apis.tests.prototype.tree = function (cat, tid, node, xmen){
    // cat: all || error   # 标签树类型为 全局知识点标签树 / 错题当中的知识点标签树
    if ( cat != "all" && cat != "error" ) throw new Error("error.");
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    
    var cat_dict = { "all": "catalog", "error": "error" };
    var url = "/instiApis/whipapp/test/#CAT#/report/" + tid;
    // 临时返回
    
    // if ( parseInt(node) != 0 ) {
    //     var response = { "code": 200, "type": "done", "body": "[]" };
    //     xmen(response); return ;
    // }
    
    url = url.replace("#CAT#", cat_dict[cat]);
    if ( cat == "all" ) url += "/"+node;
    requests.get(url, {"async": true, "callback": xmen});
};
apis.tests.prototype.leaf = function (tid, xmen){
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    var url = "/instiApis/whipapp/tests/" + tid;
    requests.get(url, {"async": true, "callback": xmen});
};

/*

    @   答题暂停

    "events" : [
        { "event" : "PAUSE", "time" : 1433821184 },
        { "event" : "PLAY", "time" : 1433821207 },
        { "event" : "PAUSE", "time" : 1433821234 },
        { "event" : "PLAY", "time" : 1433821246 }
    ],
*/
apis.tests.prototype.addEvents = function (tid, events, xmen){
    if ( parseInt(tid) < 1 ) throw new Error("error.");
    /*
    事件序列: 
        {
            "events": [
                { "event" : "PAUSE", "time": 1433821246}, 
                { "event" : "PLAY", "time": 1433821246} 
            ]
        }
    */
    var url = "/instiApis/whipapp/tests/events/" + tid;
    requests.patch(url, {"events": events}, {"async": true, "callback": xmen});
};