/**
 * 
 * @    /whipapp/user/evaluate/report
 * 
 * */

 // 个人评估报告
if ( !window.apis ) window.apis = {};

apis.report = function (){
    if ( "report" in this ) throw new Error("需要先实例化 apis.report 类( 伪 Class )");
    this.name = "report api";
    this.version = "0.1";
};

apis.report.prototype.fetch = function (xmen){
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/whipapp/user/evaluate/report";
    /*
        {
            "preScoreInfo":{
                "preScore":0.04,
                "preScoreAVG":0.11200000000000002,
                "preDefeat":0.6
            },
            "areaPreScoreInfo":{
                "areaPreScore":0.04,
                "areaPreScoreAVG":0.11200000000000002,
                "areaDefeat":0.6
            },
            "ansQuesInfo":{
                "ansQues":247,
                "ansQuesAVG":144,
                "ansQuesDefeat":0.7333333333333333
            },
            "ansTimeInfo":{
                "ansTime":20.31,
                "ansTimeAVG":144,
                "ansTimeDefeat":0.4
            }
        }

    */
    requests.get(url, {"async": true, "callback": xmen});
};


apis.report.prototype.kptree = function (cid, xmen){
    // 获取知识点能力量表
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/whipapp/user/catalog/ability/report/" + cid.toString();
    /*
        {
            "catalogs": [
                {
                    "superCid": 1298,
                    "cid": 1298,
                    "cname": "人文科技",
                    "totalNum": 16,
                    "rightNum": 0,
                    "rightRate": "0.00",
                    "ansTimeAVG": 12
                },
                {
                    "superCid": 1195,
                    "cid": 1195,
                    "cname": "经济",
                    "totalNum": 24,
                    "rightNum": 4,
                    "rightRate": "0.17",
                    "ansTimeAVG": 12
                }
            ]
        }
        * totalNum  :答题数
        * rightNum  :答对题数
        * rightRate :正确率
        * ansTimeAVG:平均时间
    */
    requests.get(url, {"async": true, "callback": xmen});
};

apis.report.prototype.kpgraph = function (xmen){
    // 获取知识点掌握热点图
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/whipapp/user/catalog/grasp/report";
    /*
        {
            "catalogs": [
                {
                    "cid": 1125,
                    "cname": "政治",
                    "catalogs": [
                        {
                            "cid": 1126,
                            "cname": "马克思主义哲学",
                            "isAnswer": 1,
                            "rightRate": "0.17"
                        },
                        {
                            "cid": 1127,
                            "cname": "概述",
                            "isAnswer": -1,
                            "rightRate": "0"
                        }
                    ]
                }
            ]
        }
        * isAnswer :是否测试(-1->未测试;1->测试)
        * rightRate:正确率
    */
    requests.get(url, {"async": true, "callback": xmen});
};
