if ( !window.components ) window.components = {};
if ( !window.components.share ) window.components.share = {};
window.components.share.index = React.createClass({displayName: "index",
      display:"公共组件(侧边栏和头部)",
      getInitialState:function(){
           return {"content":React.createElement("div", null, React.createElement("h1", null, "欢迎回来"))}
      },
      componentDidMount:function(){
             $(function(){

            $("[placeholder]").placeholder();
            
            $(".sub_list").parent().find("dl").hide();
            function sub_list_click(){
                $(this).parent().siblings().find("dl").slideUp().end().find("img").attr("src","static/images/sub_right.png");
                $(this).parent().find("dl").slideDown();
                $(this).find("img").attr("src","static/images/sub_up.png");
            }
            function sub_list_hover(){
                $(this).parent().siblings().find("dl").hide();
                $(this).parent().find("dl").show();
            }
            function sub_list_dl_mouseout(){
                $(this).hide();
            }
            
            $(".sub_list").on("click",sub_list_click);
            $(".show_sidebar").data("oc","false");
            
            //$(".subnav ul li dl").on("mouseout",sub_list_dl_mouseout);
            /*点击菜单后*/
            $(".subnav dl dt").click(function(){
                $(this).addClass("dt_hover").siblings().removeClass("dt_hover");     
            });

            /*菜单的hover效果
            $(".subnav dl dt").hover(function(){
                $(this).removeClass("sub_dt").addClass("dt_hover");
            },function(){
                $(this).removeClass("dt_hover").addClass("sub_dt");;
            });*/
            
            /*点击用户显示修改密码和退出*/
            /*
            $(".fr").on("click", function (){
                $(this).next().removeClass('none').toggle();
            });
            
            */
            $(".show_sidebar").on("click",function(){
                $(".sub_list").find("img").hide().parent().find("dl").hide();
                if($(this).data("oc")=="false"){
                    $("#share").css("paddingLeft","50px");
                    $(".subnav").addClass("a");
                    $(".sub_list").off("click");
                    $(".sub_list").on("mouseover",sub_list_hover);
                    $(".subnav ul li dl").hover(function(){
                    },function(){
                        $(this).hide();
                    });
                    $(".sidebar").stop().animate({ 
                        width: "50px"
                    }, 100 );
                    $(this).data("oc","true");
                }else{
                    $(".sub_list").find("img").show(); 
                    if ($('.arrow').size()) {
                        $('.arrow').find('img').attr('src',"static/images/sub_right.png");
                    }
                    $(".subnav").removeClass("a");
                    $(".sub_list").off("mouseover");
                    $(".subnav ul li dl").hover(function(){
                    },function(){
                        $(this).show();
                    });
                    $(".sub_list").stop().on("click",sub_list_click);
                    $(".sidebar").animate({ 
                        width: "200px"
                    }, 100 ,function(){
                        $("#share").css("paddingLeft","200px");
                    });
                    $(this).data("oc","false");
                }
            });
        });
      },
      componentDidUpdate:function(){
           
      },
      handleUserSelectMenu:function(event){
            $(event.target).next().removeClass("none").toggle();
           
        
      },
      clickOnLogo:function(){
      	    // window.location.href="/";
      },
      viewPaperList:function(){
            //试卷管理===》试卷列表
           var self = this;
           this.setState({"content":React.createElement(window.components.papers.list, {page: "0", size: "20", root: self})})
      },
      viewPaperInit:function(){
           //创建试卷
           var self = this;
           this.setState({"content":React.createElement(window.components.papers.create, {root: self})}) 
      },
      viewCatalogAtomsList:function(){
           this.setState({"content":React.createElement(window.components.catalogs.atoms.list, null)})
      },
      viewUserAdministration:function(){
           this.setState({"content":React.createElement(window.components.users.list, null)})
      },
      viewCatalogExamsList:function(){

           this.setState({"content":React.createElement(window.components.catalogs.exams.list, null)})
      },
      viewCatalogPapersList:function(){
          var self = this;
          self.setState({"content":React.createElement(window.components.catalogs.papers.list, {root: self})})
      },
      viewExamsList:function(){
           //考试列表
           this.setState({"content":React.createElement(window.components.exams.list, {page: "1", size: "20", root: this})})
      },
      logout:function(){
           var url  = "/admin/cgi-bin/auth.php";
           var body = {"hook":"auth.logout","data":{}};
           var callback = function(response){
                if(response.type == "progress"){ return "...loding";}
                if(response.code != 200){ alert("登出失败"); return;}
                try{
                      var body = JSON.parse(response.body);
                      window.location.assign("#signin");
                }catch(e){

                }
           }
          requests.put(url, body, {"async": true, "callback": callback});
      },
	   render:function(){
	  	 var self = this;
         return  React.createElement("div", {style: {"paddingLeft":"200px", "paddingTop":"50px"}, id: "share"}, 
                    React.createElement("div", {className: "header"}, 
					        React.createElement("div", {className: "logo fl"}, 
					            React.createElement("img", {src: "static/images/pic_yuan.png"}), 
					            React.createElement("span", {onClick: self.clickOnLogo}, "题库管理后台")
					        ), 
					        React.createElement("div", {className: "main-nav fl"}, 
					            React.createElement("div", {className: "show_sidebar fl"})
					        ), 
					        React.createElement("div", {className: "user"}, 
					            React.createElement("div", {className: "user-bar fr", onClick: self.handleUserSelectMenu}, 
					                 auth.name, 
					                React.createElement("img", {src: "static/images/button_pullDown.png"})
					            ), 
					            React.createElement("div", {className: "user_info none"}, 
					                
					                    React.createElement("div", {className: "exit mod", onClick: self.logout}, React.createElement("a", null, "退出"))
					            )
					        )
                   ), 
                   React.createElement("div", {className: "sidebar"}, 
					        React.createElement("div", {className: "subnav"}, 
					            React.createElement("ul", null, 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico1"}, 
					                        React.createElement("span", null, "试卷管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt", onClick: self.viewPaperList}, "试卷列表"), 
					                        React.createElement("dt", {className: "sub_dt", onClick: self.viewPaperInit}, "录入试卷"), 
					                        React.createElement("dt", {className: "sub_dt"}, "试题纠错列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico2"}, 
					                        React.createElement("span", null, "考试管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt", onClick: self.viewExamsList}, "考试列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico3"}, 
					                        React.createElement("span", null, "标签管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt", onClick: self.viewCatalogPapersList}, "试卷标签列表"), 
					                        React.createElement("dt", {className: "sub_dt", onClick: self.viewCatalogExamsList}, "考试标签列表"), 
					                        React.createElement("dt", {className: "sub_dt", onClick: self.viewCatalogAtomsList}, "试题标签列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico4"}, 
					                        React.createElement("span", null, "其他管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "意见反馈")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico5"}, 
					                        React.createElement("span", null, "系统设置"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt", onClick: self.viewUserAdministration}, "用户管理")
					                    )
					                )
					            )
					        )
					    ), 
					    React.createElement("div", {className: "main-container"}, 
				            React.createElement("div", {className: "main_content"}, 
				                React.createElement("div", {id: "container"}, 
				                self.state.content
				                )
				            )
				        )
				 )              
	    } 

})