if ( !window.components ) window.components = {};
if ( !window.components.user ) window.components.user = {};
if ( !window.components.user.settings ) window.components.user.settings = {};


window.components.user.settings.setting = React.createClass({
    displayName: "components.user.settings.setting",
    name: "个人设置主组件",
    // api: new apis.tests(), 
    getInitialState: function (){
        // props: indexbox / tab
        // tab: setTestArea / setQuestionAreaAndYear
        return {"component": this.props.component };
    },
    componentWillReceiveProps: function (){

    },
    componentDidMount: function (){
        var self = this;

    },
    logout: function (){
        var self = this;
        var url = "/cgi-bin/auth.php";
        var body = {"hook": "auth.signout", "data": {} };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert("登出失败 ...");
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                window.location.assign("#signin");
            }catch(e){
                console.warn(e); return false;
            }
        };
        requests.put(url, body, {"async": true, "callback": callback});
    },
    goUpdateTestArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setTestArea });
    },
    goUpdateQuesArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setQuestionAreaAndYear });
    },
    goUpdateProfile: function (){
        this.props.parent.setState({ "component": window.components.user.settings.profile });
    },
    goUpdatePassword: function (){
        this.props.parent.setState({ "component": window.components.user.settings.chpassword });
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "ti_header"}, 

                    React.createElement(window.components.share.headerTop, null), 

                    React.createElement("div", {className: "log_head"}, 
                        React.createElement("div", {className: "log_img"}, 
                            React.createElement("a", {href: "/#index"}, " ", React.createElement("img", {src: "static/images/login_logo.png"}))
                        )
                    )
                ), 

                React.createElement("div", {className: "common_con"}, 
                    React.createElement(this.state.component, {parent: self, root: self})
                )
            )
        );
    }
});

window.components.user.settings.profile = React.createClass({
    displayName: "components.user.settings.profile",
    name: "个人资料修改",
    // api: new apis.tests(), 
    getInitialState: function (){
        // props: root
        return {};
    },
    componentDidMount: function (){

    },
    goUpdateTestArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setTestArea });
    },
    goUpdateQuesArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setQuestionAreaAndYear });
    },
    goUpdateProfile: function (){
        this.props.parent.setState({ "component": window.components.user.settings.profile });
    },
    goUpdatePassword: function (){
        this.props.parent.setState({ "component": window.components.user.settings.chpassword });
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "common_main"}, 
                React.createElement("ul", {className: "common_list clearfix"}, 
                    React.createElement("li", {className: "common_select"}, React.createElement("a", null, "个人资料")), 
                    React.createElement("li", {onClick: self.goUpdateTestArea}, React.createElement("a", null, "考区设置")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateQuesArea}, "设置出题范围")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdatePassword}, "修改密码"))
                ), 
                React.createElement("div", {className: "common_main_type common_testset"}, 
                    React.createElement("div", {className: "common_range_title"}, "等待开放 ...")
                )
            )
        );
    }
});

window.components.user.settings.chpassword = React.createClass({
    displayName: "components.user.settings.chpassword",
    name: "修改密码",
    // api: new apis.tests(), 
    getInitialState: function (){
        // props: root / indexbox
        return {"oldpassword": "", "newpassword": "", "repassword": ""};
    },
    componentDidMount: function (){

    },
    inputOldPassword: function (e, rid){
        this.setState( {"oldpassword": $(e.target).val() } );
    },
    inputNewPassword: function (e, rid){
        this.setState( {"newpassword": $(e.target).val() } );
    },
    inputNewRePassword: function (e, rid){
        this.setState( {"repassword": $(e.target).val() } );
    },
    upload: function (){
        // 修改密码
        console.log ( JSON.stringify(this.state) );
    },
    goUpdateTestArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setTestArea });
    },
    goUpdateQuesArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setQuestionAreaAndYear });
    },
    goUpdateProfile: function (){
        this.props.parent.setState({ "component": window.components.user.settings.profile });
    },
    goUpdatePassword: function (){
        this.props.parent.setState({ "component": window.components.user.settings.chpassword });
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "common_main"}, 
                React.createElement("ul", {className: "common_list clearfix"}, 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateProfile}, "个人资料")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateTestArea}, "考区设置")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateQuesArea}, "设置出题范围")), 
                    React.createElement("li", {className: "common_select"}, React.createElement("a", null, "修改密码"))
                ), 
                React.createElement("div", {className: "common_main_type common_modify"}, 
                    React.createElement("div", {className: "modify_pass"}, 
                        React.createElement("div", {className: "modify_inp"}, 
                            React.createElement("input", {
                                    type: "password", 
                                    placeholder: "请输入旧密码", 
                                    name: "oldpassword", 
                                    value: self.state.oldpassword, 
                                    onChange: self.inputOldPassword})
                        ), 
                        React.createElement("div", {className: "modify_inp"}, 
                            React.createElement("input", {
                                    type: "password", 
                                    placeholder: "新密码", 
                                    name: "newpassword", 
                                    value: self.state.newpassword, 
                                    onChange: self.inputNewPassword})
                        ), 
                        React.createElement("div", {className: "modify_inp"}, 
                            React.createElement("input", {
                                    type: "password", 
                                    placeholder: "确认密码", 
                                    name: "repassword", 
                                    value: self.state.repassword, 
                                    onChange: self.inputNewRePassword})
                        ), 
                        React.createElement("div", {className: "modify_submit"}, 
                            React.createElement("button", {onClick: self.upload}, 
                                "提交"
                            )
                        )
                     )
                )
            )
        );
    }
});

window.components.user.settings.setTestArea = React.createClass({
    displayName: "components.user.settings.setTestArea",
    name: "设置考区",
    // api: new apis.tests(), 
    getInitialState: function (){
        // props: root / indexbox
        return {
            "provinces": [],
            "cities": [],
            "settings": {
                "QuesArea": auth.areas,
                "TestArea": [],
                "TestYear": auth.years,
                "attrs": [
                    {"省部统考": ""},
                    {"国家统考": ""}
                ],
            },
        };
    },
    componentDidMount: function (){
        var self = this;
        this.get_provinces();
        var settings = JSON.parse( JSON.stringify(this.state.settings) );
        var TestAreas = [];
        TestAreas = window.auth.areas.filter(function (v, i){
            return /\d\d0000/.test( v.toString() );
        });
        if ( TestAreas != undefined && TestAreas != null &&  TestAreas.length > 0 ) settings.TestArea = [TestAreas[0]];
        else settings.TestArea = [];
        self.setState({"settings": settings});
    },
    componentWillUnmount: function (){
        
    },
    get_provinces: function (){
        var self = this;
        var api = new apis.locations();
        var callback = function (response){
            
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return false;
            }
            self.setState({"provinces": body.locations });
        };
        api.provinces(callback);
    },
    get_cities: function (id){
        var self = this;
        var api = new apis.locations();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return false;
            }
            self.setState({"cities": body.locations });
            self.setState({"showProvinces": false});
            self.setState({"province_on_select": parseInt(id)});
        };
        api.cities(id, callback);
    },
    updateTestArea: function (id, action){
        var self = this;
        var settings = JSON.parse( JSON.stringify(this.state.settings) );
        if ( action == "append" ) {
            // settings.TestArea.push( parseInt(id) );
            settings.TestArea = [parseInt(id) ];
        } else if ( action == "remove" ) {
            settings.TestArea = [];
            // var index = -1;
            // if ( settings.TestArea.indexOf(parseInt(id)) != -1 ) index = settings.TestArea.indexOf(parseInt(id));
            // if ( settings.TestArea.indexOf( id.toString() ) != -1 ) index = settings.TestArea.indexOf(parseInt(id));
            // if ( index != -1 ) settings.TestArea.splice(index);
        } else {
            console.warn("unknow action.");
        }
        self.setState({"settings": settings});
        console.log(JSON.stringify(this.state.settings));
    },
    upload: function (){
        Array.prototype.unique = function(){
            var res = [];
            var json = {};
            for(var i = 0; i < this.length; i++){
                if(!json[this[i]]){
                    res.push(this[i]);
                    json[this[i]] = 1;
                }
            }
            return res;
        }

        var self = this;
        var url = "/cgi-bin/auth.php";
        var body = {
                "hook": "setting.updateQuesArea", 
                "data": {
                    "areas": this.state.settings.TestArea.concat(this.state.settings.QuesArea).unique(),
                }
        };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : "考区更新失败 ..." );
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                setalert("考区更新成功");
                check_login(); // 根据 cookie 更新 auth 对象
                self.goUpdateQuesArea();
            }catch(e){
                console.warn(e); return false;
            }
        };
        requests.put(url, body, {"async": true, "callback": callback});
    },
    goUpdateTestArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setTestArea });
    },
    goUpdateQuesArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setQuestionAreaAndYear });
    },
    goUpdateProfile: function (){
        this.props.parent.setState({ "component": window.components.user.settings.profile });
    },
    goUpdatePassword: function (){
        this.props.parent.setState({ "component": window.components.user.settings.chpassword });
    },
    render: function (){
        var self = this;
        console.log(self.state.settings);
        return (
            React.createElement("div", {className: "common_main"}, 
                React.createElement("ul", {className: "common_list clearfix"}, 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateProfile}, "个人资料")), 
                    React.createElement("li", {className: "common_select"}, React.createElement("a", null, "考区设置")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateQuesArea}, "设置出题范围")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdatePassword}, "修改密码"))
                ), 

                React.createElement("div", {className: "common_main_type common_testset"}, 
                    React.createElement("div", {className: "common_area"}, 
                        React.createElement("ul", {className: "clearfix area_list"}, 
                             this.state.provinces.map(function (province, index){
                                var province_name = province.name;
                                if ( province_name.length >= 5 ) province_name = province_name.slice(0, 5);
                                if ( self.state.settings.TestArea.indexOf( parseInt(province.id) ) == -1 &&
                                     self.state.settings.TestArea.indexOf( province.id.toString() ) == -1
                                 ) {
                                    return (React.createElement("li", {
                                        className: "common_li_icon", 
                                        title: province.name, 
                                        onClick: self.updateTestArea.bind(self, province.id, "append") }, 
                                        province_name
                                    ));
                                } else {
                                    return (React.createElement("li", {
                                        className: "common_li_icon common_li", 
                                        title: province.name, 
                                        onClick: self.updateTestArea.bind(self, province.id, "remove") }, 
                                        province_name
                                    ));
                                }
                            }) 
                        )
                    ), 
                    React.createElement("div", {className: "common_save"}, 
                        React.createElement("button", {id: "update_area", onClick: self.upload}, "保存地区")
                    )
                )
            )
        );
    }
});

window.components.user.settings.setQuestionAreaAndYear = React.createClass({
    displayName: "components.user.settings.setQuestionAreaAndYear",
    name: "设置出题范围及年份",
    getInitialState: function (){
        // props: root / indexbox
        return {
            "provinces": [],
            "cities": [],
            "showProvinces": false,
            "province_on_select": 0,
            "settings": {
                "QuesArea": auth.areas,   // 出题范围当中的 "省部统考"以及 “国家统考” 分别以 数字 1 和 2 进行特殊编号
                "QuesYear": auth.years,
                "TestArea": auth.areas,
                "attrs": [
                    {"省部统考": ""},
                    {"国家统考": ""}
                ],
            },
        };
    },
    componentDidMount: function (){
        var self = this;
        this.get_provinces();
        this.state.settings.QuesArea.map(function (area_id, i){
            if ( /\d\d0000/.test( area_id.toString() ) ) {
                self.get_cities( area_id );
            }
        });
    },
    componentWillUnmount: function (){

    },
    get_provinces: function (){
        var self = this;
        var api = new apis.locations();
        var callback = function (response){
            
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return false;
            }
            self.setState({"provinces": body.locations });
        };
        api.provinces(callback);
    },
    get_cities: function (id){
        var self = this;
        var api = new apis.locations();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return false;
            }
            self.setState({"cities": body.locations });
            self.setState({"showProvinces": false});
            self.setState({"province_on_select": parseInt(id)});
        };
        api.cities(id, callback);
    },
    updateQuesArea: function (cid, action){
        var self = this;
        var settings = JSON.parse( JSON.stringify(this.state.settings) );
        if ( action == "append" ) {
            settings.QuesArea.push( parseInt(cid) );
        } else if ( action == "remove" ) {
            var index = -1;
            if ( settings.QuesArea.indexOf(parseInt(cid)) != -1 ) index = settings.QuesArea.indexOf(parseInt(cid));
            if ( settings.QuesArea.indexOf( cid.toString() ) != -1 ) index = settings.QuesArea.indexOf(parseInt(cid));
            if ( index != -1 ) settings.QuesArea.splice(index);
        } else {
            console.warn("unknow action.");
        }
        self.setState({"settings": settings});
        console.log(JSON.stringify(this.state.settings));
    },
    updateQuesYear: function (year, action){
        var self = this;
        var settings = JSON.parse( JSON.stringify(this.state.settings) );
        if ( action == "append" ) {
            settings.QuesYear.push( parseInt(year) );
        } else if ( action == "remove" ) {
            var index = -1;
            if ( settings.QuesYear.indexOf(parseInt(year)) != -1 ) index = settings.QuesYear.indexOf(parseInt(year));
            if ( settings.QuesYear.indexOf( year.toString() ) != -1 ) index = settings.QuesYear.indexOf(parseInt(year));
            if ( index != -1 ) settings.QuesYear.splice(index);
        } else {
            console.warn("unknow action.");
        }
        self.setState({"settings": settings});
        console.log(JSON.stringify(this.state.settings));
    },
    upload: function (){
        // 上传更改信息
        var self = this;
        var url = "/cgi-bin/auth.php";
        var body = {
                "hook": "setting.updateQuesArea", 
                "data": {
                    "areas": this.state.settings.QuesArea,
                    "years": this.state.settings.QuesYear,
                }
        };
        // 数据检查, 试题年份 选择不少于 3 个值 & 出题范围（城市）不少于 1 个值
        if ( body.data.years.length < 3 || body.data.areas.length < 2  ) {
            setalert("很抱歉，试题年份至少需要选择 3个，试题地区至少选择 1个");
            return false;
        }

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : "出题范围更新失败 ..." );
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                setalert("出题范围更新成功");
                check_login(); // 根据 cookie 更新 auth 对象
            }catch(e){
                console.warn(e); return false;
            }
        };
        requests.put(url, body, {"async": true, "callback": callback});
    },
    showProvinces: function (){
        this.setState({"showProvinces": !this.state.showProvinces });
    },
    goUpdateTestArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setTestArea });
    },
    goUpdateQuesArea: function (){
        this.props.parent.setState({ "component": window.components.user.settings.setQuestionAreaAndYear });
    },
    goUpdateProfile: function (){
        this.props.parent.setState({ "component": window.components.user.settings.profile });
    },
    goUpdatePassword: function (){
        this.props.parent.setState({ "component": window.components.user.settings.chpassword });
    },
    render: function (){
        var self = this;
        // City: <!--<li className="area_check_li area_check_icon ">石家庄</li>-->
        // province: 
        // Years: <!--<li className="year_check_li year_check_icon ">2015</li>-->
        return (
            React.createElement("div", {className: "common_main"}, 
                React.createElement("ul", {className: "common_list clearfix"}, 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateProfile}, "个人资料")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdateTestArea}, "考区设置")), 
                    React.createElement("li", {className: "common_select"}, React.createElement("a", null, "设置出题范围")), 
                    React.createElement("li", null, React.createElement("a", {onClick: self.goUpdatePassword}, "修改密码"))
                ), 

                React.createElement("div", {className: "common_main_type common_range "}, 
                    React.createElement("div", {className: "common_range_title"}, "出题范围"), 
                    React.createElement("div", {className: "choice_area"}, "选择地区范围"), 
                    React.createElement("div", {className: "area_detail"}, 
                        React.createElement("div", {className: "range_select"}, 
                            React.createElement("div", {className: "range_inp range_inp_icon check_area", onClick: self.showProvinces}, 
                                 [1].map(function (){
                                    if ( self.state.settings.QuesArea.length > 0 ) {
                                        return self.state.provinces.map( function (province, index){
                                                if ( parseInt(self.state.province_on_select) == parseInt(province.id)  ) return province.name;
                                                else return null;
                                        }).filter(function (v){
                                            return v != undefined && v != null && v!= "";
                                        }).join(",");
                                    } else {
                                        return (
                                            React.createElement("span", null, "请选择考区")
                                        );
                                    }
                                }) 
                            ), 
                            React.createElement("span", null, "提示：以下试题均有试题，选择不可低于1个市级")
                        ), 
                         [this.state.showProvinces].map(function (isShow){
                            if ( isShow != true ) return null;
                            return (React.createElement("div", {className: "range_select_choice area_list hehe_nodata"}, 
                                self.state.provinces.map(function (province, index){
                                    var province_name = province.name;
                                    // if ( province_name.length >= 5 ) province_name = province_name.slice(0, 5);
                                    if ( self.state.settings.QuesArea.indexOf(parseInt(province.id)) == -1 &&
                                        self.state.settings.QuesArea.indexOf( province.id.toString() ) == -1 ) {
                                        return (React.createElement("a", {onClick: self.get_cities.bind(self, province.id) }, province_name));
                                    } else {
                                        return (React.createElement("a", {className: "choice_selected", title: province_name}, province_name));
                                    }
                                }) 
                            ));
                        }), 

                        React.createElement("div", {className: "area_check"}, 
                            React.createElement("ul", {className: "clearfix extent_list"}, 
                                 self.state.cities.map( function (city, index) {
                                    var city_name = city.name;
                                    if ( city_name.length >= 5 ) city_name = city_name.slice(0, 5);

                                    if ( self.state.settings.QuesArea.indexOf(parseInt(city.id)) == -1 &&
                                        self.state.settings.QuesArea.indexOf( city.id.toString() ) == -1 ) {
                                        return (React.createElement("li", {onClick: self.updateQuesArea.bind(self, city.id, "append"), className: "area_check_icon", title: city.name}, city_name));
                                    } else {
                                        return (React.createElement("li", {onClick: self.updateQuesArea.bind(self, city.id, "remove"), className: "area_check_icon area_check_li", title: city.name}, city_name));
                                    }
                                }) 
                            )
                        )
                    ), 

                    React.createElement("ul", {className: "clearfix extent_list"}, 
                         [1, 2].map( function (city_id, index) {
                            var dict = {1: "国家统考", 2: "省级统考"};
                            if ( self.state.settings.QuesArea.indexOf(parseInt(city_id)) == -1 &&
                                self.state.settings.QuesArea.indexOf( "1" ) == -1 ) {
                                return (React.createElement("li", {
                                            onClick: self.updateQuesArea.bind(self, parseInt(city_id), "append"), 
                                            className: "area_check_icon"}, dict[city_id]));
                            } else {
                                return (React.createElement("li", {
                                            onClick: self.updateQuesArea.bind(self, parseInt(city_id), "remove"), 
                                            className: "area_check_icon area_check_li"}, dict[city_id]));
                            }
                        }) 
                     ), 

                     React.createElement("div", {className: "choice_year"}, "选择年份范围"), 
                     React.createElement("div", {className: "year_detail"}, 
                        React.createElement("span", null, "提示：为了保证训练量，请选择3个年份以上"), 
                        React.createElement("div", {className: "year_check"}, 
                            React.createElement("ul", {className: "clearfix year_list"}, 
                                 [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015].map(function (year, i){
                                    if ( self.state.settings.QuesYear.indexOf( parseInt(year) ) == -1 && self.state.settings.QuesYear.indexOf( year.toString() ) == -1 ) {
                                        return (
                                            React.createElement("li", {
                                                onClick: self.updateQuesYear.bind(self, year, "append"), 
                                                className: "year_check_icon", 
                                                key: "year_" + year.toString() + "_"}, year));
                                    } else {
                                        return (
                                            React.createElement("li", {
                                                onClick: self.updateQuesYear.bind(self, year , "remove"), 
                                                className: "year_check_icon year_check_li", 
                                                key: "year_" + year.toString() + "_"}, year));
                                    }
                                }) 
                            )
                        )
                    ), 
                    React.createElement("div", {className: "orang_save_set"}, 
                        React.createElement("button", {id: "save_extent", onClick: self.upload}, 
                            "保存设置"
                        )
                    )
                )
            )
        );
    }
});





window.components.user.settings.restpassword = React.createClass({
    displayName: "components.user.settings.restpassword",
    name: "重置密码",
    // api: new apis.tests(), 
    getInitialState: function (){
        // props: root
        return {};
    },
    componentDidMount: function (){

    },
    render: function (){
        var self = this;
        return (React.createElement("div", null));
    }
});

window.components.user.settings.findpassword = React.createClass({
    displayName: "components.user.settings.findpassword",
    name: "找回密码",
    // api: new apis.tests(), 
    getInitialState: function (){
        // props: root
        return {};
    },
    componentDidMount: function (){

    },
    render: function (){
        var self = this;
        return ( React.createElement("div", null) );
    }
});
