// jsx


// var Paperxxx = React.createClass({
//   render: function() {
//     var o = $("#example").html();
//     print(':: render ... ');
//     print(o);
//     print(this);
//     return <div>i.m {this.props.name}</div>;
//   }
// });

// $(document).ready(function(){
//     React.render(<h1>Hello, world!</h1>, document.getElementById('example'));
//     React.render(<Paperxxx name="ok." />, document.getElementById('example'));
//     React.render(<Paperxxx name="ok2." />, document.getElementById('example'));
//     React.render(<Paperxxx name="ok3." />, document.getElementById('example'));
//     React.render(<Paperxxx name="ok4." />, document.getElementById('example'));
// });



var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var TodoList = React.createClass({
 displayName: "hehehe",
  getInitialState: function() {
    print("init state");
    this.data = "asdasdsad";
    print();
    return {items: ['hello', 'world', 'click', 'me']};
  },
  handleAdd: function() {
    var newItems =
      this.state.items.concat([prompt('Enter some text')]);
    this.setState({items: newItems});
  },
  handleRemove: function(i) {
    var newItems = this.state.items;
    newItems.splice(i, 1);
    this.setState({items: newItems});
  },
  render: function() {
    var items = this.state.items.map(function(item, i) {
      return (
        React.createElement("div", {key: item, onClick: this.handleRemove.bind(this, i)}, 
          item
        )
      );
    }.bind(this));
    return (
      React.createElement("div", null, 
        React.createElement("button", {onClick: this.handleAdd}, "Add Item"), 
        React.createElement(ReactCSSTransitionGroup, {transitionName: "example"}, 
          items
        )
      )
    );
  }
});

$(document).ready(function(){
    React.render(React.createElement(TodoList, null), document.getElementById('example'));
});
