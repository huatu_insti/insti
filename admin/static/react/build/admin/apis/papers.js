if ( !window.apis ) window.apis = {};

apis.papers = function (){
    if ( "papers" in this ) throw new Error("需要先实例化 apis.papers 类( 伪 Class )");
    this.name = "papers api";
    this.version = "0.1";

};

apis.papers.prototype.init = function (body, xmen){
    if (!body ) {
        var body = {
             "pid": 0, "status": 0, "name": "新增试卷", "inittime": time.time(),
             "updtime": time.time(), 
             "attrs": [
                {"name": "year", "value": "2015"},
                { "name": "area", "value": "3125"}
             ],
             "tombstone": 0,
             "struct": []
        };
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers";
    requests.put(url, body, {"async": true, "callback": xmen});
};

apis.papers.prototype.fetch = function (page, size, xmen){
    if ( !page ) var page = 1;
    if ( !size ) var size = 20;
    if ( !xmen ) var xmen = function (){};
    if ( parseInt(page) < 1 ) page = 1;
    if ( parseInt(size) < 5 ) size = 20;

    var url = "/apis/papers";
    var query = "?page=#page#&size=#size#".replace("#page#", parseInt(page)-1).replace("#size#", size);
    requests.get(url + query, {"async": true, "callback": xmen});
};

apis.papers.prototype.pull = function (pid, xmen){
    if ( !pid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers/" + pid;
    requests.get(url, {"async": true, "callback": xmen});
};
apis.papers.prototype.get = function (pid, xmen){
    apis.papers.prototype.pull(pid, xmen);
};

apis.papers.prototype.remove = function (pid, xmen){
    if ( !pid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers/" + pid;
    requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
};

apis.papers.prototype.update = function (pid, xmen){
    throw new Error("apis.papers.prototype.update unready.");
    if ( !pid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers/" + pid;
    requests.patch(url, {"async": true, "callback": xmen});
};

apis.papers.prototype.appendAttr = function (pid, attr, xmen){
    if ( !pid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers/" + pid + "/attrs";
    // attr: { "试卷年份": "2014" }
    requests.patch(url, {"attrs": attr}, {"async": true, "callback": xmen});
};
apis.papers.prototype.updateAttr = function (pid, attr, xmen){
    if ( !pid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers/" + pid + "/attrs";
    // attr: { "试卷年份": "2014" }
    requests.patch(url, {"attrs": attr}, {"async": true, "callback": xmen});
};
apis.papers.prototype.removeAttr = function (pid, attr, xmen){
    if ( !pid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers/" + pid + "/attrs";
    requests.METHOD_DELETE(url, {"attrs": attr}, {"async": true, "callback": xmen});
};

// Paper Struct API
/*

URL: http://115.159.4.84:9321/papers/:pid/struct
Method: PATCH
Parameter：{ "handler" : "paper.struct.append", "struct" : [ { "qid" : 1 }] }
Descrp:
    
    {
        "name": "修改试卷",
        "struct": [
            {
                "cqid": 1
            }
        ],
        "status": 1,
        "tombstone": 0,
        "updtime": 1427249537,
        "inittime": 1427191268,
        "pid": 491
    }
*/

apis.papers.prototype.updateBody = function (pid, body, xmen){
    if ( !pid || parseInt(pid) < 1 || !body ){
        throw new Error("apis.papers.updateBody fail");
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/apis/papers";
    requests.patch(url, body, {"async": true, "callback": xmen});
};
apis.papers.prototype.updateAttrs = function (pid, action, attrs, xmen){
    // 更新试卷元素结构
    if ( !pid || !action ) throw new Error("apis.papers.prototype.updateAttrs error.");
    if ( !xmen ) var xmen = function (){};
    if ( action != "append" && action != "remove" && action != "clean" ) throw new Error("apis.papers.prototype.updateAttrs error.");
    if ( "action" == "clean" ) attrs = [];
    /*
        Action ( handler ):
            append: paper.attrs.append
            remove: paper.attrs.remove
            clean:  paper.attrs.clean

        { "handler" : "paper.attrs.append", "attrs" : [ { "name" : "year", "value" : "2015" } ] }
    */
    var dict = {"append": "paper.attrs.append", "remove": "paper.attrs.remove", "clean": "paper.attrs.clean" };
    var url = "/apis/papers/" + pid + "/attrs";
    var data = {"handler": dict[action], "attrs": attrs };
    requests.patch(url, data, {"async": true, "callback": xmen});
};
apis.papers.prototype.updateStruct = function (pid, action, elements, xmen){
    // 更新试卷元素结构
    if ( !pid || !action ) throw new Error("apis.papers.prototype.updateStruct error.");
    if ( !xmen ) var xmen = function (){};
    if ( action != "append" && action != "remove" && action != "clean" ) throw new Error("apis.papers.prototype.updateStruct error.");
    if ( action == "clean" ) elements = [];
    /*
        Action ( handler ):
            append: paper.struct.append
            remove: paper.struct.remove
            clean:  paper.struct.clean
        
        elements: 
            [ { "qid" : 1 }],
            [ { "qid" : 1 }, { "cqid" : 2 } ],
            [ ],
        
        { "handler" : "paper.struct.append", "struct" : [ { "qid" : 1 }] }
    */
    var dict = {"append": "paper.struct.append", "remove": "paper.struct.remove", "clean": "paper.struct.clean" };
    var url = "/apis/papers/" + pid + "/struct";
    var data = {"handler": dict[action], "struct": elements };
    requests.patch(url, data, {"async": true, "callback": xmen});
};


// paper status update
/*
            "handler" : ""
    修改试卷编辑状态
        "handler" : "paper.status.reset"
    URL:    http://115.159.4.84:9321/papers/:pid/status
    e.g:    http://115.159.4.84:9321/papers/:pid/status?token=eyJhcHBpZ...
    Method: PATCH
    Parameter：{ "handler" : "paper.status.process",  "pid" : 491 }
    Descrp: 
    返回json:
    {
        "name": "修改试卷",
        "updtime": 1427191609,
        "pid": 491
    }

*/
apis.papers.prototype.updateStatus = function (pid, action, xmen){
    if ( !pid ) throw new Error("apis.papers.prototype.updateStatus error.");
    if ( !xmen ) var xmen = function (){};
    if ( action != "update" && action != "reset" ) throw new Error("apis.papers.prototype.updateStatus error.");
    /*
        Action ( handler ):
            update: paper.status.process
            reset:    paper.status.reset
        
        Method: PATCH

        Http Body: { "handler" : "paper.status.process" }

        Response:
            {
                "name": "修改试卷",
                "updtime": 1427191609,
                "pid": 491
            }
    */
    var dict = {"update": "paper.status.process", "reset": "paper.status.reset" };
    var url = "/apis/papers/" + pid + "/status";
    var data = {"handler": dict[action] };
    requests.patch(url, data, {"async": true, "callback": xmen});
};
