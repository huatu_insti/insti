
if ( !window.components ) window.components = {};
if ( !window.components.auth ) window.components.auth = {};

window.components.auth.sign = React.createClass({
    displayName: "components.auth.sign",
    name: "签名",
    // api: new apis.papers(),
    getInitialState: function (){
        window.SIGN = this;
        var self = this;
        // props: action
        //                  *   signin
        //                  *   signup
        //                  *   signout
        //                  *   resetpwd
        //                  *   findpwd
        return { "component": null };
    },
    componentWillReceiveProps: function (){
        var self = this;
    },
    componentDidMount: function (){
        var self = this;
    },
    componentWillUpdate: function (){
        var self = this;

    },
    render: function (){
        var self = this;

        var action = "signin";
        if ( this.props.action == "signin" 
                || this.props.action == "signup" 
                || this.props.action == "signout" 
                || this.props.action == "resetpwd" 
                || this.props.action == "findpwd" 
        ) action = this.props.action;

        var component = null;
        if ( action == "signin" ) {
            // self.setState({"action": <window.components.auth.signin parent={self} /> });
            component = <window.components.auth.signin parent={self} />;
        }
        if ( action == "signup" ) {
            // self.setState({"action": <window.components.auth.signup parent={self} /> });
            component = <window.components.auth.signup parent={self} />;
        }
        if ( action == "signout" ) {
            // self.setState({"action": <window.components.auth.signout parent={self} /> });
            component = <window.components.auth.signout parent={self} />;
        }
        if ( action == "resetpwd" ) {
            // self.setState({"action": <window.components.auth.resetpwd parent={self} /> });
            component = <window.components.auth.resetpwd parent={self} />;
        }
        if ( action == "findpwd" ) {
            // self.setState({"action": <window.components.auth.findpwd parent={self} /> });
            component = <window.components.auth.findpwd parent={self} />;
        }


        return (
            <div>
                <div className="log_head">
                    <div className="log_img">
                        <img src="static/images/login_logo.png"/>
                    </div>

                </div>

                <div className="log_content">
                    <div className="log_con">

                        { component }
                        <div className="log_right">
                            <img src="static/images/login_03.png"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});


window.components.auth.signin = React.createClass({
    displayName: "components.auth.signin",
    name: "登录",
    // api: new apis.papers(),
    getInitialState: function (){
        var self = this;
       
        return { "account": "", "password": "", "remember": true };
    },
    componentWillUmount:function(){
       
    },
    componentDidMount: function (){
        var self = this;
       

          $(".m_qq").hover(function(){
                 $(this).attr("src","/static/images/m_qq_hover.png");
          },function(){
                 $(this).attr("src","/static/images/m_qq.png");
          })
          $(".m_weibo").hover(function(){
                 $(this).attr("src","/static/images/m_weibo_hover.png");
          },function(){
                 $(this).attr("src","/static/images/m_weibo.png");
          })
           $(".m_weixin").hover(function(){
                 $(this).attr("src","/static/images/m_weixin_hover.png");
          },function(){
                 $(this).attr("src","/static/images/m_weixin.png");
          })
          // $(".m_qq").on("click",function(){
          //         var popup = QC.Login.showPopup({
          //              "appId":"101280324",
          //              "redirectURI":"http://tiku.easymstudy.com/index.html",
          //          });  
          // })
          $(".m_qq").on("click",function(){
               
                  $("#qq img").get(0).click();
          })
          QC.Login({"btnId":"qq"})
             
        if(window.auth != undefined ){
                QC.Login.signOut();
        }           


    },
    updateAccount: function (e, rid){
        this.setState( {"account": $(e.target).val() } );
    },
    updatePassword: function (e, rid){
        this.setState( {"password": $(e.target).val() } );
    },
    updateRemberme: function (e, rid){
        this.setState( {"remember": !this.state.remember } );
    },
    onSignin: function () {
        var self = this;
        if($.trim(self.state.account) == ""){setalert("请输入账号");return false;}
        if($.trim(self.state.password) == ""){setalert("请输入密码");return false;}
        var url = "/cgi-bin/auth.php";
        var body = {"hook": "auth.app_signin", "data": {"account": this.state.account, "password": this.state.password} };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert("登录失败 ...");
                console.warn(response); return;
            }
            try{  
                var body = JSON.parse(response.body);
				
				
                window.location.assign("#index");
            }catch(e){
                console.warn(e); return false;
            }
        };
        requests.put(url, body, {"async": true, "callback": callback});
    },
   
    render: function (){
        var self = this;
        return (
            <div className="log_left">
                
                <div className="login_title">登陆麦题库</div>
                <div className="form_login">
                    <div className="inp inp_account">
                        <input  
                                type="text" 
                                className="account" 
                                placeholder="请输入电子邮箱或手机号" 
                                value={self.state.account}
                                onChange={self.updateAccount} />
                         <div className="errorbg none">请填写账号</div>
                    </div>
                    <div className="inp inp_pass">
                        <input  
                                type="password" 
                                className="password" 
                                placeholder="密码"
                                value={self.state.password}
                                onChange={self.updatePassword} />
                         <div className="errorbg none">请填写密码</div>
                    </div>
                    <div className="Cinput">
                        <input 
                                title="同意"
                                type="checkbox" 
                                className="checkInput" 
                                checked={self.state.remember}
                                onChange={self.updateRemberme} />
                        <div className="C-zidong">
                            <label>下次自动登录</label>
                        </div>
                        <div className="C-forget">
                           
                        </div>
                    </div>
                   <div className="btn_login">
                        <button 
                                type="button" 
                                className="submit_button"
                                onClick={self.onSignin}  >
                            登录
                        </button>
                    </div>
                    <div className="Cinput">
                        <div className="otherlogin">
                            <span className="otherLogin">其他方式登陆</span>
                        </div>
                        <div className="no_account">
                            <span id="qq" style={{"display":"none"}}></span>
                           <img src="/static/images/m_qq.png" className="m_qq"/>
                            <img src="/static/images/m_weibo.png" className="m_weibo"/>
                            <img src="/static/images/m_weixin.png" className="m_weixin"/>
                        </div> 
                    </div>
                    <div>
                         <div className="no_account" style={{"marginTop":"40px","fontSize":"12px",
                                                          "height":"18px",
                                                          "textAlign":"right"
                                                          }}>
                            还没有账号？立即<a href="#signup" style={{"color":"#009245"}}>注册</a>
                         </div>
                    </div>

                </div>
            </div>
        );
    },
 

});
window.components.auth.signup = React.createClass({
    displayName: "components.auth.signup",
    name: "注册",
    // api: new apis.papers(),
    getInitialState: function (){
        // props: parent / 
        var tab = "phone";
        if ( this.props.tab == "email" || this.props.tab == "phone" ) tab = this.props.tab;
        return { 
            "tab": tab,
            "phone": {
                "phone": "", 
                "password": "",
                "repassword": "",
                "nickname":"",
                "verify": ""
            },
            "email": {
                "email": "", 
                "password": "",
                "repassword": "",
                "nickname":"",
                "verify": ""
            },
           "verifyUrl":""
        };
    },
    componentDidMount: function (){
        var self = this;
        self.getVerifyImgUrl();
      

    },
    getVerifyImgUrl:function(){
        var self = this;
        var url = "/libs/verify/verify.php?random="+Math.random();
        var callback = function(response){
                if ( response.type == 'progress' ) return 'loading ...';
                if ( response.code != 200 ) {
                      
                        console.warn(response); return;
                 }
                try{
                    var body = JSON.parse(response.body);
                    self.setState({"verifyUrl":body.img_url,"code":body.code.toLowerCase()});
                }catch(e){
                        console.warn(e); return;
                } 
        }
        requests.get(url,{"async":true,"callback":callback});    
    },
    switch_tab: function (tab){
        
        if ( tab == "phone" ) this.setState({"tab": tab});
        else if ( tab == "email" ) this.setState({"tab": tab});
        else console.warn("oops ....");
        $(".errorbg").hide();
    },
    // 手机注册 Input 输入框数据更新
    updatePhoneTabDataByPhone: function (e, rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.phone = $.trim($(target).val());
        $(".error_phone").hide();
        this.setState( {"phone": phoneData} );
    },
    updatePhoneTabDataByPassword: function (e ,rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.password = $.trim($(target).val());
        $('.error_passwd').hide();
        this.setState( {"phone": phoneData} );
    },
    updatePhoneTabDataByRepassword: function (e, rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.repassword =$.trim( $(target).val());
        $(".error_repasswd").hide();
        this.setState( {"phone": phoneData} );
    },
    updatePhoneTabDataByNickname:function(e,rid){
        var self = this; 
        var target  = $(e.target);
        var phoneData = self.state.phone;
        phoneData.nickname=$.trim( target.val() );
        self.setState({"phone":phoneData})
      

    },
    updatePhoneTabDataByVerify: function (e, rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.verify = $.trim($(target).val()).toLowerCase();
        $(".error_yanz").hide();
        this.setState( {"phone": phoneData} );
    },

    // 邮件注册 Input 输入框数据更新
    updateEmailTabDataByEmail: function (e, rid){
        var target = $(e.target);
        var emailData = this.state.email;
        emailData.email = $.trim($(target).val());
        $(".error_email").hide();
        this.setState( {"email": emailData} );
    },
    updateEmailTabDataByPassword: function (e, rid){
        var target             = $(e.target);
        var emailData          = this.state.email;
        emailData.password     = $.trim($(target).val());
        $(".error_passwd").hide();
        this.setState( {"email": emailData} );
    },
    updateEmailTabDataByRepassword: function (e, rid){
        var target             = $(e.target);
        var emailData          = this.state.email;
        emailData.repassword   = $.trim($(target).val());
        $(".error_repasswd").hide();
        this.setState( {"email": emailData} );
    },
    updateEmailTabDataByNickname:function(e,rid){
        var self              = this; 
        var target            = $(e.target);
        var emailData         = self.state.email;
        emailData.nickname    =$.trim( target.val() );
        self.setState({"email":emailData})
     

    },
    updateEmailTabDataByVerify: function (e, rid){
        var target = $(e.target);
        var emailData = this.state.email;
        emailData.verify = $.trim($(target).val()).toLowerCase();
        $(".error_yanz").hide();
        this.setState( {"email": emailData} );
    },
    check_data: function (body){
        // 检查表单数据
         var self = this;
         var data = body['data'];  
     
      
         if ( data.account_type == "phone") {
            if(data.account == undefined || data.account == ""){   $(".error_phone").text("请填写手机号").show(); return false;}
            if ( /^0?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/.test( data.account ) != true ) {
                $(".error_phone").text("手机号无效").show();
                return false;
            }
        } else if ( data.account_type == "email") {
            if(data.account == undefined || data.account == ""){   $(".error_email").text("请填写邮箱地址").show(); return false;}  
            if ( /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test( data.account ) != true ) {
                $(".error_email").text("邮箱格式不正确").show();
                return false;
            }
        }
       
        if(data.password == "") {$(".error_passwd").text("请填写密码").show(); return false;}
        if ( data.password.length > 16 || data.password.length < 6 ) {
            
            $(".error_passwd").text("密码长度错误").show();
            return false;
        }
        if(data.repassword == "") {  $(".error_repasswd").text("请填写确认密码").show(); return false;}
        if ( data.password != data.repassword ) {
         
           $(".error_repasswd").text("两次输入密码不一致").show();
            return false;
        }
        if( data.nickname == ""){ $(".error_nickname").text("请填写昵称").show();return false;}
        if(data.verify == ""){ $(".error_yanz").text("请填写验证码").show(); return false;}
      
        if(data.verify != self.state.code){ 
            $(".error_yanz").text("验证码不正确").show();
            return false;
         }
        
     
       

        return true; 
    },
    onSignup: function (){
        // submit form data.
        var self = this; 
        var url = "/cgi-bin/auth.php";
        var data = this.state[this.state.tab];
        var account_type = this.state.tab; // phone / email
        var body = {
                "hook": "auth.signup", 
                "data": {
                    "account_type": account_type,
                    "account": data.phone ? data.phone : data.email, 
                    "password": data.password,
                    "repassword": data.repassword,
                    "nickname":data.nickname,
                    "verify": data.verify,
                }
        };
        

        var callback = function (response){ console.log(response);
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : "注册失败 ..." );
                console.warn(response); return;
            }
            try{ 
                var body = JSON.parse(response.body); 
                window.location.assign("#index");
            }catch(e){
                console.warn(e); return false;
            }
        };
        if ( this.check_data(body) == true ) requests.put(url, body, {"async": true, "callback": callback});
    },
    signup_by_email: function (){
        var self = this;
        return (
                <div className="form_register">
                    <div className="inp inp_email">
                        <input  
                                type="text" 
                                className="email" 
                                placeholder="电子邮箱" 
                                autoComplete="off"
                                value={self.state.email.email}
                                onChange={self.updateEmailTabDataByEmail} />
                        <div className="errorbg none error_email">请填写电子邮箱</div>
                    </div>
                    <div className="inp inp_pass">
                        <input 
                                type="password"
                                className="password" 
                                placeholder="密码（密码为6-16位的英文、数字、下划线）"
                                value={self.state.email.password}
                                onChange={self.updateEmailTabDataByPassword} />
                        <div className="errorbg none error_passwd">请填写密码</div>
                    </div>
                     <div className="inp inp_pass">
                        <input 
                                type="password" 
                                className="repassword" 
                                placeholder="确认密码"
                                value={self.state.email.repassword}
                                onChange={self.updateEmailTabDataByRepassword} />
                        <div className="errorbg none error_repasswd">请填写确认密码</div>
                    </div>
                    <div className="inp inp_account">
                        <input 
                                type="text"
                                className="nickname" 
                                placeholder="昵称 2-18位，中英文，数字，下划线"
                                value={self.state.email.nickname}
                                onChange={self.updateEmailTabDataByNickname} />
                        <div className="errorbg none error_nickname">请填写昵称</div>
                    </div>
                    <div className="inp_div">
                        <div className="inp inp_yanz">
                            <input
                                    type="text" 
                                    title="验证码"  
                                    className="text_yanz" 
                                    placeholder="验证码" 
                                    autoComplete="off"
                                    value={self.state.email.verify}
                                    onChange={self.updateEmailTabDataByVerify} />
                            <div className="errorbg none error_yanz">请填写验证码</div>
                        </div>
                        <div className="right" style={{"marginRight":"30px"}}>
                              <img className="verify_img" src={"data:image/png;base64,"+self.state.verifyUrl} onClick={self.changeVerify}/>
                        </div>
                    </div>
                     <div className="btn_register">
                        <button 
                                type="button" 
                                className="submit_button register_button"
                                onClick={self.onSignup}>
                            注册
                        </button>
                    </div>
                   
                 </div>
        );
    },
    changeVerify:function(){ 
          var self = this;
          self.getVerifyImgUrl();
    },
   
    signup_by_phone: function (){
        var self = this;
        return (
                <div className="form_register">

                    <div className="inp inp_phone">
                        <input  
                                type="text" 
                                className="phone" 
                                placeholder="手机号"
                                value={self.state.phone.phone}
                                autoComplete="off"
                                key="register_by_phone"
                                onChange={self.updatePhoneTabDataByPhone} />
                        <div className="errorbg none error_phone">请输入正确的手机号</div>
                    </div>
                  
                    <div className="inp inp_pass">
                        <input  
                                type="password" 
                                className="password" 
                                placeholder="密码（密码为6-16位的英文、数字、下划线）"
                                value={self.state.phone.password}
                                onChange={self.updatePhoneTabDataByPassword} />
                        <div className="errorbg none error_passwd">请填写密码</div>
                    </div>
                  
                     <div className="inp inp_pass">

                        <input  
                                type="password"  
                                className="repassword " 
                                value={self.state.phone.repassword}
                                placeholder="确认密码"
                                onChange={self.updatePhoneTabDataByRepassword} />
                        <div className="errorbg none error_repasswd">请填写确认密码</div>
                    </div>
                    <div className="inp inp_account">
                        <input 
                                type="text"
                                className="nickname" 
                                placeholder="昵称 2-18位，中英文，数字，下划线"
                                value={self.state.phone.nickname}
                                onChange={self.updatePhoneTabDataByNickname} />
                        <div className="errorbg none error_nickname">请填写昵称</div>
                    </div>
                    <div className="inp_div">
                     
                        <div className="inp inp_yanz">
                            <input  
                                    type="text"  
                                    className="text_yanz"  
                                    placeholder="验证码" 
                                    autoComplete="off"
                                    value={self.state.phone.verify} 
                                    onChange={self.updatePhoneTabDataByVerify} />
                            <div className="errorbg none error_yanz">请填写验证码</div>
                        </div>
                        <div className="right" style={{"marginRight":"30px"}}>
                            <img className="verify_img" src={"data:image/png;base64,"+self.state.verifyUrl} onClick={self.changeVerify}/>
                        </div>
                    </div>
                    <div className="btn_register">
                        <button 
                                type="button" 
                                className="submit_button register_button"
                                onClick={self.onSignup} >
                            注册
                        </button>
                    </div>
                    
                </div>
        );
    },
  
    render: function (){
        var self = this;
      
        var tab = function (){
            // 标签页
            if ( self.state.tab == "phone" ) {
                return (
                    <ul className="re_tab">
                        <li className="li_p">手机注册</li>
                        <li onClick={self.switch_tab.bind(self,'email')} >邮箱注册</li>
                    </ul>
                );
            } else {
                return (
                    <ul className="re_tab">
                        <li onClick={self.switch_tab.bind(self, "phone") }>手机注册</li>
                        <li className="li_p">邮箱注册</li>
                    </ul>
                );
            }
        };

        var component = null;
        if ( self.state.tab == "phone" ) component = self.signup_by_phone();
        if ( self.state.tab == "email" ) component = self.signup_by_email();

        return (
            <div className="log_left reg_le">
                <div className="login_title fr_1">注册麦题库</div>
                    
                    
             
                {tab()}
                {component}
               
                <div className="Cinput">
                      <div className="no_account" style={{"fontSize":"12px","textAlign":"right"}}>
                            已有账号,立即<a href="#signin" style={{"color":"#009245",}}>登陆</a>
                      </div>
                </div>
            </div>
        );
    }
});

window.components.auth.signout = React.createClass({
    displayName: "components.auth.signout",
    name: "登出",
    // api: new apis.papers(),
    getInitialState: function (){
        // props: parent / 
        console.log("sign out component ...");
        return {};
    },
    componentDidMount: function (){
        
    },
    render: function (){
        var self = this;

        return (
            <div />
        );
    }
});

window.components.auth.findpwd = React.createClass({
    // 找回密码和重置密码的区别 在于 
    //      找回密码是在当用户忘记密码的情况下
    //      重置密码是用户已掌握当前密码的情况下
    displayName: "components.auth.findpwd",
    name: "找回密码",
    // api: new apis.papers(),
    getInitialState: function (){
        // props: parent / 
        var tab = "phone";
        if ( this.props.tab ) tab = this.props.tab;
        return { 
            "tab": tab,
            "pdata": {
                "phone": "",
                "verify": ""
            },
            "edata": {
                "email": "",
                "verify": ""
            },
            "verifyUrl":""
        };
    },
    componentDidMount: function (){
        
    },
    switch_tab: function (tab){
       
        if ( tab != "phone" && tab != "email" ) return;
        this.setState({"tab": tab});
        if(tab == "email"){ this.getVerifyImgUrl();}
    },
    // 根据手机号码找回 密码
    updatePhoneByPTab: function (e, rid){
        // 手机号码更新
        var pData = this.state.pdata;
        pData.phone = $(e.target).val()
        this.setState( {"pdata": pData} );
    },
    updateVerifyByPTab: function (e ,rid){
        // 验证码更新
        var pData = this.state.pdata;
        pData.verify = $(e.target).val()
        this.setState( {"pdata": pData});
    },

    // 根据电子邮箱找回密码
    updateEmailByETab: function (e, rid){
        // 电子邮箱地址更新更新
        var emailData = this.state.edata;
        emailData.email = $(e.target).val()
        this.setState( {"edata": emailData} );
    },
    updateVerifyByETab: function (e ,rid){
        // 验证码更新
        var emailData = this.state.edata;
        emailData.verify = $(e.target).val()
        this.setState( {"edata": emailData} );
    },

    sendVerifyCode: function (){
        // 根据找回方式发送验证码
        console.info(":: send verify code ....");
    },
    findMe: function (){
        // 提交找回密码数据
        var self = this; 
        var url = "/cgi-bin/auth.php";
       
        var body = {
                "hook": "auth.findpwd", 
                "data": {
                    "account_type": self.state.tab,
                    "account": self.state.edata.email
                }
        };
      
        var callback = function (response){  console.log(response);
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : "失败 ..." );
                console.warn(response); return;
            }
            try{ 

                var body = JSON.parse(response.body); 
                 setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : "邮件发往"+self.state.edata.email+"成功" );
              
            }catch(e){
                console.warn(e); return false;
            }
        };
        requests.put(url, body, {"async": true, "callback": callback});
   
                  
                  
                  
        

        

     
    },
    phoneTab: function (){
        var self = this;
        return (
            <div className="form_register">
                <div className="inp inp_phone">
                    <input 
                            type="text" 
                            className="phone" 
                            placeholder="手机号" 
                            autoComplete="off"
                            value={self.state.pdata.phone}
                            onChange={self.updatePhoneByPTab} />
                    <div className="errorbg none">请输入正确的手机号</div>
                </div>
                <div className="inp_div">
                    <div className="inp inp_yanz">
                        <input 
                                type="text"
                                className="text_yanz"
                                placeholder="验证码"
                                autoComplete="off"
                                value={self.state.pdata.verify}
                                onChange={self.updateVerifyByPTab} />
                        <div className="errorbg none">请填写验证码</div>
                    </div>
                    <div 
                            className="get_num" 
                            onClick={self.sendVerifyCode}>
                        请获取验证码
                    </div>
                </div>
                <div className="btn_register">
                    <button 
                            type="button" 
                            className="submit_button" 
                            onClick={self.findMe}>
                        确定
                    </button>
                </div>
               
            </div>
        );
    },
    changeVerify:function(){ 
          var self = this;
          self.getVerifyImgUrl();
    },
    getVerifyImgUrl:function(){
        var self = this;
        var url = "/libs/verify/verify.php?random="+Math.random();
        var callback = function(response){
                if ( response.type == 'progress' ) return 'loading ...';
                if ( response.code != 200 ) {
                      
                        console.warn(response); return;
                 }
                try{
                    var body = JSON.parse(response.body);
                    self.setState({"verifyUrl":body.img_url,"code":body.code.toLowerCase()});
                }catch(e){
                        console.warn(e); return;
                } 
        }
        requests.get(url,{"async":true,"callback":callback});    
    },
    emailTab: function (){
        var self = this;
        return (
            <div className="form_register">
                <div className="inp inp_email">
                    <input 
                        type="text" 
                        className="email" 
                        placeholder="电子邮箱" 
                        autoComplete="off"
                        value={self.state.edata.email}
                        onChange={self.updateEmailByETab} />
                    <div className="errorbg none">请填写电子邮箱</div>
                </div>
               
                <div className="inp_div">
                    <div className="inp inp_yanz">
                        <input 
                                type="text" 
                                title="验证码"
                                className="text_yanz"
                                placeholder="验证码" 
                                autoComplete="off"
                                value={self.state.edata.verify}
                                onChange={self.updateVerifyByETab} />
                        <div className="errorbg none">请填写验证码</div>
                    </div>
                    <div className="right">
                        <img className="verify_img" src={"data:image/png;base64,"+self.state.verifyUrl} onClick={self.changeVerify}/>
                    </div>
                </div>
                <div className="btn_register">
                    <button 
                            type="button"
                            className="submit_button" 
                            onClick={self.findMe}>
                        确定
                    </button>
                </div>
                <div className="Cinput">
                    <div className="has_account">
                        已有账号？立即
                        <a href="#signin" className="regist">登陆</a>
                    </div> 
                </div>
             </div>
        );
    },
    render: function (){
        var self = this;

        var tab; 
        var tab_content = null;
        if ( this.state.tab == "phone" )  tab_content = self.phoneTab();
        if ( this.state.tab == "email" )  tab_content = self.emailTab();

        if ( this.state.tab == "phone" ) {
            tab = (
                <ul className="re_tab">
                    <li className="li_p">通过手机</li>
                    <li onClick={self.switch_tab.bind(self, "email")}>通过邮箱</li>
                </ul>);
            
        } else if ( this.state.tab == "email" ) {
            tab = (
                <ul className="re_tab">
                    <li onClick={self.switch_tab.bind(self, "phone")}>通过手机</li>
                    <li className="li_p">通过邮箱</li>
                </ul>);
        } else {
            console.warn(":; unknow tab name ....");
        }
        return (
            <div className="log_left re_pass">
                <div className="login_title fr_1">找回密码</div>
                {tab}
                {tab_content}
            </div>
        );
    }
});

window.components.auth.resetpwd = React.createClass({
    displayName: "components.auth.resetpwd",
    name: "重置密码",
    getInitialState: function (){
         var search = window.location.search.substr(1);
         var token= search.split("=")[1];


        // props: parent / 
        console.log("resetpwd component ...");
        return {"account":"", "password": "", "repassword": "","token":token};
    },
    componentDidMount: function (){
        
    },
    updateAccount: function (){

    },
    updatePassword: function (e, rid){
        this.setState({"password": $(e.target).val() });
    },
    updateRepassword: function (e, rid){
        this.setState({"repassword": $(e.target).val() });
    },
    onReset: function (){
        var self = this; 
        console.log(self.state);
        if(self.state.repassword != self.state.password){
               setalert("两次输入的密码不一致");
               return;
        }
        var url = "/cgi-bin/auth.php";
       
        var body = {
                "hook": "auth.doresetpwd", 
                "data": {
                   
                    "password": self.state.password,
                     "token":self.state.token
                }
        };
      
        var callback = function (response){  console.log(response);
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : " ..." );
                console.warn(response); return;
            }
            try{ 
                
                var body = JSON.parse(response.body); 
                  setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : "重置密码成功" );

              
            }catch(e){
                console.warn(e); return false;
            }
        };
        requests.put(url, body, {"async": true, "callback": callback});
   
    
    },
    render: function (){
        var self = this;
        return (
            <div className="log_left">
                <div className="login_title">设置密码</div>
                <div className="form_login" >
                    <input type="hidden" name="token" />
                    <div className="inp inp_account">
                        <input
                                type="password"
                                className="account" 
                                placeholder="新密码" 
                                onChange={self.updatePassword} />
                         <div className="errorbg none">请填写新密码</div>
                    </div>
                    <div className="inp inp_pass">
                        <input 
                                type="password" 
                                className="password" 
                                placeholder="确认密码" 
                                onChange={self.updateRepassword} />
                         <div className="errorbg none">请填写确认密码</div>
                    </div>
                    <div className="btn_login">
                        <button 
                                className="submit_button"
                                type="button" 
                                onClick={self.onReset} >
                            提交
                        </button>
                    </div>
                </div>

            </div>
        );
    }
});