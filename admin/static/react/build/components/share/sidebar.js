if ( !window.components ) window.components = {};
if ( !window.components.share ) window.components.share = {};
window.components.share.sidebar = React.createClass({displayName: "sidebar",
	    componentDidMount:function(){
	    	$(".sub_list").parent().find("dl").hide();
	    	function sub_list_click(){
                $(this).parent().siblings().find("dl").slideUp().end().find("img").attr("src","static/images/sub_right.png");
                $(this).parent().find("dl").slideDown();
                $(this).find("img").attr("src","static/images/sub_up.png");
            }
            function sub_list_hover(){
                $(this).parent().siblings().find("dl").hide();
                $(this).parent().find("dl").show();
            }
            function sub_list_dl_mouseout(){
                $(this).hide();
            }
            
            $(".sub_list").on("click",sub_list_click);
            $(".show_sidebar").data("oc","false");
	    },
	    render:function(){
	    	  return React.createElement("div", {className: "sidebar"}, 
					        React.createElement("div", {className: "subnav"}, 
					            React.createElement("ul", null, 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico1"}, 
					                        React.createElement("span", null, "试卷管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt", onclick: "views.papers.fetch();"}, "试卷列表"), 
					                        React.createElement("dt", {className: "sub_dt", onclick: "views.papers.init();"}, "录入试卷"), 
					                        React.createElement("dt", {className: "sub_dt", onclick: "alert('正在开发中')"}, "试题纠错列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico2"}, 
					                        React.createElement("span", null, "考试管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "考试列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico3"}, 
					                        React.createElement("span", null, "标签管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "试卷标签列表"), 
					                        React.createElement("dt", {className: "sub_dt"}, "考试标签列表"), 
					                        React.createElement("dt", {className: "sub_dt"}, "试题标签列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico4"}, 
					                        React.createElement("span", null, "其他管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "意见反馈")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico5"}, 
					                        React.createElement("span", null, "系统设置"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "用户管理")
					                    )
					                )
					            )
					        )
					    )
	    }
})
					                      