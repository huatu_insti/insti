if ( !window.components ) window.components = {};
if ( !window.components.catalogs ) window.components.catalogs = {};
if ( !window.components.catalogs.exams ) window.components.catalogs.exams = {};

window.components.catalogs.exams.list =  React.createClass({
    displayName: "ExamsCatalogs",
    name: "考试标签列表",
    page: 1,
    area: -9,
    size: 20,
    api: new apis.catalogs.exams(),
    isOpen: false,
    isSync: false,
    catalogs_cache: { "list": [], "num": 0 },
    getInitialState: function (){
        return { "list": [], "num": 0 };
    },
    componentDidMount: function() {
        this.fetch();
    },
    __fetch: function (){
        var self = this; 
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body); 
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
            }catch(e){
                console.warn(e); return;
            }
            self.isSync = true;
            self.isOpen = true;
            self.replaceState(body);
        };
        /*
            list: [
                0: {name: "二级", hookid: 0, updtime: 1426655982, inittime: 1426655982, tombstone: 0, cid: 3},
                1: {name: "Food", hookid: 0, updtime: 1423122459, inittime: 1423122459, tombstone: 0, cid: 1},
            ],
            num: 5
        */
        this.api.root('0', callback);
    },
    fetch: function (){
        var self = this; 
        if ( this.isSync == true ){
            if ( this.isOpen == true ) {
                self.isOpen = false;
                self.catalogs_cache = self.state;
                self.replaceState({ "list": [], "num": 0 });
                self.forceUpdate();
                return ":: use cache ...";
            } else if ( this.isOpen == false ) {
                self.isOpen = true;
                self.replaceState(catalogs_cache);
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        self.__fetch();
    },
   
    children: function(cid, e, rid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
                // self.replaceState(body);
            }catch(e){
                console.warn(e); return;
            }
        };

        if ( parseInt($(e.target).parent().data("isopen")) < 1 ) {
            this.api.root(cid, callback);
        } else {
            
        }
    },
    add: function (hookid, name){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
               
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>父级标签&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
            self.__fetch();
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            self.api.push({"name": cname, "hookid": parseInt(self.state.cid) }, callback );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "添加考试标签",
            "width": 420,
            "height": 300
        });
    },
    remove: function (cid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            self.fetch();
        };
        this.api.remove(cid, callback);
    },
    render: function (){
        var self = this;
        var catalog_elems_count = function (catalog){
            if ( "elems" in catalog ) {
                return catalog.elems.length;
            } else {
                return 0;
            }
        };
        var style_of_col1;
        style_of_col1 = {"text-indent": 30};
        if ( self.isOpen == true ) style_of_col1.background = "url(static/images/lable_list.png) no-repeat 12px -25px #eee;";
        else style_of_col1.background = "url(static/images/lable_list.png) no-repeat 12px 10px;";

        return (
            <div>
                <div className="create_head">
                    <div className="record_paper">考试标签管理</div>
                </div>

                <div className="Tagtree" >
                    <div className="Tagtree_thead" >
                        <div className="Tagtree_row clearfix" >
                            <ul className="catalogs" >
                                <li >
                                    <div className="clearfix" >
                                        <div className="Tagtree_col1">考试标签名称</div>
                                        <div className="Tagtree_col2" >考试数量</div>
                                        <div className="Tagtree_col3" >考试排序</div>
                                        <div className="Tagtree_col4" >操作</div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div className="Tagtree_tbody" >
                        <ul className="catalogs" >
                            { this.state.list.map(function(c, i){
                                return (
                                    <window.components.catalogs.exams.list_row key={"CATALOG-"+c.cid} catalog={c} list={self} index={[i]} />
                                );
                            }) }
                            <li className="Tagtree_add" >
                                <a className="add_lable lable_icon" onClick={this.add.bind(this, '0' ,'顶级标签', null )}>添加一级标签</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        );
    }
});

window.components.catalogs.exams.list_row = React.createClass({
    displayName: "ExamsCatalogs-Row",
    name: "考试标签列表 - 列",
    api: new apis.catalogs.exams(),
    isOpen: false,
    isSync: false,
    children_copy: [],
    exams_num:0,
    getInitialState: function (){
        // {"name": "test", "hookid":0,"updtime":1429070010,"inittime":1429070010,"tombstone":0,"cid":this.props.cid}
        var c = this.props.catalog;
        c.children = [];
        return c;
    },
    componentDidMount:function () {
       var  self = this;
        // alert(self.props.catalog.name);
       if("elems" in self.props.catalog){
          self.exams_num += self.props.catalog.elems.length;
          self.forceUpdate();
       }
        //计算试卷数量
       var  compute_exams = function(cid){
            var callback = function(response){
                if ( response.type == 'progress' ) return 'loading ...';
                if ( response.code != 200 ) {
                    console.warn( response ); return;
                }
                var children = JSON.parse(response.body);
                for(var i=0;i<children.length;i++){
                     if(children[i].elems != undefined && children[i].elems.length>0){
                        self.exams_num+=children[i].elems.length;
                        self.forceUpdate();
                        
                     }
                     if( "cid" in children[i]){
                            compute_exams(children[i].cid);
                     }
                       
                }
             }
             self.api.root(cid,callback);
       }
       compute_exams(self.props.catalog.cid);
    },
    __fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn( response ); return;
            }
            var children = JSON.parse(response.body);
            if ( "list" in children != true ) children.list = [];
            self.isSync = true;
            self.isOpen = true;
            self.setState({"children": children});
        };
        this.api.root(this.props.catalog.cid, callback);
    },
    fetch: function (){
        var self = this;
        if ( this.isSync == true ){
            if ( this.isOpen == true ) {
                self.isOpen = false;
                self.children_copy = self.state.children;
                self.setState({"children": []});
                self.forceUpdate();
                return ":: use cache ...";
            } else if ( this.isOpen == false ) {
                self.isOpen = true;
                self.setState({"children": self.children_copy });
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        self.__fetch();
    },
    add: function (){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
                
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>父级标签&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", self.state.name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
            self.__fetch();
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            self.api.push({"name": cname, "hookid": parseInt(self.state.cid) }, callback );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "添加考试标签",
            "width": 420,
            "height": 300
        });
    },
    update: function (){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
              
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>当前标签名称&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", self.state.name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
       
        var callback = function (cname,response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
           
            self.setState({"name":cname})
        };
   
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            self.api.update(self.state.cid, {"name": cname, "hookid": parseInt(self.state.hookid) }, callback.bind(self,cname) );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "更新考试标签",
            "width": 520,
            "height": 320
        });
    },
    remove: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {  console.log(response);
                console.warn(response);
                return;
            }
            // self.props.list.fetch();
            $(('[data-cid="#cid#"]').replace("#cid#",self.state.cid)).remove();
        };
          $("<div class=\"confirm_exam_del\">"
            +"<div class=\"message\">确定删除此标签吗?</div>"
            +"<div class=\"add_btn\">"
                +" <a class=\"add_canal\">取消<\/a>"
                +" <a class=\"add_new\">确定<\/a>"
            +"</div></div>").dialog({
               title:"删除标签",
               close:true,
               modal:true,
               width:"400px"
        })
        $(".confirm_exam_del").find(".add_canal").on("click",function(){ $(".confirm_exam_del").dialog("close")})
                              .end().find(".add_new").on("click",function(){ 
                                     self.api.remove(self.state.cid, callback);$(".confirm_exam_del").dialog("close");})
        // this.api.remove(this.state.cid, callback);
    },
    render: function (){
        var self = this;
        if ( "children" in this.state != true ) this.setState({"children": []});

        var classname_of_col1;
        if ( self.isOpen == true ) classname_of_col1 = "-24px";
        else if ( self.isOpen == false ) classname_of_col1 = "10px";

        return (
            <li className="catalog" data-cid={self.state.cid}>
                <div className="Tagtree_row clearfix">
                    <div
                            className="Tagtree_col1"
                            style={ {
                                    "textIndent": self.props.index.length*50 - (self.props.index.length-1)*25, 
                                    "background": "url(static/images/lable_list.png) no-repeat " + (self.props.index.length*25).toString()+"px " + classname_of_col1,
                                    "cursor": "pointer"
                                    } 
                            } 
                            onClick={this.fetch} >
                        {this.state.name}
                    </div>
                    <div className="Tagtree_col2" >{self.exams_num}</div>
                    <div className="Tagtree_col3" >未知资料</div>
                    <div className="Tagtree_col4" >
                        <a className="a_oper" onClick={this.add}>添加子标签</a> | 
                        <a className="a_oper" onClick={this.update}>编辑标签</a> | 
                        <a className="a_oper" onClick={this.remove}>删除标签</a> | 
                    </div>
                </div>
                <ul className="catalogs">
                    { this.state.children.map(function(c, i){
                            return (
                                <window.components.catalogs.exams.list_row key={"CATALOG-"+c.cid} catalog={c} list={self.props.list} index={self.props.index.concat(i)} />
                            );
                    }) }
                </ul>
            </li>
        );
    }
});




/* 创建对话框 */
window.components.catalogs.exams.ktree_dialog =  React.createClass({
    displayName: "试题知识点选择对话框",
    name: "试题知识点",
    api: new apis.catalogs.exams(),
    catalogs: [],
    kps: {},           // 已选择的知识点
    getInitialState: function (){
        if ( !this.props.destroy ) this.destroy = function(){};
        else this.destroy = this.props.destroy;
        // if ( !this.props.ok ) this.ok = function(){};
        // else this.ok = this.props.ok;
        return { "list": [], "num": 0 };
    },
    componentDidMount: function() {
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
            }catch(e){
                console.warn(e); return ;
            }
            self.catalogs = body.list;
            self.forceUpdate();
        };
        /*
            list: [
                0: {name: "二级", hookid: 0, updtime: 1426655982, inittime: 1426655982, tombstone: 0, cid: 3},
                1: {name: "Food", hookid: 0, updtime: 1423122459, inittime: 1423122459, tombstone: 0, cid: 1},
            ],
            num: 5
        */
        this.api.root('0', callback);
    },
    ok: function (){
        // if ( !this.props.ok ) return;
        var self = this;
        var kps = [];
        Object.keys(this.kps).map(function (k, i){
            kps.push(self.kps[k]);
        });
        this.props.ok(kps);
        this.destroy();
    },
    destroy: function (){
        if ( !this.props.dialog ) return;
        $(this.props.dialog).dialog("close");
    },
    render: function (){
        var self = this;
        return (
                <div>
                    <div className="selectModal_mark">
                        <div className="modal-body">
                            <ul>
                                { this.catalogs.map(function(k, i){
                                    return <window.components.catalogs.exams.ktree 
                                                        key={"ktree_dialog_cids_" + k.cid.toString() + "_index_" + i.toString() }
                                                        parent={self}
                                                        root={self}
                                                        knowledge={k} 
                                                        index={i} />;
                                }) }
                            </ul>
                        </div>  
                        <div className="modal_footer">
                            <input  type="text" />
                            <a className="modal_a" href="javascript:;">搜索</a>
                            <a className="modal_ok" href="javascript:;" onClick={self.ok}>确定</a>
                        </div>
                    </div>

                    <div className="black" id="bg"></div>
                </div>
        );
    }
});

window.components.catalogs.exams.ktree =  React.createClass({
    displayName: "试题知识点树",
    name: "试题知识点树",
    api: new apis.catalogs.exams(),
    children: [],         // 子节点
    num: 0,              // 子节点元素总量
    sync_status: 0,   // 同步状态
    open_status: 0,  // 树打开状态
    is_hit: 0,             // 是否选择
    getInitialState: function (){
        if ( !this.props.knowledge ) throw new Error("ktree init fail.");
        return { "knowledge": this.props.knowledge };
    },
    componentDidMount: function() {
        // this.fetch();
    },
    fetch: function (cid, e, rid){
        var self = this;
        if ( parseInt(self.sync_status) == 1 ) {
            if ( parseInt(self.open_status) == 1 ) {
                self.open_status = 0;
                self.children_copy = self.children;
                self.children = [];
                self.forceUpdate();
                return ":: use cache ...";
            }
            if ( parseInt(self.open_status) == 0 ) {
                self.open_status = 1;
                self.children = self.children_copy;
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);
                return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body ) body = body.list;
                if ( "num" in body) self.num = body.num;
            }catch(e){
                console.warn(e); return;
            }
            self.sync_status = 1;
            self.open_status = 1;
            self.children = body;
            self.forceUpdate();
        };
        this.api.root(cid, callback);
    },
    hit_the_kp: function (cid, name){
        var self = this;

        if ( parseInt(self.sync_status) != 1 ) {
            // self.fetch();
            return;
        }
        if ( parseInt(self.children.length) == 0 ) {
            if ( parseInt(self.is_hit) == 1) {
                self.is_hit = 0;
                delete self.props.root.kps[parseInt(self.props.knowledge.cid)];
            } else {
                self.is_hit = 1;
                self.props.root.kps[parseInt(self.props.knowledge.cid)] = self.props.knowledge;
            }
            self.forceUpdate();
        } else {
            // 不可以选择

        }
    },
    render: function (){
        var self = this;
        var is_top_node = function (hookid){
            if ( hookid.toString() == "0" ) { 
                return "order_first";
            } else { 
                return "order_usually";
            }
        };
        var sync_ico, hit_classname;
        if ( self.open_status == 1 ) sync_ico = "level_two";  // 树打开状态.
        else sync_ico = "level_one";
        if ( self.is_hit == 1 ) hit_classname = "c_on"; // 选中状态
        else hit_classname = "c_ban";

        return (
            <li data-hookid={this.props.knowledge.hookid} 
                data-cid={this.props.knowledge.cid} 
                data-index={this.props.index} >

                <div className={is_top_node(this.props.knowledge.hookid)}>
                    <span className={ "checkbox_switch " + hit_classname }
                        onClick={this.hit_the_kp.bind(this, this.props.knowledge.cid, this.props.knowledge.name) }>
                    </span>
                    <span className={"level_switch " + sync_ico} 
                        onClick={this.fetch.bind(this, this.props.knowledge.cid) }>
                    </span>
                    <span className="mark_name">{this.props.knowledge.name}</span>
                    <ul>
                        { this.children.map(function (k, i){
                            return <window.components.catalogs.exams.ktree 
                                            key={"ktree_dialog_cids_" + k.cid.toString() + "_index_" + i.toString() } 
                                            parent={self} 
                                            root={self.props.root}
                                            knowledge={k} 
                                            index={i} />
                        }) }
                    </ul>
                </div>
            </li>
        );
    }
});