/*

{
    "content": "test",
    "struct": [
        {"qid": 1}, {"cqid": 1}
    ],
    "tombstone": 0,
    "updtime": 1427185803,
    "inittime": 1427185803,
    "cqid": 118
}

*/
if ( !window.apis ) window.apis = {};

apis.composites = function (){
    if ( "composites" in this ) throw new Error("需要先实例化 apis.composites 类( 伪 Class )");
    this.name = "composites api";
    this.version = "0.1";
};

apis.composites.prototype.init = function (body, xmen){
    if ( !body ){
        var body = {
                "cqid": 0,
                "content": "",
                "tombstone":0,
                "updtime":time.time(),
                "inittime":time.time(),
                "struct": []
            };
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipapp/composites";
    requests.put(url, body, {"async": true, "callback": xmen});
};
apis.composites.prototype.pull = function (cqid, xmen){
    if ( !cqid || parseInt(cqid) < 1 ){
        throw new Error("apis.composites.pull fail");
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipapp/composites/" + cqid;
    requests.get(url, {"async": true, "callback": xmen});
};

apis.composites.prototype.updateBody = function (cqid, body, xmen){
    if ( !cqid || parseInt(cqid) < 1 || !body ){
        throw new Error("apis.composites.updateBody fail");
    }
    /*
        {
            "content": "test",
            // "struct": [],  // updateBody 要去掉该字段
            "tombstone": 0,
            "updtime": 1427186401,
            "cqid": 118
        }

    */
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipapp/composites";
    requests.patch(url, body, {"async": true, "callback": xmen});
};
apis.composites.prototype.updateStruct = function (cqid, action, elem, xmen){
    /*
        Action ( handler ):
            append: composite.struct.append
            remove: composite.struct.remove
            clean:  composite.struct.clean

        Method: PATCH

        Http Body: { "handler" : "composite.struct.append", "struct" : [{"cid":1,"cname":"test","year":2015}] }

        Response:
            {
                "content": "test",
                "struct": [
                    {"qid": 1},
                    {"cqid": 2},
                    {"qid": 2}
                ],
                "tombstone": 0,
                "updtime": 1427186401,
                "inittime": 1427185803,
                "cqid": 118
            }
        Note:
            cid: catlog id.
            cname: catalog name.
            attrs: [ {"cid":1,"cname":"test"}, {"year":2015} ]
    */
    if ( "map" in elem ) var elements = elem;
    else if ( typeof elem == "object" ) var elements = [elem];
    
    if ( !cqid || parseInt(cqid) < 1 || !action || !elem ){
        throw new Error("apis.composites.updateStruct fail");
    }
    if ( !xmen ) var xmen = function (){};
    if ( "action" == "clean" ) elements = [];

    var dict = {"append": "composite.struct.append", "remove": "composite.struct.remove", "clean": "composite.struct.clean" };
    var url = "/instiApis/whipapp/composites/" + cqid + "/struct";
    var attrs = { "handler" : dict[action], "struct" : elements };
    requests.patch(url, attrs, {"async": true, "callback": xmen});
};

apis.composites.prototype.remove = function (ccqid, xmen){
    if ( !cqid || parseInt(cqid) < 1 ){
        throw new Error("apis.composites.delete fail");
    }
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipapp/composites/" + cqid;
    requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
};

