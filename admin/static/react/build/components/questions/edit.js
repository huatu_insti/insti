

if ( !window.components ) window.components = {};
if ( !window.components.questions ) window.components.questions = {};
components.questions.edit =  React.createClass({
    displayName: "Question",
    name: "普通题",
    tixu: 0,
    api: new apis.questions(),
    getInitialState: function(){
        if ( !this.props.qid ||  parseInt(this.props.pid < 0 ) ) throw new Error("components.questions.edit react init fail.");
        this.tixu = this.props.index.join("-");
        return {
            "tombstone":1,
            "updtime":time.time(),
            "inittime":time.time(),
            "qid":0,
            "score": 0,
            "content": "",
            "ans": "",
            "ansnum": 0,
            "choices": [],
            "attrs": []
        }
    },
    // 试题 主体 ( question body )
    init: function (){
        var self = this;
        var question = {
            "tombstone":0,
            "updtime":time.time(),
            "inittime":time.time(),
            "qid":0,
            "score": 0,
            "content": "",
            "ans": "",
            "ansnum": 0,
            "choices": [],
            "attrs": []
        };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.init() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                self.replaceState(body);
                self.props.paper.elements["qid"+body.qid] = self;
                // Note: 试题初始化步骤已经转移到 父级 对象内部实现，避免回调增多。
                // if ( "parent" in self.props ){
                //     // 当前元素含有父级元素(composite)
                //     // composite.updateStruct
                //     // self.parent.
                // }
            }catch(e){
                print(":: question.init fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        this.api.init(question, callback);
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(e);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "content" in body != true ) body.content = "";
                if ( "choices" in body != true ) body.choices = [];
                if ( "attrs" in body != true ) body.attrs = [];
                self.props.paper.elements["qid"+body.qid] = self;
                self.replaceState(body);
            }catch(e){
                console.warn(e);
            }
        };
        this.api.pull(this.props.qid, callback);
    },
    componentDidMount: function (){
        if ( parseInt(this.props.qid) == 0 ){
            this.init();
        } else if ( parseInt(this.props.qid) > 0 ) {
            this.pull();
        }
    },
    remove: function (){
        // remove element from paper struct.
        var self = this;
        var elem = {"qid": parseInt(this.state.qid)};
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function paper.remove() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "parent" in this.props ) {
                    // 当前元素为子元素，刷新父级元素的结构即可，不用刷新整套试卷结构
                    self.props.parent.pull();
                } else {
                    self.replaceState(body);    
                }
            }catch(e){
                print(":: paper.remove fail. json decode fail.");
                print(response);
            }
        };
        if ( "paper" in this.props ){
            this.props.paper.updateStruct( "remove", elem, callback );
        } else {
            throw new Error("remove question element from paper struct fail.\npaper object required.");
        }
    },
    addAttr: function (e, rid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.addAttr() fail.');
                print(response);
                return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                print(":: question.addAttr fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        this.api.updateAttrs(this.state.qid, "append", {"name":"新增标签", "value": ""}, callback);
    },
    updateAttr: function (e, rid){
        var self = this;
        var attrs = JSON.parse(JSON.stringify(this.state.attrs));

        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var index = parseInt($(obj).data("index"));

        var key = $(obj).find(".attr").text();
        var value = $(obj).find(".pape_attr_edit").html();


        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function Question.updateAttr fail.');
                print(response);
                return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                print(':: function Question.updateAttr fail.');
                print(response);
                print(e);
                return false;
            }
        };
        // diff
        if ( this.state.attrs[index].name == key &&  this.state.attrs[index].value == value ){
            // 无须更新
            print(":: 没有需要更新的数据");
        } else {
            if ( this.state.attrs[index].name == key && this.state.attrs[index].value != value ){
                // 更新 属性值
                print("update value");
                this.api.updateAttrs( this.state.qid, "append", {"name": key, "value": value }, callback);
            } else if ( this.state.attrs[index].name != key ){
                print("remove old key and add new key.");
                this.api.updateAttrs( this.state.qid, "remove", this.state.attrs[index] );  // 不检查是否删除成功
                this.api.updateAttrs( this.state.qid, "append", {"name": key, "value": value }, callback);
            } else {
                print("unknow type (question.updateAttrs) .");
            }
            // this.api.updateAttrs( this.state.qid, "append", {"name": key, "value": value }, callback);
        }
        
    },
    removeAttr: function (index, e, rid){
        var self = this;
        // var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        // var i = $(obj).parent().data("index");
        // var key = $(obj).find(".attr").text();
        // var value = $(obj).find(".pape_attr_edit").html();
        var callback = function(response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function Question.removeAttr fail.');
                print(response);
                return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                print(':: function Question.removeAttr fail.');
                print(response);
                print(e);
                return false;
            }

        };
        this.api.updateAttrs( this.state.qid, "remove", this.state.attrs[parseInt(index)], callback);
    },
    // choices
    addChoice: function (e, rid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.addChoice() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                self.replaceState(body);
            }catch(e){
                print(":: question.addChoice fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        var question = JSON.parse(JSON.stringify(this.state));
        if ( "choices" in question != true ) question.choices = [];

        question.choices.push("");
        this.api.updateBody( this.state.qid, question, callback);
    },
    updateChoice: function (e, rid){
        var self = this;
        var question = JSON.parse(JSON.stringify(this.state));

        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var index = parseInt($(obj).data("index"));
        var card = $(obj).find(".paper_sel_letter").text();
        var choice = $(obj).find(".paper_edit_div").html();

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.updateChoice() fail.');
                print(response);
                return false;
            }
            try{
                var body = JSON.parse(response.body);
                self.replaceState(body);
            }catch(e){
                print(":: question.updateChoice fail. json decode fail.");
                print(e);
                print(response);
            }
        };

        // Diff
        if ( this.state.choices[index] == choice ) {
            print(":: 没有需要更新的数据");
            return false;
        } else {
            question.choices[index] = choice;
            this.api.updateBody( this.state.qid, question, callback);
        }
        
    },
    removeChoice: function (index, e, rid){
        var self = this;
        // var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        // var i = $(obj).parent().data("index");
        // var key = $(obj).find(".attr").text();
        // var value = $(obj).find(".pape_attr_edit").html();
        var callback = function(response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function Question.removeChoice fail.');
                print(response);
                return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                print(':: function Question.removeChoice fail.');
                print(response);
                print(e);
                return false;
            }
        };
        var question = JSON.parse(JSON.stringify(this.state));
        question.choices = [];
        this.state.choices.map(function (choice, cindex){
            if ( parseInt(cindex) != parseInt(index) ){
                question.choices.push(choice);
            }
        });
        this.api.updateBody( this.state.qid, question, callback);
    },
    // body update
    setContent: function (e, rid){
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var question = JSON.parse(JSON.stringify(this.state));
        // delete question['attrs'];
        question.content = $(obj).html();

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.setContent() fail.');
                print(response);
                return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                print(":: question.setContent fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        this.api.updateBody( this.state.qid, question, callback);
    },
    setAnswer: function (e, rid){
        // 设置 正确答案 或者 主观题 答案
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid)); // paper_sel_letter
        var ans = $(obj).find(".paper_sel_letter").text();
        var question = JSON.parse(JSON.stringify(this.state));
        var tans = question.ans;
        if ( !tans ) tans = "";

        if ( tans.indexOf(ans) == -1 ){
            tans += ans;
        } else {
            tans = tans.replace(ans, "");
        }
        question.ans = tans;

        question.ansnum = question.ans.length;

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(':: function question.setAnswer fail.');
                print(response);
                return false;
            }
            try{
                self.pull();
            }catch(e){
                print(":: question.setAnswer fail. json decode fail.");
                print(e);
                print(response);
            }
        };
        this.api.updateBody( this.state.qid, question, callback);
    },
    setTixu: function (tixu){
        this.tixu = parseInt(tixu);
        return true;
    },
    setIndex: function (index){
        this.index = parseInt(index);
        return true;
    },

    // 拖动事件 Drag Event
    handleDragStart: function (e, rid){
        // 拖放开始
        if ( $(e.target).hasClass('element') ){
            var tixu = $(e.target).find(".tixu").text();
            print("question: 正在拖动元素 " + tixu);
            window._dragevent = {"src": tixu, "dest": null};
        }
    },
    handleDragEnter: function (e, rid){
        // 拖动进入的新 DOM 节点
        if ( $(e.target).hasClass('element') ){
            // 正确的位置
            var tixu = $(e.target).find(".tixu").text();
            print("question: 拖动元素至 " + tixu);
            window._dragevent.dest = tixu;
        }
    },
    handleDragEnd: function (e, rid){
        // 处理拖动效果
        var self = this;
        var api = {"question": this.api, "composite": new apis.composites() };
        if ( window._dragevent.dest == null || window._dragevent.src == null ) {
            print("question: 指令无法执行");
            return false;
        }
        if ( $(e.target).hasClass('element') ){
            // DEBUG.
            print(window._dragevent);
            var dest = window._dragevent.dest.split("-");
            var src = window._dragevent.src.split("-");

            var src_element = null;
            var dest_element = null;
            var dest_dom = $( 'dt[data-tixu="#tixu#"]'.replace("#tixu#", dest.join("-") ) );
            var elements = self.props.paper.state.struct; // paper struct

            if ( dest.length < 1 ) throw new Error("drag fail.");

            if ( !this.props.parent ) {
                // paper struct
                if ( dest_dom.data("cqid") != undefined ){
                    dest_element = self.props.paper.elements["cqid"+$(dest_dom).data("cqid")];
                } else {
                    dest_element = self.props.paper.elements["qid"+$(dest_dom).data("qid")];
                }
                if ( !dest_element.props.parent ){
                    // paper
                    if ( "cqid" in dest_element.state ){
                        // 
                        self.props.paper.updateStruct("remove", {"qid": self.state.qid} );
                        dest_element.updateStruct( "append", {"qid": self.state.qid });
                    } else if ( "qid" in dest_element.state ) {
                        self.props.paper.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(0, 1)) });
                    }
                } else {
                    // composite
                    if ( "cqid" in dest_element.state ){
                        // 
                        self.props.paper.updateStruct("remove", {"qid": self.state.qid} );
                        dest_element.updateStruct( "append", {"qid": self.state.qid });
                    } else if ( "qid" in dest_element.state ) {
                        dest_element.props.parent.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(-1)) });
                    }
                }
            } else {
                // composite struct
                if ( dest_dom.data("cqid") != undefined ){
                    dest_element = self.props.paper.elements["cqid"+$(dest_dom).data("cqid")];
                } else {
                    dest_element = self.props.paper.elements["qid"+$(dest_dom).data("qid")];
                }
                if ( !dest_element.props.parent ){
                    // paper
                    if ( "cqid" in dest_element.state ){
                        self.props.parent.updateStruct("remove", {"qid": self.state.qid} );
                        dest_element.updateStruct( "append", {"qid": self.state.qid });
                    } else if ( "qid" in dest_element.state ) {
                        self.props.paper.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(0, 1)) });
                    }
                } else {
                    // composite
                    if ( "cqid" in dest_element.state ){
                        // 
                        self.props.parent.updateStruct("remove", {"qid": self.state.qid} );
                        dest_element.updateStruct( "append", {"qid": self.state.qid });
                    } else if ( "qid" in dest_element.state ) {
                        dest_element.props.parent.updateStruct( "append", {"qid": self.state.qid, "index": parseInt(dest.slice(-1)) });
                    }
                }
            }

        }
    },
    
    render: function (){
        var self = this;
        var computer_choice_className = function (choice_card){
            if ( self.state.ans.indexOf(choice_card) != -1) {
                return "paper_choice on";
            } else {
                return "paper_choice";
            }
        };
        //                     （<span className="flag">{this.name}</span>）
        return (
            React.createElement("dt", {className: "element", 
                 "data-tixu": this.tixu, 
                 "data-qid": this.state.qid, 
                 draggable: "true", 
                 onDragStart: this.handleDragStart, 
                 onDragEnter: this.handleDragEnter, 
                 onDragEnd: this.handleDragEnd}, 
                React.createElement("div", {className: "paper_item_slide clearfix"}, 
                    React.createElement("div", {className: "paper_drag_item none"}), 
                    React.createElement("div", {className: "paper_item_slide_tit", title: "题序"}, 
                        React.createElement("span", {className: "tixu", contentEditable: "true"}, this.tixu)
                    ), 
                    React.createElement("div", {className: "paper_item_score"}, 
                        React.createElement("span", {className: "score", contentEditable: "true"}, "0")
                    ), 
                    React.createElement("div", {className: "paper_item_title", contentEditable: "true", onBlur: this.setContent}, this.state.content), 
                    React.createElement("div", {className: "paper_item_del", onClick: this.remove}), 
                    React.createElement("div", {className: "paper_item_toggle paper_item_hide none"})
                ), 

                React.createElement("div", {className: "paper_item_substance"}, 
                    React.createElement("div", {className: "paper_item_choice"}, 
                         this.state.choices.map(function(choice, index){
                            return (
                                React.createElement("div", {className:  computer_choice_className(chr(65+index)), 
                                        key: "qid"+self.state.qid + "_choice_"+index, 
                                        onBlur: self.updateChoice, 
                                        onClick: self.setAnswer, 
                                        "data-index": index

                                        }, 
                                    React.createElement("div", {className: "paper_sel_letter"}, chr(65+index)), 
                                    React.createElement("div", {className: "paper_edit_div fl", contentEditable: "true"}, choice), 
                                    React.createElement("div", {className: "paper_choice_del fr none", onClick: self.removeChoice.bind(self, index)}), 
                                    React.createElement("div", {className: "clearfix"})
                                )
                            );
                        }) 
                    ), 
                    React.createElement("div", {className: "paper_choice_add"}, 
                        React.createElement("a", {onClick: this.addChoice, className: "new_choose"}, "新增一个选项"), 
                        React.createElement("a", {onClick: this.setAnswer, className: "new_answer"}, "新增一个答案")
                    ), 

                    React.createElement("div", {className: "paper_item_attr"}, 
                        React.createElement("div", {className: "paper_attr_sel"}, 
                            React.createElement("span", {className: "attr"}, "知识点"), 
                            React.createElement("a", {href: "javascript:;"}, "点击选择知识点")
                        ), 
                         this.state.attrs.map(function(attr, index){
                            return (
                                React.createElement("div", {className: "pape_attr", key: "qid"+self.state.qid + "_attrs_"+index, onBlur: self.updateAttr, "data-index": index}, 
                                    React.createElement("span", {className: "attr", contentEditable: "true"}, attr.name), 
                                    React.createElement("div", {className: "pape_attr_edit", contentEditable: "true"}, attr.value), 
                                    React.createElement("span", {className: "paper_attr_del fr none", onClick: self.removeAttr.bind(self, index)}), 
                                    React.createElement("div", {className: "clearfix"})
                                )
                            );
                        }) 
                    ), 
                    React.createElement("div", {className: "paper_attr_add"}, 
                        React.createElement("a", {onClick: this.addAttr}, "新增一个标签")
                    )
                )
            )
            );
    }
});
