
if ( !window.components ) window.components = {};
if ( !window.components.share ) window.components.share = {};

window.components.share.headerTop = React.createClass({
    displayName: "components.share.headerTop",
    name: "顶栏公共菜单",
    getInitialState: function (){
      
        return {
            "title": "事业单位题库",
            "user": {
                "name": window.auth.name,
                "token": "",
                "appkey": "",
                "appid": "",
                "openid": "",
                "exp": 78 // 练习时长
            },
            "inbox": {
                "news": 1
            },
            "hover": false,
            "unfinished": 0,
			"isShowWeixin":{display:"none"},
		  	"isShowWeibo":{display:"none"},
		  	"isShowMobileQrcode":{display:"none"}
        };
    },
    fetch_message: function (){
        // 消息中心 未读消息
    },
    componentWillReceiveProps: function (){

    },
    componentDidMount: function (){
        // 初始化效果
        var self = this;
        // 个人资料 下拉
       $('.ti_user_list').hover(function () {
            $(this).find('.dl_down').show();
            $(this).find('.jiantou').attr('src','static/images/ti_head_03.png');
        },function () {
            $(this).find('.jiantou').attr('src','static/images/ti_head_003.png');
            $(this).find('.dl_down').hide();    
        })

        // 消息中心展开
        $(".m_f4").bind('click', function() {
            var $i_inform = $(this).prev().prev().find(".i_inform");
            var $i_title = $(this).prev().prev().find(".i_title");
            var $img = $(this).prev().prev().prev().find("img");
            if ($i_inform.is(":hidden")) {
                $(this).html("收起").addClass("m_f44");
                $i_title.css("font-weight", "normal");
                $img.attr("src", "static/images/mess_01.png");
                $i_inform.slideDown();
            } else {
                $(this).html("展开").removeClass("m_f44");
                $i_inform.slideUp();
            }
        });
    },
    logout: function (){
       
       
      
        var url ="/cgi-bin/auth.php";
        var body = {
             "hook":"auth.signout"
        }
        var callback = function(response){
            if(response.type == "progress") return false;
            if(response.code != 200) {setalert("系统出错");}
            window.location.assign("#signin");
        }
        requests.put(url,body,{'async':true,"callback":callback});

        
      
    },
    mouseOver: function () {
        // 下拉框
        this.setState({"hover": true});
    },
    mouseOut: function () {
        // 下拉框
        var self = this;
        var callback = function (){
            self.setState({"hover": false});
        };
        setTimeout(callback, 650);
    },
	
	mouseOnWeixin:function(event){
		   var that=this;
		   that.setState({isShowWeixin:{display:"block"}});
		   event.target.onmouseout=function(){
					 that.setState({isShowWeixin:{display:"none"}});
		   }
	
	 },
	 mouseOnWeibo:function(event){
			var that=this;
			this.setState({isShowWeibo:{display:"block"}});
			 
	 },
	 clickWeixin:function(event){
		   event.preventDefault();
	 },
	 mouseOutWeibo:function(){
		   this.setState({isShowWeibo:{display:"none"}})
	 },
	 mouseOnWeiboBox:function(){ 
		 this.setState({isShowWeibo:{display:"block"}})
	 },
	 weiboGuanZhu:function(){
		  window.open("http://weibo.com/u/5610893146");
	 },
	 mouseOnMobile:function(event){
	   var  that=this;
		   that.setState({isShowMobileQrcode:{display:"block"}});
			event.target.onmouseout=function(){
					 that.setState({isShowMobileQrcode:{display:"none"}});
		   }
		  
	 },
     goTestArea:function(){
           var self = this; 
           if(self.props.root === undefined){
                      self.props.setting.setState({"component":window.components.user.settings.setTestArea});
           }else{
                       var props = {
                                  "component":window.components.user.settings.setTestArea,
                                  "root" :self.props.root,
                                  "key":"setTestArea"
                       }
                    var content = React.createElement(window.components.user.settings.setting, props)
                        self.props.root.setState({"content":content,"showTop":false});
          }

          
        
                      

           // window.location.assign("#setting_test_area");

         
           
     },
     goQuestion:function(){
          var self = this;
          if(self.props.root === undefined){
                  self.props.setting.setState({"component":window.components.user.settings.setQuestionAreaAndYear});
          }else{
                   var props = {
                              "component":window.components.user.settings.setQuestionAreaAndYear,
                              "root" :self.props.root,
                              "key":"setQuestionAreaYear"
                   }
                   var content = React.createElement(window.components.user.settings.setting, props)

                   self.props.root.setState({"content":content,"showTop":false});

       }
          


     },

    render: function (){
        var self = this;
        var __header_tab_class = "none";
        if ( this.state.hover == true ) __header_tab_class = "";

        return (
            React.createElement("div", {className: "ti_header_top"}, 
                React.createElement("div", {className: "header_Top"}, 
                   
                    React.createElement("div", {className: "top_right fr"}, 
                        React.createElement("ul", {className: "clearfix"}, 
                            React.createElement("li", {className: "ti_user_list"}, 
    
                                React.createElement("div", {className: "ti_user_txt", title: this.state.user.name}, 
                                    React.createElement("img", {className: "img_01", src: "static/images/ti_head_02.png"}), 
                                    this.state.user.name, 
                                    React.createElement("img", {className: "img_02", src: "static/images/ti_head_03.png"})
                                ), 

                                React.createElement("dl", {className: "dl_down none"}, 
                                  
                                    React.createElement("dt", null, React.createElement("a", {onClick: self.goTestArea}, "考区设置")), 
                                    React.createElement("dt", null, React.createElement("a", {onClick: self.goQuestion}, "出题范围")), 
                                    React.createElement("dt", {className: "ti_exit"}, 
                                        React.createElement("a", {onClick: self.logout}, "退出")
                                    )
                                )
                            ), 

                            React.createElement("li", {className: "ti_weixin"}, 
							  React.createElement("a", {href: "", onMouseOver: this.mouseOnWeixin, onClick: this.clickWeixin}, "微信"), 
							  React.createElement("div", {className: "wx", style: this.state.isShowWeixin}, 
							  React.createElement("div", {className: "left"}, React.createElement("img", {src: "static/images/new_wxma.png"})), 
							  React.createElement("div", {className: "right"}, React.createElement("p", null, "麦学习 扫一扫"), React.createElement("p", null, "微信号:wodezuoye"))
							  )
							 

							), 
							React.createElement("li", {className: "ti_weibo", onMouseOver: this.mouseOnWeibo, onMouseOut: this.mouseOutWeibo}, 
							  React.createElement("a", {href: "http://weibo.com/u/5610893146", target: "_blank"}, "微博"), 
								React.createElement("div", {className: "wb", onMouseOver: this.mouseOnWeiboBox, style: this.state.isShowWeibo}, 
									React.createElement("div", {className: "left"}, React.createElement("img", {src: "static/images/new_index_mtk.png"})), 
									React.createElement("div", {className: "right"}, React.createElement("p", null, "麦学习作业"), React.createElement("p", null, React.createElement("img", {src: "static/images/new_guanzhu.png", onClick: this.weiboGuanZhu})))
							  )
							)
                        )
                    )
                )



            )
        );
    }
});



window.components.share.header = React.createClass({
    displayName: "components.share.header",
    name: "公共头以及菜单",
    getInitialState: function (){
        // if ( "auth" in window != true || "name" in window.auth != true ) {
        //     window.location.assign("#signin");
        //     // return null;
        // }
        // if ( !window.auth ) window.auth = {};
        // if ( !window.auth.name  ) window.auth.name = "测试用户";
        return {
            "title": "事业单位题库",
            "user": {
                "name": window.auth.name,
                "token": "",
                "appkey": "",
                "appid": "",
                "openid": "",
                "exp": 78 // 练习时长
            },
            "inbox": {
                "news": 1
            },
            "hover": false,
            "unfinished": 0
        };
    },
    pull: function (){
        
    },
    componentWillReceiveProps: function (){
        if ( this.isMounted() == true ) {
            this.fetch_unfinished();
        }
    },
    componentDidMount: function (){
        // 初始化效果
        var self = this;
        // this.fetch_unfinished();

        // 个人资料 下拉
       $('.ti_user_list').hover(function () {
            $(this).find('.dl_down').show();
            $(this).find('.jiantou').attr('src','static/images/ti_head_03.png');
        },function () {
            $(this).find('.jiantou').attr('src','static/images/ti_head_003.png');
            $(this).find('.dl_down').hide();    
        })

        //滑过背景色渐入渐出
        $(".con_test").hover(function() {
            $(this).stop().animate({
                backgroundColor: "#F3F3F3"
            }, 300);
        }, function() {
            $(this).stop().animate({
                backgroundColor: "#fff"
            }, 300);
        });

        // 消息中心展开
        $(".m_f4").bind('click', function() {
            var $i_inform = $(this).prev().prev().find(".i_inform");
            var $i_title = $(this).prev().prev().find(".i_title");
            var $img = $(this).prev().prev().prev().find("img");
            if ($i_inform.is(":hidden")) {
                $(this).html("收起").addClass("m_f44");
                $i_title.css("font-weight", "normal");
                $img.attr("src", "static/images/mess_01.png");
                $i_inform.slideDown();
            } else {
                $(this).html("展开").removeClass("m_f44");
                $i_inform.slideUp();
            }
        });

        /* 全选复选框单击事件*/
        $("#cheAll").bind('click', function() {
            if (this.checked) { //如果自己被选中
                $(".m_list .m_f1 input[type=checkbox]").attr("checked", true);
            } else {
                $(".m_list .m_f1 input[type=checkbox]").attr("checked", false);
            }
        });

        // 修改密码获得焦点和失去焦点
        $(".modify_inp input").bind('focusin focusout', function(event) {
            if (event.type == "focusin") {
                $(this).parent().addClass("modify_inp_01");
            } else if (event.type == "focusout") {
                $(this).parent().removeClass("modify_inp_01");
            }
        });

        //绑定密码
        $(".common_bind").bind('click', function() {
            if ($(this).hasClass("bind_icon")) {
                $(this).addClass("bind_icon_01").html("绑定手机");
            } else {
                $(this).removeClass("bind_icon_01").html("解除绑定");
            }
        });

    },
    fetch_unfinished: function (){
        // 获取未完成的练习记录
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "records" in body != true ) body.records = [];
                if ( "total" in body != true ) body.total = 0;
            }catch(e){
                console.warn(e); return false;
            }
            self.setState( {"unfinished": body.total } );
            // self.setState( {"total": body.total+self.state.total} );
        };
        var size = 5;     // this.size
        var page = 0;   // parseInt(this.page)-1
        var api = new apis.user.records.tests();
        api.unfinished( {"size": size, "page": page }, callback );
    },
    goindex: function (){
        var self = this;
        $('.heade_nav .nav_select').removeClass("nav_select").addClass("nav_default");
        $($('.heade_nav .nav_default')[0]).removeClass("nav_default").addClass("nav_select");

        this.props.root.setState({"content": React.createElement(window.components.index.boxs, {root: self.props.root})});
    },
    goreport: function (){
        var self = this;
        $('.heade_nav .nav_select').removeClass("nav_select").addClass("nav_default");
        $($('.heade_nav .nav_default')[1]).removeClass("nav_default").addClass("nav_select");
        this.props.root.setState({"content": React.createElement(window.components.report.report, {root: self.props.root})});
    },
    gohistory: function (){
        var self = this;
        $('.heade_nav .nav_select').removeClass("nav_select").addClass("nav_default");
        $($('.heade_nav .nav_default')[2]).removeClass("nav_default").addClass("nav_select");
        this.props.root.setState({
                "content": React.createElement(window.components.user.history.view, {
                                        root: self.props.root, 
                                        root0: self.props.root, 
                                        tab: "records", 
                                        indexbox: self.props.indexbox})
        });
    },
    
    mouseOver: function () {
        // 下拉框
        this.setState({"hover": true});
    },
    mouseOut: function () {
        // 下拉框
        var self = this;
        var callback = function (){
            self.setState({"hover": false});
        };
        setTimeout(callback, 650);
    },
    render: function (){
        var self = this;
        var __header_tab_class = "none";
        if ( this.state.hover == true ) __header_tab_class = "";

        return (
            React.createElement("div", null, 
                React.createElement(window.components.share.headerTop, {root: this.props.root}), 
                
                React.createElement("div", {className: "ti_header"}, 
                    React.createElement("div", {className: "header_type"}, 
                        React.createElement("div", {className: "header_type_list"}, 

                            React.createElement("div", {className: "logo", style: {"position":"relative"}}, 
                                React.createElement("img", {src: "static/images/mtk_logo.png", style: {"position":"relative","top":"-5px"}}), 
                                React.createElement("img", {src: "static/images/beta_03.png", style: {"position":"absolute","top":"10px","right":"-30px"}}), 
                                React.createElement("span", null, "  ·  事业单位")
                            ), 

                            React.createElement("ul", {className: "sild_list clearfix"}, 
                                React.createElement("li", {className: "li_act"}, React.createElement("a", {href: "javascript:;"}, "公共基础")), 
                                React.createElement("li", null, React.createElement("a", {href: "javascript:;"}, "综合写作")), 
                                React.createElement("li", {className: "sild_test"}, React.createElement("a", {href: "javascript:;"}, "能力测试"))



                            )
                        )
                    ), 
                    React.createElement("div", {className: "header_menu"}, 
                        React.createElement("ul", {className: "heade_nav clearfix"}, 
                            React.createElement("li", {className: "nav_select", onClick: this.goindex}, "练习中心"), 
                            React.createElement("li", {className: "nav_default", onClick: this.goreport}, "能力评估报告"), 
                            React.createElement("li", {className: "nav_default", "data-yellow": "nav_practise", onClick: this.gohistory}, 
                                "我的练习", 
                                 range(this.state.unfinished).map(function (index){
                                    if ( index == 0 ) {
                                        return (
                                            [(React.createElement("img", {src: "static/images/item_clock.png"})),
                                (React.createElement("span", null, self.state.unfinished))]
                                        );
                                    } else {
                                        return null;
                                    }
                                }) 
                                
                            )
                        )
                    )
                )
            )
        );
    }
});
