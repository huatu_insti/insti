if ( !window.components ) window.components = {};
if ( !window.components.catalogs ) window.components.catalogs = {};
if ( !window.components.catalogs.papers ) window.components.catalogs.papers = {};

window.components.catalogs.papers.list =  React.createClass({
    displayName: "PaperCatalogs",
    name: "试卷标签列表",
    page: 1,
    area: -9,
    size: 20,
    api: new apis.catalogs.papers(),
    getInitialState: function (){
        this.fetch();
        return { "list": [], "num": 0 };
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs fetch fail.");
                console.warn(response);
                return;
            }
            try{
               
                var body = JSON.parse(response.body); 
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
                self.replaceState(body);
            }catch(e){
                console.warn(e);
            }
        };
        /*
            list: [
                0: {name: "二级", hookid: 0, updtime: 1426655982, inittime: 1426655982, tombstone: 0, cid: 3},
                1: {name: "Food", hookid: 0, updtime: 1423122459, inittime: 1423122459, tombstone: 0, cid: 1},
            ],
            num: 5
        */
        this.api.root('0', callback);
    },
    addTag: function (cid, hookid, name){
        var self = this;
        // if ( !name ) {
        //     // 交互
        //     var name = prompt();
        // }
        // var callback = function (response){
        //     if ( response.type == 'progress' ) return 'loading ...';
        //     if ( response.code != 200 ) {
        //         console.warn(":: paper.catalogs.add fail.");
        //         console.warn(response);
        //     }
        //     self.fetch();
        // };
        // if ( cid == '0' && hookid == '0' ) this.api.push({"name": name, "hookid": "0" }, callback );
        // else this.api.push({"name": name, "hookid": cid.toString() }, callback );

         var dialog = $(
            '<div class="lable_mark">'
              
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>父级标签&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", "顶级标签")
               
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs.add fail.");
                console.warn(response);
            }
            self.fetch();
            $(dialog).dialog("close");
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
             if($.trim(cname) == "") {  alert("标签名称不能为空");return;}
             if ( cid == '0' && hookid == '0' ) self.api.push({"name": cname, "hookid": "0" }, callback );
             else self.api.push({"name": cname, "hookid": cid.toString() }, callback );
           
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "添加试卷一级标签",
            "width": 420,
            "height": 300
        }).find(".lable_footer").css({"background":"transparent"})

    },
    updateTag: function (cid, hookid, name){
        if ( !name ) {
            // 交互
            var name = prompt();
        }
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs.update fail.");
                console.warn(response); return ;
            }
            self.fetch();
        };
        this.api.update(cid, {"name": name, "hookid": hookid }, callback );
    },
    removeTag: function (cid){
        var self = this; alert(323); return;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: paper.catalogs.remove fail.");
                console.warn(response); return ;
            }
            self.fetch();
        };
       
        
    },
   
     render:function(){
        var self = this;
         return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "试卷标签管理")
                ), 
                React.createElement("div", {className: "Tagtree"}, 
                    React.createElement("div", {className: "Tagtree_thead"}, 
                        React.createElement("div", {className: "Tagtree_row clearfix"}, 
                            React.createElement("ul", {className: "catalogs"}, 
                                React.createElement("li", null, 
                                    React.createElement("div", {className: "clearfix"}, 
                                        React.createElement("div", {className: "Tagtree_col1"}, "试卷标签名称"), 
                                        React.createElement("div", {className: "Tagtree_col2"}, "试卷数量"), 
                                        React.createElement("div", {className: "Tagtree_col3"}, "试卷排序"), 
                                        React.createElement("div", {className: "Tagtree_col4"}, "操作")
                                    )
                                )
                            )
                        )

                    ), 

                    React.createElement("div", {className: "Tagtree_tbody"}, 
                       React.createElement("ul", {className: "catalogs"}, 
                            this.state.list.map(function(c, i){
                                return (
                                    React.createElement(window.components.catalogs.papers.list_row, {key: "CATALOG-"+c.cid, catalog: c, list: self, index: [i]})
                                );
                            }), 
                           React.createElement("li", {className: "Tagtree_add"}, 
                                React.createElement("a", {className: "add_lable lable_icon", onClick: this.addTag.bind(this, '0' ,'顶级标签', null)}, "添加一级标签")
                            )
                       )
                    )

                )
            )
        );
    }
     
});


window.components.catalogs.papers.list_row = React.createClass({
    displayName: "ExamsCatalogs-Row",
    name: "试卷标签列表 - 一条记录",
    api: new apis.catalogs.papers(),
    isOpen: false,
    isSync: false,
    children_copy: [],
    getInitialState: function (){
        // {"name": "test", "hookid":0,"updtime":1429070010,"inittime":1429070010,"tombstone":0,"cid":this.props.cid}
        var c = this.props.catalog;
        c.children = [];
        return c;
    },
    __fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn( response ); return;
            }
            var children = JSON.parse(response.body);
            if ( "list" in children != true ) children.list = [];
            self.isSync = true;
            self.isOpen = true;
            self.setState({"children": children});
        };
        this.api.root(this.props.catalog.cid, callback);
    },
    fetch: function (){
        var self = this;
        if ( this.isSync == true ){
            if ( this.isOpen == true ) {
                self.isOpen = false;
                self.children_copy = self.state.children;
                self.setState({"children": []});
                self.forceUpdate();
                return ":: use cache ...";
            } else if ( this.isOpen == false ) {
                self.isOpen = true;
                self.setState({"children": self.children_copy });
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        self.__fetch();
    },
    add: function (){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
                 + '<div class="lable_head">'
                    + '<span>添加试卷标签</span><img  src="static/images/add_test_close.png"/>'
                 + '</div>'
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>父级标签&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", self.state.name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
            self.__fetch();
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            self.api.push({"name": cname, "hookid": parseInt(self.state.cid) }, callback );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "添加试卷标签",
            "width": 420,
            "height": 300
        });
    },
    update: function (){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
                 + '<div class="lable_head">'
                    + '<span>更新试卷标签</span><img  src="static/images/add_test_close.png"/>'
                 + '</div>'
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>当前标签名称&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", self.state.name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
            // self.__fetch();
            self.props.list.fetch();
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            self.api.update(self.state.cid, {"name": cname, "hookid": parseInt(self.state.hookid) }, callback );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "更新试卷标签",
            "width": 420,
            "height": 300
        });
    },
    remove: function (){
        var self = this; console.log(self.props);
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {  console.log(response);
                console.warn(response);
                return;
            }
            self.props.list.fetch();

           
        };
     
         $("<div class=\"confirm_exam_del\">"
            +"<div class=\"message\">确定删除此标签吗?</div>"
            +"<div class=\"add_btn\">"
                +" <a class=\"add_canal\">取消<\/a>"
                +" <a class=\"add_new\">确定<\/a>"
            +"</div></div>").dialog({
               title:"删除标签",
               close:true,
               modal:true,
               width:"400px"
        })
        $(".confirm_exam_del").find(".add_canal").on("click",function(){ $(".confirm_exam_del").dialog("close")})
                              .end().find(".add_new").on("click",function(){ 
                                     self.api.remove(self.state.cid, callback);$(".confirm_exam_del").dialog("close");})
    },
    render: function (){
        var self = this;
        if ( "children" in this.state != true ) this.setState({"children": []});

        var classname_of_col1;
        if ( self.isOpen == true ) classname_of_col1 = "-24px";
        else if ( self.isOpen == false ) classname_of_col1 = "10px";

        return (
            React.createElement("li", {className: "catalog"}, 
                React.createElement("div", {className: "Tagtree_row clearfix"}, 
                    React.createElement("div", {
                            className: "Tagtree_col1", 
                            style:  {
                                    "text-indent": self.props.index.length*50 - (self.props.index.length-1)*25, 
                                    "background": "url(static/images/lable_list.png) no-repeat " + (self.props.index.length*25).toString()+"px " + classname_of_col1,
                                    "cursor": "pointer"
                                    }, 
                            
                            onClick: this.fetch}, 
                        this.state.name
                    ), 
                    React.createElement("div", {className: "Tagtree_col2"}, "欠缺资料"), 
                    React.createElement("div", {className: "Tagtree_col3"}, "未知资料"), 
                    React.createElement("div", {className: "Tagtree_col4"}, 
                        React.createElement("a", {className: "a_oper", onClick: this.add}, "添加子标签"), " |",  
                        React.createElement("a", {className: "a_oper", onClick: this.update}, "编辑标签"), " |",  
                        React.createElement("a", {className: "a_oper", onClick: this.remove}, "删除标签"), " |" 
                    )
                ), 
                React.createElement("ul", {className: "catalogs"}, 
                     this.state.children.map(function(c, i){
                            return (
                                React.createElement(window.components.catalogs.papers.list_row, {key: "CATALOG-"+c.cid, catalog: c, list: self, index: self.props.index.concat(i)})
                            );
                    }) 
                )
            )
        );
    }
});