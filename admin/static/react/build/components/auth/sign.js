if ( !window.components ) window.components = {};
if ( !window.components.auth ) window.components.auth = {};

window.components.auth.sign = React.createClass({
    displayName: "components.auth.sign",
    name: "签名",
    // api: new apis.papers(),
    getInitialState: function (){
        window.SIGN = this;
        var self = this;
        // props: action
        //                  *   signin
        //                  *   signup
        //                  *   signout
        //                  *   resetpwd
        //                  *   findpwd
        return { "component": null };
    },
    componentWillReceiveProps: function (){
        var self = this;
    },
    componentDidMount: function (){
        var self = this;
    },
    componentWillUpdate: function (){
        var self = this;

    },
    render: function (){
        var self = this;

        var action = "signin";
        if ( this.props.action == "signin" 
                || this.props.action == "signup" 
                || this.props.action == "signout" 
                || this.props.action == "resetpwd" 
                || this.props.action == "findpwd" 
        ) action = this.props.action;

        var component = null;
        if ( action == "signin" ) {
            // self.setState({"action": <window.components.auth.signin parent={self} /> });
            component = React.createElement(window.components.auth.signin, {parent: self});
        }
        if ( action == "signup" ) {
            // self.setState({"action": <window.components.auth.signup parent={self} /> });
            component = React.createElement(window.components.auth.signup, {parent: self});
        }
        if ( action == "signout" ) {
            // self.setState({"action": <window.components.auth.signout parent={self} /> });
            component = React.createElement(window.components.auth.signout, {parent: self});
        }
        if ( action == "resetpwd" ) {
            // self.setState({"action": <window.components.auth.resetpwd parent={self} /> });
            component = React.createElement(window.components.auth.resetpwd, {parent: self});
        }
        if ( action == "findpwd" ) {
            // self.setState({"action": <window.components.auth.findpwd parent={self} /> });
            component = React.createElement(window.components.auth.findpwd, {parent: self});
        }


        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "log_head"}, 
                    React.createElement("div", {className: "log_img"}, 
                        React.createElement("img", {src: "static/images/login_logo.png"})
                    )
                ), 

                React.createElement("div", {className: "log_content"}, 
                    React.createElement("div", {className: "log_con"}, 
                         component, 
                        React.createElement("div", {className: "log_right"}, 
                            React.createElement("img", {src: "static/images/login_03.png"})
                        )
                    )
                )
            )
        );
    }
});


window.components.auth.signin = React.createClass({
    displayName: "components.auth.signin",
    name: "登录",
    // api: new apis.papers(),
    getInitialState: function (){
        // props: parent / 
        return { "account": "", "password": "", "remember": true };
    },
    componentDidMount: function (){
        var self = this;
    },
    updateAccount: function (e, rid){
        this.setState( {"account": $(e.target).val() } );
    },
    updatePassword: function (e, rid){
        this.setState( {"password": $(e.target).val() } );
    },
    updateRemberme: function (e, rid){
        this.setState( {"remember": !this.state.remember } );
    },
    onSignin: function () {
        var self = this;
        console.info(":: on submit form data .... ");
        console.log(":: form data: " + JSON.stringify(this.state) );
        var url = "/cgi-bin/auth.php";
        var body = {"hook": "auth.signin", "data": {"account": this.state.account, "password": this.state.password} };
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert("登录失败 ...");
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                window.location.assign("#index");
            }catch(e){
                console.warn(e); return false;
            }
        };
        requests.put(url, body, {"async": true, "callback": callback});
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "log_left"}, 
                React.createElement("div", {className: "login_title"}, "登录砖题库"), 
                React.createElement("div", {className: "form_login"}, 
                    React.createElement("div", {className: "inp inp_account"}, 
                        React.createElement("input", {
                                type: "text", 
                                className: "account", 
                                placeholder: "请输入题库账号/教育网账号/网校账号", 
                                value: self.state.account, 
                                onChange: self.updateAccount}), 
                         React.createElement("div", {className: "errorbg none"}, "请填写账号")
                    ), 
                    React.createElement("div", {className: "inp inp_pass"}, 
                        React.createElement("input", {
                                type: "password", 
                                className: "password", 
                                placeholder: "密码", 
                                value: self.state.password, 
                                onChange: self.updatePassword}), 
                         React.createElement("div", {className: "errorbg none"}, "请填写密码")
                    ), 
                    React.createElement("div", {className: "Cinput"}, 
                        React.createElement("input", {
                                title: "同意", 
                                type: "checkbox", 
                                className: "checkInput", 
                                checked: self.state.remember, 
                                onChange: self.updateRemberme}), 
                        React.createElement("div", {className: "C-zidong"}, 
                            React.createElement("label", null, "下次自动登录")
                        ), 
                        React.createElement("div", {className: "C-forget"}, 
                            React.createElement("a", {href: "#findpwd"}, "忘记密码？")
                        )
                    ), 
                   React.createElement("div", {className: "btn_login"}, 
                        React.createElement("button", {
                                type: "button", 
                                className: "submit_button", 
                                onClick: self.onSignin}, 
                            "登录"
                        )
                    ), 
                    React.createElement("div", {className: "Cinput"}, 
                        React.createElement("div", {className: "no_account"}, 
                            "还没有账号？立即", 
                            React.createElement("a", {href: "#signup", className: "regist"}, "注册")
                        )
                    )
                )
            )
        );
    // <!--<a id="login_button" className="btn_re">登录</a>-->
    }
});
window.components.auth.signup = React.createClass({
    displayName: "components.auth.signup",
    name: "注册",
    // api: new apis.papers(),
    getInitialState: function (){
        // props: parent / 
        var tab = "phone";
        if ( this.props.tab == "email" || this.props.tab == "phone" ) tab = this.props.tab;
        return { 
            "tab": tab,
            "phone": {
                "phone": "", 
                "password": "",
                "repassword": "",
                "verify": ""
            },
            "email": {
                "email": "", 
                "password": "",
                "repassword": "",
                "verify": ""
            },
        };
    },
    componentDidMount: function (){
        var self = this;
    },
    switch_tab: function (tab){
        if ( tab == "phone" ) this.setState({"tab": tab});
        else if ( tab == "email" ) this.setState({"tab": tab});
        else console.warn("oops ....");
    },
    // 手机注册 Input 输入框数据更新
    updatePhoneTabDataByPhone: function (e, rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.phone = $(target).val()
        this.setState( {"phone": phoneData} );
    },
    updatePhoneTabDataByPassword: function (e ,rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.password = $(target).val()
        this.setState( {"phone": phoneData} );
    },
    updatePhoneTabDataByRepassword: function (e, rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.repassword = $(target).val()
        this.setState( {"phone": phoneData} );
    },
    updatePhoneTabDataByVerify: function (e, rid){
        var target = $(e.target);
        var phoneData = this.state.phone;
        phoneData.verify = $(target).val()
        this.setState( {"phone": phoneData} );
    },

    // 邮件注册 Input 输入框数据更新
    updateEmailTabDataByEmail: function (e, rid){
        var target = $(e.target);
        var emailData = this.state.email;
        emailData.email = $(target).val()
        this.setState( {"email": emailData} );
    },
    updateEmailTabDataByPassword: function (e, rid){
        var target = $(e.target);
        var emailData = this.state.email;
        emailData.password = $(target).val()
        this.setState( {"email": emailData} );
    },
    updateEmailTabDataByRepassword: function (e, rid){
        var target = $(e.target);
        var emailData = this.state.email;
        emailData.repassword = $(target).val()
        this.setState( {"email": emailData} );
    },
    updateEmailTabDataByVerify: function (e, rid){
        var target = $(e.target);
        var emailData = this.state.email;
        emailData.verify = $(target).val()
        this.setState( {"email": emailData} );
    },
    check_data: function (body){
        // 检查表单数据
        var data = body['data'];
        if ( data.password.length > 16 || data.password.length < 6 ) {
            console.info("密码长度错误。");
            return false;
        }
        if ( data.password != data.repassword ) {
            console.info("两次密码不一致。");
            return false;
        }
        if ( data.account_type == "phone") {
            if ( /^0?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/.test( data.account ) != true ) {
                console.info("手机号码无效。");
                return false;
            }
        } else if ( data.account_type == "email") {
            if ( /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test( data.account ) != true ) {
                console.info("邮件地址格式不正确。");
                return false;
            }
        }
        if ( data.verify.length < 3 ) {
            console.info("验证码长度不能小于3");
            return false;
        }

        return true; 
    },
    onSignup: function (){
        // submit form data.
        var self = this;
        var url = "/cgi-bin/auth.php";
        var data = this.state[this.state.tab];
        var account_type = this.state.tab; // phone / email
        var body = {
                "hook": "auth.signup", 
                "data": {
                    "account_type": account_type,
                    "account": data.phone ? data.phone : data.email, 
                    "password": data.password,
                    "repassword": data.repassword,
                    "verify": data.verify,
                }
        };
        

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                setalert(JSON.parse(response.body).info? JSON.parse(response.body).info : "注册失败 ..." );
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                window.location.assign("#index");
            }catch(e){
                console.warn(e); return false;
            }
        };
        if ( this.check_data(body) == true ) requests.put(url, body, {"async": true, "callback": callback});
    },
    signup_by_email: function (){
        var self = this;
        return (
                React.createElement("div", {className: "form_register"}, 
                    React.createElement("div", {className: "inp inp_email"}, 
                        React.createElement("input", {
                                type: "text", 
                                className: "email", 
                                placeholder: "电子邮箱", 
                                autoComplete: "off", 
                                value: self.state.email.email, 
                                onChange: self.updateEmailTabDataByEmail}), 
                        React.createElement("div", {className: "errorbg none"}, "请填写电子邮箱")
                    ), 
                    React.createElement("div", {className: "inp inp_pass"}, 
                        React.createElement("input", {
                                type: "password", 
                                className: "password", 
                                placeholder: "密码（密码为6-16位的英文、数字、下划线）", 
                                value: self.state.email.password, 
                                onChange: self.updateEmailTabDataByPassword}), 
                        React.createElement("div", {className: "errorbg none"}, "请填写密码")
                    ), 
                     React.createElement("div", {className: "inp inp_pass"}, 
                        React.createElement("input", {
                                type: "password", 
                                className: "repassword", 
                                placeholder: "确认密码", 
                                value: self.state.email.repassword, 
                                onChange: self.updateEmailTabDataByRepassword}), 
                        React.createElement("div", {className: "errorbg none"}, "请填写确认密码")
                    ), 
                    React.createElement("div", {className: "inp_div"}, 
                        React.createElement("div", {className: "inp inp_yanz"}, 
                            React.createElement("input", {
                                    type: "text", 
                                    title: "验证码", 
                                    className: "text_yanz", 
                                    placeholder: "验证码", 
                                    autoComplete: "off", 
                                    value: self.state.email.verify, 
                                    onChange: self.updateEmailTabDataByVerify}), 
                            React.createElement("div", {className: "errorbg none"}, "请填写验证码")
                        ), 
                        React.createElement("div", {className: "right"}, 
                            React.createElement("img", {className: "verify_img", srca: "{:U('User/verify')}"})
                        )
                    ), 
                     React.createElement("div", {className: "btn_register"}, 
                        React.createElement("button", {
                                type: "button", 
                                className: "submit_button register_button", 
                                onClick: self.onSignup}, 
                            "注册"
                        )
                    ), 
                    React.createElement("div", {className: "Cinput"}, 
                        React.createElement("div", {className: "has_account"}, 
                            "已有账号？立即", 
                            React.createElement("a", {href: "#signin", className: "regist"}, "登陆")
                        )
                    )
                 )
        );
    },
    signup_by_phone: function (){
        var self = this;
        return (
                React.createElement("div", {className: "form_register"}, 
                    React.createElement("div", {className: "inp inp_phone"}, 
                        React.createElement("input", {
                                type: "text", 
                                className: "phone", 
                                placeholder: "手机号", 
                                value: self.state.phone.phone, 
                                autoComplete: "off", 
                                onChange: self.updatePhoneTabDataByPhone}), 
                        React.createElement("div", {className: "errorbg none"}, "请输入正确的手机号")
                    ), 
                    React.createElement("div", {className: "inp inp_pass"}, 
                        React.createElement("input", {
                                type: "password", 
                                className: "password", 
                                placeholder: "密码（密码为6-16位的英文、数字、下划线）", 
                                value: self.state.phone.password, 
                                onChange: self.updatePhoneTabDataByPassword}), 
                        React.createElement("div", {className: "errorbg none"}, "请填写密码")
                    ), 
                     React.createElement("div", {className: "inp inp_pass"}, 
                        React.createElement("input", {
                                type: "password", 
                                className: "repassword ", 
                                value: self.state.phone.repassword, 
                                placeholder: "确认密码", 
                                onChange: self.updatePhoneTabDataByRepassword}), 
                        React.createElement("div", {className: "errorbg none"}, "请填写确认密码")
                    ), 
                    React.createElement("div", {className: "inp_div"}, 
                        React.createElement("div", {className: "inp inp_yanz"}, 
                            React.createElement("input", {
                                    type: "text", 
                                    className: "text_yanz", 
                                    placeholder: "验证码", 
                                    autoComplete: "off", 
                                    value: self.state.phone.verify, 
                                    onChange: self.updatePhoneTabDataByVerify}), 
                            React.createElement("div", {className: "errorbg none"}, "请填写验证码")
                        ), 
                        React.createElement("div", {className: "right"}, 
                            React.createElement("img", {className: "verify_img", srca: "{:U('User/verify')}"})
                        )
                    ), 
                    React.createElement("div", {className: "btn_register"}, 
                        React.createElement("button", {
                                type: "button", 
                                className: "submit_button register_button", 
                                onClick: self.onSignup}, 
                            "注册"
                        )
                    ), 
                    React.createElement("div", {className: "Cinput"}, 
                        React.createElement("div", {className: "has_account"}, 
                                "已有账号？立即", 
                                React.createElement("a", {href: "#signin", className: "regist"}, "登陆")
                        )
                    )
                )
        );
    },
    render: function (){
        var self = this;

        var tab = function (){
            // 标签页
            if ( self.state.tab == "phone" ) {
                return (
                    React.createElement("ul", {className: "re_tab"}, 
                        React.createElement("li", {className: "li_p"}, "手机注册"), 
                        React.createElement("li", {onClick: self.switch_tab.bind(self, "email") }, "邮箱注册")
                    )
                );
            } else {
                return (
                    React.createElement("ul", {className: "re_tab"}, 
                        React.createElement("li", {onClick: self.switch_tab.bind(self, "phone") }, "手机注册"), 
                        React.createElement("li", {className: "li_p"}, "邮箱注册")
                    )
                );
            }
        };

        var component = null;
        if ( self.state.tab == "phone" ) component = self.signup_by_phone();
        if ( self.state.tab == "email" ) component = self.signup_by_email();

        return (
            React.createElement("div", {className: "log_left reg_le"}, 
                React.createElement("div", {className: "login_title fr_1"}, "注册砖题库"), 
                tab(), 
                component
            )
        );
    }
});

window.components.auth.signout = React.createClass({
    displayName: "components.auth.signout",
    name: "登出",
    // api: new apis.papers(),
    getInitialState: function (){
        // props: parent / 
        console.log("sign out component ...");
        return {};
    },
    componentDidMount: function (){
        
    },
    render: function (){
        var self = this;

        return (
            React.createElement("div", null)
        );
    }
});

window.components.auth.findpwd = React.createClass({
    // 找回密码和重置密码的区别 在于 
    //      找回密码是在当用户忘记密码的情况下
    //      重置密码是用户已掌握当前密码的情况下
    displayName: "components.auth.findpwd",
    name: "找回密码",
    // api: new apis.papers(),
    getInitialState: function (){
        // props: parent / 
        var tab = "phone";
        if ( this.props.tab ) tab = this.props.tab;
        return { 
            "tab": tab,
            "pdata": {
                "phone": "",
                "verify": ""
            },
            "edata": {
                "email": "",
                "verify": ""
            }
        };
    },
    componentDidMount: function (){
        
    },
    switch_tab: function (tab){
        if ( tab != "phone" && tab != "email" ) return;
        this.setState({"tab": tab});
    },
    // 根据手机号码找回 密码
    updatePhoneByPTab: function (e, rid){
        // 手机号码更新
        var pData = this.state.pdata;
        pData.phone = $(e.target).val()
        this.setState( {"pdata": pData} );
    },
    updateVerifyByPTab: function (e ,rid){
        // 验证码更新
        var pData = this.state.pdata;
        pData.verify = $(e.target).val()
        this.setState( {"pdata": pData} );
    },

    // 根据电子邮箱找回密码
    updateEmailByETab: function (e, rid){
        // 电子邮箱地址更新更新
        var emailData = this.state.edata;
        emailData.email = $(e.target).val()
        this.setState( {"edata": emailData} );
    },
    updateVerifyByETab: function (e ,rid){
        // 验证码更新
        var emailData = this.state.edata;
        emailData.verify = $(e.target).val()
        this.setState( {"edata": emailData} );
    },

    sendVerifyCode: function (){
        // 根据找回方式发送验证码
        console.info(":: send verify code ....");
    },
    findMe: function (){
        // 提交找回密码数据
        console.info("Find Me ....");
        console.log( " The Way:  " + this.state.tab);
        console.log( " The Date:  \n" + JSON.stringify(this.state) );
    },
    phoneTab: function (){
        var self = this;
        return (
            React.createElement("div", {className: "form_register"}, 
                React.createElement("div", {className: "inp inp_phone"}, 
                    React.createElement("input", {
                            type: "text", 
                            className: "phone", 
                            placeholder: "手机号", 
                            autoComplete: "off", 
                            value: self.state.pdata.phone, 
                            onChange: self.updatePhoneByPTab}), 
                    React.createElement("div", {className: "errorbg none"}, "请输入正确的手机号")
                ), 
                React.createElement("div", {className: "inp_div"}, 
                    React.createElement("div", {className: "inp inp_yanz"}, 
                        React.createElement("input", {
                                type: "text", 
                                className: "text_yanz", 
                                placeholder: "验证码", 
                                autoComplete: "off", 
                                value: self.state.pdata.verify, 
                                onChange: self.updateVerifyByPTab}), 
                        React.createElement("div", {className: "errorbg none"}, "请填写验证码")
                    ), 
                    React.createElement("div", {
                            className: "get_num", 
                            onClick: self.sendVerifyCode}, 
                        "请获取验证码"
                    )
                ), 
                React.createElement("div", {className: "btn_register"}, 
                    React.createElement("button", {
                            type: "button", 
                            className: "submit_button", 
                            onClick: self.findMe}, 
                        "确定"
                    )
                ), 
                React.createElement("div", {className: "Cinput"}, 
                    React.createElement("div", {className: "has_account"}, 
                        "已有账号？立即", 
                            React.createElement("a", {href: "#signin", className: "regist"}, "登陆")
                    )
                )
            )
        );
    },
    emailTab: function (){
        var self = this;
        return (
            React.createElement("div", {className: "form_register"}, 
                React.createElement("div", {className: "inp inp_email"}, 
                    React.createElement("input", {
                        type: "text", 
                        className: "email", 
                        placeholder: "电子邮箱", 
                        autoComplete: "off", 
                        value: self.state.edata.email, 
                        onChange: self.updateEmailByETab}), 
                    React.createElement("div", {className: "errorbg none"}, "请填写电子邮箱")
                ), 
               
                React.createElement("div", {className: "inp_div"}, 
                    React.createElement("div", {className: "inp inp_yanz"}, 
                        React.createElement("input", {
                                type: "text", 
                                title: "验证码", 
                                className: "text_yanz", 
                                placeholder: "验证码", 
                                autoComplete: "off", 
                                value: self.state.edata.verify, 
                                onChange: self.updateVerifyByETab}), 
                        React.createElement("div", {className: "errorbg none"}, "请填写验证码")
                    ), 
                    React.createElement("div", {className: "right"}, 
                        React.createElement("img", {className: "verify_img", srca: "{:U('User/verify')}"})
                    )
                ), 
                React.createElement("div", {className: "btn_register"}, 
                    React.createElement("button", {
                            type: "button", 
                            className: "submit_button", 
                            onClick: self.findMe}, 
                        "确定"
                    )
                ), 
                React.createElement("div", {className: "Cinput"}, 
                    React.createElement("div", {className: "has_account"}, 
                        "已有账号？立即", 
                        React.createElement("a", {href: "#signin", className: "regist"}, "登陆")
                    )
                )
             )
        );
    },
    render: function (){
        var self = this;

        var tab; 
        var tab_content = null;
        if ( this.state.tab == "phone" )  tab_content = self.phoneTab();
        if ( this.state.tab == "email" )  tab_content = self.emailTab();

        if ( this.state.tab == "phone" ) {
            tab = (
                React.createElement("ul", {className: "re_tab"}, 
                    React.createElement("li", {className: "li_p"}, "通过手机"), 
                    React.createElement("li", {onClick: self.switch_tab.bind(self, "email")}, "通过邮箱")
                ));
            
        } else if ( this.state.tab == "email" ) {
            tab = (
                React.createElement("ul", {className: "re_tab"}, 
                    React.createElement("li", {onClick: self.switch_tab.bind(self, "phone")}, "通过手机"), 
                    React.createElement("li", {className: "li_p"}, "通过邮箱")
                ));
        } else {
            console.warn(":; unknow tab name ....");
        }
        return (
            React.createElement("div", {className: "log_left re_pass"}, 
                React.createElement("div", {className: "login_title fr_1"}, "找回密码"), 
                tab, 
                tab_content
            )
        );
    }
});

window.components.auth.resetpwd = React.createClass({
    displayName: "components.auth.resetpwd",
    name: "重置密码",
    getInitialState: function (){
        // props: parent / 
        console.log("resetpwd component ...");
        return {"account":"", "password": "", "repassword": "" };
    },
    componentDidMount: function (){
        
    },
    updateAccount: function (){

    },
    updatePassword: function (e, rid){
        this.setState({"password": $(e.target).val() });
    },
    updateRepassword: function (e, rid){
        this.setState({"repassword": $(e.target).val() });
    },
    onReset: function (){
        console.info(":: 重置密码 ...");
        console.log(":: Data: " + JSON.stringify(this.state) );
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "log_left"}, 
                React.createElement("div", {className: "login_title"}, "设置密码"), 
                React.createElement("div", {className: "form_login"}, 
                    React.createElement("input", {type: "hidden", name: "token"}), 
                    React.createElement("div", {className: "inp inp_account"}, 
                        React.createElement("input", {
                                type: "password", 
                                className: "account", 
                                placeholder: "新密码", 
                                onChange: self.updatePassword}), 
                         React.createElement("div", {className: "errorbg none"}, "请填写新密码")
                    ), 
                    React.createElement("div", {className: "inp inp_pass"}, 
                        React.createElement("input", {
                                type: "password", 
                                className: "password", 
                                placeholder: "确认密码", 
                                onChange: self.updatePassword}), 
                         React.createElement("div", {className: "errorbg none"}, "请填写确认密码")
                    ), 
                    React.createElement("div", {className: "btn_login"}, 
                        React.createElement("button", {
                                className: "submit_button", 
                                type: "button", 
                                onClick: self.onReset}, 
                            "提交"
                        )
                    )
                )

            )
        );
    }
});
