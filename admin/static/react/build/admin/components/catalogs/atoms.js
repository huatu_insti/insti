if ( !window.components ) window.components = {};
if ( !window.components.catalogs ) window.components.catalogs = {};
if ( !window.components.catalogs.atoms ) window.components.catalogs.atoms = {};

window.components.catalogs.atoms.list =  React.createClass({
    displayName: "AtomsCatalogs",
    name: "试题标签列表",
    page: 1,
    area: -9,
    size: 20,
    api: new apis.catalogs.atoms(),
    isOpen: false,
    isSync: false,
    catalogs_cache: { "list": [], "num": 0 },
    getInitialState: function (){
        return { "list": [], "num": 0 };
    },
    componentDidMount: function() {
        this.fetch();
    },
    __fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
            }catch(e){
                console.warn(e); return;
            }
            self.isSync = true;
            self.isOpen = true;
            self.replaceState(body);
        };
        /*
            list: [
                0: {name: "二级", hookid: 0, updtime: 1426655982, inittime: 1426655982, tombstone: 0, cid: 3},
                1: {name: "Food", hookid: 0, updtime: 1423122459, inittime: 1423122459, tombstone: 0, cid: 1},
            ],
            num: 5
        */
        this.api.root('0', callback);
    },
    fetch: function (){
        var self = this;
        if ( this.isSync == true ){
            if ( this.isOpen == true ) {
                self.isOpen = false;
                self.catalogs_cache = self.state;
                self.replaceState({ "list": [], "num": 0 });
                self.forceUpdate();
                return ":: use cache ...";
            } else if ( this.isOpen == false ) {
                self.isOpen = true;
                self.replaceState(catalogs_cache);
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        self.__fetch();
    },
    children: function(cid, e, rid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
                // self.replaceState(body);
            }catch(e){
                console.warn(e); return;
            }
        };

        if ( parseInt($(e.target).parent().data("isopen")) < 1 ) {
            this.api.root(cid, callback);
        } else {
            
        }
    },
    add: function (hookid, name){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
                 + '<div class="lable_head">'
                    + '<span>添加试题标签</span><img  src="/static/images/add_test_close.png"/>'
                 + '</div>'
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>父级标签&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
            self.__fetch();
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            if ( hookid === '0' ) self.api.push({"name": cname, "hookid": "0" }, callback );
            else self.api.push({"name": cname, "hookid": parseInt(hookid) }, callback );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "添加试题标签",
            "width": 420,
            "height": 300
        });

    },
    remove: function (cid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            self.fetch();
        };
        this.api.remove(cid, callback);
    },
    render: function (){
        var self = this;
        var catalog_elems_count = function (catalog){
            if ( "elems" in catalog ) {
                return catalog.elems.length;
            } else {
                return 0;
            }
        };
        var style_of_col1;
        style_of_col1 = {"text-indent": 30};
        if ( self.isOpen == true ) style_of_col1.background = "url(/static/images/lable_list.png) no-repeat 12px -25px #eee;";
        else style_of_col1.background = "url(/static/images/lable_list.png) no-repeat 12px 10px;";

        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "试题标签管理")
                ), 
                React.createElement("div", {className: "Tagtree"}, 
                    React.createElement("div", {className: "Tagtree_thead"}, 
                        React.createElement("div", {className: "Tagtree_row clearfix"}, 
                            React.createElement("ul", {className: "catalogs"}, 
                                React.createElement("li", null, 
                                    React.createElement("div", {className: "clearfix"}, 
                                        React.createElement("div", {className: "Tagtree_col1"}, "试题标签名称"), 
                                        React.createElement("div", {className: "Tagtree_col2"}, "试题数量"), 
                                        React.createElement("div", {className: "Tagtree_col3"}, "试题排序"), 
                                        React.createElement("div", {className: "Tagtree_col4"}, "操作")
                                    )
                                )
                            )
                        )

                    ), 

                    React.createElement("div", {className: "Tagtree_tbody"}, 
                        React.createElement("ul", {className: "catalogs"}, 
                             this.state.list.map(function(c, i){
                                return (
                                    React.createElement(window.components.catalogs.atoms.list_row, {key: "CATALOG-"+c.cid, catalog: c, list: self, index: [i]})
                                );
                            }), 
                            React.createElement("li", {className: "Tagtree_add", key: "LAST_ROW"}, 
                                React.createElement("a", {className: "add_lable lable_icon", onClick: this.add.bind(this, '0' ,'顶级标签', null)}, "添加一级标签")
                            )
                        )
                    )

                )
            )
        );
    }
});

window.components.catalogs.atoms.list_row = React.createClass({
    displayName: "AtomsCatalogs-Row",
    name: "试题标签列表 - 列",
    api: new apis.catalogs.atoms(),
    isOpen: false,
    isSync: false,
    children_copy: [],
    getInitialState: function (){
        // {"name": "test", "hookid":0,"updtime":1429070010,"inittime":1429070010,"tombstone":0,"cid":this.props.cid}
        var c = this.props.catalog;
        c.children = [];
        return c;
    },
    __fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn( response ); return;
            }
            var children = JSON.parse(response.body);
            if ( "list" in children != true ) children.list = [];
            self.isSync = true;
            self.isOpen = true;
            self.setState({"children": children});
        };
        this.api.root(this.props.catalog.cid, callback);
    },
    fetch: function (){
        var self = this;
        if ( this.isSync == true ){
            if ( this.isOpen == true ) {
                self.isOpen = false;
                self.children_copy = self.state.children;
                self.setState({"children": []});
                self.forceUpdate();
                return ":: use cache ...";
            } else if ( this.isOpen == false ) {
                self.isOpen = true;
                self.setState({"children": self.children_copy });
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        self.__fetch();
    },
    add: function (){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
                 + '<div class="lable_head">'
                    + '<span>添加试题标签</span><img  src="/static/images/add_test_close.png"/>'
                 + '</div>'
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>父级标签&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", self.state.name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
            self.__fetch();
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            self.api.push({"name": cname, "hookid": parseInt(self.state.cid) }, callback );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "添加试题标签",
            "width": 420,
            "height": 300
        });
    },
    update: function (){
        var self = this;
        var dialog = $(
            '<div class="lable_mark">'
                 + '<div class="lable_head">'
                    + '<span>更新试题标签</span><img  src="/static/images/add_test_close.png"/>'
                 + '</div>'
                 + '<div class="lable_body">'
                    + '<div class="lable_name">'
                        + '<span>标签名称</span>'
                        + '<input  type="text" name="cname" />'
                    + '</div>'
                    + '<span>当前标签名称&nbsp;&nbsp;#CATALOG_NAME#</span>'.replace("#CATALOG_NAME#", self.state.name)
                 + '</div> '
                 + '<div class="lable_footer">'
                    + '<a href="javascript:;" class="modal_cannal">取消</a>'
                    + '<a href="javascript:;" class="modal_ok">添加</a>'
                 + '</div> '
            + '</div>'
        );
        // 添加标签结果处理
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            $(dialog).dialog("close");
            // self.__fetch();
            self.props.list.fetch();
        };
        // 添加标签
        var add_catalog = function (){
            var cname = $(this).parent().parent().find('input[name="cname"]').val();
            self.api.update(self.state.cid, {"name": cname, "hookid": parseInt(self.state.hookid) }, callback );
        };
        // 事件绑定
        $(dialog).find(".lable_footer .modal_cannal").on('click', function (){
            $(dialog).dialog("close");
        });
        $(dialog).find(".lable_footer .modal_ok").on('click', add_catalog);

        $(dialog).dialog({
            "title": "更新试题标签",
            "width": 420,
            "height": 300
        });
    },
    remove: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            self.props.list.fetch();
        };
        this.api.remove(this.state.cid, callback);
    },
    render: function (){
        var self = this;
        if ( "children" in this.state != true ) this.setState({"children": []});
        
        var classname_of_col1;
        if ( self.isOpen == true ) classname_of_col1 = "-24px";
        else if ( self.isOpen == false ) classname_of_col1 = "10px";

        return (
            React.createElement("li", {className: "catalog", key: "CATALOG-"+this.state.cid+"-raw"+self.props.index.join('-')}, 
                React.createElement("div", {className: "Tagtree_row clearfix"}, 
                    React.createElement("div", {
                        className: "Tagtree_col1", 
                        style:  {
                                "text-indent": self.props.index.length*50 - (self.props.index.length-1)*25, 
                                "background": "url(/static/images/lable_list.png) no-repeat " + (self.props.index.length*25).toString()+"px " + classname_of_col1,
                                "cursor": "pointer"
                                }, 
                        
                        onClick: this.fetch}, 
                        this.state.name
                    ), 
                    React.createElement("div", {className: "Tagtree_col2"}, "欠缺资料"), 
                    React.createElement("div", {className: "Tagtree_col3"}, "未知资料"), 
                    React.createElement("div", {className: "Tagtree_col4"}, 
                        React.createElement("a", {className: "a_oper", onClick: this.add}, "添加子标签"), " |",  
                        React.createElement("a", {className: "a_oper", onClick: this.update}, "编辑标签"), " |",  
                        React.createElement("a", {className: "a_oper", onClick: this.remove}, "删除标签"), " |" 
                    )
                ), 
                React.createElement("ul", {className: "catalogs"}, 
                     this.state.children.map(function(c, i){
                            return (
                                React.createElement(window.components.catalogs.atoms.list_row, {key: "CATALOG-"+c.cid, catalog: c, list: self, index: self.props.index.concat(i)})
                            );
                    }) 
                )
            )
        );
    }
});


/* 对话框组件 */

window.components.catalogs.atoms.ktree_dialog =  React.createClass({
    displayName: "试题知识点选择对话框",
    name: "试题知识点",
    api: new apis.catalogs.atoms(),
    catalogs: [],
    kps: {},           // 已选择的知识点
    getInitialState: function (){
        if ( !this.props.destroy ) this.destroy = function(){};
        else this.destroy = this.props.destroy;

        // if ( !this.props.ok ) this.ok = function(){};
        // else this.ok = this.props.ok;

        return { "list": [], "num": 0 };
    },
    componentDidMount: function() {
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body != true ) body.list = [];
                if ( "num" in body != true ) body.num = 0;
            }catch(e){
                console.warn(e); return ;
            }
            self.catalogs = body.list;
            self.forceUpdate();
        };
        /*
            list: [
                0: {name: "二级", hookid: 0, updtime: 1426655982, inittime: 1426655982, tombstone: 0, cid: 3},
                1: {name: "Food", hookid: 0, updtime: 1423122459, inittime: 1423122459, tombstone: 0, cid: 1},
            ],
            num: 5
        */
        this.api.root('0', callback);
    },
    ok: function (){
        // if ( !this.props.ok ) return;
        var self = this;
        var kps = [];
        Object.keys(this.kps).map(function (k, i){
            kps.push(self.kps[k]);
        });
        this.props.ok(kps);
        this.destroy();
    },
    destroy: function (){
        if ( !this.props.dialog ) return;
        $(this.props.dialog).dialog("close");
    },
    render: function (){
        var self = this;
        return (
                React.createElement("div", null, 
                    React.createElement("div", {className: "selectModal_mark"}, 
                        React.createElement("div", {className: "modal-body"}, 
                            React.createElement("ul", null, 
                                 this.catalogs.map(function(k, i){
                                    return React.createElement(window.components.catalogs.atoms.ktree, {
                                                        key: "ktree_dialog_cids_" + k.cid.toString() + "_index_" + i.toString(), 
                                                        parent: self, 
                                                        root: self, 
                                                        knowledge: k, 
                                                        index: i});
                                }) 
                            )
                        ), 
                        React.createElement("div", {className: "modal_footer"}, 
                            React.createElement("input", {type: "text"}), 
                            React.createElement("a", {className: "modal_a", href: "javascript:;"}, "搜索"), 
                            React.createElement("a", {className: "modal_ok", href: "javascript:;", onClick: self.ok}, "确定")
                        )
                    ), 

                    React.createElement("div", {className: "black", id: "bg"})
                )
        );
    }
});

window.components.catalogs.atoms.ktree =  React.createClass({
    displayName: "试题知识点树",
    name: "试题知识点树",
    api: new apis.catalogs.atoms(),
    children: [],         // 子节点
    num: 0,              // 子节点元素总量
    sync_status: 0,   // 同步状态
    open_status: 0,  // 树打开状态
    is_hit: 0,             // 是否选择
    getInitialState: function (){
        if ( !this.props.knowledge ) throw new Error("ktree init fail.");
        return { "knowledge": this.props.knowledge };
    },
    componentDidMount: function() {
        // this.fetch();
    },
    fetch: function (cid, e, rid){
        var self = this;
        if ( parseInt(self.sync_status) == 1 ) {
            if ( parseInt(self.open_status) == 1 ) {
                self.open_status = 0;
                self.children_copy = self.children;
                self.children = [];
                self.forceUpdate();
                return ":: use cache ...";
            }
            if ( parseInt(self.open_status) == 0 ) {
                self.open_status = 1;
                self.children = self.children_copy;
                self.forceUpdate();
                return ":: use cache ...";
            }
        }
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);
                return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "list" in body ) body = body.list;
                if ( "num" in body) self.num = body.num;
            }catch(e){
                console.warn(e); return;
            }
            self.sync_status = 1;
            self.open_status = 1;
            self.children = body;
            self.forceUpdate();
        };
        this.api.root(cid, callback);
    },
    hit_the_kp: function (cid, name){
        var self = this;

        if ( parseInt(self.sync_status) != 1 ) {
            // self.fetch();
            return;
        }
        if ( parseInt(self.children.length) == 0 ) {
            if ( parseInt(self.is_hit) == 1) {
                self.is_hit = 0;
                delete self.props.root.kps[parseInt(self.props.knowledge.cid)];
            } else {
                self.is_hit = 1;
                self.props.root.kps[parseInt(self.props.knowledge.cid)] = self.props.knowledge;
            }
            self.forceUpdate();
        } else {
            // 不可以选择

        }
    },
    render: function (){
        var self = this;
        var is_top_node = function (hookid){
            if ( hookid.toString() == "0" ) { 
                return "order_first";
            } else { 
                return "order_usually";
            }
        };
        var sync_ico, hit_classname;
        if ( self.open_status == 1 ) sync_ico = "level_two";  // 树打开状态.
        else sync_ico = "level_one";
        if ( self.is_hit == 1 ) hit_classname = "c_on"; // 选中状态
        else hit_classname = "c_ban";

        return (
            React.createElement("li", {"data-hookid": this.props.knowledge.hookid, 
                "data-cid": this.props.knowledge.cid, 
                "data-index": this.props.index}, 

                React.createElement("div", {className: is_top_node(this.props.knowledge.hookid)}, 
                    React.createElement("span", {className:  "checkbox_switch " + hit_classname, 
                        onClick: this.hit_the_kp.bind(this, this.props.knowledge.cid, this.props.knowledge.name) }
                    ), 
                    React.createElement("span", {className: "level_switch " + sync_ico, 
                        onClick: this.fetch.bind(this, this.props.knowledge.cid) }
                    ), 
                    React.createElement("span", {className: "mark_name"}, this.props.knowledge.name), 
                    React.createElement("ul", null, 
                         this.children.map(function (k, i){
                            return React.createElement(window.components.catalogs.atoms.ktree, {
                                            key: "ktree_dialog_cids_" + k.cid.toString() + "_index_" + i.toString(), 
                                            parent: self, 
                                            root: self.props.root, 
                                            knowledge: k, 
                                            index: i})
                        }) 
                    )
                )
            )
        );
    }
});
