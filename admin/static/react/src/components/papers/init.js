
if ( !window.components ) window.components = {};
if ( !window.components.papers ) window.components.papers = {};

window.components.papers.create = React.createClass({
    displayName: "components.papers.create",
    name: "试卷创建",
    provinces: [],
    api: new apis.papers(),
    getInitialState: function (){
        window.paper_create = this;
        return {
             "pid": 0, "status": 0, "name": "", "inittime": time.time(),
             "updtime": time.time(), 
             "attrs": [
                {"name": "year", "value": "2015"},
                { "name": "area", "value": "3125"}
             ],
             "tombstone": 0,
             "struct": []
        };
    },
    componentDidMount: function (){
        this.getProvinces();  
        if ( parseInt(this.props.pid) != 0 ) { 
                this.pull()
        }else{

        }

    },
    init: function (xmen){
        var self = this;
        if ( !xmen ) var xmen = function(){};
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                self.replaceState(JSON.parse(response.body));
                xmen(true);
            }catch(e){
                console.warn(e);
                xmen(false);
            }
        };
        this.api.init(this.state, callback);
    },
    pull: function (){
        // if ( this.props.pid && parseInt(this.props.pid) > 1 ) {
        //     var pid = 1;
        // } else {
        //     throw new Error("Error.");
        // }
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body =  JSON.parse(response.body);
                for(var i=0;i<body.attrs.length;i++){
                     if(body.attrs[i].name == "year") { self.year = body.attrs[i].value;}
                     if(body.attrs[i].name == "area") { self.area = body.attrs[i].value;}
                }
                $(".test").attr("value",body.name);
                self.replaceState(body);
            }catch(e){
                console.warn(e);return;
            }
        };
        this.api.pull(this.props.pid, callback);
    },
    getProvinces: function (){
        // /locs/provinces
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);return;
            }
            try{
                self.provinces = JSON.parse(response.body).locations;
                self.setState({"reload": time.time() });
            }catch(e){
                console.warn(e);return;
            }
        };
        var api = new apis.locations();
        api.fetch(0, callback);
        // api.provinces(callback);
    },
    setYear: function (e, rid){
        var attrs = this.state.attrs.concat();
        // var year = $('[data-reactid="#rid#"]'.replace("#rid#", rid)).find("option:selected").val();
        attrs[0] = { "name": "year", "value": e.target.value.toString()  };
        this.setState({"attrs": attrs} );
    },
    setArea: function (e, rid){
        var attrs = this.state.attrs.concat();
        // var area = $('[data-reactid="#rid#"]'.replace("#rid#", rid)).find("option:selected").val();
        var target = $(e.target).find("option:selected");
        var id = target.data("id");
        var name = target.data("name");
        
        attrs[1] = { "name": "area", "value": id+":"+name };
        this.setState({"attrs": attrs} );
    },
    setName: function (e, rid){
        this.setState({"name": e.target.value} );
    },
    edit: function (){
        // 录题
        var self = this;
        if(self.props.action != undefined && self.props.action == "update"){
                var callback = function(response){
                      if( response.type == "progress") { return "loading...";}
                      if( response.code !=200 ) { return false;}
                      try{
                          var body = JSON.parse(response.body);
                          if(parseInt(self.state.attrs[0].value) != parseInt(self.year) || self.state.attrs[1].value != self.area){
                                  var callback2 = function(res){ 
                                         if( res.type == "progress") { return "loading...";}
                                         if( res.code !=200 ) { return false;}
                                         try{ 
                                            self.props.root.setState({"content":<window.components.papers.paper  
                                                              pid={self.state.pid} tixu_card2={[]} root={self.props.root}/>})
                                         }catch(e){}
                                  }
                                  self.api.updateAttrs(self.props.pid,"append",self.state.attrs,callback2);
                          }else{
                               self.props.root.setState({"content":<window.components.papers.paper  
                                                              pid={self.state.pid} tixu_card2={[]} root={self.props.root}/>})
                          }
                       
                      }catch(e){

                      }
                }
                var updateData = {"name":self.state.name,"pid":self.props.pid}
                 self.api.update(updateData,callback);
              

                                 
                
        }else{
               
                var callback = function (status){
                    if ( status == true ){
                        // window.views.papers.edit(self.state.pid);
                       self.props.root.setState({"content":<window.components.papers.paper  pid={self.state.pid} tixu_card2={[]} root={self.props.root}/>})
                    } else {
                        console.warn("试卷创建失败！"); return;
                    }
                };
                if ( this.state.name.length < 1 ) {
                    alert("试卷名还没有填写哦");return;
                }
                this.init(callback);
         }


        
    },
    backToListtable:function(){
        var self = this;
        self.props.root.setState({"content":<window.components.papers.list page="0" size="20" root={self.props.root}/>});
    },
    render: function (){
        var self = this;
        if(self.props.pid == undefined ){
               $(".test").attr("value","");
               self.state.attrs[0].value = "";
               self.state.attrs[1].value = "";

        }
        return (
            <div>
                <div className="create_head">
                    <div className="record_paper">创建试卷</div>
                    <ul className="creatte_ul clearfix">
                        <li className="cre_f1 create_on">1.创建试卷</li>
                        <li className="cre_f1">2.录入试卷</li>
                        <li className="cre_f1">3.预览试卷</li>
                        <li className="cre_f1">4.提交审核</li>
                    </ul>
                    <div className="return_list" onClick={self.backToListtable}><a>返回列表</a></div>
                    <div className="input_test" onClick={self.edit}><a>录入试题</a></div>
                </div>
                <div className="create_con">
                    <div className="create_info">
                        <div className="create_inp">
                            <div className="papaer_name">试卷名称</div>
                            <input className="test" type="text"  placeholder="请输入试卷名称" onBlur={this.setName} />
                        </div>
                        <div className="create_inp">
                            <div className="papaer_name">试卷年份</div>
                            <select id="select_area" onBlur={this.setYear}>
                                <option value="0">请选择试卷年份</option>
                                { range(2000, 2016).map(function(year, index){ 
                                    if ( parseInt(year) == parseInt(self.state.attrs[0].value) ){
                                        return <option value={year} selected="true">{year}</option>;
                                    } else {
                                        return <option value={year}>{year}</option>;
                                    }
                                }) }
                            </select>
                        </div>
                        <div className="create_inp">
                            <div className="papaer_name">试卷地区</div>
                            <select id="select_area" onBlur={this.setArea}>
                                <option value="0">请选择试卷地区</option>
                                { this.provinces.map(function(province, index){
                                    if ( parseInt(province.id) == parseInt(self.state.attrs[1].value) ){
                                        return <option value={province.id} data-id={province.id} data-name={province.name} selected="true">{province.name}</option>;
                                    } else {
                                        return <option value={province.id} data-id={province.id} data-name={province.name}>{province.name}</option>;
                                    }
                                }) }
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});