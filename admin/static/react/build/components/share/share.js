if ( !window.components ) window.components = {};
if ( !window.components.share ) window.components.share = {};
window.components.share.index = React.createClass({displayName: "index",
      getInitialState:function(){
           return {"content":""}
      },
	  render:function(){
	  	 var self = this;
         return  React.createElement("div", null, 
                    React.createElement("div", {className: "header"}, 
					        React.createElement("div", {className: "logo fl"}, 
					            React.createElement("img", {src: "static/images/pic_yuan.png"}), 
					            React.createElement("span", {onClick: "window.location.href='/';"}, "题库管理后台")
					        ), 
					        React.createElement("div", {className: "main-nav fl"}, 
					            React.createElement("div", {className: "show_sidebar fl"})
					        ), 
					        React.createElement("div", {className: "user"}, 
					            React.createElement("div", {className: "user-bar fr", onClick: "$(this).next().removeClass('none').toggle();"}, 
					                "admin", 
					                React.createElement("img", {src: "static/images/button_pullDown.png"})
					            ), 
					            React.createElement("div", {className: "user_info none"}, 
					                    React.createElement("div", {className: "modify_pass mod"}, React.createElement("a", {href: "tor.views.auth.chpasswd"}, "修改密码")), 
					                    React.createElement("div", {className: "exit mod"}, React.createElement("a", {href: "/logout"}, "退出"))
					            )
					        )
                   ), 
                   React.createElement("div", {className: "sidebar"}, 
					        React.createElement("div", {className: "subnav"}, 
					            React.createElement("ul", null, 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico1"}, 
					                        React.createElement("span", null, "试卷管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt", onclick: "views.papers.fetch();"}, "试卷列表"), 
					                        React.createElement("dt", {className: "sub_dt", onclick: "views.papers.init();"}, "录入试卷"), 
					                        React.createElement("dt", {className: "sub_dt", onclick: "alert('正在开发中')"}, "试题纠错列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico2"}, 
					                        React.createElement("span", null, "考试管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "考试列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico3"}, 
					                        React.createElement("span", null, "标签管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "试卷标签列表"), 
					                        React.createElement("dt", {className: "sub_dt"}, "考试标签列表"), 
					                        React.createElement("dt", {className: "sub_dt"}, "试题标签列表")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico4"}, 
					                        React.createElement("span", null, "其他管理"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "意见反馈")
					                    )
					                ), 
					                React.createElement("li", null, 
					                    React.createElement("div", {className: "sub_list list_ico5"}, 
					                        React.createElement("span", null, "系统设置"), 
					                        React.createElement("img", {src: "static/images/sub_right.png"})
					                    ), 
					                    React.createElement("dl", null, 
					                        React.createElement("dt", {className: "sub_dt"}, "用户管理")
					                    )
					                )
					            )
					        )
					    ), 
					    React.createElement("div", {className: "main-container"}, 
				            React.createElement("div", {className: "main_content"}, 
				                React.createElement("div", {id: "container"}
				              
				                )
				            )
				        )
				 )              
	    } 

})