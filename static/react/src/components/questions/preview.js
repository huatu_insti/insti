
if ( !window.components ) window.components = {};
if ( !window.components.questions ) window.components.questions = {};

window.components.questions.preview = React.createClass({
    displayName: "QuestionPreview",
    name: "普通题",
    tixu: 0,
    api: new apis.questions(),
    getInitialState: function(){
        if ( !this.props.qid || parseInt(this.props.qid) < 1 ) {
            throw new Error("QuestionPreview react init fail.");
        }
        if ( !this.props.index ) {
            throw new Error("QuestionPreview react init fail.");
        }
        if ( !this.props.paper ) {
            throw new Error("QuestionPreview react init fail.");
        }
        this.tixu = this.props.index.join("-");
        
        return {
                "content": "",
                "choices": [],
                "attrs":[],   // "attrs":[{"name":"comment","value":"test"}]
                "ans":"",
                "ansnum":0,
                "score":0,
                "tombstone":0,
                "updtime":time.time(),
                "inittime":time.time(),
                "qid": parseInt(this.props.qid)
            };
    },
    componentDidMount: function (){
        var self = this;
        this.pull();
        this.props.paper.questions[this.props.qid] = {"tixu": this.tixu, "element": this.state, "component": this};
        self.props.paper.forceUpdate();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                var body = JSON.parse(response.body);
                // FIX.
                if ( "choices" in body != true ) body.choices = [];
                if ( "content" in body != true ) body.content = "";
                if ( "attrs" in body != true ) body.attrs = [];
            }catch(e){
                console.warn(e); return false;
            }
            self.replaceState(body);
            self.props.paper.questions[self.props.qid] = {"tixu": self.tixu, "element": body, "component": self};
            self.props.paper.forceUpdate();
        };
        this.api.pull(this.props.qid, callback);
    },
    render: function (){
        var self = this;
        return (
            <div className="element" data-qid={this.state.qid.toString()} data-index={this.tixu}>
                <div className="body">
                    <span className="tixu">{this.tixu}</span>、（
                    <span className="flag">{this.name}</span>）
                    <span className="content" dangerouslySetInnerHTML={{__html: this.state.content}} />
                </div>
                <div className="choices">
                    { this.state.choices.map(function(choice, index){
                        return (
                            <div className="choice"
                                     key={"q_choice_"+index.toString()}
                                     data-index={index}>
                                <span className="key">{chr(65+index)}</span>.
                                <span className="value" dangerouslySetInnerHTML={{__html: choice}} />
                            </div>
                        );
                    }) }
                </div>
                <div className="attrs">
                    { this.state.attrs.map(function(attr, index){
                            if ( attr.name == "知识点" ) {
                                var kps = JSON.parse(attr.value);
                                if ( typeof kps == "string" ) kps = JSON.parses(kps);
                                return (
                                    <div className="attr" 
                                            key={"q_attr_"+index.toString()}
                                            data-index={index}>
                                        <span className="key">{attr.name}</span>
                                        <div className="value">
                                            <ul>
                                                { kps.map(function (kp, i){
                                                    return (<li><span data-cid={kp.cid}>{kp.name}</span></li>);
                                                }) }
                                            </ul>
                                        </div>
                                    </div>
                                );
                            } else {
                                return (
                                    <div className="attr" 
                                                key={"q_attr_"+index.toString()}
                                                data-index={index}>
                                            <span className="key">{attr.name}</span>
                                            <span className="value" dangerouslySetInnerHTML={{__html: attr.value}} />
                                    </div>
                                );
                            }
                    }) }
                    <div className="attr" key={"q_attr_score_"} >
                            <span className="key">分数</span>
                            <span className="value">{this.state.score}</span>
                    </div>
                </div>
            </div>
        );
    }
});
