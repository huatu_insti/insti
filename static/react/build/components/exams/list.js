if ( !window.components ) window.components = {};
if ( !window.components.exams ) window.components.exams = {};

window.components.exams.list =  React.createClass({
    displayName: "ExamsList",
    name: "考试列表",
    exams: [],           // 单条试卷信息栏
    page: 1,
    area: -9,
    size: 20,
    api: new window.apis.exams(),
    getInitialState: function (){
        if ( !this.props.page || !this.props.size ) throw new Error("window.components.exams.list react init error.");
        if ( parseInt(this.props.page) < 1 || parseInt(this.props.size<1) ) throw new Error("window.components.exams.list react init error.");
        return { "exams": [], "count": 0 };
    },
    componentDidMount: function (){
        this.page = parseInt(this.props.page);
        this.size = parseInt(this.props.size);
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "exams" in body != true ) body.exams = [];
                if ( "count" in body != true ) body.count = 0;
            }catch(e){
                console.warn(e);  return false;
            }
            self.replaceState(body);
        };
        this.api.fetch(this.page, this.size, callback);
    },
    remove: function (eid){
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            } else {
                $("tr[data-eid='#eid#']".replace("#eid#", eid)).remove();
            }
        };
        // callback({"type": "hehe", "code": 500});
        this.api.remove(eid, callback);
    },
    onSetPage: function (page, e){
        var self = this;

        if ( ! page || (parseInt(page) < 1  && page != "goto" ) ) return false;
        if ( page == "goto" ) page = parseInt($(e.target).val());
        if ( parseInt(page) == parseInt(this.page) ) return true;
        if ( parseInt(page) > 0 && parseInt(page) < Math.ceil(this.state.count/this.size)+1 ){
            this.page = parseInt(page);
            this.fetch();
        }
    },
    render: function (){
        // 渲染模板
        var self = this;
        /*
        count: 1,
        exams: [
            0: {
                eid: 2,
                inittime: 1424446662,
                lowertime: 1424446662,
                name: "test",
                tombstone: 0,
                updtime: 1424446662
            }
        ]
        */
        
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "试卷列表")
                ), 

                React.createElement("div", {className: "paper_sort_main"}, 
                    React.createElement("div", {className: "paper_sort_title"}, 
                        React.createElement("span", null, "试卷名称"), 
                        React.createElement("input", {type: "text", placeholder: "请输入试卷名称", className: "inp_search"}), 
                        React.createElement("a", {className: "sort_search", href: "javascript:;"}, "搜索")
                    ), 
                    React.createElement("table", {className: "tab_01", bordercolor: "#ddd", border: "1px", align: "left", cellSpacing: "0", cellPadding: "0", width: "100%"}, 
                       React.createElement("thead", null, 
                            React.createElement("tr", null, 
                                React.createElement("th", null, "编号"), 
                                React.createElement("th", null, "试卷名称"), 
                                React.createElement("th", null, "考试类型"), 
                                React.createElement("th", null, "科目"), 
                                React.createElement("th", null, "标签"), 
                                React.createElement("th", null, "添加时间"), 
                                React.createElement("th", null, "编辑")
                           )
                        ), 
                        React.createElement("tbody", {id: "tid", align: "center"}, 
                             this.state.exams.map(function (exam, index){
                                return (
                                    React.createElement("tr", {"data-pid": exam.eid, key: exam.eid}, 
                                        React.createElement("td", null, exam.eid), 
                                        React.createElement("td", null, exam.name), 
                                        React.createElement("td", null, "未知"), 
                                        React.createElement("td", null, "未知"), 
                                        React.createElement("td", null, "未知"), 
                                        React.createElement("td", null, time.ctime(exam.inittime)), 
                                        React.createElement("td", {className: "sort_operate"}, 
                                            React.createElement("button", {type: "button", className: "btn btn-default"}, React.createElement("a", {onClick: window.views.exams.view.bind(self, exam.eid)}, "查看")), 
                                            React.createElement("button", {type: "button", className: "btn btn-default"}, React.createElement("a", {onClick:  self.remove.bind(this, exam.eid) }, "撤出考试"))
                                        )
                                    )
                                );
                            }), 
                            
                            React.createElement("tr", {key: "exams_list_nav___"}, 
                                React.createElement("td", {colSpan: "7"}, 
                                    React.createElement("div", {className: "page_left"}, React.createElement("span", null, "共", this.state.count, "条记录，每页", this.size, "条，共", Math.ceil(this.state.count/this.size), "页")), 
                                    React.createElement("div", {className: "page_right"}, 
                                        React.createElement("span", {className: "page_sp"}, 
                                            "跳转至第          ", 
                                            React.createElement("input", {type: "text", className: "page_num", 
                                                        placeholder: this.page+1, 
                                                        onChange: this.onSetPage.bind(self, "goto") }), "页，" + ' ' +
                                            "页数 ", this.page, " / ", Math.ceil(this.state.count/this.size)
                                        ), 
                                        React.createElement("a", {className: "page_top", onClick: this.onSetPage.bind(self, this.page-1) }, "<"), 
                                        React.createElement("a", {className: "page_down", onClick: this.onSetPage.bind(self, this.page+1) }, ">")
                                    )
                                )
                            )
                       )
                  )
                )
            )
            );
    }

});
