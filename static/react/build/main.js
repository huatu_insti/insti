// Main JavaScript Document

if ( !window.views ) window.views = {};

if ( !window.views.clean ) {
    window.views.clean = function (){
        try{
            React.unmountComponentAtNode(document.getElementById('container'));
            $("#container").empty();
        }catch(e){
            throw new Error("unmount react element fail.");
        }
    };
}

if ( !window.views.auth ) window.views.auth = {};

views.auth.signin = function (page, size, area){
    // 提取试卷列表
    window.views.clean();
    var el = React.createElement(window.components.auth.signin, { } );
    React.render(el, document.getElementById("container") );
};

if ( !window.views.tests ) window.views.tests = {};

views.tests.answer = function (page, size, area){
    // 提取试卷列表
    window.views.clean();
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    var el = React.createElement(window.components.tests.answer.test, {"tid": 33,  "page": page, "size": size } );
    React.render(el, document.getElementById("container") );
};
views.tests.report = function (page, size, area){
    // 提取试卷列表
    window.views.clean();
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    var el = React.createElement(window.components.tests.report, { "page": page, "size": size } );
    React.render(el, document.getElementById("container") );
};

if ( !window.views.papers ) window.views.papers = {};
views.papers.fetch = function (page, size, area){
    // 提取试卷列表
    window.views.clean();
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    var el = React.createElement(window.components.papers.list, { "page": page, "size": size } );
    React.render(el, document.getElementById("container") );
};
views.papers.init = function ( pid ){
    // 创建试卷
    if ( !pid || parseInt(pid) < 1 ) var pid = "0";
    window.views.clean();
    var el = React.createElement( window.components.papers.create, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.preview = function ( pid ) {
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.preview, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.review = function ( pid ) {
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.review, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.feedback = function (){

};


/**

    @   exams

**/
if ( !window.views ) window.views = {};
if ( !window.views.exams ) window.views.exams = {};

views.exams.fetch = function (page, size, area ){
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    window.views.clean();
    var el = React.createElement(window.components.exams.list, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};
views.exams.view = function (eid ){
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    window.views.clean();
    var el = React.createElement(window.components.exams.view, { "eid": eid } );
    React.render(el, document.getElementById('container'));
};





window.App = React.createClass({displayName: "App",
  getInitialState: function  () {
    return {
      "route": window.location.hash.substr(1),
      // "component": el
    };
  },
  componentDidMount: function () {
    var self = this; 

    var onHashChange = function (e){
        var component = null;
        //console.log( "HashChange: route " + window.location.hash.substr(1) );
        self.setState( { "route": window.location.hash.substr(1) } ); // , "component": component
    };
    window.addEventListener('hashchange', onHashChange);
    //检测是否是ie9以下
    var check_browser_version = function(){
       
        var browser=navigator.appName
        var b_version=navigator.appVersion
        var version=b_version.split(";");
      
        var trim_Version=version[1].replace(/[ ]/g,"");
        var browser_version = trim_Version.substr(4);
        if(browser=="Microsoft Internet Explorer" &&  parseInt(browser_version)<9){
                setalert("为了更好的使用体验，请您使用谷歌或火狐浏览器");
        }
    }
   check_browser_version();
  },
  views: function (){
        var self = this;
        check_login();
         // window.auth = {
         //            name:'merry chirismary',
         //            token:'65794a68634842705a4349364e6977696332566a636d5630615751694f6a45324f5377696157357064485270625755694f6a45304e5441354d6a51334d446373496d563463476c795a576c75496a6f784e4455784d54677a4f54413366513d3d2e574232774c6a634245584263304a6e326f522f44387652543267453d'
         //            }
        var view = {"component": null, "props": {} };

        var route = self.state.route;
        if ( "auth" in window != true || "name" in window.auth != true ) {
            if ( self.state.route != "signin" && self.state.route != "signup" && self.state.route != "findpwd" ) {
               
                window.location.assign("#signin");
                route = "signin";

              
            }
        }
        
        if ( route.length < 1 ) route = "index";
        
        switch ( route ) {

            case 'index': view.component = window.components.index.index; view.props = {}; break;
            case 'signin': view.component = window.components.auth.sign; view.props = {"action": "signin"}; break;
            case 'signup': view.component = window.components.auth.sign; view.props = {"action": "signup"}; break;
            case 'resetpwd': view.component = window.components.auth.sign; view.props = {"action": "resetpwd"}; break;
            case 'findpwd': view.component = window.components.auth.sign; view.props = {"action": "findpwd"}; break;
            case 'setting': view.component = window.components.user.settings.setting; view.props = {"component": window.components.user.settings.setTestArea}; break;
            case 'setting_test_area': 
                       
                        view.component = window.components.user.settings.setting;
                        view.props = {"component": window.components.user.settings.setTestArea  };
                        break;
            case 'setting_ques_area':
                        view.component = window.components.user.settings.setting;
                        view.props = {"component": window.components.user.settings.setQuestionAreaAndYear};
                        break;
            default:
                        view.component = window.components.error;
                        view.props = {"code": "404", "msg": "页面不存在"};
                        break;
        };
        if ( view.component == null ) return null;
        return React.createElement(view.component, view.props );
  },
  render: function () {
    var self = this;
    return (
        React.createElement("div", null, 
             self.views() 
        )
    );
  }
});


function check_login (){
    var isLogin =  QC.Login.check();
    if(isLogin){ 
        if(window.location.hash == "#index"){  
          if( window.opener != null){
               window.opener.location.assign("#index");
               window.close();
           }
        }
             
         
    
   
   
  

  
         QC.Login.getMe(function(openId, accessToken){
               
                 QC.api("get_user_info",{"oauth_consumer_key":"101280324"}).success(function(userinfo){
                    
                    
                      var url = "/cgi-bin/auth.php";
                      var body = {"hook": "auth.qq_signin", "data": {"qq_openid":openId,"nickname":userinfo.data.nickname} };
                      var callback = function (response){
                              if ( response.type == 'progress' ) return 'loading ...';
                              if ( response.code != 200 ) {
                                     setalert("登录失败 ...");
                                     console.warn(response); return;
                               }
                             try{  //console.log(response.body);
                                var body = JSON.parse(response.body);
                                document.cookie.split("; ").map(function(cookie,index){
                                     var key= cookie.split("=")[0];
                                     var value=cookie.split("=")[1];
                                     if(key == "insti_apis"){
                                        var uinfo = JSON.parse(decodeURIComponent(value));
                                        if ( typeof uinfo.year == "string" ) uinfo.year = JSON.parse(uinfo.year);
                                        if ( typeof uinfo.extent == "string" ) uinfo.extent = JSON.parse(uinfo.extent);
                                 //console.log(uinfo);
                                        window.auth = { 
                                                "token": uinfo.token.token, 
                                                "sid": uinfo.token.token, 
                                                "name": uinfo.username,
                                                "area": uinfo.area,
                                                "years": uinfo.year,
                                                "extent":uinfo.extent
                                            };
                     
                                     
                                       if(window.auth.area == 0){
                                            window.location.assign("#setting_test_area");
                                       }else if( auth.years.length == 0 || auth.extent.length == 0){
                                            window.location.assign("#setting_ques_area");
                                       }else{
                                            window.location.assign("#index");
                                       }
                                     }
                                    })
                              
                            
                                
                              
                             }catch(e){
                                console.warn(e); return false;
                             }
                   };
                    requests.put(url, body, {"async": true, "callback": callback});
                      
                 })
              
               
         })
            
         
    }else{
          document.cookie.split("; ").map(function (cookie, index){
            try{
                key = cookie.split('=')[0];
                value = cookie.split("=")[1];
                if ( key == 'insti_apis' ) {
                    var uinfo = JSON.parse(decodeURIComponent(value));
                     
                    if ( typeof uinfo.year == "string" ) uinfo.year = JSON.parse(uinfo.year);
                    if ( typeof uinfo.extent == "string" ) uinfo.extent = JSON.parse(uinfo.extent);
                    
                    window.auth = { 
                                            "token": uinfo.token.token, 
                                            "sid": uinfo.token.token, 
                                            "name": uinfo.username,
                                            "years": uinfo.year,
                                            "area": uinfo.area,
                                            "extent":uinfo.extent
                                        };
                　//用户首次登陆或没有设置考区
                    if ( uinfo.area == 0) {
                       window.location.assign("#setting_test_area");

                    } 
                    else if ( uinfo.year.length == 0 || uinfo.extent.length == 0 ) {
                       window.location.assign("#setting_ques_area");
                    } else {
                
                    }
                }
            } catch (e){
                window.location.assign("#signin");
                return;
            }
        });
      
    }
  

}