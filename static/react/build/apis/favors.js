/*

2.2.8 用用户收藏记录操作


获取用用户收藏记录
Operation
	
	GET /favors/:secretid

Request Sample
	
	curl “http://115.159.4.84:9123/favors/1”


清空用用户收藏记录
Operation
	
	DELETE /favors/:secretid

Request Sample
	
	curl -X DELETE “http://115.159.4.84:9123/favors/1”


添加/移除指定的用用户收藏记录
Operation

	PUT

Request Sample

	/favors/:secretid

	curl
		-X PUT
		--header “Content-type:application/json
		-d ‘{
		“action” : “ADD”,
		“favors” : [{“qid”:1}, {“qid”:2}]
		}’
		“http://115.159.4.84:9123/favors/1”
Or
	curl
		-X PUT
		--header “Content-type:application/json
		-d ‘{
		“action” : “REMOVE”,
		“favors” : [{“qid”:1}, {“qid”:2}]
		}’
		“http://115.159.4.84:9123/favors/1” 


*/
