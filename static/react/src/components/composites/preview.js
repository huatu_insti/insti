
if ( !window.components ) window.components = {};
if ( !window.components.composites ) window.components.composites = {};

window.components.composites.preview = React.createClass({
    displayName: "CompositesPreview",
    name: "分组",
    tixu: 0,
    api: new apis.composites(),
    getInitialState: function(){
        if ( !this.props.cqid || parseInt(this.props.cqid) < 1 ) {
            throw new Error("CompositesPreview react init fail.");
        }
        if ( !this.props.index ) {
            throw new Error("CompositesPreview react init fail.");
        }
        if ( !this.props.paper ) {
            throw new Error("CompositesPreview react init fail.");
        }
        this.tixu = this.props.index.join("-");
        return {
                "cqid": parseInt(this.props.cqid),
                "content": "",
                "tombstone":0,
                "updtime":time.time(),
                "inittime":time.time(),
                "struct": []
            };
    },
    componentDidMount: function (){
        var self = this;
        this.pull();
        this.props.paper.composites[this.props.cqid] = {"tixu": this.tixu, "element": this.state, "component": this};
        self.props.paper.forceUpdate();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "content" in body != true ) body.content = "";
                if ( "struct" in body != true ) body.struct = [];
            }catch(e){
                console.warn(e); return false;
            }
            self.replaceState(body);
            self.props.paper.composites[self.props.cqid] = {"tixu": self.tixu, "element": body, "component": self};
            self.props.paper.forceUpdate();
        };
        this.api.pull(this.props.cqid, callback);
    },
    render: function (){
        var self = this;
        return (
            <div className="element" data-cqid={this.state.cqid.toString()} data-index={this.tixu}>
                <div className="body">
                    <span className="tixu">{this.tixu}</span>、（
                    <span className="flag">{this.name}</span>）
                    <span className="content" dangerouslySetInnerHTML={{__html: this.state.content}} />
                </div>
                <div className="elements">
                    { this.state.struct.map(function(elem, index){
                        if ( "qid" in elem ){
                            return <window.components.questions.preview 
                                        key={"question_" + elem.qid.toString()}
                                        qid={elem.qid.toString()} 
                                        paper={self.props.paper} 
                                        index={self.props.index.concat(index+1)} />
                        } else if ( "cqid" in elem ){
                            return <window.components.composites.preview 
                                        key={"composite_" + elem.cqid.toString() }
                                        cqid={elem.cqid.toString()} 
                                        paper={self.props.paper} 
                                        index={self.props.index.concat(index+1)} />
                        } else {
                            console.warn("unknow element type.");
                            return <div className="element">Sync Element Fail.</div>;
                        }
                    }) }
                </div>
            </div>
        );
    }
});