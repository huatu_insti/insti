if ( !window.components ) window.components = {};
if ( !window.components.user ) window.components.user = {};
if ( !window.components.user.favors ) window.components.user.favors = {};

window.components.user.favors.tree = React.createClass({
    displayName: "components.user.favors.tree",
    name: "我的收藏记录",
    api: new apis.user.records.favors(),
    size: 20,
    page: 1,
    getInitialState: function (){
        // props: page / size / parent / root
        return { "catalogs": [], "total": 0 };
    },
    componentDidMount: function (){
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            /*
            {
                "catalogs": [
                    {
                        "superCid": 1195
                        "cid": 1234,
                        "cname": "me",
                        "totalNum": 1,
                        "rightNum": 0,
                        "rightRate": 0.0
                    }
                ]
            }
            */
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
                if ( "total" in body != true ) body.total = 0;
            }catch(e){
                console.warn(e); return false;
            }
            self.replaceState(body);
        };
        this.api.fetch( "0", callback );
    },
    render: function (){
        var self = this;
        var tips = (React.createElement("div", {className: "pra_list f1"}, "你还没有收藏试题 "));
        if ( this.state.catalogs.length != 0 ) tips = null;

        return (
            React.createElement("div", {className: "pra_main pra_main_action"}, 
                React.createElement("ul", {className: "clearfix"}, 
                    tips, 
                     this.state.catalogs.map(function (catalog, index){
                        return (
                            React.createElement(window.components.user.favors.leaf, {
                                            key: "tree_of_favors_cid_"+catalog.cid+"_index_"+index+"_", 
                                            index: [index+1], 
                                            catalog: catalog, 
                                            root: self.props.root, 
                                            parent: self})
                        );
                    }) 
                )
            )
        );
    }
});


window.components.user.favors.leaf = React.createClass({
    displayName: "components.user.favors.tree",
    name: "叶子",
    api: new apis.user.records.favors(),
    size: 20,
    page: 1,
    getInitialState: function (){
        // props: parent / root / catalog
        var catalog = this.props.catalog;
        if ( "catalogs" in catalog != true ) catalog.catalogs =  [];
        return catalog;
    },
    componentDidMount: function (){
        // this.fetch();
    },
    fetch: function (e, rid){
        var self = this;
        if ( self.props.index.length == 3 ) return null;

        var target = $(e.target);
        if ( $(e.target).hasClass("noa") ){
            
        } else {
            $(e.target).removeClass("a").addClass("noa");
            self.setState({"catalogs": [] });
            return;
        }

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
                if ( "total" in body != true ) body.total = 0;
            }catch(ee){
                console.warn(ee); return false;
            }
            $(target).removeClass("noa").addClass("a");
            self.setState({"catalogs": body.catalogs});
        };
        this.api.fetch( this.state.cid, callback );
    },
    view: function (){
        var self = this;
        // self.props.root.components.favors = <window.components.user.favors.tree root={self} />;
        self.props.root.components.favors = React.createElement(window.components.user.favors.view.parse, {
                                                                                                                    root: self.props.root, 
                                                                                                                    cid: self.state.cid});
        self.props.root.component = self.props.root.components.favors;
        self.props.root.forceUpdate();
    },
    render: function (){
        var self = this;
        return (
            React.createElement("li", {"data-cid": this.state.cid}, 
                React.createElement("div", {className: "tk_tab_list"}, 
                    React.createElement("div", {className: "tk_tab_1 noa", 
                        onClick: self.fetch, 
                        dangerouslySetInnerHTML: {__html: [this.state.cname, "（共", this.state.totalNum, "道题）"].join("")}}), 
                    React.createElement("div", {className: "tk_tab_5"}, 
                        React.createElement("a", {className: "ti_a", onClick: self.view}, "查看题目")
                    )
                ), 
                React.createElement("ul", null, 
                     this.state.catalogs.map(function (catalog, index){
                        if ( self.props.index.length == 3 ) return null;
                        return (
                            React.createElement(window.components.user.favors.leaf, {
                                            key: "tree_of_favors_cid_"+catalog.cid+"_index_"+index+"_", 
                                            index: self.props.index.concat(index+1), 
                                            catalog: catalog, 
                                            root: self.props.root, 
                                            parent: self})
                        );
                    }) 
                )
            )
        );
    }
});
