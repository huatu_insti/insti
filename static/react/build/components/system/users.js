if ( !window.components ) window.components = {};
if ( !window.components.users ) window.components.users = {};

window.components.users.list = React.createClass({
    displayName: "UsersList",
    name: "用户列表",
    page: 1,
    size: 20,
    area: -9,
    getInitialState: function(){
        return {"users": [],"count": 0 };
    },
    componentDidMount: function (){
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = {};
                var users = JSON.parse(response.body);
                if ( "users" in body != true ) body.users = users;
                if ( "count" in body != true ) body.count = 0;
            }catch(e){
                console.warn(response); return;
            }
            self.replaceState(body);
        };
        requests.get("/user/list?page=#page#&size=#size#".replace("#page#", this.page).replace("#size#", this.size), {"async": true, "callback": callback});
    },
    update: function (obj, user, dialog){
            var self = this;
            var obj = $(obj).parent().parent();
            
            var uname = $(obj).find('input[name="uname"]').val();
            var password = $(obj).find('input[name="password"]').val();
            var repassword = $(obj).find('input[name="repassword"]').val();

            $(obj).find('input[name="uname"]').val(user.uname);

            if ( password != repassword ) {
                alert("两次密码输入不一致");return;
            }
            if ( password.length < 10 ) {
                alert("密码长度不能小于 10位");return;
            }
            var data = {"uuid": user.uuid, "uname": user.uname, "password": password, "repassword": repassword};
            var callback = function (response){
                if ( response.type == 'progress' ) return 'loading ...';
                if ( response.code != 200 ) {
                    console.warn(response); return;
                }
                console.log("user #uuid# update success.".replace("#uuid#", user.uuid) );
                $(dialog).dialog("close");
                self.fetch();
            };
            requests.post( "/user/update?uuid="+user.uuid, data, {"async": true, "callback": callback} );
    },
    remove: function (uuid, r, rid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            console.log(":: remove user " + uuid + " success.");
            self.fetch();
        };
        requests.METHOD_DELETE("/user/remove?uuid=" + uuid, {"async": true, "callback": callback});
    },
    edit: function (user, e, rid){
        var self = this;
        var dialog = $('<div class="lable_mark"></div>');

        var co = $( ('<div class="lable_head"><span>编辑用户</span><img  src="/static/images/add_test_close.png" /></div>'
            + '<div class="lable_body">' 
            +   '<div class="lable_name"><span>用户名</span><input  type="text" name="uname" value="#UNAME#" /></div>'
            +   '<div class="lable_name"><span>密码</span><input  type="password" name="password" /></div>'
            +   '<div class="lable_name"><span>重复密码</span><input  type="password" name="repassword" /></div>'
            + '</div>').replace("#UNAME#", user.uname) );

        var update_wrap = function (){
            self.update(this, user, dialog);
        };
        var cb = $('<div class="lable_footer"></div>').append(
            $('<a class="modal_cannal">取消</a>').on("click", function (){ $(dialog).dialog("close"); })
        ).append(
            $('<a class="modal_ok">提交</a>')
                .on( "click", update_wrap)
        );

        dialog.append(co).append(cb);

        $(dialog).dialog({
            "title": "编辑用户",
            "width": 420,
            "height": 300
        });

    },
    add: function (e, rid){
        var self = this;


        var dialog = $('<div class="lable_mark"></div>');

        var co = $( '<div class="lable_head"><span>添加用户</span><img  src="/static/images/add_test_close.png" /></div>'
            + '<div class="lable_body">' 
            +   '<div class="lable_name"><span>用户名</span><input  type="text" name="uname" /></div>'
            +   '<div class="lable_name"><span>密码</span><input  type="password" name="password" /></div>'
            +   '<div class="lable_name"><span>重复密码</span><input  type="password" name="repassword" /></div>'
            + '</div>');
        var add_user = function (){
            var uname = $(this).parent().parent().find('input[name="uname"]').val();
            var password = $(this).parent().parent().find('input[name="password"]').val();
            var repassword = $(this).parent().parent().find('input[name="repassword"]').val();
            // check
            if ( uname.length < 4 ) {
                alert("用户名长度不能小于 4");return;
            }
            if ( password != repassword ) {
                alert("两次密码输入不一致");return;
            }
            if ( password.length < 10 ) {
                alert("密码长度不能小于 10位");return;
            }
            var data = {"uname": uname, "password": password, "repassword": repassword};
            var callback = function (response){
                if ( response.type == 'progress' ) return 'loading ...';
                if ( response.code != 200 ) {
                    console.warn("subuser-add: fail");
                    return;
                }
                console.info("subuser-add: success.");
                $(dialog).dialog("close");
                self.fetch();
            };
            requests.post( "/user/add", data, {"async": true, "callback": callback} );

        };
        var cb = $('<div class="lable_footer"></div>').append(
            $('<a href="javascript:;" class="modal_cannal">取消</a>').on("click", function (){ $(dialog).dialog("close"); })
        ).append(
            $('<a href="javascript:;" class="modal_ok">添加</a>').on( "click", add_user )
        );

        dialog.append(co).append(cb);

        $(dialog).dialog({
            "title": "添加子用户",
            "width": 420,
            "height": 300
        });

    },
    onSetPage: function (page, e){
        var self = this;

        if ( ! page || (parseInt(page) < 1  && page != "goto" ) ) return false;
        if ( page == "goto" ) page = parseInt($(e.target).val());
        if ( parseInt(page) == parseInt(this.page) ) return true;
        if ( parseInt(page) > 0 && parseInt(page) < Math.ceil(this.state.count/this.size)+1 ){
            this.page = parseInt(page);
            this.fetch();
        }
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "用户列表"), 
                    React.createElement("div", {className: "input_test"}, React.createElement("a", {onClick: this.add}, "添加用户"))
                ), 
                React.createElement("div", {className: "paper_sort_main"}, 
                    React.createElement("div", {className: "paper_sort_title"}, 
                        React.createElement("span", null, "用户名称："), 
                        React.createElement("input", {type: "text", placeholder: "请输入用户名称", className: "inp_search"}), 
                        React.createElement("a", {className: "sort_search", href: "javascript:;"}, "搜索")
                        
                    ), 
                    React.createElement("table", {className: "tab_01", bordercolor: "#ddd", border: "1px", align: "left", cellSpacing: "0", cellPadding: "0", width: "100%"}, 
                       React.createElement("thead", null, 
                            React.createElement("tr", null, 
                                React.createElement("th", null, "序号"), 
                                React.createElement("th", null, "昵称"), 
                                React.createElement("th", null, "帐号"), 
                                React.createElement("th", null, "角色"), 
                                React.createElement("th", null, "添加时间"), 
                                React.createElement("th", null, "操作")
                           )
                        ), 

                        React.createElement("tbody", {id: "tid", align: "center"}, 
                             this.state.users.map(function (user, index){
                                return (
                                    React.createElement("tr", {"data-uuid": user.uuid, key: index+1+user.name}, 
                                        React.createElement("td", null, index+1), 
                                        React.createElement("td", null, user.name), 
                                        React.createElement("td", null, user.uname), 
                                        React.createElement("td", null, user.role), 
                                        React.createElement("td", null, time.ctime(user.ctime)), 
                                        React.createElement("td", {className: "sort_operate"}, 
                                            React.createElement("button", {type: "button", className: "btn btn-default"}, 
                                                React.createElement("a", {onClick: self.edit.bind(self, user) }, "编辑")
                                            ), 
                                            React.createElement("button", {type: "button", className: "btn btn-default"}, 
                                                React.createElement("a", {onClick: self.remove.bind(self, user.uuid)}, "删除")
                                            )
                                        )
                                    )
                                );
                            }), 
                            
                            React.createElement("tr", {key: "users_list_nav___"}, 
                                React.createElement("td", {colSpan: "6"}, 
                                    React.createElement("div", {className: "page_left"}, React.createElement("span", null, "共", this.state.count, "条记录，每页", this.size, "条，共", Math.ceil(this.state.count/this.size), "页")), 
                                    React.createElement("div", {className: "page_right"}, 
                                        React.createElement("span", {className: "page_sp"}, 
                                            "跳转至第          ", 
                                            React.createElement("input", {type: "text", className: "page_num", 
                                                        placeholder: this.page+1, 
                                                        onChange: this.onSetPage.bind(self, "goto") }), "页，" + ' ' +
                                            "页数 ", this.page, " / ", Math.ceil(this.state.count/this.size)
                                        ), 
                                        React.createElement("a", {className: "page_top", onClick: this.onSetPage.bind(self, this.page-1) }, "<"), 
                                        React.createElement("a", {className: "page_down", onClick: this.onSetPage.bind(self, this.page+1) }, ">")
                                    )
                                )
                            )
                       )
                  )
                )
            )
            );
    }
});
