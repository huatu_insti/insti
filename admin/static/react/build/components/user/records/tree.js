if ( !window.components ) window.components = {};
if ( !window.components.user ) window.components.user = {};
if ( !window.components.user.record ) window.components.user.record = {};

window.components.user.record.tree = React.createClass({
    displayName: "components.user.record.tree",
    name: "我的练习记录",
    api: new apis.user.records.tests(),
    size: 10,
    page: 1,
    getInitialState: function (){
        // props: page / size / parent / root
        if ( this.props.page && parseInt(this.props.page) > 0 ) this.page = this.props.page;
        if ( this.props.size && parseInt(this.props.size) < 5 ) this.size = this.props.size;
        return { "records": [], "unfinished": [], "total": 0 };
    },
    componentDidMount: function (){
        this.fetch();
        this.fetch_unfinished();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "records" in body != true ) body.records = [];
                if ( "total" in body != true ) body.total = 0;
            }catch(e){
                console.warn(e); return false;
            }
            self.setState( {"records": body.records} );
            self.setState( {"total": body.total } );
        };
        this.api.fetch( {"size": this.size, "page": parseInt(this.page)-1}, callback );
    },
    fetch_unfinished: function (){
        // 获取未完成的练习记录
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "records" in body != true ) body.records = [];
                if ( "total" in body != true ) body.total = 0;
            }catch(e){
                console.warn(e); return false;
            }
            self.setState( {"unfinished": body.records.slice(0, 5) } );
            // self.setState( {"total": body.total+self.state.total} );
        };
        var size = 5;     // this.size
        var page = 0;   // parseInt(this.page)-1
        this.api.unfinished( {"size": size, "page": page }, callback );
    },
    fly: function (page){
        var self = this;
        if ( parseInt(page) < 1 )  return false;
        console.info("Total: " + Math.ceil( self.state.total/self.size) );
        if ( parseInt(page) > Math.ceil( self.state.total/self.size) ) return false;
        this.page = parseInt(page);
        this.fetch();
    },
    goReport: function (tid){
        var self = this;
        var indexbox = self.props.root.props.root;
        console.log(indexbox);
        indexbox.setState({
                "content": React.createElement(window.components.tests.report.test, {
                                                        tid: tid, 
                                                        tab: "report", 
                                                        root: indexbox, 
                                                        indexbox: indexbox})
        });
    },
    goParse: function (tid){
        var self = this;
        var indexbox = self.props.root.props.root;
        indexbox.setState({
                "content": React.createElement(window.components.tests.report.test, {
                                                        tid: tid, 
                                                        tab: "parse", 
                                                        root: indexbox, 
                                                        indexbox: indexbox})
        });
    },
    goAnswer: function (tid){
        var self = this;
        var indexbox = self.props.root.props.root;
        
        indexbox.setState({
                    "content": React.createElement(window.components.tests.answer.test, {
                                            tid: tid, 
                                            root: indexbox, 
                                            indexbox: indexbox})
        });
    },
    render: function (){
        var self = this;
        // 计算分页
        var prePage = [];
        if ( self.page > 1 ) {

        }
        
        var tips1 = (React.createElement("div", {className: "pra_list f1"}, "完成练习是种美德，我们只留5条记录"));
        var tips2 = (React.createElement("div", {className: "pra_list f1"}, "你还没有已完成的测试数据哦 "));
        
        if ( this.state.unfinished.length == 0 ) tips1 = (React.createElement("div", {className: "pra_list f1"}, "太好了，你没有未完成的测试 ... "));
        if ( this.state.records.length != 0 ) tips2 = null;

        // <a href="#" className="pra_btn" data-tid ="">继续练习</a>
        return (
                React.createElement("div", {className: "pra_main", style: {"overflow": "hidden"}}, 
                     tips1, 
                     this.state.unfinished.map(function (record, index){
                        return (
                            React.createElement("div", {className: "pra_list", "data-tid": record.tid}, 
                                React.createElement("div", {className: "pra_list_left"}, 
                                    React.createElement("div", null, record.tname), 
                                    React.createElement("span", null, "交卷时间：", time.ctime(record.time))
                                ), 
                                React.createElement("a", {
                                    className: "pra_a", 
                                    onClick: self.goAnswer.bind(self, record.tid)}, "继续练习")
                            )
                        );
                    }), 
                     tips2, 
                     this.state.records.map(function (record, index){
                        return (
                            React.createElement("div", {className: "pra_list", "data-tid": record.tid}, 
                                React.createElement("div", {className: "pra_list_left"}, 
                                    React.createElement("div", null, record.tname), 
                                    React.createElement("span", null, "交卷时间：", time.ctime(record.time))
                                ), 
                                React.createElement("a", {
                                    className: "pra_a", 
                                    onClick: self.goReport.bind(self, record.tid)}, "查看报告"), 
                                React.createElement("a", {
                                    className: "pra_a", 
                                    onClick: self.goParse.bind(self, record.tid)}, "查看解析")
                            )
                        );
                    }), 
  
                    React.createElement("div", {className: "page_div"}, 
                        React.createElement("div", {className: "right"}, 
                            range(0, (self.page - 1) ).map(function (p, i){
                                if ( p == 0 ) {
                                    return (
                                        [(React.createElement("a", {onClick: self.fly.bind(self, self.page-1)}, " < ")) ,
                                        (React.createElement("a", {onClick: self.fly.bind(self, p+1)}, " ", p+1, " "))]
                                    );
                                } else {
                                    return (
                                        React.createElement("a", {onClick: self.fly.bind(self, p+1)}, p+1)
                                    );
                                }
                            }), 

                             range(self.page, Math.ceil( self.state.total/self.size) +1 ).map(function (p, i){
                                if ( parseInt(p) == parseInt(self.page) ){
                                    return (React.createElement("span", {className: "current"}, p));
                                } else if ( parseInt(p) == Math.ceil( self.state.total/self.size)  ){
                                    return (React.createElement("a", {onClick: self.fly.bind(self, p)}, " > "));
                                } else {
                                    return (React.createElement("a", {onClick: self.fly.bind(self, p)}, p));
                                }
                            }) 
                        )
                    )
                    
                )
        );
    }
});
