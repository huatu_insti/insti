
if ( !window.components ) window.components = {};
if ( !window.components.papers ) window.components.papers = {};

window.components.papers.preview =  React.createClass({
    displayName: "components.papers.preview",
    name: "试卷预览",
    api: new apis.papers(),
    questions: {},
    composites: {},
    tixu_card: [],
    getInitialState: function (){

        window.paper_preview = this;
        return {"pid": 0, "name": "", "score": 0, "struct": [ ] };
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);
            }
            try{
                self.replaceState(JSON.parse(response.body));
              
            }catch(e){
                console.warn(e);
            }
        };
        this.api.pull( parseInt(this.props.pid), callback);
    },
    componentDidMount: function (){
        this.pull();
    },
    total: function (t){
        var self = this;
      
        if ( t == "elements" ) {
              return Object.keys(self.questions).length;
            // return Object.keys(self.composites).length + Object.keys(self.questions).length;
        } else if ( t == "score" ) {
            var score = 0;
            Object.keys(self.questions).map(function(qid, index){
                score += parseFloat(self.questions[qid].element.score);
            });
            return score;
        }
    },
    mk_tixu_card: function (){
        // 题序卡
        var self = this;
        var tixu_card = [];

        var parse_question = function (question){
            if ( parseInt(question.qid) in self.questions ) {
                tixu_card.push(self.questions[parseInt(question.qid)] );
            }
        };
        var parse_composite = function (composite){
            if ( composite.cqid in self.composites ) {
                tixu_card.push(self.composites[parseInt(composite.cqid)] );
            }
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };

        var i = 0;
        for (i=0; i<this.state.struct.length; i++ ) {
            if ( "qid" in this.state.struct[i] == true) {
                parse_question(this.state.struct[i]);
            } else if ( "cqid" in this.state.struct[i] == true) {
                parse_composite(this.state.struct[i]);
            } 
        }
        return tixu_card;
    },
    gotoReview:function(){
        var self = this;
        self.props.root.setState({"content":React.createElement(window.components.papers.review, {pid: self.state.pid, root: self.props.root})})
    },
    backToLastStep:function(){
        var self = this;
        self.props.root.setState({"content":React.createElement(window.components.papers.paper, {pid: self.state.pid, tixu_card2: [], root: self.props.root}) })
    },
    auditing:function(){
       var self = this;
       self.props.root.setState({"content":React.createElement(window.components.papers.review, {pid: self.state.pid, root: self.props.root})})
    },
    computeQuestion:function(element){
        var pos = 0;
        var num = 0;
        var self = this;
        var parse_question = function(question){
             num += 1; 
             if( "qid" in element == true && parseInt(question.qid) == parseInt(element.qid)){ pos = num;}
        }
        var parse_composite = function( composite ){
            
            if ( "cqid" in element == true && parseInt(element.cqid) == parseInt(composite.cqid) ) pos = num+1;

            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }
        }
        var paperStruct = self.state.struct;
        for( var i=0;i<paperStruct.length;i++){
             if("qid" in paperStruct[i] ) {
                  parse_question(paperStruct[i]);
             }else if("cqid" in paperStruct[i]){
                  parse_composite(paperStruct[i]);
             }
        }
    
       return pos;

    },
     goTo: function (event){
        var tixu = $(event.target).data("kaxu");
        var t = $('[data-tixu="#TIXU#"]'.replace("#TIXU#", tixu)).position().top;
        var paperScrollTop = $("#paper").scrollTop(); 
        t = t+paperScrollTop;
        $('#paper').animate({scrollTop:t},700)
    },
     handleCard:function(){
        $(".fastBottom").toggle()
    },
    render: function (){
        var self = this;
        tixu_card = this.mk_tixu_card();
        

        return ( React.createElement("div", null, 
            React.createElement("div", {style: {"overflow-y":"scroll","height":"701px","position":"relative"}, id: "paper"}, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "试卷预览"), 
                    React.createElement("ul", {className: "clearfix"}, 
                        React.createElement("li", {className: "create_off"}, "1.创建试卷"), 
                        React.createElement("li", {className: "create_off"}, "2.录入试卷"), 
                        React.createElement("li", {className: "create_on"}, "3.预览试卷"), 
                        React.createElement("li", {className: "create_off"}, "4.提交审核")
                    ), 
                    React.createElement("div", {className: "return_list", onClick: self.backToLastStep}, React.createElement("a", null, "上一步")), 
                    React.createElement("div", {className: "input_test", onClick: self.auditing}, React.createElement("a", null, "提交审核"))
                ), 
                React.createElement("div", {className: "pre_title"}, 
                    React.createElement("h3", {dangerouslySetInnerHTML: {__html: "试卷名称：" + this.state.name}}), 
                    React.createElement("span", null, "总分：", this.total("score")), 
                    React.createElement("span", {style: {"marginLeft":"12px"}}, "题量：", this.total("elements"))
                ), 
                React.createElement("div", {className: "pre_group"}
                  
                ), 
                React.createElement("div", {className: "elements"}, 
                     this.state.struct.map(function(elem, index){
                        if ( "qid" in elem ){

                            return React.createElement(window.components.questions.preview, {
                                        key: "preview_question_" + elem.qid.toString(), 
                                        qid: elem.qid.toString(), 
                                        paper: self, 
                                        tixu: self.computeQuestion(elem), 
                                        index: [index+1]})
                        } else if ( "cqid" in elem ){
                            return React.createElement(window.components.composites.preview, {
                                        key: "preview_composite_" + elem.cqid.toString(), 
                                        cqid: elem.cqid.toString(), 
                                        paper: self, 
                                        index: [index+1].concat(index+elem.struct.length)})
                                        
                        } else {
                            console.warn("unknow element type.");
                            return React.createElement("div", {className: "element"}, "Sync Element Fail.");
                        }
                    }) 
                )
             ), 
             React.createElement("div", {className: "fast_card"}, 
                    React.createElement("div", {className: "fastBtn"}, 
                        React.createElement("div", {className: "fastShowBtn"}, 
                            React.createElement("div", {className: "fastInfo", onClick: self.handleCard}, "试题卡(共计", React.createElement("span", null, this.total("elements")), "道题)")
                    ), 
                    React.createElement("div", {className: "fastLine"}), 
                    React.createElement("div", {className: "fastBottom"}, 
                        React.createElement("ul", null, 
                        tixu_card.map(function(element,index){
                               if(typeof element.tixu == 'number'){
                                   if(element.tixu % 5 == 0 ){
                                       return React.createElement("li", {style: {"marginRight":"30px"}}, 
                                                React.createElement("a", {"data-kaxu": element.tixu, onClick: self.goTo}, element.tixu))

                                   }else{
                                       return React.createElement("li", {"data-kaxu": element.tixu}, 
                                                 React.createElement("a", {"data-kaxu": element.tixu, onClick: self.goTo}, element.tixu))
                                   }
                                
                                 
                               }
                         })
                        )
                    )
                  )
            )
            )
            );
    }
});


/*
    Status Code:
        - EDITING = 0           编辑中
        - PREPARING = 1     待审核
        - REVIEWING = 2     审核中
        - PUBLIC = 3              发布
    Request Method: PATCH
*/
window.components.papers.review = React.createClass({
    displayName: "components.papers.review",
    name: "试卷审核",
    api: new apis.papers(),
    getInitialState: function (){
        if ( !this.props.pid || parseInt(this.props.pid) < 1 ) throw new Error("window.components.papers.review error.");
        window.paper_review = this;
        
        return {"pid": this.props.pid, "status": 0, "attrs": [
            {"name": "year", "value": ""},
            {"name": "area", "value": ""},
        ], 
        "struct": []
        }
    },
    componentDidMount: function (){
        this.updateStatus();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return ;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                console.warn(e);
            }
        };
        this.api.pull( parseInt(this.props.pid), callback);
    },
    updateStatus: function (){
        var self = this;
        var xmen = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(":: window.components.papers.review.updateStatus fail");
                console.warn(response);
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                console.warn(e);
            }
        };
        this.api.updateStatus(this.props.pid, "update", xmen );
    },
    goHome: function (){
        var self = this;
        self.props.root.setState({"content":React.createElement(window.components.papers.list, {page: "1", size: "20", root: self.props.root})})
    },
    backToLastStep:function(){
        var self = this;
        self.props.root.setState({"content":React.createElement(window.components.papers.preview, {pid: self.props.pid, root: self.props.root})})
    },
    render: function (){
        var self = this;
        var name, year, province_id, questions=0;

        name = this.state.name;
        year = this.state.attrs.filter(function(t, i){
            return t.name == "year";
        })[0].value;

        province_id = this.state.attrs.filter(function(t, i){
            return t.name == "area";
        })[0].value;

        // 计算试卷试题总量
        var compute_total = function (elem){
            if ( "struct" in elem != true )  elem.struct = [];
            questions += elem.struct.length;
            var i = 0;
            for (i=0; i<elem.struct.length; i++ ) {
                if ( "struct" in elem.struct[i] == true) {
                    compute_total(elem.struct[i]);
                }
            }
        };
        compute_total(this.state);

        var message_dict = { 
                "0": "正在提交审核 ... ", 
                "1": "您的试卷已提交成功，请等待审核", 
                "2": "您的试卷已经通过审核", 
                "3": "你的试卷已经发布" 
        };
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "提交审核"), 
                    React.createElement("ul", {className: "clearfix"}, 
                        React.createElement("li", {className: "create_off"}, "1.创建试卷"), 
                        React.createElement("li", {className: "create_off"}, "2.录入试卷"), 
                        React.createElement("li", {className: "create_off"}, "3.预览试卷"), 
                        React.createElement("li", {className: "create_on"}, "4.提交审核")
                    ), 
                    React.createElement("div", {className: "return_list", onClick: self.backToLastStep}, "上一步"), 
                    React.createElement("div", {className: "input_test", onClick: this.goHome}, "完成")
                ), 

                React.createElement("div", {className: "submit_con"}, 
                    React.createElement("div", {className: "submit_status"}, message_dict[this.state.status.toString()]), 
                    React.createElement("div", {className: "submit_info"}, 
                        React.createElement("p", null, "试卷名称：", name), 
                        React.createElement("p", null, "试卷年份：", year, "年"), 
                        React.createElement("p", null, "试卷地区：", province_id), 
                        React.createElement("p", null, "试卷题量：", questions), 
                        React.createElement("p", null, "试卷分数：000分（暂不统计） ")
                    )
                )
            )
            );
    }
});
