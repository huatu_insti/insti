<?php

/*
 后台认证
 */
require_once __DIR__.'/../session.php';

class  Auth {
   private static $sqlite_db_path = "../db.sqlite3";  //sqlite文件数据库存放路径(后台用户数据库)
   public static function signin($account,$password){
           if(!file_exists(self::$sqlite_db_path)){ exit("找不到数据库文件");}
           $db = new PDO("sqlite:".self::$sqlite_db_path);
           $sql = 'select uuid,uname,passwd,appid,appkey from Auth where uname=?';
           // $sql = 'UPDATE Auth SET passwd=? where uname="admin"';
           $sth = $db->prepare($sql);
           //执行预处理语句,(预处理语句可以防止sql注入);
           $sth->execute(array("admin")); 
           $userInfo = $sth->fetch(PDO::FETCH_ASSOC); 
           if($userInfo){ 
                if ( $userInfo['passwd'] == $password ){ 
                         $token = Token::get($userInfo['appid'],$userInfo['appkey']);  
                         $result = array("token"=>(string)$token['token'],"uname"=>(string)$userInfo['uname']);
                         //写入cookie
                         setcookie("insti_admin",json_encode($result),time()+24*60*60,"/","",0);
                         try{
                               Header("Content-Type: application/json;");
                         }catch(Exception $e) {};
                         echo json_encode( array('code'=>200,'info'=>'登录成功') );
                         exit();
                }else{
                          try{
                              Header("Content-Type: application/json;");
                          }catch(Exception $e) {};
                          http_response_code(403);
                          echo json_encode( array('code'=>403,'info'=>'帐号不存在或密码有误!') );
                          exit();
                }
            }else{
                          try{
                              Header("Content-Type: application/json;");
                          }catch(Exception $e) {};
                          http_response_code(403);
                          echo json_encode( array('code'=>403,'info'=>'帐号不存在或密码有误!') );
                          exit();
           }
          
   }
           
    public static function logout(){
        setcookie("insti_admin", "" , time()-3600, "/", "", 0);
        http_response_code(200);
        echo json_encode( array('code'=>200,'info'=>'登出成功') );
      
    }
}


                        
                        

                       

//后台用户的token
class Token {
     // public static $host = "sy.tiku.huatu.com";
     public static $host = '115.29.177.200';
     public static function get($appid,$appkey){
          //获取指定appid的token
             $host = self::$host;
             $url = "http://{$host}/auth/backends/{$appid}";
             // class `Requests` from `session.php`;
             $response = Requests::get($url); 
             if ( (int)$response['code'] == 200) {
                 $body = json_decode($response['body'],true);
                 $token_verify_result = Token::verify($body["appid"],$body["appkey"],$body["token"]); 
                
                 if(!$token_verify_result){  
                     $newToken = Token::gen($appid,$appkey); 
                     if(!$newToken){ 
                          try{
                                Header("Content-Type: application/json;");
                          }catch(Exception $e) {};
                          http_response_code(403);
                          echo json_encode( array('code'=>403,'info'=>'更新token失败!') );
                          exit();
                      }
                    return $newToken;
                  }else{
                    return $body;
                  }
                 
                 
                 
             }else{
                        try{
                          Header("Content-Type: application/json;");
                          }catch(Exception $e) {};
                          http_response_code(403);
                          echo json_encode( array('code'=>403,'info'=>'获取token失败!') );
                          exit();
             }
     }
     public static function gen($appid,$appkey){ 

          //生成新的token给指定的appid和appkey。
             $host = self::$host;
             $url  = "http://{$host}/auth/backends/token/gen";
             $data = array("appid"=>(int)$appid,"appkey"=>(string)$appkey);
             // exit(json_encode($data));
             $response = Requests::patch( $url, $data ); 
             if ( (int)$response['code'] == 200 ){
                  $body = json_decode( $response['body'], true );
                  if ( !is_array($body) ) return false;
                  return $body;
              } else return false;
         
     }
     public static function verify($appid,$appkey,$token){  
        
            //验证指定appid的传入token是否有效
            $host = self::$host;
            $url  = "http://{$host}/auth/backends/token/verify";
            $data =  array("appid"=>(int)$appid,"appkey"=>(string)$appkey,"token"=>(string)$token);
            // exit(json_encode($data));
            $response = Requests::patch($url,$data);   
            // echo json_encode($response);exit();
            if ( (int)$response['code'] == 200 ){
                 $body = json_decode( $response["body"],true);
                 if (! is_array($body)) return false;
                 return true;
                 
            }else{
                 return false;
            }

     }
}


function main ($http_body=null){
    if ( $http_body == null ) $http_body = json_decode( file_get_contents('php://input'), true );
    if (isset($http_body['hook']) && $http_body['hook'] == "auth.signin" ){
        //登陆
        Auth::signin($http_body['data']['account'], $http_body['data']['password']);
    }
    if(isset($http_body["hook"]) && $http_body["hook"] == "auth.logout"){
        Auth::logout();
    }
}         
          
// Auth::signin("admin","123"); 
//文件入口 
if ( isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) ) {
       main();    

}else{
      exit("access not allowed");
}







          

      
         

           
      
                 
           

  
     