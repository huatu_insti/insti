/*

2.2.6 测试操作
获取单个测试
Operation
GET
 /tests/:tid
Request Sample
curl “http://115.159.4.84:9123/tests/1”
初始化单个“错题练习”测试
Operation
PUT
 /tests/factory/errors
Request Sample
curl
-X PUT
--header “Content-type:application/json
-d ‘{
“name” : “错题重练名称”,
“secretid” : 1,
“num” : 15,
“duration” : 900
}’
“http://115.159.4.84:9123/tests/factory/errors”
初始化单个“强化练习”测试
Operation
PUT
Request/tests/factory/catalog
Sample
curl
-X PUT
--header “Content-type:application/json
-d ‘{
“name” : “强化练习名称”,
“cid” : 1,
“secretid" : 1,
“num” : 15,
“duration” : 900
}’
“http://115.159.4.84:9123/tests/factory/catalog”

初始化单个“考试练习”测试
Operation
PUT
 /tests/factory/exam
Request Sample
curl
-X PUT
--header “Content-type:application/json
-d ‘{ “name” : “考试名称”, “eid” : 1, “secretid" : 1 }’
“http://115.159.4.84:9123/tests/factory/exam”
结束单个测试
Operation
PATCH
 /tests/factory/exam
Request Sample
curl -X PATCH “http://115.159.4.84:9123/tests/1/end”
提交用用户答案
Operation
PATCH
 /answers/:tid
Request Sample
curl
-X PATCH
--header “Content-type:application/json
-d ‘{
“time” : 1422619200,
“answers" : [
{“qid”:1, “qans”:”A”, “uans”:”C”, “time”: 1422619201},
{“qid”:2, “qans”:”B”, “uans”:”A”, “time”: 1422619202}
]
}’
“http://115.159.4.84:9123/answers/1”
获取用用户答案
Operation
GET
 /answers/:tid
Request Sample
curl “http://115.159.4.84:9123/answers/1” 



*/
