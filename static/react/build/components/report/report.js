if ( !window.components ) window.components = {};
if ( !window.components.report ) window.components.report = {};

window.components.report.report = React.createClass({
    displayName: "components.report.report",
    name: "能力评估报告中心",
    api: new apis.report(),
    getInitialState: function (){
        // props: cid / root
        return {
            "preScoreInfo": {
                "preScore": 0.00,
                "preScoreAVG": 0.00000001,
                "preDefeat": 0.0
            },
            "areaPreScoreInfo": {
                "areaPreScore": 0.00,
                "areaPreScoreAVG": 0.00000001,
                "areaDefeat": 0.00
            },
            "ansQuesInfo": {
                "ansQues": 0,
                "ansQuesAVG": 0,
                "ansQuesDefeat": 0.00000001
            },
            "ansTimeInfo": {
                "ansTime": 0.00,
                "ansTimeAVG": 0,
                "ansTimeDefeat": 0.00
            }
        };
    },
    componentWillUnmount: function (){
        var self = this;
    },
    componentDidMount: function (){
        this.pull();
    },
    animate: function (){
        // 动画效果
        var self = this;
        var obj1 = document.getElementById('box');
        var obj2 = document.getElementById('box1');
        // 动画 一
        var tkeep1 = [];                    // 计时器
        obj1.innerHTML ="0分";
        var score, id1, id2;
        var i = 0;
        var animate1 = function (){
            i++
            if(i > self.state.preScoreInfo.preScore){
                score = self.state.preScoreInfo.preScore;
                i = Math.round(self.state.preScoreInfo.preScore);
                tkeep1.map(function (tk){
                    clearInterval(tk);
                });
            } else {
                score = i;  
            }
            var imgLeft = -(i*160+(i*10))+'px'
            obj1.style.backgroundPosition = imgLeft+'\t'+'0px'
            obj1.innerHTML =score+"分";
        };
        tkeep1.push(setInterval(animate1,50));
        // 动画 二
        var tkeep2 = [];                    // 计时器
        obj2.innerHTML ="0分";
        i = 0; score = 0;
        var animate2 = function (){
            i++
            if(i > self.state.areaPreScoreInfo.areaPreScore){
                score = self.state.areaPreScoreInfo.areaPreScore;
                i = Math.round(self.state.areaPreScoreInfo.areaPreScore);
                tkeep2.map(function (tk){
                    clearInterval(tk);
                });
            } else {
                score = i;  
            }
            var imgLeft = -(i*160+(i*10))+'px'
            obj2.style.backgroundPosition = imgLeft+'\t'+'0px'
            obj2.innerHTML =score+"分";
        };
        tkeep2.push(setInterval(animate2,50));
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return;
            }
            self.replaceState(body);
            self.animate();
        };
        this.api.fetch(callback);
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "ti_container"}, 
                React.createElement("div", {className: "rep_content"}, 
                React.createElement("div", {className: "rep_top clearfix"}, 
                    React.createElement("div", {className: "rep_divine"}, 
                        React.createElement("div", {className: "rep_divi_title"}, "您的全国预测分"), 
                        React.createElement("div", {className: "rep_score item_01"}, 
                            React.createElement("div", {id: "box"}, " 0 分")
                        ), 
                        React.createElement("div", {className: "rep_le"}, "同类考生平均预测分 ", 
                            React.createElement("span", {
                                dangerouslySetInnerHTML: {__html: Math.round(this.state.preScoreInfo.preScoreAVG*100)/100}})
                        ), 
                        React.createElement("div", {className: "rep_rg"}, 
                            "已经击败同类考生", 
                            React.createElement("span", {
                                dangerouslySetInnerHTML: {__html: (Math.round(this.state.preScoreInfo.preDefeat*100*100)/100).toString() + "%"}})
                        )
                    ), 
                    React.createElement("div", {className: "rep_divine"}, 
                        React.createElement("div", {className: "rep_divi_title"}, "您的地区预测分"), 
                        React.createElement("div", {className: "rep_score item_02"}, 
                            React.createElement("div", {id: "box1"}, " 0 分")
                        ), 
                        React.createElement("div", {className: "rep_le"}, "同类考生平均预测分 ", 
                            React.createElement("span", {
                                dangerouslySetInnerHTML: {__html: Math.round(this.state.areaPreScoreInfo.areaPreScoreAVG*100)/100}})
                        ), 
                        React.createElement("div", {className: "rep_rg"}, "已经击败同类考生", 
                            React.createElement("span", {
                                dangerouslySetInnerHTML: {__html: (Math.round(this.state.areaPreScoreInfo.areaDefeat*100*100)/100).toString() + "%"}})
                        )
                    ), 
                    React.createElement("div", {className: "rep_divine"}, 
                        React.createElement("div", {className: "rep_divi_title"}, "您的答题总量"), 
                        React.createElement("div", {className: "ans_count item_03"}, 
                            React.createElement("div", null, this.state.ansQuesInfo.ansQues, " 题")
                        ), 
                        React.createElement("div", {className: "rep_le"}, 
                            "同类考生平均答题量 ", 
                            React.createElement("span", {dangerouslySetInnerHTML: {__html: this.state.ansQuesInfo.ansQuesAVG + "题"}})
                        ), 
                        React.createElement("div", {className: "rep_rg"}, 
                            "已经击败同类考生", 
                            React.createElement("span", {
                                dangerouslySetInnerHTML: {__html: (Math.round(this.state.ansQuesInfo.ansQuesDefeat*100*100)/100).toString() + "%"}})
                        )
                    ), 
                    React.createElement("div", {className: "rep_divine"}, 
                        React.createElement("div", {className: "rep_divi_title"}, "您的答题速度"), 
                        React.createElement("div", {className: "rep_score item_04"}, 
                            React.createElement("div", {className: "sp_02", 
                                dangerouslySetInnerHTML: {__html: time.ptime(this.state.ansTimeInfo.ansTime).minutes.toString() 
                                                                                                + String.fromCharCode(8242) 
                                                                                                + time.ptime(this.state.ansTimeInfo.ansTime).seconds.toString() 
                                                                                                + String.fromCharCode(8243)}})
                        ), 
                        React.createElement("div", {className: "rep_le"}, "同类考生平均答题速度 ", 
                            React.createElement("span", {
                                dangerouslySetInnerHTML: {__html: time.ptime(this.state.ansTimeInfo.ansTimeAVG).minutes.toString() 
                                                                                                + String.fromCharCode(8242) 
                                                                                                + time.ptime(this.state.ansTimeInfo.ansTimeAVG).seconds.toString() 
                                                                                                + String.fromCharCode(8243)}})
                        ), 
                        React.createElement("div", {className: "rep_rg"}, "已经击败同类考生", 
                            React.createElement("span", {
                                dangerouslySetInnerHTML: {__html: (Math.round(this.state.ansTimeInfo.ansTimeDefeat*100*100)/100).toString() + "%"}})
                        )
                    )
                ), 
                
                    // 知识点掌握情况

                
                React.createElement(window.components.report.kpbox, {indexbox: this.props.root})

            )
        )
        );
    }
});


window.components.report.kpbox = React.createClass({
    displayName: "components.report.kpbox",
    name: "知识点掌握情况",
    api: new apis.report(),
    getInitialState: function (){
        // props: cid / indexbox
        var tab = React.createElement(window.components.report.kptree, {indexbox: this.props.indexbox});
        return { "tab": tab, "name": "kptree" };
    },
    componentWillUnmount: function (){
        // var self = this;
    },
    componentDidMount: function (){

    },
    showKpTree: function (){
        // 展示知识点能力量表
        var tab = React.createElement(window.components.report.kptree, {indexbox: this.props.indexbox});
        this.setState({"tab": tab, "name": "kptree" });
    },
    showKpGraph: function (){
        // 展示 知识点掌握热点图
        var tab = React.createElement(window.components.report.kpgraph, {indexbox: this.props.indexbox});
        this.setState({"tab": tab, "name": "kpgraph" });
    },
    render: function (){
        var self = this;

        return (
            React.createElement("div", {className: "rep_bottom"}, 
            React.createElement("div", {className: "rep_point"}, "知识点掌握情况"), 
            React.createElement("div", {className: "point_main"}, 
                React.createElement("ul", {className: "point_ul"}, 
                     [0, 1].map(function (a, b){
                        if ( a == 0 && self.state.name == "kptree" ) return (React.createElement("li", {className: "li_select"}, "知识点能力表"));
                        else if ( a == 0 && self.state.name != "kptree" ) return (React.createElement("li", {onClick: self.showKpTree}, "知识点能力表"));

                        if ( a == 1 && self.state.name == "kpgraph" ) return (React.createElement("li", {className: "li_select"}, "知识点掌握热点图"));
                        else if ( a == 1 && self.state.name != "kpgraph" ) return (React.createElement("li", {onClick: self.showKpGraph}, "知识点掌握热点图"));
                    }) 
                ), 
                 this.state.tab
            )
        )
        );
    }
});


window.components.report.kptree = React.createClass({
    // 组件
    displayName: "components.report.kptree",
    name: "知识点能力量表",
    api: new apis.report(),
    getInitialState: function (){
        // props: cid / indexbox
        return { "catalogs": [] };
    },
    componentWillUnmount: function (){
        var self = this;
    },
    componentDidMount: function (){
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
            }catch(e){
                console.warn(e); return;
            }
            self.replaceState(body);
        };
        this.api.kptree("0", callback);
    },
    render: function (){
        var self = this;

        return (
            React.createElement("div", {className: "point_01 point_biao "}, 
                React.createElement("div", {className: "tk_answer_list"}, 
                   React.createElement("div", {className: "tk_answer_list_top"}, 
                        React.createElement("div", {className: "tk_tab_1"}, "考点"), 
                        React.createElement("div", {className: "tk_tab_2"}, "答对题数 / 总数"), 
                        React.createElement("div", {className: "tk_tab_3"}, "正确率"), 
                        React.createElement("div", {className: "tk_tab_4"}, "平均用时"), 
                        React.createElement("div", {className: "tk_tab_5"}, "训练推荐")
                    ), 
                    React.createElement("div", {className: "tk_answer_list_bottom tk_have_tab_action"}, 
                        React.createElement("ul", {className: "clearfix"}, 
                             this.state.catalogs.map(function (catalog, index){
                                return (React.createElement(window.components.report.kpleaf, {
                                                    catalog: catalog, 
                                                    indexbox: self.props.indexbox, 
                                                    index: [index+1], 
                                                    tree: self, 
                                                    parent: self}));
                            }) 
                        )
                    )
                )
            )
        );
    }
});

window.components.report.kpleaf = React.createClass({
    displayName: "components.report.kpleaf",
    name: "知识点能力量表 - 叶子",
    api: new apis.report(),
    getInitialState: function (){
        // props: catalog / index / tree / parent / indexbox
        var catalog = this.props.catalog;
        catalog.catalogs = [];
        return catalog;
    },
    componentWillUnmount: function (){
        var self = this;
    },
    componentDidMount: function (){
        // this.pull();
    },
    pull: function (e, rid){
        var self = this;
        if ( self.props.index.length >= 3 ) {
            return ;
        }
        var target = $(e.target);
        if ( target.hasClass("noa") ){
            // $(e.target).removeClass("noa").addClass("a");
        } else {
            target.removeClass("a").addClass("noa");
            self.setState( {"catalogs": [] } );
            return ;
            // self.state.catalogs = []
        }

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
            }catch(e){
                console.warn(e); return;
            }
            // 树状图标按钮
            target.removeClass("noa").addClass("a");

            self.setState( { "catalogs": body.catalogs } );
        };
        this.api.kptree(this.state.cid.toString(), callback);
    },
    mk_test: function (source, cid){
        var self = this;
        console.log(self.state);
        console.info( ":: mk test ... ... " );
        console.log("source: " + source + "  cid: " + cid);
        var api = new apis.tests();
        if ( parseInt(cid) < 1 ) return ;

        loading.show('正在组卷');
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                loading.hide();
                console.warn(e); return;
            }
            loading.hide();
            self.props.indexbox.setState({"content": React.createElement(window.components.tests.answer.test, {tid: body.tid, root: self.props.indexbox})});
        };
        if ( source == "errors" ) api.init("errors", { "num" : 15, "duration" : 0, "cid": parseInt(cid) }, callback);
        else if ( source == "catalogs" ) api.init("catalog", {"num" : 20, "duration" : 0 ,"cid": parseInt(cid) }, callback);
        else console.warn(":: 组卷失败……");
    },
    render: function (){
        var self = this;

        /*
            {
                "superCid": 1298,
                "cid": 1298,
                "cname": "人文科技",
                "totalNum": 16,
                "rightNum": 0,
                "rightRate": "0.00",
                "ansTimeAVG": 12
            }
        */
        return (
            React.createElement("li", {"data-cid": this.state.cname, "data-cname": this.state.cname}, 
                React.createElement("div", {className: "tk_tab_list"}, 
                    React.createElement("div", {className: "tk_tab_1 noa", onClick: this.pull}, this.state.cname), 
                    React.createElement("div", {className: "tk_tab_2"}, this.state.rightNum, " / ", this.state.totalNum, " "), 
                    React.createElement("div", {className: "tk_tab_3"}, (Math.round(parseFloat(this.state.rightRate)*100*100)/100).toString() + "%"), 
                    React.createElement("div", {className: "tk_tab_4", dangerouslySetInnerHTML: {__html: time.ptime(this.state.ansTimeAVG).minutes.toString() 
                                                                                                + String.fromCharCode(8242) 
                                                                                                + time.ptime(self.state.ansTimeAVG).seconds.toString() 
                                                                                                + String.fromCharCode(8243)}}), 
                    React.createElement("div", {className: "tk_tab_5"}, 
                        React.createElement("div", null, React.createElement("a", {onClick: self.mk_test.bind(self, "catalogs", this.state.cid)}, "强化训练"))
                    )
                ), 
                React.createElement("ul", {className: "clearfix"}, 
                     this.state.catalogs.map(function (catalog, index){
                        return (React.createElement(window.components.report.kpleaf, {
                                                    catalog: catalog, 
                                                    index: self.props.index.concat(index+1), 
                                                    tree: self.props.tree, 
                                                    indexbox: self.props.indexbox, 
                                                    parent: self}));
                    }) 
                )
            )
        );
    }
});

window.components.report.kpgraph = React.createClass({
    displayName: "components.report.kpgraph",
    name: "知识点掌握热点图",
    api: new apis.report(),
    getInitialState: function (){
        // props: cid / root
        return { "catalogs": [] };
    },
    componentWillUnmount: function (){
        // var self = this;
    },
    componentDidMount: function (){
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
            }catch(e){
                console.warn(e); return;
            }
            self.replaceState( body );
        };
        this.api.kpgraph(callback);
    },
    onMouseMove: function (e, rid){
        // 鼠标停留在知识点上面
        $(document.getElementById(rid)).remove();
        
        var target = $(e.target);
        var cid = parseInt(target.data("cid"));
        var cname = target.data("cname");
        var isAnswer = parseInt(target.data("isanswer"));
        var rightRate = Math.round(parseFloat(target.data("rightrate"))*100*100)/100;

        var tips = document.createElement("div");
        tips.id = rid;

        var descp = "";
        if ( isAnswer == -1 ) {
            descp = "掌握程度: 未测试";
        } else if ( isAnswer == 1 ) {
            descp = "掌握程度: " + rightRate + "%";
        } else {
            descp = "掌握程度: oops";
        }

        tips = $(tips).append(
            $(
                   "<span class=\"nameA\">#CNAME#</span>\n".replace("#CNAME#", cname)
                + "<span class=\"descp\">#DESCP#</span>\n".replace("#DESCP#", descp)
            )
        );
        $(tips).css("left", target.offset().left + 10);
        $(tips).css("top", target.offset().top + -30 );
        $(tips).css("position", "absolute");
        $(tips).css("display", "block");
        $(tips).css({"backgroundColor":"black","color":"#fff","padding":"5px"})
        $(".nameA",$(tips)).css('color','#fff600')
        $('body').append(tips);
    },
    onMouseOut: function (e, rid){
        // 鼠标离开知识点
        $(document.getElementById(rid)).remove();
    },
    render: function (){
        var self = this;
        /*
        <li className="point_orange"></li>
        <li className="point_gray"></li>
        
        */
        return (
            React.createElement("div", {className: "point_01 point_tu"}, 
                React.createElement("div", {className: "point_con clearfix"}, 
                     this.state.catalogs.map(function (catalog, index){
                        return (
                            React.createElement("div", {
                                    key: catalog.cid, 
                                    className: "point_type", 
                                    "data-cid": catalog.cid, 
                                    "data-cname": catalog.cname}, 
                                React.createElement("div", {className: "point_type_title"}, catalog.cname), 
                                React.createElement("div", {className: "point_list"}, 
                                    React.createElement("ul", {className: "clearfix"}, 
                                         catalog.catalogs.map(function (clog, i){
                                            /*
                                                 {
                                                    "cid": 1126,
                                                    "cname": "马克思主义哲学",
                                                    "isAnswer": 1,
                                                    "rightRate": "0.17"
                                                }
                                                * isAnswer :是否测试(-1->未测试;1->测试)
                                                * rightRate:正确率
                                            */
                                            if ( parseInt(clog.isAnswer) == -1 ){
                                                return (React.createElement("li", {
                                                                key: "catalog_box_" + clog.cid.toString() + "_", 
                                                                "data-cid": clog.cid, 
                                                                "data-cname": clog.cname, 
                                                                "data-rightrate": clog.rightRate, 
                                                                "data-isanswer": clog.isAnswer, 

                                                                onMouseOver: self.onMouseMove, 
                                                                onMouseOut: self.onMouseOut, 
                                                                className: "point_gray"}));
                                            } else if ( parseFloat(clog.rightRate) >= 0.7  ) {
                                                return (React.createElement("li", {
                                                                key: "catalog_box_" + clog.cid.toString() + "_", 
                                                                "data-cid": clog.cid, 
                                                                "data-cname": clog.cname, 
                                                                "data-rightrate": clog.rightRate, 
                                                                "data-isanswer": clog.isAnswer, 

                                                                onMouseOver: self.onMouseMove, 
                                                                onMouseOut: self.onMouseOut, 
                                                                className: "point_green"}));
                                            } else {
                                                return (React.createElement("li", {
                                                                key: "catalog_box_" + clog.cid.toString() + "_", 
                                                                "data-cid": clog.cid, 
                                                                "data-cname": clog.cname, 
                                                                "data-rightrate": clog.rightRate, 
                                                                "data-isanswer": clog.isAnswer, 

                                                                onMouseOver: self.onMouseMove, 
                                                                onMouseOut: self.onMouseOut, 
                                                                className: "point_orange"}));
                                            }
                                        }) 
                                    )
                                )
                            )
                        );
                    }) 
                ), 
               
               React.createElement("div", {className: "point_sign"}, 
                    React.createElement("ul", {className: "clearfix"}, 
                        React.createElement("li", null, 
                            React.createElement("div", {className: "point_sign_01 point_gray"}), 
                            React.createElement("span", null, "未测试")
                        ), 
                        React.createElement("li", null, 
                            React.createElement("div", {className: "point_sign_01 point_orange"}), 
                            React.createElement("span", null, "未达到最低标准")
                        ), 
                        React.createElement("li", null, 
                            React.createElement("div", {className: "point_sign_01 point_green"}), 
                            React.createElement("span", null, "通过基础测试")
                        )
                    )
               )
            )
        );
    }
});