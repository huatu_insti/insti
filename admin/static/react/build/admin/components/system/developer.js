if ( !window.components ) window.components = {};
if ( !window.components.developer ) window.components.developer = {};


window.components.developer.options = React.createClass({
    displayName: "DeveloperOptions",
    name: "开发者选项",
    getInitialState: function(){
        return {};
    },
    componentDidMount: function() {
        $(function () {
            function winH() {
                var interfaceH = $(".interface").find("li");
                interfaceH.height(interfaceH.width());
            }
            winH();
            window.onresize = function () {
                winH();
            };
        });
    },
    componentWillUnmount: function() {
        // pass unmount event.


    },
    render: function (){
        var self = this;
        var appid = 0;
        var appkey = "";

        document.cookie.split("; ").map(function(kv, i){
            var k = kv.split("=")[0];
            var v = kv.split("=")[1].replace("\"", "");
            if ( k == "appkey" ) appkey = v;
            if ( k == "appid" ) appid = v;
        });
        
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "开发者选项")
                ), 
                React.createElement("div", {className: "system"}, 
                    React.createElement("span", null, "开发者ID"), 
                    React.createElement("div", {className: "sys_apply"}, "AppID（应用ID）      ", React.createElement("span", null, appid)), 
                    React.createElement("div", {className: "sys_apply"}, "AppSecret（应用密钥）      ", React.createElement("span", null, appkey))
                ), 
                React.createElement("div", {className: "interface"}, 
                    React.createElement("span", null, "接口文档"), 
                    React.createElement("ul", null, 
                        React.createElement("li", null, 
                            React.createElement("div", {className: "doc_img"}, 
                                React.createElement("img", {src: "/static/images/system_doc.png"}), 
                                React.createElement("span", null, React.createElement("a", {href: "javascript:;"}, "接口文档1"))
                            )
                        ), 
                        React.createElement("li", null, 
                            React.createElement("div", {className: "doc_img"}, 
                                React.createElement("img", {src: "/static/images/system_doc.png"}), 
                                React.createElement("span", null, React.createElement("a", {href: "javascript:;"}, "接口文档1"))
                            )
                        ), 
                        React.createElement("li", null, 
                            React.createElement("div", {className: "doc_img"}, 
                                React.createElement("img", {src: "/static/images/system_doc.png"}), 
                                React.createElement("span", null, React.createElement("a", {href: "javascript:;"}, "接口文档1"))
                            )
                        ), 
                        React.createElement("li", null, 
                            React.createElement("div", {className: "doc_img"}, 
                                React.createElement("img", {src: "/static/images/system_doc.png"}), 
                                React.createElement("span", null, React.createElement("a", {href: "javascript:;"}, "接口文档1"))
                            )
                        ), 
                        React.createElement("li", {className: "li_more"}, 
                            React.createElement("img", {src: "/static/images/points.png"}), 
                            React.createElement("span", null, React.createElement("a", {href: "javascript:;"}, "更多"))
                        )
                    )
                )
            )

            );
    }
});
