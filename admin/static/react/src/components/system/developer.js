
if ( !window.components ) window.components = {};
if ( !window.components.developer ) window.components.developer = {};


window.components.developer.options = React.createClass({
    displayName: "DeveloperOptions",
    name: "开发者选项",
    getInitialState: function(){
        return {};
    },
    componentDidMount: function() {
        $(function () {
            function winH() {
                var interfaceH = $(".interface").find("li");
                interfaceH.height(interfaceH.width());
            }
            winH();
            window.onresize = function () {
                winH();
            };
        });
    },
    componentWillUnmount: function() {
        // pass unmount event.


    },
    render: function (){
        var self = this;
        var appid = 0;
        var appkey = "";

        document.cookie.split("; ").map(function(kv, i){
            var k = kv.split("=")[0];
            var v = kv.split("=")[1].replace("\"", "");
            if ( k == "appkey" ) appkey = v;
            if ( k == "appid" ) appid = v;
        });
        
        return (
            <div>
                <div className="create_head">
                    <div className="record_paper">开发者选项</div>
                </div>
                <div className="system">
                    <span>开发者ID</span>
                    <div className="sys_apply">AppID（应用ID）      <span>{appid}</span></div>
                    <div className="sys_apply">AppSecret（应用密钥）      <span>{appkey}</span></div>
                </div>
                <div className="interface">
                    <span>接口文档</span>
                    <ul>
                        <li>
                            <div className="doc_img">
                                <img src="/static/images/system_doc.png"/>
                                <span><a href="javascript:;">接口文档1</a></span>
                            </div>
                        </li>
                        <li>
                            <div className="doc_img">
                                <img src="/static/images/system_doc.png"/>
                                <span><a href="javascript:;">接口文档1</a></span>
                            </div>
                        </li>
                        <li>
                            <div className="doc_img">
                                <img src="/static/images/system_doc.png"/>
                                <span><a href="javascript:;">接口文档1</a></span>
                            </div>
                        </li>
                        <li>
                            <div className="doc_img">
                                <img src="/static/images/system_doc.png"/>
                                <span><a href="javascript:;">接口文档1</a></span>
                            </div>
                        </li>
                        <li className="li_more">
                            <img  src="/static/images/points.png"/>
                            <span><a href="javascript:;">更多</a></span>
                        </li>
                    </ul>
                </div>
            </div>

            );
    }
});