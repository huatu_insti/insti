

if ( !window.components ) window.components = {};
if ( !window.components.exams ) window.components.exams = {};

window.components.exams.showCatalog =  React.createClass({
    displayName: "exams.init.dialog",
    name: "添加考试-选择试卷标签",
    api: new apis.catalogs.exams(),
    getInitialState: function (){
        if ( !this.props.parent ) throw new Error(" exams.init.dialog getInitialState fail. ");
    },
    fetch: function (cid){

    },
    componentDidMount: function (){
        $(this.getDOMNode()).dialog({
            title: '选择标签',
            width: 700,
            height: 550,
            closed: true,
            cache: false,
            // href: 'get_content.php',
            modal: true
        });
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", null, 
                React.createElement("h1", null, "标签列表"), 
                React.createElement("table", {className: "table"}, 
                    React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, "标签编号"), 
                            React.createElement("th", null, "标签名称")
                        )
                    ), 
                    React.createElement("tbody", null, 
                        React.createElement("tr", null, 
                            React.createElement("td", {onClick: self.props.onselected}, "1"), 
                            React.createElement("td", null, "标签1")
                        ), 
                        React.createElement("tr", null, 
                            React.createElement("td", null, "2"), 
                            React.createElement("td", null, "标签2")
                        )
                    )
                )
            )
        );
    }
});


window.components.exams.init =  React.createClass({
    displayName: "exams.init",
    name: "考试初始化",
    api: new apis.exams(),
    getInitialState: function (){
        print(this.props);
        if ( !this.props.paper || parseInt(this.props.paper) < 1 ) throw new Error("window.components.exams.init fail.");
        /*
        "uppertime": 1427347782,
        "lowertime": 1427347782,
        "duration": 20,
        "tombstone": 0,
        "inittime": 1427365517,
        "updtime": 1427365517,
        "eid": 26

                        var exam = {
                            "pid" : paper.pid,
                            "name" : paper.name, 
                            "uppertime" : time.time() + 1000,
                            "lowertime" : time.time() + 960000,
                            "duration" : 120*60
                         };
                exam.uppertime = 0;
                exam.lowertime = 0;
        */
        return this.props.paper; // {"pid": this.props.pid, "eid": 0, "name": "", "struct": [], "attrs": [] }
    },
    componentDidMount: function (){
        // this.pull();
        $(this.getDOMNode()).dialog({
            title: '发布考试',
            width: 700,
            height: 550,
            closed: true,
            cache: false,
            // href: 'get_content.php',
            modal: true
        });
    },
    show_dialog: function (){
        $(this.getDOMNode()).dialog({
            title: '发布考试',
            width: 700,
            height: 550,
            closed: true,
            cache: false,
            // href: 'get_content.php',
            modal: true
        });
    },
    pull: function (){
        // pull paper data from server.
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                print(":: paper.pull fail");
                print(response);
            }
            try{
                var body = JSON.parse(response.body);
                if ( "status" in body == true && parseInt(body.status) == 2 ){
                    // 试卷当前状态为 审核通过
                    self.replaceState(body);
                    self.show_dialog.bind(self);
                } else {
                    // 该试卷不再允许编辑
                    alert("该试卷无法发布。");
                    throw new Error("试卷状态不为2，禁止发布。");
                }
            }catch(e){
                console.warn(e);
            }
        };
        var api = new apis.papers();
        api.pull(this.state.pid, callback);
    },
    make: function (){
        // JSON.parse( JSON.stringify(this.state) );
        // this.api.make(exam, callback);
    },
    showCatalog: function (e, rid){
        // 选择试卷标签
        var self = this;
        var onselected = function (e, rid){
            print(":: on selected function .");
            print(rid);
        };
        var catalog_view = React.createElement(window.components.exams.showCatalog, { "onselected": self} );
        // var catalog_view = <window.components.exams.showCatalog onselected={self} />;
        React.render(catalog_view, document.getElementById('dialog'));
    },
    destroy: function (){
        $(this.getDOMNode()).dialog("close");
        // $(".ui-dialog").remove();
        // $(".ui-widget-overlay").remove();
    },
    render: function (){
        var self = this;
        var questions_total = 0;
        if ( "score" in this.state != true ) var paper_score = 0;
        else var paper_score = this.state.score;
        if ( "attrs" in this.state != true ) {
            return React.createElement("div", {className: "add_test"});
        }

        return (
            React.createElement("div", {className: "add_test"}, 
                React.createElement("div", {className: "add_test_title"}, "添加考试", React.createElement("img", {src: "/static/images/add_test_close.png"})), 
                React.createElement("div", {className: "form1", "data-method": "post"}, 
                    React.createElement("div", {className: "add_int"}, 
                        React.createElement("label", null, "试卷名称"), 
                        React.createElement("input", {className: "add_inp", type: "text", name: "test_name", value: this.state.name})
                    ), 
                    React.createElement("div", {className: "add_int"}, 
                        React.createElement("div", {className: "add_int_num"}, 
                            React.createElement("label", null, "试卷题量"), 
                            React.createElement("input", {className: "add_inp_num", type: "text", name: "test_num", value: questions_total})
                        ), 
                        React.createElement("div", {className: "add_int_score"}, 
                            React.createElement("label", null, "试卷分数"), 
                            React.createElement("input", {className: "add_inp_score", type: "text", name: "test_score", value: paper_score})
                        )
                    ), 
                    React.createElement("div", {className: "add_int"}, 
                         this.state.attrs.map(function(attr, index){
                            if ( attr.name == "year" ) {
                                return (
                                    React.createElement("div", {className: "add_int_year"}, 
                                        React.createElement("label", null, "试卷年份"), 
                                        React.createElement("input", {className: "add_inp_year", type: "text", name: "test_num", value: attr.value})
                                    ));

                            } else if ( attr.name == "area" ) {
                                return (
                                    React.createElement("div", {className: "add_int_area"}, 
                                        React.createElement("label", null, "试卷地区"), 
                                        React.createElement("input", {className: "add_inp_area", type: "text", name: "test_score", value: attr.value})
                                    ));
                            }
                        }) 

                    ), 
                    React.createElement("div", {className: "add_int"}, 
                        React.createElement("label", null, "考试时长"), 
                        React.createElement("input", {className: "add_inp_time", type: "text", name: "test_name", value: this.state.duration})
                    ), 
                    React.createElement("div", {className: "add_int"}, 
                        React.createElement("label", null, "试卷标签"), 
                        React.createElement("a", {className: "add_int_choose", onClick: this.showCatalog}, "点击选择标签")
                    ), 
                    React.createElement("div", {className: "add_int"}, 
                        React.createElement("label", null, "答题报告"), 
                        React.createElement("input", {className: "add_inp_check", type: "checkbox"}), 
                        React.createElement("span", null, "交卷后立即查看")
                    ), 
                     React.createElement("div", {className: "add_int add_timeto"}, 
                        React.createElement("div", {className: "add_int_endtime"}, 
                            React.createElement("label", null, "最晚入场时间"), 
                            React.createElement("input", {className: "add_inp_endtime", type: "text", name: "test_name", value: time.time()+3*60*60})
                        ), 
                        React.createElement("div", {className: "add_int_to"}, 
                            React.createElement("label", null, "至"), 
                            React.createElement("input", {className: "add_inp_totime", type: "text", name: "test_name", value: time.time()+3*60*60})
                        )
                    ), 
                    React.createElement("div", {className: "add_btn"}, 
                        React.createElement("a", {className: "add_canal", onClick: this.destroy}, "取消"), 
                        React.createElement("a", {className: "add_new", onClick: this.make}, "添加")
                    )
                
                )

            )

        );
    }
});

