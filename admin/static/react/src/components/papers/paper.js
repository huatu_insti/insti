

if ( !window.components ) window.components = {};
if ( !window.components.papers ) window.components.papers = {};

// React.unmountComponentAtNode(document.getElementById('content'));

QuestionBox = React.createClass({
    displayName: "QuestionBox",
    name: "试题",
    tixu: 0,            // string
    tixu2: "",
    index: 0,         // int
    ishide: true,
    getInitialState: function (){
        if ( !this.props.index || ! this.props.paper ) throw Error("QuestionBox init fail.");
        this.tixu = this.props.index.join("-");
        // this.tixu2 = this.props.paper.compute_questions( "pos", this.props.paper.questions[this.props.qid] ) + 1;
        this.tixu2 = this.props.tx2;
        return {};
    },
    componentDidMount: function() {
        // console.info("QuestionBox is mount.");
        var self = this;
        this.props.index.map(function(t, i){
            self.index += parseInt(t);
        });

        var new_tcard = {"index": this.index, "tixu": this.tixu, "tixu2": this.props.tx2, "component" : this, "element": this.props.paper.questions[this.props.qid] };
        /*
        try{
            var ii = this.props.paper.tixu.map(function (t, i){
                if ( "qid" in t.element && parseInt(t.element.qid) == parseInt(self.props.qid) ) {
                    return i;
                }
            } ).filter(function(t, i){
                return t != undefined; 
            });
            if ( ii.length>0 ) self.props.paper.tixu[ii[0]] = new_tcard;
            else self.props.paper.tixu[self.props.paper.tixu.length] = new_tcard;
            // this.props.paper.tixu[this.props.paper.tixu.length] = {"index": this.index, "tixu": this.tixu, "component" : this, "element": this.props.paper.questions[this.props.qid] };
        }catch(e){ };
        */
        this.props.paper.tixu_card[this.tixu] =  new_tcard;
        this.props.paper.elements.questions[this.props.qid] =  new_tcard;
        this.forceUpdate();
        this.props.paper.forceUpdate();
    },
    componentDidUpdate:function(){
       this.props.paper.elements.questions[this.props.qid].tixu2 = this.props.tx2;
       //鼠标悬浮样式 
    //    $(".paper_item_slide").hover(function(){

    //                 $(this).find(".paper_drag_item").show().end().find(".paper_item_del").show()
    //                        .end().find(".paper_item_toggle").show()
    //                        .end().find(".tixu").css({"background":"#ddd"})
    //                        .end().find(".score").css({"background":"#ddd"})
    //                        .end().css({"background":"#ddd"})

    //                },function(){
    //                  $(this).find(".paper_drag_item").hide().end().find(".paper_item_del").hide()
    //                         .end().find(".paper_item_toggle").hide().end().css({"backgound":"#fff"})
    // })
       //初始设置宽度
    $(".import_word").css("height", $(".enter_paper").height() - 1);
    //初始设置宽度
    $(".import_word").width( $('.main').width() - $('.enter_paper').width() - 3 );
    //第一个
    $('.paper_item_slide').eq(0).addClass('noborder');
    
    /**resize**/
    $(window).resize(function(){
        $(".import_word").width( $('.paper_enter').width() - $('.enter_paper').width() - 43 );
    });
    
    //编辑模式
    $("dl").on("click", '.paper_item_title', function(){
        var _this = $(this);
            $parent = _this.parent(),
            $toggle = $parent.find('.paper_item_toggle'),
            $del = $parent.find('.paper_item_del');
            
        $parent.find('.paper_drag_item').addClass('none');  
        $toggle.removeClass('paper_item_show').addClass('paper_item_hide').hide();
        $del.hide();
        $parent.next().show();
        _this.removeClass('title_h');
        
        //普通和复合
        if ( _this.parent().hasClass('composite') ) {
            _this.css('width','432px');
        } else {
            _this.css('width','370px');
        }
        
    });
    
    //编辑失去焦点
    $('dl').on('blur', '.paper_item_title', function(){

        var _this = $(this);
            $parent = _this.parent(),
            $toggle = $parent.find('.paper_item_toggle'),
            $del = $parent.find('.paper_item_del');
        
        _this.css('width','300px'); 
        $toggle.show();
        $del.show();
        
    });
    
    /****title****/
    $("dl").on("mouseover", '.paper_item_slide', function(){
        var _this = $(this),
            $substance = _this.next(),
            $toggle = _this.find('.paper_item_toggle'),
            $drag = _this.find('.paper_drag_item');
        
        if ($substance.is(':hidden')) {
            $toggle.addClass('paper_item_show');
        } else {
            $toggle.addClass('paper_item_hide');
        }   
        
        $drag.removeClass('none');  
        _this.addClass('paper_item_slide_hover').find('.paper_item_score input').addClass('paper_item_slide_hover');
        _this.find('.paper_item_slide_tit input').addClass('paper_item_slide_hover');
        
        $toggle.removeClass('none');    
        _this.find(".paper_item_del").removeClass('none');  
    });
    
    $("dl").on("mouseout", '.paper_item_slide', function(){
        var _this = $(this),
            $toggle = _this.find('.paper_item_toggle'),
            $drag = _this.find('.paper_drag_item');
        
        $drag.addClass('none'); 
        
        /*
        _this.css('backgroundColor','#fff');
        _this.find('.paper_item_score input').css('backgroundColor','#fff');
        */
        _this.removeClass('paper_item_slide_hover').find('.paper_item_score input').removeClass('paper_item_slide_hover');
        _this.find('.paper_item_slide_tit input').removeClass('paper_item_slide_hover');

        
        $toggle.addClass('none');   
        _this.find(".paper_item_del").addClass('none');
    });
    
    // to show
    $('dl').on('click','.paper_item_show',function(){
        var _this = $(this),
            $substance = _this.parent().next();
            /*$drag = _this.parent().find('.paper_drag_item');*/
        $substance.show();
        _this.removeClass('paper_item_show').addClass('paper_item_hide');
        //$drag.addClass('none');   
        
        _this.parent().find('.paper_item_title').removeClass('title_h');
    });

    // to hide
    $('dl').on('click','.paper_item_hide',function(){
        var _this = $(this),
            $substance = _this.parent().next();
            //$drag = _this.parent().find('.paper_drag_item');
        $substance.hide();
        _this.removeClass('paper_item_hide').addClass('paper_item_show');
        //$drag.removeClass('none');
        
        _this.parent().find('.paper_item_title').addClass('title_h');
    });
    
    //title,score
    $('dl').on('mouseover', '.paper_item_slide_tit input,.paper_item_title,.paper_item_score input,.paper_edit_div,.pape_attr_edit', function(){
        $(this).addClass('on');
    });
    
    $('dl').on('mouseout', '.paper_item_slide_tit input,.paper_item_title,.paper_item_score input,.paper_edit_div,.pape_attr_edit', function(){
        $(this).removeClass('on');
    });
    
    //选项可编辑区域
    $('dl').on('click', '.paper_edit_div', function(){
        var _this = $(this);
        
        
        
            $parent = _this.parent();
        
        $parent.find('.paper_choice_del').hide();
        _this.css('width','415px');
    });
    
    $('dl').on('blur', '.paper_edit_div', function(){
        var _this = $(this);
            $parent = _this.parent();
        
        $parent.find('.paper_choice_del').show();
        _this.css('width','350px');
    });
    
    //属性编辑区域
    $('dl').on('click', '.pape_attr_edit', function(){
        var _this = $(this);
            $parent = _this.parent();
        
        $parent.find('.paper_attr_del').hide();
        _this.css('width','415px');
    });
    
    $('dl').on('blur', '.pape_attr_edit', function(){
        var _this = $(this);
            $parent = _this.parent();
        
        $parent.find('.paper_attr_del').show();
        _this.css('width','350px');
    });

    /***choice***/
    $('dl').on('mouseover','.paper_choice',function(){
        var _this = $(this);
        _this.css('backgroundColor','#eee');
        _this.find('.paper_choice_del').removeClass('none');    
    });

    $('dl').on('mouseout','.paper_choice',function(){
        var _this = $(this);
        _this.css('backgroundColor','#fff');
        _this.find('.paper_choice_del').addClass('none');
    });
    
    
    /**attr**/
    $('dl').on('mouseover' ,'.pape_attr', function(){
        var _this = $(this);
        _this.css('backgroundColor','#eee');
        _this.find('.paper_attr_del').removeClass('none');
    });
    
    $('dl').on('mouseout' ,'.pape_attr', function(){
        var _this = $(this);
        _this.css('backgroundColor','#fff');
        _this.find('.paper_attr_del').addClass('none');
    });

    $('.paper_attr_sel').on('mouseover','li',function(){
        var _this=$(this);
        _this.find('img').removeClass('none');
    });
    
    $('.paper_attr_sel').on('mouseout','li',function(){
        var _this=$(this);
        _this.find('img').addClass('none');
    });
    
    },
    setContent: function (e, rid){
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        this.props.paper.questions[this.props.qid].content = $(obj).html().replace(/<(?!img)(?:.|\s)*?>/ig, "");
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    addChoice: function (e, rid){
        if ( this.props.paper.questions[this.props.qid].ans.length != 0 && this.props.paper.questions[this.props.qid].choices.length == 0 ) { 
            // 主观题，不可以添加选项
            console.warn("只有选择题才可以添加选项哦");
            alert("只有选择题才可以添加选项哦");
        } else {
            this.props.paper.questions[this.props.qid].choices.push("");
            this.forceUpdate();
            this.props.paper.diffElement({"qid": this.props.qid});
        }
        
    },
    updateChoice: function (e, rid){
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var index = parseInt($(obj).data("index"));
        var card = $(obj).find(".paper_sel_letter").text().replace(/<(?!img)(?:.|\s)*?>/ig, "");
        var choice = $(obj).find(".paper_edit_div").html().replace(/<(?!img)(?:.|\s)*?>/ig, "");
        this.props.paper.questions[this.props.qid].choices[index] = choice;
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    removeChoice: function (index, e, rid){
        var self = this;
        var choices = [];
        this.props.paper.questions[this.props.qid].choices.map(function(choice, i){
            if ( parseInt(i) != parseInt(index) ){
                choices.push(choice);
            }
        });
        this.props.paper.questions[this.props.qid].ans = this.props.paper.questions[this.props.qid].ans.replace(chr(65+parseInt(index)), "");
        this.props.paper.questions[this.props.qid].choices = choices;
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    setChoice: function (e, rid){
        // 将该选项作为该题的答案之一
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid)); // paper_sel_letter
        // var ans = $(obj).find(".paper_sel_letter").text();
        var ans = $(obj).text().replace(/<(?!img)(?:.|\s)*?>/ig, "");
        var s_ans = JSON.parse(JSON.stringify(this.props.paper.questions[this.props.qid].ans));

        if ( !s_ans ) s_ans = "";

        if ( s_ans.indexOf(ans) == -1 ) s_ans += ans;
        else s_ans = s_ans.replace(ans, "");

        this.props.paper.questions[this.props.qid].ans = s_ans;
        this.props.paper.questions[this.props.qid].ansnum = this.props.paper.questions[this.props.qid].ans.length;
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    addAnswer: function (e, rid){
        if ( this.props.paper.questions[this.props.qid].ans.length == 0 ) {
            this.props.paper.questions[this.props.qid].ans = "请填写你的答案 ... ... ";
            this.forceUpdate();
        } else {
            // 不允许再添加主观答案
            console.warn("只有主观题才可以添加答案哦");
            alert("只有主观题才可以添加答案哦");
        }
    },
    setAnswer: function (e, rid){
        // e/rid
        this.props.paper.questions[this.props.qid].ans = e.target.textContent.replace(/<(?!img)(?:.|\s)*?>/ig, "");
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    setScore: function (e, rid){
        var self = this;
        // var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var score = parseFloat(e.target.value);

        if ( parseFloat(this.props.paper.questions[this.props.qid].score) != score && !isNaN(score) ) {
            this.props.paper.questions[this.props.qid].score = score;
            this.props.paper.forceUpdate();
            this.props.paper.diffElement({"qid": this.props.qid});
        }
        // console.log(this.props.paper.questions[this.props.qid].score);
    },
    addAttr: function (e, rid, attr){
        if ( !attr ) var attr = {"name": "新增标签", "value": ""};
        // this.props.paper.questions[this.props.qid].attrs.push(attr);
        var goon = true;
        this.props.paper.questions[this.props.qid].attrs.map(function(attr, index){
            if ( attr.name == "新增标签" ) goon = false;
        });
        if ( goon == true ) {
            this.props.paper.questions[this.props.qid].attrs.push(attr);
        } else {
            console.warn("已经存在一个同名标签");
            alert("该标签名已经存在，请更换一个吧");
        }
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    updateAttr: function (e, rid){
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var index = parseInt($(obj).data("index"));

        var key = $(obj).find(".attr").text().replace(/<(?!img)(?:.|\s)*?>/ig, "");
        var value = $(obj).find(".pape_attr_edit").html().replace(/<(?!img)(?:.|\s)*?>/ig, "");
        var question_object = this.props.paper.questions[this.props.qid];

        // diff
        if ( question_object.attrs[index].name == key &&  question_object.attrs[index].value == value ){
            // 无须更新
            console.info(":: 没有需要更新的数据");
        } else {
            if ( question_object.attrs[index].name == key && question_object.attrs[index].value != value ){
                // 更新 属性值
                this.props.paper.questions[this.props.qid].attrs[index].value = value;
            } else if ( question_object.attrs[index].name != key ){
                // 判断该 标签名是否已经存在
                var goon = true;
                this.props.paper.questions[this.props.qid].attrs.map(function(attr, i){
                    if ( attr.name == key ) goon = false;
                });
                if ( goon == true ) {
                    this.props.paper.questions[this.props.qid].attrs[index] = {"name": key, "value": value};
                } else {
                    console.warn("已经存在一个同名标签");
                    alert("该标签名已经存在，请更换一个吧");
                }
                
            } else {
                console.warn("unknow type (question.updateAttrs) .");
            }
        }
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    removeAttr: function (index, e, rid){
        var self = this;
        var attrs = [];
        this.props.paper.questions[this.props.qid].attrs.map(function(attr, i){
            if ( parseInt(i) != parseInt(index) ){
                attrs.push(attr);
            }
        });
        this.props.paper.questions[this.props.qid].attrs = attrs;
        this.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    updateAttr_Chouti: function (e, rid){
        var self = this;
        // var isChecked = e.target.checked;
        // var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        var obj = $(e.target);
        var index = parseInt($(obj).data("index"));
        var isChecked = $(obj).is(":checked");
        var this_question = this.props.paper.questions[this.props.qid];

        if ( $(obj).hasClass("radio-01") ) {
            if ( isChecked == true ) {
                // 参与抽题
                this_question.attrs[index].value = "是";
                $(obj)[0].checked = true;
                $(obj).parent().parent().find("input")[1].checked = false;
            } else {
                this_question.attrs[index].value = "否";
                $(obj)[0].checked = false;
                $(obj).parent().parent().find("input")[1].checked = true;
            }
        } else if ( $(obj).hasClass("radio-02") ) {
            if ( isChecked == true ) {
                // 不参与抽题
                this_question.attrs[index].value = "否";
                $(obj)[0].checked = true;
                $(obj).parent().parent().find("input")[0].checked = false;
            } else {
                // 参与抽题
                this_question.attrs[index].value = "是";
                $(obj)[0].checked = false;
                $(obj).parent().parent().find("input")[0].checked = true;
            }
        }
        this.forceUpdate();
        this.props.paper.forceUpdate();
        this.props.paper.diffElement({"qid": this.props.qid});
    },
    removeDialog: function (e, rid){
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid))
                                        .parent().parent()
                                        .find('.papaer_del[data-qid="' + this.props.qid + '"]');
        $(obj).removeClass("none");
    },
    remove: function (action, e, rid){
        var self = this;
        if ( action == "cancel" ) {
            $("[data-reactid='#rid#']".replace("#rid#", rid)).parent().addClass("none");
            return ;
        } else if ( action == "delete" ) {
            var struct = JSON.stringify(this.props.paper.paper.struct);
            var qid = parseInt(this.props.qid);
            // self.props.paper.questions.splice(se
            ///删除操作
            delete self.props.paper.questions[self.props.qid]; //删除questions的某题，改变分数
            delete self.props.paper.elements.questions[self.props.qid];
            // for(var cqid in self.props.paper.composites){
            //      for(var i=0;i<self.props.paper.composites[cqid].struct.length;i++){
            //          if( self.props.paper.composites[cqid].struct[i][self.props.qid] != undefined){
            //              delete  self.props.paper.composites[cqid].struct[i][self.props.qid];
            //          }
            //      }
            // }
            // console.log(self.props.paper.composites);
            
              
                  
                  
            
           

          
            // this.props.paper.elements.questions[this.props.qid].tixu2 = this.props.tx2;
           
            var re, new_struct, rege;
            if ( new RegExp("\\[\{\"qid\":" + qid + "\}").test(struct) ){
                re = new RegExp("\{\"qid\":" + qid + "\},*");
                new_struct = struct.replace(re, "");
            } else if ( new RegExp("\{\"qid\":" + qid + "\}\\]").test(struct) ){
                re = new RegExp(",*\{\"qid\":" + qid + "\}");
                new_struct = struct.replace(re, "");
            } else {
                re = new RegExp(",*\{\"qid\":" + qid + "\}");
                new_struct = struct.replace(re, "");
            }
            this.props.paper.paper.struct = JSON.parse(new_struct);
            for(var qid in self.props.paper.elements.questions){

                   var tx = self.props.paper.compute_questions("pos",self.props.paper.questions[qid])+1;
                   self.props.paper.elements.questions[qid].tixu2 = tx;
            }
            if ( typeof this.props.parent.tixu == "string" ){
                // console.log(this.props.paper.composites[this.props.parent.props.cqid].struct);
                var new_cstruct = this.props.paper.composites[this.props.parent.props.cqid].struct.filter(function(e, i){
                    if ( "qid" in e != true || parseInt(e.qid) != parseInt(self.props.qid) ){
                        return true;
                    } else return false;
                });

                this.props.paper.composites[this.props.parent.props.cqid].struct = new_cstruct;
                this.props.paper.diffElement({"cqid": this.props.parent.props.cqid});
                //重新计算tx2 
                var tx2 = [ self.props.paper.compute_questions( "pos", self.props.paper.composites[this.props.parent.props.cqid] ) + 1, 
                            self.props.paper.compute_questions( "pos", self.props.paper.composites[this.props.parent.props.cqid] ) 
                           + self.props.paper.compute_questions( "children", self.props.paper.composites[this.props.parent.props.cqid] ) ].join("-");
                this.props.paper.elements.composites[this.props.parent.props.cqid].tixu2 = tx2;

            }
            self.forceUpdate();
            this.props.paper.forceUpdate();
            
            this.props.paper.diffElement({"pid": this.props.paper.paper.pid});
        } else {
            console.warn("unknow remove action.");
        }
    },
    handleDragStart: function (e, rid){
        // 拖放开始
        if ( $(e.target).hasClass('element') ){
            var tixu = $(e.target).data("tixu");
            console.info("question: 正在拖动元素 " + tixu);
            window._dragevent = {"src": tixu.toString(), "dest": null};
        }
    },
    handleDragEnter: function (e, rid){
        // 拖动进入的新 DOM 节点
        if ( $(e.target).hasClass('element') ){
            // 正确的位置
            var tixu = $(e.target).data("tixu");
            console.info("question: 拖动元素至 " + tixu);
            window._dragevent.dest = tixu.toString();
        } else if ( $(e.target).parent().hasClass('element') ) {
            var tixu = $(e.target).parent().data("tixu");
            console.info("question: 拖动元素至 " + tixu);
            window._dragevent.dest = tixu.toString();
        } else {
            // console.log( $(e.target) );
        }
    },
    handleDragEnd: function (e, rid){
        // 处理拖动效果
        var self = this;
        
        if ( window._dragevent.dest == null || window._dragevent.src == null ) {
            console.warn("question: 指令无法执行");
            return false;
        }
        if ( window._dragevent.dest == null || window._dragevent.src == null ) {
            console.warn("question: 指令无法执行");
            return false;
        }

        console.info(window._dragevent);

        var dest_tixu = window._dragevent.dest;
        var src_tixu = window._dragevent.src;
        
        if ( dest_tixu.toString() == src_tixu.toString()  ) {
            console.info(":: 不能把自己当作拖动目标。")
            return true;
        }
        /*
        var dest_index = 0;
        dest_tixu.split("-").map(function (c, i){
            dest_index += parseInt(c);
        });
        dest_index = dest_index - 1;

        var src_index = 0;
        src_tixu.split("-").map(function (c, i){
            src_index += parseInt(c);
        });
        src_index = src_index - 1;
        */
        // { "index": this.index, "tixu": this.tixu, "component" : this, "element": this.props.paper.questions[this.props.qid] };
        //var src = this.props.paper.tixu[src_index];
        //var dest = this.props.paper.tixu[dest_index];

        /*
        var src = this.props.paper.tixu.filter(function(e, i){
            return e.tixu.toString() == src_tixu.toString();
        })[0];
        var dest = this.props.paper.tixu.filter(function(e, i){
            return e.tixu.toString() == dest_tixu.toString();
        })[0];
        */

        var src = this.props.paper.tixu_card[src_tixu];
        var dest = this.props.paper.tixu_card[dest_tixu];

        dest.dom = $( 'div[data-tixu="#tixu#"]'.replace("#tixu#", dest.tixu ) );
        src.dom = $( 'div[data-tixu="#tixu#"]'.replace("#tixu#", src.tixu ) );

        var ok;
        if ( !$(e.target).hasClass('element') )  return false;
        if ( dest.tixu.split("-").length < 1 ) throw new Error("drag fail.");
        
        var isbrother;
        if ( typeof dest.component.props.parent.tixu ==  "object" && typeof src.component.props.parent.tixu == "object" ){
            isbrother = true;
        } else if ( typeof dest.component.props.parent.tixu ==  "string" && typeof src.component.props.parent.tixu == "string" ) {
            if ( dest.component.props.parent.tixu.toString() == src.component.props.parent.toString() ) {
                isbrother = true;
            } else {
                isbrother = false;
            }
        } else {
            isbrother = false;
        }
        
        console.log(src); console.log(dest);
        
        /////////////////////////////////////////////////////////////////////////////////////////////////
        if ( dest.tixu.split("-").length < 2 && src.tixu.split("-").length < 2 ) {
            // 顶层位移
            if ( "qid" in src.element  && "cqid" in dest.element ) {
                // 注入 分组元素当中
                console.log("qid to cqid in paper struct.");

                self.props.paper.paper.struct.splice( parseInt(src.tixu.split("-").slice(-1)[0])-1, 1);
                if ( parseInt(dest.tixu.split("-").slice(-1)[0]) < parseInt( src.tixu.split("-").slice(-1)[0] ) ){
                    self.props.paper.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-1].struct.push(
                        {"qid": src.element.qid }
                    );
                } else {
                    self.props.paper.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-2].struct.push(
                        {"qid": src.element.qid }
                    );
                }
                
                // self.props.paper.paper.struct.splice( parseInt(src.tixu.split("-").slice(-1)[0])-1, 1);
                // self.props.paper.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-1].struct.push( {"qid": src.element.qid } );
                self.props.paper.composites[dest.element.cqid].struct.push( {"qid": src.element.qid } );
                this.props.paper.diffElement({"cqid": dest.element.cqid});
            } else {
                // 调换次序
                console.log("qid/cqid to cqid/qid postion in paper struct.");
                self.props.paper.paper.struct.splice(
                            parseInt(dest.tixu.split("-").slice(-1)[0])-1, 
                            0,
                            self.props.paper.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                var new_struct = self.props.paper.paper.struct.filter(function (e, i){
                    return e != undefined && e != null && typeof e == 'object';
                });
                self.props.paper.paper.struct = new_struct;
            }
        } else {
            // 非顶层复杂变幻 (相同节点之间元素互相调换)
            if ( parseInt(dest.tixu.split("-").splice(-2)[0]) == parseInt(src.tixu.split("-").splice(-2)[0]) && dest.tixu.split("-").length == src.tixu.split("-").length ){
                // 位移(同层)
                if ( "qid" in src.element  && "cqid" in dest.element ) {
                    // 注入 分组元素当中
                    console.log("位移-注入: qid to cqid in paper struct.");
                    self.props.paper.composites[src.component.props.parent.props.cqid].struct.splice( 
                        parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                        0,
                        self.props.paper.composites[src.component.props.parent.props.cqid]
                            .struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    var target_node = null;
                    target_node_tixu.split("-").map(function(i, index){
                        if ( target_node == null ) target_node = self.props.paper.paper.struct[parseInt(i)-1];
                        else target_node = target_node.struct[parseInt(i)-1];
                    });
                    if ( target_node != null && target_node != undefined ){
                        target_node.struct.splice( 
                            parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                            0,
                            target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                    }
                } else {
                    // 调换次序
                    console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");
                    self.props.paper.paper.struct.splice(
                                parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                                0,
                                self.props.paper.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                            );
                    var new_struct = self.props.paper.paper.struct.filter(function (e, i){
                        return e != undefined && e != null && typeof e == 'object';
                    });
                    self.props.paper.paper.struct = new_struct;
                }

                console.log( JSON.stringify( new_struct ) );
                console.log( JSON.stringify( self.props.paper.paper.struct ) );
            } else {
                // 层移 （不同节点之间元素 移位）
                console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");
                if ( "qid" in src.element  && "cqid" in dest.element ) {
                    // 注入 到分组元素当中 （跨节点远程注入）
                    console.log("位移-注入: qid to cqid in paper struct.");
                    self.props.paper.composites[src.component.props.parent.props.cqid].struct.splice( 
                        parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                        0,
                        self.props.paper.composites[src.component.props.parent.props.cqid]
                            .struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    var target_node = null;
                    target_node_tixu.split("-").map(function(i, index){
                        if ( target_node == null ) target_node = self.props.paper.paper.struct[parseInt(i)-1];
                        else target_node = target_node.struct[parseInt(i)-1];
                    });
                    if ( target_node != null && target_node != undefined ){
                        target_node.struct.splice( 
                            parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                            0,
                            target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                    }
                } else {
                    // 调换次序
                    // DEBUG
                    // console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");

                    // self.props.paper.paper.struct.splice(
                    //     parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                    //     0,
                    //     self.props.paper.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    // );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    if ( typeof target_node_tixu == "string" ){
                        var target_node = null;
                        target_node_tixu.split("-").map(function(i, index){
                            if ( target_node == null ) target_node = self.props.paper.paper.struct[parseInt(i)-1];
                            else target_node = target_node.struct[parseInt(i)-1];
                        });
                    } else {
                        target_node = src.component.props.paper.paper;
                    }

                    var dest_node_tixu = dest.component.props.parent.tixu;
                    if ( typeof dest_node_tixu == "string" ){
                        var dest_node = null;
                        dest_node_tixu.split("-").map(function(i, index){
                            if ( dest_node == null ) dest_node = self.props.paper.paper.struct[parseInt(i)-1];
                            else dest_node = dest_node.struct[parseInt(i)-1];
                        });
                    } else {
                        dest_node = dest.component.props.paper.paper;
                    }


                    dest_elem = dest_node.struct.splice( parseInt( dest.tixu.split("-").slice(-1)[0] )-1, 1 )[0];
                    target_elem = target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0];

                    dest_node.struct.splice( parseInt( dest.tixu.split("-").slice(-1)[0] )-1, 0,  target_elem);
                    target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 0,  dest_elem);

                    if ( "cqid" in dest_node ) {
                        self.props.paper.composites[dest_node.cqid].struct.splice(
                            parseInt( dest.tixu.split("-").slice(-1)[0] )-1,
                            1,
                            target_elem
                        );
                    } else if ( "qid" in dest_node ) {

                    } else if ( "pid" in dest_node ) {

                    }

                    if ( "cqid" in target_node ) {
                        self.props.paper.composites[target_node.cqid].struct.splice(
                            parseInt( src.tixu.split("-").slice(-1)[0] )-1,
                            1,
                            dest_elem
                        );
                    } else if ( "qid" in target_node ) {

                    } else if ("pid" in target_node){

                    }

                }

                // console.log( JSON.stringify( new_struct ) );
                // console.log( JSON.stringify( self.props.paper.paper.struct ) );

            }
        }
        this.props.paper.diffElement({"pid": this.props.paper.paper.pid});

        // console.log( JSON.stringify(self.props.paper.paper.struct));
        self.props.paper.forceUpdate();
        
        return false;

        try{
            setTimeout(function(){
                console.log("... unmount ... ");
                React.unmountComponentAtNode(document.getElementById('container'));
                $("#container").empty();
                window.views.papers.edit(self.props.paper.paper.pid);
            }, 500);
        }catch(e){
            throw new Error("unmount react element fail.");
        }
        return true;
    },
    hit_the_kp: function (cid, cname){
        // 选中 知识点
        var self = this;
        var attrs = this.props.paper.questions[this.props.qid].attrs;

        var attr_kp_index = 0;
        var attr_kp = attrs.filter(function(attr, index){
            if ( attr.name == "知识点" ) {
                attr_kp_index = index;
                return true;
            }
        });
        attr_kp = attr_kp[0];

        var kps = attr_kp.value.split(",").concat();
        if ( kps[0] == "" || kps[0].indexOf(":") == -1 ) kps.splice(0, 1);

        if ( attr_kp.value.indexOf( cid+":"+cname ) == -1 ) {
            attr_kp.value = kps.concat( cid+ ":" + cname).join(",");
            this.props.paper.questions[this.props.qid].attrs[attr_kp_index] = attr_kp;

            this.props.paper.diffElement({"qid": this.props.qid});
            this.forceUpdate();
        } else {
            console.info(":: 重复添加");
        }
    },
    hit_the_kps: function (kps){
        var self = this;
        var api = self.props.paper.api.catalog.question;

        kps.map(function (kp, i){
            self.hit_the_kp(kp.cid, kp.name);
            // 绑定试题元素至
            api.bind_elems("append", kp.cid, {"id": self.props.qid, "name": ""});
        });
    },
    remove_kp: function (kp){
        // kp: { "cid": 234, "name": "" }
        var self = this;
        var attrs = this.props.paper.questions[this.props.qid].attrs;

        var attr_kp_index = 0;
        var attr_kp = attrs.filter(function(attr, index){
            if ( attr.name == "知识点" ) {
                attr_kp_index = index;
                return true;
            }
        });
        attr_kp = attr_kp[0];

        var kps = attr_kp.value.split(",");
        if ( kps[0] == "" || kps[0].indexOf(":") == -1 ) kps.splice(0, 1);

        if ( attr_kp.value.indexOf( kp.cid + ":" + kp.name ) != -1 ) {
            kps.splice(kps.indexOf( kp.cid + ":" + kp.name ), 1);

            attr_kp.value = kps.join(",");
            // 从标签元素树当中剔除当前试题元素
            var api = self.props.paper.api.catalog.question;
            api.bind_elems("remove", kp.cid, {"id": self.props.qid, "name": ""});

            this.props.paper.questions[this.props.qid].attrs[attr_kp_index] = attr_kp;
            this.props.paper.diffElement({"qid": this.props.qid});
            this.forceUpdate();
        } else {
            console.info(":: 该标签不存在与该试题当中 ... ");
        }
    },
    clean_kps: function (e, rid){
        // 清空知识点
        var self = this;
        var attrs = this.props.paper.questions[this.props.qid].attrs;

        var attr_kp_index = 0;
        var attr_kp = attrs.filter(function(attr, index){
            if ( attr.name == "知识点" ) {
                attr_kp_index = index;
                return true;
            }
        });
        attr_kp = attr_kp[0];

        var kps = attr_kp.value.split(",");
        if ( kps[0] == "" || kps[0].indexOf(":") == -1 ) kps.splice(0, 1);

        var api = self.props.paper.api.catalog.question;
        kps.map(function (__kp, _index){
            // 从标签元素树当中剔除当前试题元素
            if ( __kp.length < 1 || __kp.indexOf(":") == -1 ) return false;
            api.bind_elems("remove", parseInt(__kp.split(":")[0]), {"id": self.props.qid, "name": ""});
        });
        
        this.props.paper.questions[this.props.qid].attrs[attr_kp_index] = {name: "知识点", value: "" };
        this.props.paper.diffElement({"qid": this.props.qid});
        this.forceUpdate();
    },
    ktree: function (){
        // this.mktree();
        var self = this;
        var node = $('<div class="ktree_dialog"></a>');
        node.dialog({
            "title": "选择知识点",
            "width": 375,
            "height": 360
        });
        var destroy = function (){
            $(node).dialog("close");
        };
        React.render(
            React.createElement( window.components.catalogs.atoms.ktree_dialog, {'ok': self.hit_the_kps, 'destroy': destroy } ), 
            node[0]
            // document.getElementById("dialog") 
        );
    },
    ltree: function (){

    },
    hide: function (event){
        if ( this.ishide == false ) {
            this.ishide = true;
            $(event.target).addClass("paper_item_show").removeClass("paper_item_hide");
        } else if ( this.ishide == true ) {
            this.ishide = false;
            $(event.target).addClass("paper_item_hide").removeClass("paper_item_show");
        } else {
            console.warn("unknow status...");
        }
        this.forceUpdate();
    },
    render: function (){
        var self = this;
        if ( !this.props.paper.questions[this.props.qid] ) {
            var _ques = JSON.parse(JSON.stringify(this.props.paper.statics.constant.question));
            _ques.qid = this.props.qid;
            this.props.paper.questions[this.props.qid] = _ques;
        }

        var computer_choice_className = function (choice_card){
            if ( self.props.paper.questions[self.props.qid].ans.indexOf(choice_card) != -1) {
                return "letter_active";
            } else {
                return "paper_sel_letter";
            }
        };
        var computer_attr_ischouti = function (value, n){
            if ( parseInt(n) == 1 ) {
                if ( !value ) return false;
                if ( value == "是" ) return true;
                else return false;
            } else if ( parseInt(n) == -1 ) {
                if ( !value ) return true;
                if ( value == "是" ) return false;
                else return true;
            } else {
                console.warn(":: 判断 试题属性当中的 抽题 属性 失败!");
                return false;
            }
        };
        var computer_choices_dom = function (question){

            /* 
              {"ans":"","ansnum":0,"attrs":[{"name":"","value":""}],
               "choices":[],"content":"","inittime":14545454545,"updtime":121254545454,"score":0,
               "tombstone":0,"qid":8888
               }
            */
            //
            if ( !question ) return [];
            var choices_dom = [];
            if ( question.choices.length == 0 && question.ans.length != 0 ){
                // 主观题
                choices_dom.push(<div className="paper_choice" style={ {"backgroundColor": "rgb(255, 255, 255)"} } key={"qid"+self.props.qid + "_answer_"} >
                                    <div className="paper_sel_answer">答</div>
                                    <div contentEditable="true" className="paper_edit_div fl" onBlur={self.setAnswer}>{question.ans}</div>
                                    <div className="clearfix"></div>
                                </div>);
            } else if ( question.choices.length > 0 ){
                // 选择题
                question.choices.map(function (choice, index){
                    choices_dom.push(<div className="paper_choice"  
                            key={"qid"+self.props.qid + "_choice_"+index} 
                            onBlur={self.updateChoice} 
                            data-index={index}
                            >
                        <div className={ computer_choice_className(chr(65+index)) } onClick={self.setChoice}>{chr(65+index)}</div>
                        <div className="paper_edit_div fl" contentEditable="true" dangerouslySetInnerHTML={{__html: choice}} />
                        <div className="paper_choice_del fr" onClick={self.removeChoice.bind(self, index)}></div>
                        <div className="clearfix"></div>
                    </div>);
                });
            } else {
                if ( question.choices.length == 0 && question.ans.length == 0 ) {

                } else {
                    console.warn("unknow question answer type.");
                }
            }
            return choices_dom;
        };
        var computer_choices_addButton = function (question){
            if ( !question ) return [];
            var button_group = [];
            if ( question.choices.length == 0 && question.ans.length != 0 ){
                // 主观题
                button_group.push(
                    <div className="paper_choice_add">
                        <a onClick={self.addAnswer} className="new_choose">新增一个答案</a>
                    </div>
                );
            } else if ( question.choices.length > 0 ){
                // 选择题
                button_group.push(
                    <div className="paper_choice_add">
                        <a onClick={self.addChoice} className="new_choose">新增一个选项</a>
                    </div>
                );
            } else {
                if ( question.choices.length == 0 && question.ans.length == 0 ) {
                    button_group.push(
                    <div className="paper_choice_add">
                        <a onClick={self.addChoice} className="new_choose">新增一个选项</a>
                        <a onClick={self.addAnswer} className="new_answer">新增一个答案</a>
                    </div>
                );
                } else {
                    console.warn("unknow question answer type.");
                }
            }
            return button_group;
        };

        var parse_attr_kp = function (attr){

        };

        var substance_class;
        if ( self.ishide == true ) substance_class = "none";
        else substance_class = "";

        // defaultValue
        return (
            <div className="element" 
                 key={"Question_"+this.tixu+"_qid_"+this.props.qid}
                 data-tixu={this.tixu} 
                 data-qid={this.props.qid}
                 draggable='true'
                 onDragStart={this.handleDragStart} 
                 onDragEnter={this.handleDragEnter} 
                 onDragEnd={this.handleDragEnd}>
                <div className="paper_item_slide clearfix" >
                    <div className="paper_drag_item none"></div>
                    <div className="paper_item_slide_tit" title="题序">
                        <input className="tixu" type="text" maxlength="10" value={this.props.tx2} />
                    </div>

                    <div className="paper_item_score">
                        <input 
                                    type="text" 
                                    maxlength="3" 
                                    className="score"
                                    placeholder={this.props.paper.questions[this.props.qid].score} 
                                    onChange={this.setScore} />分
                    </div>
                    <div className="paper_item_title" 
                            contentEditable="true" 
                            onBlur={this.setContent} dangerouslySetInnerHTML={{__html: this.props.paper.questions[this.props.qid].content}} />
                    <div className="paper_item_del none" onClick={this.removeDialog}></div>
                    <div className="paper_item_toggle paper_item_hide none" onClick={this.hide}></div>
                </div>
                <div className="papaer_del none" data-qid={this.props.qid}>
                    <a className="cancel" onClick={this.remove.bind(self, 'cancel')} data-qid={this.props.qid}>取消</a>
                    <a className="delete" onClick={this.remove.bind(self, 'delete')} data-qid={this.props.qid}>删除</a>
                </div>

                <div className="paper_item_substance" style={{"display": substance_class}}>
                    <div className="paper_item_choice">
                        {computer_choices_dom(this.props.paper.questions[this.props.qid])}
                    </div>
                    {computer_choices_addButton(this.props.paper.questions[this.props.qid])}

                    <div className="paper_item_attr">
                        { this.props.paper.questions[this.props.qid].attrs.map(function(attr, index){
                            if ( attr.name == "知识点" ) {
                                var kps = attr.value.split(",");
                                if ( kps[0] == "" || kps[0].indexOf(":") == -1 ) kps.splice(0, 1);
                                
                                return (
                                    <div className="pape_attr paper_attr_sel clearfix" style={ {"backgroundColor": "rgb(255, 255, 255)"} }>
                                        <span className="attr_sel">知识点</span>
                                        <ul className="clearfix">
                                            { kps.map(function(kp, i){
                                                if ( kp.length < 1 || kp.indexOf(":") == -1 ) return null;
                                                var cid = parseInt(kp.split(":")[0]);
                                                var name = kp.split(":")[1];
                                                // console.log("CID:" + cid + "   " + "CNAME: " + name);
                                                return (
                                                    <li 
                                                        key={"Question" + self.props.qid + "_attrs_"+index+"_kp_"+cid.toString()+"_"} 
                                                        className="kp" 
                                                        data-cid={cid}>
                                                            {name}
                                                        <a onClick={self.remove_kp.bind(self, {"cid": cid, "name": name})}>
                                                            <img src="static/images/add_test_close.png" className="li_img" />
                                                        </a>
                                                    </li>
                                                );
                                            }) }
                                        </ul>
                                        <a className="li_a" onClick={self.ktree}></a>
                                        <span className="paper_attr_del fr none" onClick={self.clean_kps}></span>
                                        <div className="paper_item_del none"></div>
                                    </div>
                                );
                            } else if ( attr.name == "抽题" ) {
                                return (
                                    <div className="pape_attr" key={"Question"+self.props.qid + "_attrs_"+index+attr.name} data-index={index} >
                                        <span className="attr">{attr.name}</span>
                                        <div className="pape_attr_draw">是否参与</div>
                                        <fieldset className="radios">
                                            <label className="label_radio r_on">
                                                <input type="radio" 
                                                       key={"Question"+self.props.qid + "_attrs_"+index+"_radio-01_"+attr.name}
                                                       name={"qid"+self.props.qid+"__ATTR__sample-radio-01"} 
                                                       className="radio-01" 
                                                       data-index={index}
                                                       checked={computer_attr_ischouti(attr.value, 1)} 
                                                       onChange={self.updateAttr_Chouti} />是
                                            </label>
                                            <label className="label_radio">
                                                <input type="radio" 
                                                       key={"qid"+self.props.qid + "_attrs_"+index+"_radio-02_"}
                                                       name={"qid"+self.props.qid+"__ATTR__sample-radio-02"} 
                                                       className="radio-02" 
                                                       data-index={index}
                                                       checked={computer_attr_ischouti(attr.value, -1)} 
                                                       onChange={self.updateAttr_Chouti} />否
                                            </label>
                                        </fieldset>
                                        
                                        <div className="clearfix"></div>
                                    </div>
                                );
                            } else if ( attr.name == "年份" ) {
                                return (
                                    <div className="pape_attr" 
                                            key={"qid"+self.props.qid + "_attrs_"+index}  
                                            data-index={index} >
                                        <span className="attr" contentEditable="true" >{attr.name}</span>
                                        <select name="QuestionYear">
                                            { range(2000,2005).map(function (y, i){
                                                if ( parseInt(attr.value) == parseInt(y) ) {
                                                    return ( <option value={y} selected>{y}</option> );
                                                } else {
                                                    return ( <option value={y}>{y}</option> );
                                                }
                                            }) }
                                        </select>
                                        <div className="clearfix"></div>
                                    </div>
                                );
                            } else if ( attr.name == "地区" ) {
                                var kps = attr.value.split(",");
                                if ( kps[0] == "" || kps[0].indexOf(":") == -1 ) kps.splice(0, 1);
                                
                                return (
                                    <div className="pape_attr paper_attr_sel clearfix" style={ {"backgroundColor": "rgb(255, 255, 255)"} }>
                                        <span className="attr_sel">地区</span>
                                        <ul className="clearfix">
                                            { kps.map(function(kp, i){
                                                if ( kp.length < 1 || kp.indexOf(":") == -1 ) return null;
                                                var cid = parseInt(kp.split(":")[0]);
                                                var name = kp.split(":")[1];
                                                // console.log("CID:" + cid + "   " + "CNAME: " + name);
                                                return (
                                                    <li 
                                                        key={"Question" + self.props.qid + "_attrs_"+index+"_kp_"+cid.toString()+"_"} 
                                                        className="kp" 
                                                        data-cid={cid}>
                                                            {name}
                                                        <a onClick={self.ltree.bind(self, {"cid": cid, "name": name})}>
                                                            <img src="static/images/add_test_close.png" className="li_img" />
                                                        </a>
                                                    </li>
                                                );
                                            }) }
                                        </ul>
                                        <a className="li_a" onClick={self.ltree}></a>
                                        <span className="paper_attr_del fr none" onClick={self.ltree}></span>
                                        <div className="paper_item_del none"></div>
                                    </div>
                                );
                            } else if ( ["作者", "来源", "解析", "纠错", "拓展"].indexOf(attr.name) != -1 ) {
                                if ( attr.value == undefined ) {
                                    // console.info( attr.value );
                                    return null;
                                }
                                return (
                                    <div className="pape_attr" 
                                            key={"qid"+self.props.qid + "_attrs_"+index}  
                                            onBlur={self.updateAttr}
                                            data-index={index} >
                                        <span className="attr">{attr.name}</span>
                                        <div className="pape_attr_edit" contentEditable="true" dangerouslySetInnerHTML={{ __html: attr.value }} />
                                        <div className="clearfix"></div>
                                    </div>
                                );
                            } else {
                                return (
                                    <div className="pape_attr" 
                                            key={"qid"+self.props.qid + "_attrs_"+index}  
                                            onBlur={self.updateAttr}
                                            data-index={index} >
                                        <span className="attr" contentEditable="true" >{attr.name}</span>
                                        <div className="pape_attr_edit" contentEditable="true" dangerouslySetInnerHTML={{__html: attr.value}} />
                                        <span className="paper_attr_del fr none" onClick={self.removeAttr.bind(self, index)}></span>
                                        <div className="clearfix"></div>
                                    </div>
                                );
                            }
                        }) }
                    </div>
                    <div className="paper_attr_add">
                        <a onClick={this.addAttr}>新增一个标签</a>
                    </div>                        
                </div>
            </div>
        );
    }
});



CompositeBox = React.createClass({
    displayName: "CompositeBox",
    name: "复合题",
    tixu: 0,
    tixu2: "",
    index: 0,
    ishide: false,
    api : new apis.composites(),
    composite: {
                    "cqid": "0",
                    "content": "",
                    "tombstone":0,
                    "updtime": time.time(),
                    "inittime": time.time(),
                    "struct": []
                },
    getInitialState: function (){
        var self = this;
        if ( !this.props.index || ! this.props.paper ) throw Error("CompositesBox init fail.");
        this.tixu = this.props.index.join("-");
        // this.tixu2 = [ 
        //                         this.props.paper.compute_questions( "pos", this.props.paper.composites[this.props.cqid] ) + 1 , 
        //                         this.props.paper.compute_questions( "pos", this.props.paper.composites[this.props.cqid] ) + 1 + 
        //                         this.props.paper.compute_questions( "children", this.props.paper.composites[this.props.cqid] )
        //                     ].join("-");
        this.tixu2 = this.props.tx2;

        // {"struct":[{"qid":"67758"},{"qid":"67759"},{"qid":"67760"}]};
        
        return {};
    },
    componentDidMount: function() {
        console.info("CompositeBox is mount.");
        var self = this;
        // self.pull();
        this.props.index.map(function(t, i){
            self.index += parseInt(t);
        });
        
        var new_tcard = {
                "index": this.index, 
                "tixu": this.tixu, 
                "tixu2": this.props.tx2,
                "component" : this, 
                "element": this.props.paper.composites[this.props.cqid] 
        };
        /*
        try{
            var ii = this.props.paper.tixu.map(function (t, i){
                if ( "cqid" in t.element && parseInt(t.element.cqid) == parseInt(self.props.cqid) ) {
                    return i;
                }
            }).filter(function(t, i){
                return t != undefined; 
            });

            if ( ii.length>0 ) self.props.paper.tixu[ii[0]] = new_tcard;
            else self.props.paper.tixu[self.props.paper.tixu.length] = new_tcard;
            // this.props.paper.tixu[this.props.paper.tixu.length] = {"index": this.index, "tixu": this.tixu, "component" : this, "element": this.props.paper.composites[this.props.cqid] };
        }catch(e){};
        */
        // console.log(":: composite tixu card : ");
        // console.log(new_tcard);
        this.props.paper.tixu_card[this.tixu] =  new_tcard;
        this.props.paper.elements.composites[this.props.cqid] =  new_tcard;
        this.props.paper.forceUpdate();
        this.forceUpdate();
    },
    // pull:function(){
    //     var self = this;
    //     var  callback = function(response){
    //           if( response.type == "progress") return "loading...";
    //           if( response.code != 200){ return false;}
    //           try{
    //               var body = JSON.parse(response.body);
    //                self.state.struct = body.struct;
    //                self.forceUpdate();
    //           }catch(e){ 

    //           }
    //     }
    //     self.api.pull(self.props.cqid,callback);
    // },
    componentDidUpdate: function (){
        // console.info("CompositeBox will update");
        var new_tcard = {
                "index": this.index, 
                "tixu": this.tixu, 
                "tixu2": this.props.tx2,
                "component" : this, 
                "element": this.props.paper.composites[this.props.cqid] 
        };
        this.props.paper.tixu_card[this.tixu] =  new_tcard;
        this.props.paper.elements.composites[this.props.cqid] =  new_tcard;
    },
    setContent: function (e, rid){
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid));
        this.props.paper.composites[this.props.cqid].content = $(obj).html().replace(/<(?!img)(?:.|\s)*?>/ig, "");
        this.forceUpdate();
        this.props.paper.diffElement({"cqid": this.props.cqid});
    },
    removeDialog: function (e, rid){
        var self = this;
        var obj = $("[data-reactid='#rid#']".replace("#rid#", rid))
                                        .parent().parent()
                                        .find('.papaer_del[data-cqid="' + this.props.cqid + '"]');
        $(obj).removeClass("none");
    },
    remove: function (action, e, rid){
        var self = this;
        if ( action == "cancel" ) {
            $("[data-reactid='#rid#']".replace("#rid#", rid)).parent().addClass("none");
            return ;
        } else if ( action == "delete" ) {
            var struct = JSON.parse(JSON.stringify(this.props.paper.paper.struct));
            var cqid = parseInt(this.props.cqid);
            var new_struct = [];

            var parse_question = function (question) {
                return question;
            };
            var parse_composite = function (composite){
                if ( "struct" in composite != true ) composite.struct = [];
                var elem;
                var _struct = [];
                for ( var i =0; i<composite.struct.length; i++ ){
                    elem = composite.struct[i];
                    if ( "cqid" in elem  ) {
                        if ( parseInt(elem.cqid) != cqid ) {
                            _struct[_struct.length] = parse_composite(elem);
                        }
                    } else if ( "qid" in elem ) {
                        _struct[_struct.length] = parse_question(elem);
                    } else {
                        console.warn("unknow type element.");
                    }
                }
                composite.struct = _struct;
                return composite;
            };


            struct.map(function (elem, index){
                if ( "cqid" in elem  ) {
                    if ( parseInt(elem.cqid) != cqid ) {
                        new_struct[new_struct.length] = parse_composite(elem);
                    }
                } else if ( "qid" in elem ) {
                    new_struct[new_struct.length] = parse_question(elem);
                } else {
                    console.warn("unknow type element.");
                }
            });

            console.log( JSON.stringify(struct) );
            console.log( JSON.stringify(new_struct) );

            this.props.paper.paper.struct = new_struct;
            this.props.paper.forceUpdate();
            this.props.paper.diffElement({"pid": this.props.paper.paper.pid});
        } else {
            console.warn("unknow remove action.");
        }
    },
    handleDragStart: function (e, rid){
         if ( $(e.target).hasClass('element') ){
            var tixu = $(e.target).data("tixu");
            console.info("composite: 正在拖动元素 " + tixu);
            window._dragevent = {"src": tixu.toString(), "dest": null};
        }
    },
    handleDragEnter: function (e, rid){
        if ( $(e.target).hasClass('element') ){
            // 正确的位置
            var tixu = $(e.target).data("tixu");
            console.info("composite: 拖动元素至 " + tixu);
            window._dragevent.dest = tixu.toString();
        } else if ( $(e.target).parent().hasClass('element') ) {
            var tixu = $(e.target).parent().data("tixu");
            console.info("composite: 拖动元素至 " + tixu);
            window._dragevent.dest = tixu.toString();
        } else {
            // console.log( $(e.target) );
        }
    },
    handleDragEnd: function (e, rid){
        var self = this;
        console.info("composite drag .");
        if ( window._dragevent.dest == null || window._dragevent.src == null ) {
            console.warn("question: 指令无法执行");
            return false;
        }
        if ( window._dragevent.dest == null || window._dragevent.src == null ) {
            console.warn("question: 指令无法执行");
            return false;
        }

        console.info(window._dragevent);

        var dest_tixu = window._dragevent.dest;
        var src_tixu = window._dragevent.src;
        
        if ( dest_tixu.toString() == src_tixu.toString()  ) {
            console.info(":: 不能把自己当作拖动目标。")
            return true;
        }

        var src = this.props.paper.tixu_card[src_tixu];
        var dest = this.props.paper.tixu_card[dest_tixu];

        dest.dom = $( 'div[data-tixu="#tixu#"]'.replace("#tixu#", dest.tixu ) );
        src.dom = $( 'div[data-tixu="#tixu#"]'.replace("#tixu#", src.tixu ) );

        var ok;
        if ( !$(e.target).hasClass('element') )  return false;
        if ( dest.tixu.split("-").length < 1 ) throw new Error("drag fail.");
        
        var isbrother;
        if ( typeof dest.component.props.parent.tixu ==  "object" && typeof src.component.props.parent.tixu == "object" ){
            isbrother = true;
        } else if ( typeof dest.component.props.parent.tixu ==  "string" && typeof src.component.props.parent.tixu == "string" ) {
            if ( dest.component.props.parent.tixu.toString() == src.component.props.parent.toString() ) {
                isbrother = true;
            } else {
                isbrother = false;
            }
        } else {
            isbrother = false;
        }
        
        console.log(src); console.log(dest);
        /////////////////////////////////////////////////////////////////////////////////////////////////
        if ( dest.tixu.split("-").length < 2 && src.tixu.split("-").length < 2 ) {
            // 顶层位移
            if ( "qid" in src.element  && "cqid" in dest.element ) {
                // 注入 分组元素当中
                console.log("qid to cqid in paper struct.");
                self.props.paper.paper.struct.splice( parseInt(src.tixu.split("-").slice(-1)[0])-1, 1);
                if ( parseInt(dest.tixu.split("-").slice(-1)[0]) < parseInt( src.tixu.split("-").slice(-1)[0] ) ){
                    self.props.paper.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-1].struct.push(
                        {"qid": src.element.qid }
                    );
                } else {
                    self.props.paper.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-2].struct.push(
                        {"qid": src.element.qid }
                    );
                }
                // self.props.paper.paper.struct.splice( parseInt(src.tixu.split("-").slice(-1)[0])-1, 1);
                // self.props.paper.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-1].struct.push( {"qid": src.element.qid } );
                self.props.paper.composites[dest.element.cqid].struct.push( {"qid": src.element.qid } );
                this.props.paper.diffElement({"cqid": dest.element.cqid});
            } else {
                // 调换次序
                console.log("qid/cqid to cqid/qid postion in paper struct.");
                self.props.paper.paper.struct.splice(
                            parseInt(dest.tixu.split("-").slice(-1)[0])-1, 
                            0,
                            self.props.paper.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                var new_struct = self.props.paper.paper.struct.filter(function (e, i){
                    return e != undefined && e != null && typeof e == 'object';
                });
                self.props.paper.paper.struct = new_struct;
            }
        } else {
            // 非顶层复杂变幻 (相同节点之间元素互相调换)
            if ( parseInt(dest.tixu.split("-").splice(-2)[0]) == parseInt(src.tixu.split("-").splice(-2)[0]) ){
                // 位移
                if ( "qid" in src.element  && "cqid" in dest.element ) {
                    // 注入 分组元素当中
                    console.log("位移-注入: qid to cqid in paper struct.");
                    self.props.paper.composites[src.component.props.parent.props.cqid].struct.splice( 
                        parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                        0,
                        self.props.paper.composites[src.component.props.parent.props.cqid]
                            .struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    var target_node = null;
                    target_node_tixu.split("-").map(function(i, index){
                        if ( target_node == null ) target_node = self.props.paper.paper.struct[parseInt(i)-1];
                        else target_node = target_node.struct[parseInt(i)-1];
                    });
                    if ( target_node != null && target_node != undefined ){
                        target_node.struct.splice( 
                            parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                            0,
                            target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                    }
                } else {
                    // 调换次序
                    console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");
                    self.props.paper.paper.struct.splice(
                                parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                                0,
                                self.props.paper.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                            );
                    var new_struct = self.props.paper.paper.struct.filter(function (e, i){
                        return e != undefined && e != null && typeof e == 'object';
                    });
                    self.props.paper.paper.struct = new_struct;
                }

                console.log( JSON.stringify( new_struct ) );
                console.log( JSON.stringify( self.props.paper.paper.struct ) );
            } else {
                // 层移 （不同节点之间元素 移位）
                console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");
                if ( "qid" in src.element  && "cqid" in dest.element ) {
                    // 注入 到分组元素当中 （跨节点远程注入）
                    console.log("位移-注入: qid to cqid in paper struct.");
                    self.props.paper.composites[src.component.props.parent.props.cqid].struct.splice( 
                        parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                        0,
                        self.props.paper.composites[src.component.props.parent.props.cqid]
                            .struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    var target_node = null;
                    target_node_tixu.split("-").map(function(i, index){
                        if ( target_node == null ) target_node = self.props.paper.paper.struct[parseInt(i)-1];
                        else target_node = target_node.struct[parseInt(i)-1];
                    });
                    if ( target_node != null && target_node != undefined ){
                        target_node.struct.splice( 
                            parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                            0,
                            target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                    }
                } else {
                    // 调换次序
                    
                }
                
            }
        }
        this.props.paper.diffElement({"pid": this.props.paper.paper.pid});

        console.log( JSON.stringify(self.props.paper.paper.struct));
        self.props.paper.forceUpdate();
        
    },
    hide: function (){
        if ( this.ishide == false ) {
            this.ishide = true;
        } else if ( this.ishide == true ) {
            this.ishide = false;
        } else {
            console.warn("unknow status...");
        }
        this.forceUpdate();
    },
    compute_questions: function (elem){
        var num = 0;

        var parse_question = function (question){
            // if ( parseInt(question.qid) in self.elements.questions ) {
            //     tixu_card.push(self.elements.questions[parseInt(question.qid)] );
            // }
            num += 1;
        };
        var parse_composite = function (composite){
            // if ( composite.cqid in self.elements.composites ) {
            //     tixu_card.push(self.elements.composites[parseInt(composite.cqid)] );
            // }
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };
        var i = 0;
        for (i=0; i<elem.struct.length; i++ ) {
            if ( "qid" in elem.struct[i] == true) {
                parse_question(elem.struct[i]);
            } else if ( "cqid" in elem.struct[i] == true) {
                parse_composite(elem.struct[i]);
            } 
        }
        console.info("该分组总共有" + num + "  个最小元素.");
        return num-1;
    },
     onAddQues: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn("onAddQues Fail");console.warn(response);
                return;
            }
            try{
                var body = JSON.parse(response.body);console.log(body);
            }catch(e){
                console.warn(e); return false;
            }
            // self.questions[body.qid] = body;
            
            var i =self.props.index[0]-1;
            self.props.paper.questions[body.qid] = body;
            self.props.paper.paper.struct[i].struct.push({"qid":body.qid});
            self.props.paper.composites[self.props.cqid].struct.push({"qid":body.qid});
            
            // alert( self.props.paper.elements.composites[parseInt(self.props.cqid)].tixu2)
            // self.props.paper.elements.composites[self.props.cqid].struct.push({"qid":body.qid});
            // self.props.paper.elements.composites[self.props.cqid].element.struct.push({"qid":body.qid});
            // self.props.paper.api.paper.updateStruct(self.props.paper.props.pid,"append",[{"cqid":body.}])
                // self.api.paper.updateStruct(self.props.pid, "append", [{ "cqid": body.cqid ,"struct":[]}]);
            self.props.paper.api.composite.updateStruct(self.props.cqid, "append", [{ "qid": body.qid }]);
            var new_tixu2 = [ self.props.paper.compute_questions( "pos", self.props.paper.composites[self.props.cqid] ) + 1 , 
                             self.props.paper.compute_questions( "pos", self.props.paper.composites[self.props.cqid] ) 
                             + self.props.paper.compute_questions( "children", self.props.paper.composites[self.props.cqid] )].join("-");
          
            self.props.paper.elements.composites[parseInt(self.props.cqid)].tixu2 = new_tixu2;
            // self.props.paper.forceUpdate();
            // slef.props.paper.forceUpdate();
            // console.log(new_tixu2);
            // var new_tcard = {
            //     "index": self..index, 
            //     "tixu": self.tixu, 
            //     "tixu2": new_tixu2,
            //     "component" : self, 
            //     "element": self.props.paper.composites[self.props.cqid] 
            //    };
            //  self.props.paper.tixu_card[this.tixu] =  new_tcard;
            //  self.props.paper.elements.composites[self.props.cqid] =  new_tcard;
            // self.props.paper.api.paper.updateStruct(self.props.pid,"")
            self.forceUpdate();
            self.props.paper.forceUpdate();
            //--------------------------------------------------------------------------
            // self.composites[body.cqid] = body;
            // self.paper.struct.push( {"cqid": body.cqid, "struct": [] } );
            // self.api.paper.updateStruct(self.props.pid, "append", [{ "cqid": body.cqid }]);
            // self.forceUpdate();
        };
        // 试卷地区 & 试卷年份
        var year="", area="";
        this.props.paper.paper.attrs.map(function (attr, index){
            if ( attr.name == "year" || attr.name == "年份" ) year = attr.value;
            if ( attr.name == "area" || attr.name == "地区" ) area = attr.value;
        });

        var question = this.props.paper.statics.constant.question;
        var attrs = question.attrs.concat();

        attrs.map(function (attr, index){
            if ( attr.name == "年份") attrs.splice(index, 1); // target_index.push(index);
        });
        attrs.map(function (attr, index){
            if ( attr.name == "地区") attrs.splice(index, 1); // target_index.push(index);
        });

        attrs.push({"name": "年份", "value": year.toString() });
        attrs.push({"name": "地区", "value": area.toString() });

        question.attrs = attrs;
        // self.statics.constant.question.attrs.push({"name": "年份", "value": year.toString() });
        // self.statics.constant.question.attrs.push({"name": "地区", "value": area.toString() });
        this.props.paper.api.question.init( question, callback );
    },
    render: function (){
        var self = this;
        var __style = {};
        if ( this.ishide == true ) __style.display = "none";
        
        return (
            <div className="element"
                key={"Composite_"+this.tixu+"_cqid_"+this.props.cqid}
                data-tixu={this.tixu} 
                data-cqid={this.props.cqid}
                draggable='true'
                onDragStart={this.handleDragStart} 
                onDragEnter={this.handleDragEnter} 
                onDragEnd={this.handleDragEnd}>
                
                <div className="paper_item_slide clearfix composite">
                    <div className="paper_drag_item none"></div>
                    <div className="paper_item_slide_tit" title="题序">
                        <input className="tixu" type="text" maxlength="10" value={this.props.tx2} style={{"width":"50px"}}/>
                        （<span className="flag">{this.name}</span>）
                    </div>
                    <div className="paper_item_title" contentEditable="true" onBlur={this.setContent}>{this.props.paper.composites[this.props.cqid].content}</div>
                    <div className="paper_item_del none" onClick={this.removeDialog}></div>
                    <div className="paper_item_toggle paper_item_hide none" onClick={this.hide}></div>
                </div>
                <div className="papaer_del none" data-cqid={this.props.cqid}>
                    <a className="cancel" onClick={this.remove.bind(self, 'cancel')} data-cqid={this.props.cqid}>取消</a>
                    <a className="delete" onClick={this.remove.bind(self, 'delete')} data-cqid={this.props.cqid}>删除</a>
                </div>

                <div className="paper_item_substance_composite" style={__style} >
                    { self.props.paper.composites[self.props.cqid].struct.map(function(elem, index){
                        
                        if ( "qid" in elem && self.props.paper.questions[elem.qid] != undefined) {
                            return <QuestionBox 
                                        key={"Question"+elem.qid}
                                        qid={elem.qid} 
                                        index={self.props.index.concat(index+1)} 
                                        // tx2={self.props.paper.compute_questions( "pos", self.props.paper.questions[elem.qid] ) + 1}
                                        tx2={self.props.paper.compute_questions( "pos", self.props.paper.composites[self.props.cqid] ) + 1 +index }
                                        paper={self.props.paper}
                                        parent={self} />
                        } else if ("cqid" in elem && self.props.paper.composites[elem.cqid] != undefined && "content" in self.props.paper.composites[elem.cqid] ) {
                            return <CompositeBox 
                                        key={"Composite"+elem.cqid} 
                                        cqid={elem.cqid} 
                                        index={self.props.index.concat(index+1)} 
                                        tx2={[ self.props.paper.compute_questions( "pos", self.props.paper.composites[elem.cqid] ) + 1 , self.props.paper.compute_questions( "pos", self.props.paper.composites[elem.cqid] ) + self.props.paper.compute_questions( "children", self.props.paper.composites[elem.cqid] )].join("-")}
                                        paper={self.props.paper} 
                                        parent={self} />
                        } else {
                            // console.warn("CompositeBox say:  unknow elem type. \'composite \' ??? ");
                            // console.log(elem); 
                        }
                    } ) }
                        <div className="paper_add">
                                        <a className="add_single" onClick={this.onAddQues}>新增普通题</a>
                                      
                        </div>
                </div>
            </div>
        );
    }
});





window.components.papers.paper =  React.createClass({
    displayName: "components.papers.paper",
    name: "试卷容器",
    api: { 
            "paper": new apis.papers(), 
            "composite": new apis.composites(), 
            "question": new apis.questions(),
            "catalog": {
                "paper": new apis.catalogs.papers(),
                "exam": new apis.catalogs.exams(),
                "question": new apis.catalogs.atoms(),
                // "composite": new apis.catalogs.composites(),
            }
    },
    
    paper: {
                    "pid": 0,
                    "status": 0,
                    "name": "",
                    "inittime": time.time(),
                    "updtime": time.time(),
                    "attrs": [],
                    "struct": [],
                    "tombstone": 0,
                },
    composites: {},
    questions: {},
    tixu: [],
    tixu_card: {}, // 题序卡
    tixu_card2: [],
    elements: {"questions":{}, "composites": {}},
    docx_html: "",
    counter: 0,         // 计数器  ( 试题计数器, 不包括 组合元素 )
    getInitialState: function (){
        this.statics = { 
            "constant": {
                "question": {
                    "tombstone":0,
                    "updtime":time.time(),
                    "inittime":time.time(),
                    "qid":0,
                    "score": 0,
                    "content": "",
                    "ans": "",
                    "ansnum": 0,
                    "choices": [],

                    // "attrs": [{"name": "知识点", "value": JSON.stringify( [{"cid", "name"}, ] ) }] 
                    // 985:默认地区,
                    "attrs": [
                        {"name": "知识点", "value": "" },
                        {"name": "来源", "value": "" },
                        {"name": "年份", "value": "2015"},
                        {"name": "地区", "value": ""},
                        {"name": "作者", "value": "" },
                        {"name": "解析", "value": "" },
                        {"name": "纠错", "value": "" },
                        {"name": "拓展", "value": "" },
                        {"name": "抽题", "value": "否" },
                    ]
                },
                "composite": {
                    "cqid": 0,
                    "content": "",
                    "tombstone":0,
                    "updtime": time.time(),
                    "inittime": time.time(),
                    "struct": []
                },
                "paper": {
                    "pid": 0,
                    "status": 0,
                    "name": "",
                    "inittime": time.time(),
                    "updtime": time.time(),
                    "attrs": [],
                    "struct": [],
                    "tombstone": 0,
                }
            }
        };
        this.paper = this.statics.constant.paper;
        window.paper_edit = this;
        return {
                    "paper": this.statics.constant.paper,
                    "composites": {},
                    "questions": {},
                    "tixu": {},
                    "update": time.time() 
                };
    },
    componentDidMount: function() {
        var self = this;
        if ( parseInt(this.props.pid) > 0 ){
            this.pull();
        } else {
            console.warn("试卷编号 #pid# 不正确".replace("#pid#", this.props.pid) );
        }
        ////////////////////////////////效果///////////////////////////////////////
        //初始设置宽度
        $(".import_word").css("height", $(".enter_paper").height() - 1);
        //初始设置宽度
        $(".import_word").width( $('.main').width() - $('.enter_paper').width() - 3 );
        //第一个
        $('.paper_item_slide').eq(0).addClass('noborder');
        $(window).resize(function(){
            $(".import_word").width( $('.paper_enter').width() - $('.enter_paper').width() - 43 );
        });
        //    //试卷信息block固定
        // $(window).scroll(function(){
        //     var scrollTop = $(window).scrollTop(); console.log(scrollTop);
     
        //     if(scrollTop>100){
        //             $(".paper_info").css({"position":"fixed","top":"55px","border":"1px solid red","width":"649px","z-index":"9000000px"})
        //     }else{
        //             $(".paper_info").css({"position":"relative","top":"0"}) 
        //     }
        // })
       
         // $.get("http://218.244.132.33:8080/word2html/moc/async/content/html/81e56933d2292be2f636d1f6d6aa60d9",function(html){
         //        self.docx_html = html;
         //        self.forceUpdate();
         // })
       
        
    },
    componentWillUnmount: function (){
        var self = this;
        // self = undefined; 
        // $(".ui-dialog").remove();
        $("#ajaxLoading").remove();


    },
    // shouldComponentUpdate: function (){
    //     // console.info("** paper shouldComponentUpdate .. ");
    //     //this.mk_tixu_card();
    // },
    // componentWillUpdate: function (){
    //     // this.tixu = [];
    //     // console.info("** paper component will update ...");
    // },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                var body = JSON.parse(response.body);
                if ( body.status == undefined && parseInt(body.status) != 0 ){
                    console.warn("该试卷状态已锁定");
                    alert("该试卷不再允许进行编辑。");
                    window.location.reload();
                    return true;
                }
            }catch(e){
                console.warn(e);
                alert("出错啦 ... ");
                window.location.reload();
            }
            // 清零数据
            console.info("清零数据");
            this.composites = {};
            this.questions = {};
            this.tixu = [];
            tixu_card = {}; // 题序卡
            tixu_card2 = [];
            elements = {"questions":{}, "composites": {}};
            docx_html = "";
            counter = 0;         // 计数器  ( 试题计数器, 不包括 组合元素 )

            this.paper = body;
            //试题多了之后加载变慢，考虑是否加提示
            // if(body.struct.length >22){
            //      var dialog = $('<div id="loadques" style="height:30px;width:300px;font-weight:bold">试题有点多,正在加载,请稍等...</div>');
            //      $(dialog).dialog({
            //                "title":"加载试题",
            //                "width":300,
            //                "height":80,
            //                "modal":true
            //      })
            // }
            this.syncStruct(body);

          
        };
        this.api.paper.pull(this.props.pid, callback.bind(this) );
    },
    reciverElement: function (response){
        var self = this;
        if ( response.type == 'progress' ) return 'loading ...';
        if ( response.code != 200 ) {
            console.warn(response); return;
        }
        try {
            var body = JSON.parse(response.body);
            if ( "cqid" in body ) {
                if ( "struct" in body != true ) body.struct = [];
                this.composites[body.cqid] = body;
            } else if ( "qid" in body ) {
                if ( "attrs" in body != true ) body.attrs = [];
                if ( "choices" in body != true ) body.choices = [];
                this.questions[body.qid] = body;
            } else {
                console.warn("未知元素类型");
                console.warn(response.body); return;
            }
        } catch (e) {
            console.warn(e); return;
        }
        setTimeout(self.forceUpdate.bind(self), 500);
        // this.forceUpdate();
    },
    syncStruct: function (body){
        // 同步 试卷结构
        var self = this;
        var sync_question, sync_composite;

        sync_question = function (elem, index){
            self.api.question.pull(elem.qid, self.reciverElement.bind(self) );
        };
        sync_composite = function (elem, index){
            for ( var ii=0; ii<elem.struct.length; ii++ ) {
                if ( "cqid" in elem.struct[ii] ){
                    sync_composite(elem.struct[ii], index.push(ii) );
                } else if ( "qid" in elem.struct[ii] ) {
                    sync_question(elem.struct[ii], index.push(ii) );
                } else {
                    console.warn("未知的元素类型位于试卷索引"+i+"中");
                    console.warn(elem.struct[ii]);
                }
            }
            self.api.composite.pull(elem.cqid, self.reciverElement.bind(self) );
        };
        for ( var i=0; i<body.struct.length; i++ ) { 

            if ( "cqid" in body.struct[i] ){
                sync_composite(body.struct[i], [i]);
            } else if ( "qid" in body.struct[i] ) {
                sync_question(body.struct[i], [i]);
            } else {
                console.warn("未知的元素类型位于试卷索引"+i+"中");
                console.warn(this.state.paper.struct[i]);
            }
        }
        this.forceUpdate();
    },
    // Diff
    MakeComparison: function (response){
        // 比较元素之间的差异
        var self = this;
        if ( response.type == 'progress' ) return 'loading ...';
        if ( response.code != 200 ) {
            console.warn(response); return;
        }
        try {
            var body = JSON.parse(response.body);
        } catch (e) {
            console.warn(e); return;
        }

        var ok;        // callback.

        if ( "cqid" in body ) {
            if ( JSON.stringify(body) != JSON.stringify(this.composites[body.cqid]) ){
                ok = function (response){
                    if ( response.type == 'progress' ) return 'loading ...';
                    if ( response.code != 200 ) {
                        console.warn(response); return;
                    }
                    self.api.composite.updateBody(body.cqid, self.composites[body.cqid] ); // 不检测同步状态
                };
                var __composite = JSON.parse( JSON.stringify(self.composites[body.cqid]) );
                var __struct = __composite['struct'];
                // var __attrs = __composite['attrs'];
                // delete __composite['struct'];
                // delete __composite['attrs'];
                // this.api.composite.updateStruct(body.cqid, "clean", [] );
                // this.api.composite.updateStruct(body.cqid, "append", __struct );
                self.api.composite.updateBody(body.cqid, __composite );
                // this.api.composite.updateBody(body.cqid, this.composites[body.cqid] ); // 不检测同步状态
            }
        } else if ( "qid" in body ) {
            // if ( this.questions[body.qid].attrs[0].name == "知识点" ) this.questions[body.qid].attrs[0] = {"name": "知识点", "value": JSON.stringify(this.questions[body.qid].attrs[0].value) }
            if ( JSON.stringify(body) != JSON.stringify(this.questions[body.qid]) ){
                this.api.question.updateBody(body.qid, this.questions[body.qid] ); // 不检测同步状态
            }
        } else if ( "pid" in body ) {
            if ( JSON.stringify(body) != JSON.stringify(this.paper) ){
                ok = function (response){
                    if ( response.type == 'progress' ) return 'loading ...';
                    if ( response.code != 200 ) {
                        console.warn(response); return;
                    }
                    var __paper = JSON.parse(JSON.stringify(self.paper));
                    var __struct = __paper['struct'];
                    if ( "attrs" in  __paper != true ) __paper.attrs = []; 
                    var __attrs = __paper['attrs'];

                    delete __paper['struct'];
                    delete __paper['attrs'];
                    
                    self.api.paper.updateStruct(body.pid, "append", __struct );

                    self.api.paper.updateAttrs(body.pid, "clean", __attrs );
                    self.api.paper.updateAttrs(body.pid, "append", __attrs );

                    self.api.paper.updateBody(body.pid, __paper );
                };
                this.api.paper.updateStruct(body.pid, "clean", [], ok);
            }
        } else {
            console.warn("未知元素类型");
            console.warn(response.body);
            return;
        }
    },
    diffPaper: function (){
        // 比较服务器和本地之间的数据差异 ( 比较整张试卷 )
        var self = this;
        var diff_question, diff_composite, sync_element;

        diff_question = function (elem, index){
            self.api.question.pull(elem.qid, self.MakeComparison.bind(self) );
        };
        diff_composite = function (elem, index){
            for ( var ii=0; ii<elem.struct.length; ii++ ) {
                if ( "cqid" in elem.struct[ii] ){
                    diff_composite(elem.struct[ii], index.push(ii) );
                } else if ( "qid" in elem.struct[ii] ) {
                    diff_question(elem.struct[ii], index.push(ii) );
                } else {
                    console.warn("未知的元素类型位于试卷索引"+i+"中");
                    console.warn(elem.struct[ii]);
                }
            }
            self.api.composite.pull(elem.cqid, self.MakeComparison.bind(self) );
        };
        diff_paper = function (pid){
            self.api.paper.pull(pid, self.MakeComparison.bind(self) );
        };
        for ( var i=0; i<this.paper.struct.length; i++ ) {
            if ( "cqid" in this.paper.struct[i] ){
                diff_composite(this.paper.struct[i], [i]);
            } else if ( "qid" in this.paper.struct[i] ) {
                diff_question(this.paper.struct[i], [i]);
            } else {
                console.warn("未知的元素类型位于试卷索引"+i+"中");
                console.warn(this.state.paper.struct[i]);
            }
        }
        diff_paper(this.paper.pid);
        // this.forceUpdate();
    },
    diffElement: function (elem){
        // 比较服务器和本地之间的数据差异 ( 比较单个元素 )
        if ( !elem ) return false;
        var self = this;
        if ( "qid" in elem ) {
            self.api.question.pull(elem.qid, self.MakeComparison.bind(self) );
        } else if ( "cqid" in elem ) {
            self.api.composite.pull(elem.cqid, self.MakeComparison.bind(self) );
        } else if ( "pid" in elem ) {
            self.api.paper.pull(elem.pid, self.MakeComparison.bind(self) );
        } else {
            console.warn("unknow element type to diff.");
        }
    },
    removeElement: function (elem){
        if ( "cqid" in elem != true && "qid" in elem != true ) return false;
        var self = this;
    },
    tick: function (){
        // 定时器
        
    },
    onAddQues: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn("onAddQues Fail");console.warn(response);
                return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                console.warn(e); return false;
            }
            self.questions[body.qid] = body;
            self.paper.struct.push( { "qid": body.qid } );
            self.api.paper.updateStruct(self.props.pid, "append", [{ "qid": body.qid }]);
            self.forceUpdate();
            // $('.element[data-tixu="#TIXU#"]'.replace("#TIXU#", tixu)).find(".paper_item_substance").show().end().siblings().find(".paper_item_substance").hide();            
        };
        // 试卷地区 & 试卷年份
        var year="", area=""; 
        this.paper.attrs.map(function (attr, index){
            if ( attr.name == "year" || attr.name == "年份" ) year = attr.value;
            if ( attr.name == "area" || attr.name == "地区" ) area = attr.value;
        });

        var question = this.statics.constant.question;
        var attrs = question.attrs.concat();

        attrs.map(function (attr, index){
            if ( attr.name == "年份") attrs.splice(index, 1); // target_index.push(index);
        });
        attrs.map(function (attr, index){
            if ( attr.name == "地区") attrs.splice(index, 1); // target_index.push(index);
        });

        attrs.push({"name": "年份", "value": year.toString() });
        attrs.push({"name": "地区", "value": area.toString() });

        question.attrs = attrs;
        // self.statics.constant.question.attrs.push({"name": "年份", "value": year.toString() });
        // self.statics.constant.question.attrs.push({"name": "地区", "value": area.toString() });
        this.api.question.init( question, callback );
    },
    onAddComposite: function (){ 
        var self = this;   console.log(13434);
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);return;
            }
            try{
                var body = JSON.parse(response.body);

                 console.log(body);
            }catch(e){
                console.warn(e); return false;
            }
            self.composites[body.cqid] = body;
            self.paper.struct.push( {"cqid": body.cqid, "struct": [] } );
            self.api.paper.updateStruct(self.props.pid, "append", [{ "cqid": body.cqid ,"struct":[]}]);
            self.forceUpdate();
       
          
        };
        this.api.composite.init( this.statics.constant.composite, callback );
    },
    handleDragStart: function (e, rid){
         if ( $(e.target).hasClass('tixuCard') ){
            var tixu = $(e.target).data("tixu");
            console.info("tixuCard: 正在拖动元素 " + tixu);
            window._dragevent2 = {"src": tixu.toString(), "dest": null};
        }
    },
    handleDragEnter: function (e, rid){
        if ( $(e.target).hasClass('tixuCard') ){
            // 正确的位置
            var tixu = $(e.target).data("tixu");
            console.info("tixuCard: 拖动元素至 " + tixu);
            window._dragevent2.dest = tixu.toString();
        } else if ( $(e.target).parent().hasClass('tixuCard') ) {
            var tixu = $(e.target).parent().data("tixu");
            console.info("tixuCard: 拖动元素至 " + tixu);
            window._dragevent2.dest = tixu.toString();
        } else {
            // console.log( $(e.target) );
        }
    },
    handleDragEnd: function (e, rid){
        var self = this;
        
        if ( window._dragevent2.dest == null || window._dragevent2.src == null ) {
            console.warn("question: 指令无法执行");
            return false;
        }
        if ( window._dragevent2.dest == null || window._dragevent2.src == null ) {
            console.warn("question: 指令无法执行");
            return false;
        }

        console.info(window._dragevent2);

        var dest_tixu = window._dragevent2.dest;
        var src_tixu = window._dragevent2.src;
        
        if ( dest_tixu.toString() == src_tixu.toString()  ) {
            console.info(":: 不能把自己当作拖动目标。")
            return true;
        }

        var src = this.tixu_card[src_tixu];
        var dest = this.tixu_card[dest_tixu];

        dest.dom = $( 'div[data-tixu="#tixu#"]'.replace("#tixu#", dest.tixu ) );
        src.dom = $( 'div[data-tixu="#tixu#"]'.replace("#tixu#", src.tixu ) );

        var ok;
        if ( !$(e.target).hasClass('tixuCard') )  return false;
        if ( dest.tixu.split("-").length < 1 ) throw new Error("drag fail.");
        
        var isbrother;
        if ( typeof dest.component.props.parent.tixu ==  "object" && typeof src.component.props.parent.tixu == "object" ){
            isbrother = true;
        } else if ( typeof dest.component.props.parent.tixu ==  "string" && typeof src.component.props.parent.tixu == "string" ) {
            if ( dest.component.props.parent.tixu.toString() == src.component.props.parent.toString() ) {
                isbrother = true;
            } else {
                isbrother = false;
            }
        } else {
            isbrother = false;
        }
        
        console.log(src); console.log(dest);
        
        /////////////////////////////////////////////////////////////////////////////////////////////////
        if ( dest.tixu.split("-").length < 2 && src.tixu.split("-").length < 2 ) {
            // 顶层位移
            if ( "qid" in src.element  && "cqid" in dest.element ) {
                // 注入 分组元素当中
                console.log("qid to cqid in paper struct.");

                self.paper.struct.splice( parseInt(src.tixu.split("-").slice(-1)[0])-1, 1);
                if ( parseInt(dest.tixu.split("-").slice(-1)[0]) < parseInt( src.tixu.split("-").slice(-1)[0] ) ){
                    self.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-1].struct.push(
                        {"qid": src.element.qid }
                    );
                } else {
                    self.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-2].struct.push(
                        {"qid": src.element.qid }
                    );
                }
                
                // self.props.paper.paper.struct.splice( parseInt(src.tixu.split("-").slice(-1)[0])-1, 1);
                // self.props.paper.paper.struct[parseInt(dest.tixu.split("-").slice(-1)[0])-1].struct.push( {"qid": src.element.qid } );
                self.composites[dest.element.cqid].struct.push( {"qid": src.element.qid } );
                this.diffElement({"cqid": dest.element.cqid});
            } else {
                // 调换次序
                console.log("qid/cqid to cqid/qid postion in paper struct.");
                self.paper.struct.splice(
                            parseInt(dest.tixu.split("-").slice(-1)[0])-1, 
                            0,
                            self.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                var new_struct = self.paper.struct.filter(function (e, i){
                    return e != undefined && e != null && typeof e == 'object';
                });
                self.paper.struct = new_struct;
            }
        } else {
            // 非顶层复杂变幻 (相同节点之间元素互相调换)
            if ( parseInt(dest.tixu.split("-").splice(-2)[0]) == parseInt(src.tixu.split("-").splice(-2)[0]) && dest.tixu.split("-").length == src.tixu.split("-").length ){
                // 位移(同层)
                if ( "qid" in src.element  && "cqid" in dest.element ) {
                    // 注入 分组元素当中
                    console.log("位移-注入: qid to cqid in paper struct.");
                    self.composites[src.component.props.parent.props.cqid].struct.splice( 
                        parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                        0,
                        self.composites[src.component.props.parent.props.cqid]
                            .struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    var target_node = null;
                    target_node_tixu.split("-").map(function(i, index){
                        if ( target_node == null ) target_node = self.paper.struct[parseInt(i)-1];
                        else target_node = target_node.struct[parseInt(i)-1];
                    });
                    if ( target_node != null && target_node != undefined ){
                        target_node.struct.splice( 
                            parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                            0,
                            target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                    }
                } else {
                    // 调换次序
                    console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");
                    self.paper.struct.splice(
                                parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                                0,
                                self.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                            );
                    var new_struct = self.paper.struct.filter(function (e, i){
                        return e != undefined && e != null && typeof e == 'object';
                    });
                    self.paper.struct = new_struct;
                }

                console.log( JSON.stringify( new_struct ) );
                console.log( JSON.stringify( self.paper.struct ) );
            } else {
                // 层移 （不同节点之间元素 移位）
                console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");
                if ( "qid" in src.element  && "cqid" in dest.element ) {
                    // 注入 到分组元素当中 （跨节点远程注入）
                    console.log("位移-注入: qid to cqid in paper struct.");
                    self.composites[src.component.props.parent.props.cqid].struct.splice( 
                        parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                        0,
                        self.composites[src.component.props.parent.props.cqid]
                            .struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    var target_node = null;
                    target_node_tixu.split("-").map(function(i, index){
                        if ( target_node == null ) target_node = self.paper.struct[parseInt(i)-1];
                        else target_node = target_node.struct[parseInt(i)-1];
                    });
                    if ( target_node != null && target_node != undefined ){
                        target_node.struct.splice( 
                            parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                            0,
                            target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                        );
                    }
                } else {
                    // 调换次序
                    // DEBUG
                    // console.log("位移-调换: qid/cqid to cqid/qid postion in paper struct.");

                    // self.props.paper.paper.struct.splice(
                    //     parseInt(dest.tixu.split("-").slice(-1)[0]-1 ), 
                    //     0,
                    //     self.props.paper.paper.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0]
                    // );
                    // 调整 试卷里面的元素映射结构
                    var target_node_tixu = src.component.props.parent.tixu;
                    if ( typeof target_node_tixu == "string" ){
                        var target_node = null;
                        target_node_tixu.split("-").map(function(i, index){
                            if ( target_node == null ) target_node = self.paper.struct[parseInt(i)-1];
                            else target_node = target_node.struct[parseInt(i)-1];
                        });
                    } else {
                        target_node = src.component.props.paper.paper;
                    }

                    var dest_node_tixu = dest.component.props.parent.tixu;
                    if ( typeof dest_node_tixu == "string" ){
                        var dest_node = null;
                        dest_node_tixu.split("-").map(function(i, index){
                            if ( dest_node == null ) dest_node = self.paper.struct[parseInt(i)-1];
                            else dest_node = dest_node.struct[parseInt(i)-1];
                        });
                    } else {
                        dest_node = dest.component.props.paper.paper;
                    }


                    dest_elem = dest_node.struct.splice( parseInt( dest.tixu.split("-").slice(-1)[0] )-1, 1 )[0];
                    target_elem = target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 1 )[0];

                    dest_node.struct.splice( parseInt( dest.tixu.split("-").slice(-1)[0] )-1, 0,  target_elem);
                    target_node.struct.splice( parseInt( src.tixu.split("-").slice(-1)[0] )-1, 0,  dest_elem);

                    if ( "cqid" in dest_node ) {
                        self.composites[dest_node.cqid].struct.splice(
                            parseInt( dest.tixu.split("-").slice(-1)[0] )-1,
                            1,
                            target_elem
                        );
                    } else if ( "qid" in dest_node ) {

                    } else if ( "pid" in dest_node ) {

                    }

                    if ( "cqid" in target_node ) {
                        self.composites[target_node.cqid].struct.splice(
                            parseInt( src.tixu.split("-").slice(-1)[0] )-1,
                            1,
                            dest_elem
                        );
                    } else if ( "qid" in target_node ) {

                    } else if ("pid" in target_node){

                    }

                }

                console.log( JSON.stringify( new_struct ) );
                console.log( JSON.stringify( self.paper.struct ) );

            }
        }
        this.diffElement({"pid": this.paper.pid});
        console.log( JSON.stringify(self.paper.struct));
        self.forceUpdate();
    },
    mk_tixu_card: function (){
        // 题序卡
        var self = this;
        var tixu_card = [];
       
        var parse_question = function (question){
                 // console.log(parseInt(question.qid) in self.elements.questions);

            if ( parseInt(question.qid) in self.elements.questions ) {
                 tixu_card.push(self.elements.questions[parseInt(question.qid)] );
            }
        };
        var parse_composite = function (composite){
               
            if ( composite.cqid in self.elements.composites ) {
                // alert(self.elements.composites[parseInt(composite.cqid)].tixu2);
                tixu_card.push(self.elements.composites[parseInt(composite.cqid)] );
            }
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;  
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };
        var i = 0;
        for (i=0; i<this.paper.struct.length; i++ ) {
            if ( "qid" in this.paper.struct[i] == true) {
                parse_question(this.paper.struct[i]);
            } else if ( "cqid" in this.paper.struct[i] == true) {
                parse_composite(this.paper.struct[i]);
            } 
        }
        this.tixu_card2 = tixu_card;
        //this.setProps({"tixu_card2": tixu_card});
        return tixu_card;
    },
    total: function (t){
        var self = this;
        if ( t == "elements" ) {
            return self.compute_questions( "children", self.paper );
            // return Object.keys(this.composites).length + Object.keys(this.questions).length;
        } else if ( t == "score" ) {
            var score = 0;
            // this.paper.struct.map( function (elem, i){
            //     if ( "cqid" in elem == true ) {

            //     }
            // } )

            Object.keys(this.questions).map(function(qid, index){
                score += parseFloat(self.questions[qid].score);
            });
            return score;
        }
    },
    selectFiles: function (){
        this.refs.file_input.getDOMNode().click();
    },
    
    //sunqi 上传文件重写wordtohtml
    upload:function(){
     
       var self = this;
     
       var loading = $('<div style="background:#ccc; height: 60px;font-size:3px; width: 300px;border-radius: 3px"' 
            +'id="ajaxLoading"><div id="ajaxLoading-inner" style="background: green; height: 100%; width:0px;border-radius: 3px"></div></div>');
         $(loading).dialog({
                 "title":"20%",
                 "width":300,
                 "height":60,
                 "modal":true
         })
        var loadingInnerBar = $("#ajaxLoading-inner");
        loadingCount = 0;
        var deferred = setInterval(function(){ 
               if(loadingCount > 40){ clearInterval(deferred);return false;}
               loadingCount++;
               loadingInnerBar.width(loadingCount+"%");
               $(".ui-dialog-title").text("导入试卷文档"+loadingCount+"%");
         },100);
        var file = this.refs.file_input.getDOMNode().files[0];
        var fd = new FormData();
        var xhr = new XMLHttpRequest();
        var getHtmlByMd5 = function(path){
             var winUrl = "http://218.244.140.132/";
             // var old    = "http://218.244.132.33:8080/word2html/moc/async/content/html/";
           $.get(winUrl+path,function(html){
                    if($.trim(html) == ""){
                        clearInterval(deferred);
                        $(".ui-dialog-title").text("导入失败,请检查是否是word.doc格式");
                    }else{
                        self.docx_html = html;
                        self.forceUpdate();
                        var deferred2 = setInterval(function(){ 
                                       if(loadingCount >= 100){ clearInterval(deferred2);return false;}
                                       loadingCount++;
                                       loadingInnerBar.width(loadingCount+"%");
                                       $(".ui-dialog-title").text("导入试卷文档"+loadingCount+"%");
                                 },10);
                     }
       
            })
                               
        }
       xhr.onreadystatechange = function(){
            if(xhr.readyState == 4 && xhr.status == 200){
                 
                    var body = JSON.parse(xhr.responseText);
                    console.log(body);
                    /*
                       body = {"name":"2015.doc","is_exist":"","md5":"81e56933d2292be2f636d1f6d6aa60d9"}
                     */
                    if(parseInt(body.is_exist) == 1){ 
                        getHtmlByMd5(body.path);
                    }else if(parseInt(body.is_exist) == -1){
                          // var a="http://218.244.132.33:8080/word2html/moc/async/convert?name="+body.name+"&format=docx";
                        var index = body.name.lastIndexOf(".");
                        var type  = index.substr(index+1);
                        $.get("http://218.244.140.132:9000/doc2Html/conver/"+body.md5+"/"+type,function(result){
                              if(result.Status === 1){ 
                                getHtmlByMd5(result.path);
                              }
                        },"json");
                    }
                        
                                 
                  
            }
       }
       
       // var url = "http://218.244.132.33:8080/word2html/moc/upload";
       var url = "http://218.244.140.132:9000/doc2Html/";
       xhr.open("post",url,true);
       fd.append("file",file);
       xhr.send(fd);

    },
  
    componentDidUpdate:function(){
        var docx = $(".import_word .docx");
        if(docx.height()>800){
            docx.css({"overflow-y":"scroll",'maxHeight':'800px'})
        }
     
      
    },
    //调用wordToHtml的接口
    uploadByInvokeApi:function(){
         var api = "http://218.244.132.33:8080/word2html/moc/upload";

    },    




    uploadDocx: function (e, rid){
        var self = this;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
          // Great success! All the File APIs are supported.
        } else {
          alert('The File APIs are not fully supported in this browser.'); return;
        }
        
        var files = this.refs.file_input.getDOMNode().files;
        var path = this.refs.file_input.getDOMNode().value;

        // var files = $(e.target).parent().find("input")[0].files;
        // var path = $(e.target).parent().find("input")[0].value;

        var reader = new FileReader();
        var file = files[0];
        file.path = path;
        console.log(file);

        var data = {
                "result": "", 
                "info": {
                    "name": file.name, 
                    "path": file.path, 
                    "size": file.size, 
                    "type": file.type, 
                    "lastModified": file.lastModified,
                    "lastModifiedDate": file.lastModifiedDate
                } 
        };

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            console.log(response.body);
            self.docx_html = response.body;
            self.forceUpdate();
        };
        reader.onload = function (evt){
             //console.log(evt.target.result);
            data.result = evt.target.result;
            //console.log(data.result);
            result = [];
            for ( var i=0; i<data.result.length; i++ ){
                result.push(ord(data.result[i]));
            }
            data.result = JSON.stringify(result);
            console.log(data.result);
            requests.post(
                "/admin/cgi-bin/translate.php",
                { "result": data.result, "info": JSON.stringify(data.info) }, 
                { "async": true, "callback": callback }
            ); // "content_type": "application/json"
        };
        //reader.readAsBinaryString(file);  //  readAsText readAsDataURL
        // reader.readAsDataURL(file);
        reader.readAsText(file,"utf-8");
    },
    compute_questions: function (action, elem){
        var num = 0;
        var pos = 0;

        var parse_question = function (question){
            // if ( parseInt(question.qid) in self.elements.questions ) {
            //     tixu_card.push(self.elements.questions[parseInt(question.qid)] );
            // }
            if(elem == undefined) return;
            if ( "qid" in elem == true && parseInt(elem.qid) == parseInt(question.qid) ) pos = num;
            num += 1;

        };
        var parse_composite = function (composite){
            // if ( composite.cqid in self.elements.composites ) {
            //     tixu_card.push(self.elements.composites[parseInt(composite.cqid)] );
            // }
            if ( "cqid" in elem == true && parseInt(elem.cqid) == parseInt(composite.cqid) ) pos = num;

            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };
        var i = 0;
        var target;
        if ( action == "children" ) target = elem;
        else if ( action == "pos" ) target = this.paper;

        if ( typeof target == 'undefined' ) {
            console.warn(" target 没有 struct.");
            return 1;
        }
        for (i=0; i<target.struct.length; i++ ) {
            if ( "qid" in target.struct[i] == true) {
                parse_question(target.struct[i]);
            } else if ( "cqid" in target.struct[i] == true) {
                parse_composite(target.struct[i]);
            } 
        }

        if ( action == "children" ) return num;
        else if ( action == "pos" ) return pos;
    },
    goto: function (e, rid){
        var tixu = $(e.target).data("tixu");
        $('.element[data-tixu="#TIXU#"]'.replace("#TIXU#", tixu)).find(".paper_item_substance").show().end().siblings().find(".paper_item_substance").hide();
        $("body, html").scrollTop( $('.element[data-tixu="#TIXU#"]'.replace("#TIXU#", tixu)).offset().top-55 );
    },
    preview:function(){
         var self = this; 
         self.props.root.setState({"content":<window.components.papers.preview pid={self.paper.pid} root={self.props.root}/>})
    },
    backToLastStep:function(){
         var self = this;
         self.props.root.setState({"content":<window.components.papers.create pid={self.paper.pid} action="update" root={self.props.root}/>})
    },
    render: function (){
        var self = this;
        this.mk_tixu_card();

        return (
                <div className="container-fluid paper_enter">
                    <div className="create_head">
                        <div className="record_paper">录入试卷</div>
                        <ul className="creatte_ul clearfix">
                            <li className="cre_f1">1.创建试卷</li>
                            <li className="cre_f1 create_on">2.录入试卷</li>
                            <li className="cre_f1">3.预览试卷</li>
                            <li className="cre_f1">4.提交审核</li>
                        </ul>
                        <div className="return_list" onClick={self.backToLastStep} style={{"color":"#fff"}}>上一步</div>
                        <div className="input_test" onClick={self.preview} style={{"color":"#fff"}}>
                            试卷预览
                        </div>
                    </div>

                    <div className="clearfix"></div>

                    <div className="main">
                        <div className="import_word fl">
                            <div className="docx" dangerouslySetInnerHTML={{__html: this.docx_html}} ></div>

                              <input style={{"display": "none"}} type="file" name="docx" ref="file_input" onChange={this.upload} multiple />

                            <a onClick={this.selectFiles} className="import_word_btn">导入试卷文档</a>

                            <p>仅支持office2007及以上版本</p>
                        </div>

                        <div className="enter_paper fr">
                            <div className="paper_info">
                                <div className="paper_title fl"><span className="name">{this.paper.name}</span></div>
                                <div className="paper_sroce fr">分数: <span className="score">{this.total("score")}</span>分</div>
                                <div className="paper_num fr"> 题量: <span className="total">{this.total("elements")}</span>道</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="paper_item">
                                <div className="paper_item_edit fl">
                                    <dl className="paper_item_dl">
                                        { this.paper.struct.map(function (elem, index){
                                            if ( "qid" in elem && self.questions[elem.qid] != undefined && "content" in self.questions[elem.qid] ) {
                                                return <QuestionBox 
                                                            key={"Question"+elem.qid+"_index_"+index.toString() + "_tixu_" + (index+1).toString() } 
                                                            qid={elem.qid} 
                                                            index={[index+1]} 
                                                            tx2={self.compute_questions( "pos", self.questions[elem.qid] ) + 1}
                                                            paper={self}
                                                            parent={self} />
                                            } else if ("cqid" in elem && self.composites[elem.cqid] != undefined && "content" in self.composites[elem.cqid] ) {
                                                return <CompositeBox 
                                                            key={"Composite"+elem.cqid+"_index_"+index.toString() + "_tixu_" + (index+1).toString() } 
                                                            cqid={elem.cqid} 
                                                            index={[index+1]} 
                                                            tx2={[ self.compute_questions( "pos", self.composites[elem.cqid] ) + 1, self.compute_questions( "pos", self.composites[elem.cqid] ) + self.compute_questions( "children", self.composites[elem.cqid] ) ].join("-")}
                                                            paper={self}
                                                            parent={self}/>
                                                          
                                            } else {
                                                // console.warn(" unknow elem type. \'composite\' ??? ");
                                            }
                                        }) }
                                        
                                    </dl>
                                </div>

                                <div className="paper_item_group fr">
                                    <div className="paper_group_view">试题一览</div>
                                    <div className="paper_group_name">
                                        <ul>
                                            { this.tixu_card2.map(function(atom, index){ 
                                                return (
                                                    <li 
                                                        key={"PaperTiXuCard_tixu_" + atom.tixu + "_index_" +index.toString() } 
                                                        className="tixuCard"
                                                        data-index={index} 
                                                        data-tixu={atom.tixu}
                                                        data-tixu2={atom.tixu2}
                                                        draggable='true'
                                                        onDragStart={self.handleDragStart} 
                                                        onDragEnter={self.handleDragEnter} 
                                                        onDragEnd={self.handleDragEnd}
                                                        onClick={self.goto}>
                                                        <a href="javascript:;">{atom.tixu2}</a>
                                                    </li>
                                                );
                                            }) }
                                        </ul>
                                    </div>
                                </div>

                                <div className="clearfix"></div>

                                <div className="paper_add">
                                    <a className="add_single" onClick={this.onAddQues}>新增普通题</a>
                                    <a className="add_group" onClick={this.onAddComposite}>新增复合题</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
        );
    }
});