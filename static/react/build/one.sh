#!/bin/sh

echo "@@ 合并JS ...";
rm apis.js;
rm components.js;

find apis -name '*.js' -exec cat {} > apis.js \;;
find components -name '*.js' -exec cat {} > components.js \;;

cat apis.js > all.jsx;
cat components.js >> all.jsx;

jsx all.jsx > all.js;
echo "@@ JS 合并完成 .";
