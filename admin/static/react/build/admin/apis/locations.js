if ( !window.apis ) window.apis = {};

apis.locs = function (){
    if ( "locs" in this ) throw new Error("需要先实例化 apis.locs 类( 伪 Class )");
    this.name = "locs api";
    this.version = "0.1";
};

apis.locs.prototype.provinces = function (xmen){
    var url = "/apis/locs/provinces";
    requests.get(url, {"async": true, "callback": xmen});
};


/////////////////////////新 地理编号接口////////////////////////////////////
apis.locations = function (){
    if ( "locations" in this ) throw new Error("需要先实例化 apis.locations 类( 伪 Class )");
    this.name = "locations api";
    this.version = "0.1";
};
/*
	{
    "locations": [
        {
            "id": 110000,
            "code": 110000,
            "name": "北京市"
        },
        {
            "id": 120000,
            "code": 120000,
            "name": "天津市"
        },
        {
            "id": 130000,
            "code": 130000,
            "name": "河北省"
        },
        {
            "id": 140000,
            "code": 140000,
            "name": "山西省"
        },
        {
            "id": 150000,
            "code": 150000,
            "name": "内蒙古自治区"
        }
    ],
    "count": 31
}

*/
apis.locations.prototype.fetch = function (id, xmen){
    if ( parseInt(id) == 0 ) var url = "/apis/locations/provinces";
    else var url = "/apis/locations/cities/"+parseInt(id);
    requests.get(url, {"async": true, "callback": xmen});
};
apis.locations.prototype.get = function (id, xmen){
    var url = "/apis/locations/" + parseInt(id);
    requests.get(url, {"async": true, "callback": xmen});
};


apis.locations.prototype.provinces = function (xmen){
    var url = "/apis/locations/provinces";
    requests.get(url, {"async": true, "callback": xmen});
};
apis.locations.prototype.cities = function (id, xmen){
    var url = "/apis/locations/cities/"+parseInt(id);
    requests.get(url, {"async": true, "callback": xmen});
};
