/*

2.2.7 用用户错题记录操作

获取用用户错题记录
Operation

	GET /errors/:secretid

Request Sample

	curl “http://115.159.4.84:9123/errors/1”

清空用用户错题记录
Operation
	
	DELETE /errors/:secretid

Request Sample
	
	curl -X DELETE “http://115.159.4.84:9123/errors/1”

添加/删除指定的错题记录(Cooming soon)


获取用用户的错题重练推荐排名
Operation

	GET /errors/:secretid/rank?offset=:offset&num=:num

Request Sample

	curl “http://115.159.4.84:9123/errors/1/rank?offset=0&num=10” 


*/


if ( !window.apis ) window.apis = {};

apis.errors = function (){
    if ( "exams" in this ) throw new Error("需要先实例化 apis.errors 类( 伪 Class )");
    this.name = "errors api";
    this.version = "0.1";
};
apis.errors.prototype.fetch = function (secretid, xmen){
	// 获取用用户错题记录
    if (!secretid ) throw new Error("Error .");
    if ( !xmen ) var xmen = function (){};
    var url = "/httk-admin/errors/" + secretid;
    requests.get(url, {"async": true, "callback": xmen});
};
apis.errors.prototype.clean = function (secretid, xmen){
	// 清空用用户错题记录
    if ( !secretid ) throw new Error("Error.");
    if ( !xmen ) var xmen = function (){};
    var url = "/httk-admin/errors/" + secretid;
    requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
};
apis.errors.prototype.rank = function (secretid, offset, size, xmen){
	// 获取用用户的错题重练推荐排名
    if ( !secretid ) throw new Error("Error.");
    if ( !offset ) var offset = 0;
    if ( !size ) var size = 20;
    if ( !xmen ) var xmen = function (){};
    var url = "/httk-admin/errors/#secretid#/rank?offset=#offset#&num=#size#"
    		  .replace("#secretid#", secretid)
    		  .replace("#offset#", offset)
    		  .replace("#size#", size);
    requests.get(url, {"async": true, "callback": xmen});
};
