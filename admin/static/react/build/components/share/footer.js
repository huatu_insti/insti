if ( !window.components ) window.components = {};
if ( !window.components.share ) window.components.share = {};

window.components.share.footer = React.createClass({
    displayName: "components.share.footer",
    name: "页脚",
    //api: new apis.tests(),
    getInitialState: function (){
        return {};
    },
    pull: function (){
        
    },
    componentDidMount: function (){
        
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "ti_footer"}, 
                React.createElement("div", {className: "foot"}, 
                    React.createElement("div", {className: "foot_left"}, 
                        React.createElement("img", {src: "static/images/footer_logo.png"})
                    ), 
                    React.createElement("div", {className: "foot_right"}, 
                        React.createElement("div", {className: "flinks"}, 
                            React.createElement("a", {target: "_blank", href: "#"}, "网站地图"), " |",  
                            React.createElement("a", {target: "_blank", href: "#"}, "活动公告"), " |",  
                            React.createElement("a", {target: "_blank", href: "#"}, "公考社区"), " |",  
                            React.createElement("a", {target: "_blank", href: "#"}, "联系我们")
                         ), 
                         React.createElement("div", {className: "copyright"}, 
                            "京ICP备05066753京号ICP证090387号" + ' ' +
                            "京公网安备11010802010141" + ' ' +
                            "电信业务审批【2009】字第233号函"
                        )
                    )
                )
            )
        );
    }
});
