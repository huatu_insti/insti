if ( !window.components ) window.components = {};
if ( !window.components.auth ) window.components.auth = {};

window.components.auth.signin = React.createClass({
    displayName: "components.auth.signin",
    name: "登录",
    provinces: [],
    // api: new apis.papers(),
    getInitialState: function (){
        return {};
    },
    componentDidMount: function (){
        
    },
    render: function (){
        var self = this;

        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "tk_login_header"}, 
                    React.createElement("div", {className: "tk_login_head"}, 
                        React.createElement("div", {className: "tk_login_logo"}, 
                            React.createElement("a", {href: "javascript:;"}, React.createElement("img", {src: "/static/images/tk_login_logo.png"}))
                        )
                    )
                ), 
                React.createElement("div", {className: "tk_login_content"}, 
                    React.createElement("div", {className: "tk_login_content_div"}, 
                        React.createElement("div", {className: "tk_login_div"}, 
                            React.createElement("div", {className: "tk_login_div_tit"}, "登录麦题库"), 
                            React.createElement("div", {className: "tk_login_input tk_user_input"}, 
                                React.createElement("input", {type: "text", placeholder: "请输入题库账号"})
                            ), 
                            React.createElement("div", {className: "tk_login_input tk_password_input"}, 
                                React.createElement("input", {type: "password", placeholder: "密码"})
                            ), 
                            React.createElement("div", {className: "tk_login_fn clearfix"}, 
                                React.createElement("div", {className: "tk_login_rem fl a"}, "下次自动登录"), 
                                React.createElement("div", {className: "tk_pass_forget fr"}, React.createElement("a", {href: "#"}, "忘记密码?"))
                            ), 
                            React.createElement("div", {className: "tk_login_submit"}, "登录"), 
                            React.createElement("div", {className: "tk_login_to_reg clearfix"}, 
                                React.createElement("div", {className: "tk_login_to_register fr"}, React.createElement("a", {href: "#"}, "还没有账号？立即注册"))
                            )
                        )
                    )
                ), 
                React.createElement("div", {className: "tk_login_foot clearfix"}, 
                    React.createElement("div", {className: "tk_login_footer clearfix"}, 
                        React.createElement("div", {className: "tk_login_fl fl"}, 
                            React.createElement("a", {href: "javascript:;"}, React.createElement("img", {src: "/static/images/tk_login_bot_logo.png"}))
                        ), 
                        React.createElement("div", {className: "tk_login_fr fr"}, 
                            React.createElement("div", {className: "tk_login_foot_link"}, 
                            React.createElement("ul", {className: "clearfix"}, 
                                React.createElement("li", null, React.createElement("a", {href: "javascript:;"}, "网站地图"), " | "), 
                                React.createElement("li", null, React.createElement("a", {href: "javascript:;"}, "活动公告"), " | "), 
                                React.createElement("li", null, React.createElement("a", {href: "javascript:;"}, "公考社区"), " | "), 
                                React.createElement("li", null, React.createElement("a", {href: "javascript:;"}, "联系我们"))
                            )
                            ), 
                            React.createElement("div", {className: "login_foot_copyright"}, 
                                "京ICP备05066753号京ICP证090387号 京公网安备11010802010141 电信业务审批【2009】字第233号函"
                            )
                        )
                    )
                )
            )
        );
    }
});
