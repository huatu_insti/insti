
if ( !window.components ) window.components = {};
if ( !window.components.user ) window.components.user = {};
if ( !window.components.user.favors ) window.components.user.favors = {};
if ( !window.components.user.favors.view ) window.components.user.favors.view = {};

window.components.user.favors.view.parse = React.createClass({
    displayName: "components.user.favors.view.parse",
    name: "题目解析",
    api: new apis.user.records.favors(),
    elements: { "composites": {}, "questions": {} },
    getInitialState: function (){
        // props: cid/ root
        return {"questions": [] };
    },
    componentWillUnmount: function (){
        var self = this;
        self.props.root.components.favors = React.createElement(window.components.user.favors.tree, {root: self.props.root});
        // self.props.root.components.favors = <window.components.user.favors.view.parse root={self} cid={self.state.cid} />;
        // self.props.root.component = self.props.root.components.favors;
        // self.props.root.forceUpdate();
    },
    componentDidMount: function (){
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "questions" in body != true ) body.questions = {};
            }catch(e){
                console.warn(e); return;
            }
            self.replaceState(body);
        };
        this.api.fetchByCatalog(this.props.cid, callback);
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "ans_main_type ans_jiexi elements"}, 
                 this.state.questions.map(function (qid, index){
                    return (
                        React.createElement(window.components.user.favors.view.parse_question, {
                            qid: qid, 
                            root: self, 
                            parent: self, 
                            index: [index+1]})
                    );
                }) 
            )
        );
    }
});


window.components.user.favors.view.parse_question = React.createClass({
    displayName: "components.user.favors.view.parse_question",
    name: "单选题",
    tixu: 1,
    tixu2: "1",
    api: new apis.questions(),
    isShow: false,
    getInitialState: function (){
        this.tixu = this.props.index.join("-");
        this.tixu2 = this.props.tixu;
        return {
            "content": "",
            "choices": [],
            "attrs": [],
            "ans": "",
            "ansnum": 1,
            "score": 0,
            "tombstone": 0,
            "updtime": 1427183577,
            "inittime": 1427183577,
            "isMark": false,
            "qid": this.props.qid
        };
    },
    componentWillMount: function (){
        // this.props.root.jobs += 1;
    },
    componentDidMount: function (){
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "content" in body != true ) body.content = "";
                if ( "attrs" in body != true ) body.attrs = [];
                if ( "choices" in body != true ) body.choices = [];
            }catch(e){
                console.warn(e); return;
            }
            if ( parseInt(body.ansnum) > 1 ) self.name = "多选题";
            if ( self.props.root.jobs > 0 ) self.props.root.jobs--;
            // if ( self.props.root.jobs == 0 ){
            //     self.props.root.jump();
            // }
            body.isMark = false;
            self.replaceState(body);
            self.isMark();   // 同步收藏状态
        };
        this.api.pull(this.props.qid, callback);
    },
    addMark: function (){
        var self = this;
        var api = new window.apis.user.records.favors();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);return;
            }
            self.setState({"isMark": true});
        };
        api.append([{"qid": this.state.qid, "time": time.time()}], callback);
    },
    removeMark: function (){
        var self = this;
        var api = new window.apis.user.records.favors();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);return;
            }
            self.setState({"isMark": false});
        };
        api.remove([{"qid": this.state.qid, "time": time.time()}], callback);
    },
    updateMarkStatus: function (){
        // 更新 收藏状态
        var self = this;
        if ( self.state.isMark == false ) {
            self.addMark();
        } else {
            self.removeMark();
        }
    },
    isMark: function (){
        var self = this;
        var api = new window.apis.user.records.favors();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                self.setState({"isMark": false});
                return;
            }
            self.setState({"isMark": true});
        };
        api.hit( this.state.qid, callback );
    },
    showJiexi: function (){
        if ( this.isShow == false ) {
            this.isShow = true;
            this.forceUpdate();
        } else {
            this.isShow = false;
            this.forceUpdate();
        }
    },
    render: function (){
        var self = this;
        var _class;
        if ( this.isShow == false ) _class = "none";
        else _class = "";

        var m_class;
        if ( self.state.isMark == true ) {
            m_class = "jiexi_icon_01_active";
        } else {
            m_class = "jiexi_icon_01";
        }
        return (
            React.createElement("div", {className: "ans_jiexi_model element", "data-qid": this.state.qid, "data-tixu": this.tixu}, 
                React.createElement("div", {className: "jiexi_model clearfix"}, 
                    React.createElement("div", null, this.tixu), 
                     React.createElement("div", {className: "content", dangerouslySetInnerHTML: {__html: "（" + this.name + "）" + this.state.content}})
                ), 
                React.createElement("div", {className: "jiexi_choice"}, 
                    React.createElement("ul", null, 
                         self.state.choices.map(function (choice, index){
                            if ( choice == "{%NULL%}" || choice == "{%NULL%} " ) return undefined;
                            return (
                                React.createElement("li", {key: "question_"+self.state.qid+"_choice_"+index+"_"}, 
                                    React.createElement("div", {className: "value", dangerouslySetInnerHTML: {__html: chr(65+index) + ". " + choice}})
                                )
                            );
                        }) 
                    )
                ), 
                React.createElement("div", {className: "jiexi_info"}, 
                    React.createElement("div", {className: "jiexi_info_top"}, 
                        React.createElement("div", {className: "jiexi_info_left fl"}, 
                            React.createElement("span", {onClick: this.showJiexi}, "展开解析"), 
                            React.createElement("a", {onClick: this.showJiexi}, React.createElement("img", {src: "static/images/jiexi_down.png"}))
                        ), 
                        React.createElement("div", {className: "jiex_info_right fr"}, 
                            React.createElement("ul", {className: "clearfix"}, 
                                React.createElement("li", {className: m_class, onClick: self.updateMarkStatus}, "收藏本题"), 
                                React.createElement("li", {className: "jiexi_icon_02", style: {"display": "none"}}, "名师讲"), 
                                React.createElement("li", {className: "jiexi_icon_03", style: {"display": "none"}}, "想听老师讲"), 
                                React.createElement("li", {className: "jiexi_icon_04", style: {"display": "none"}}, "纠错")
                            )
                        )
                    ), 
                    
                    React.createElement("div", {className: "jiexi_info_div " + _class}, 
                         this.state.attrs.map(function (attr, index){
                            if ( attr.value == undefined || attr.value == null  ) attr.value = "";
                            return (
                                React.createElement("div", {key: "question_"+self.state.qid+"_attr_"+index+"_", className: "jiexi_info_come clearfix"}, 
                                    React.createElement("div", {className: "jiexi_info_come_le key"}, attr.name), 
                                    React.createElement("div", {className: "value", dangerouslySetInnerHTML: {__html: attr.value.toString()}})
                                )
                            );
                        }), 
                        React.createElement("div", {className: "jiexi_info_come clearfix", style: {"display": "none"}}, 
                            React.createElement("div", {className: "jiexi_info_come_top"}, 
                                React.createElement("div", {className: "jiexi_info_come_left"}, "笔记"), 
                                React.createElement("div", {className: "jiexi_edit"}, "编辑笔记")
                            ), 
                            React.createElement("div", {className: "jiexi_info_come_bottom none"}, 
                                React.createElement("textarea", null), 
                                React.createElement("span", {className: "info_span"}, "还可以输入500个字符"), 
                                React.createElement("a", {href: "javascript:;", className: "info_come_reacord"}, "记录")
                            )
                        )
                    )
                )
            )
        );
    }
});