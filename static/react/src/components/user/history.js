
if ( !window.components ) window.components = {};
if ( !window.components.user ) window.components.user = {};
if ( !window.components.user.history ) window.components.user.history = {};

window.components.user.history.view = React.createClass({
    displayName: "components.user.history.view",
    name: "我的练习主框架",
    api: new apis.tests(), 
    components: {"records": "", "favors": "", "errors": ""},
    component: "",
    getInitialState: function (){
        // props: root / root0 / tab
        return {};
    },
    componentDidMount: function (){
        // this.pull();
        var self = this;
        this.components.records = <window.components.user.record.tree root={self} />;
        this.components.favors = <window.components.user.favors.tree root={self} />;
        this.components.errors = <window.components.user.errors.tree root={self} indexbox={self.props.root} />;

        if ( self.props.tab == "records" ) {
            $('li[data-component="records"]').siblings().removeClass("li_select");
            $('li[data-component="records"]').addClass("li_select");
            self.component = <window.components.user.record.tree root={self} />;
        } else if ( self.props.tab == "favors" ) {
            $('li[data-component="favors"]').siblings().removeClass("li_select");
            $('li[data-component="favors"]').addClass("li_select");
            self.component = <window.components.user.favors.tree root={self} />;
        } else if ( self.props.tab == "errors" ) {
            $('li[data-component="errors"]').siblings().removeClass("li_select");
            $('li[data-component="errors"]').addClass("li_select");
            self.component = <window.components.user.errors.tree root={self} />;
        } else {
            console.warn("unknow tab name...");
        }
        self.forceUpdate();
    },
    pull: function (){
        var self = this;
    },
    switch_component: function (e, rid){
        var self = this;
        var cp_name = $(e.target).data("component");
        if ( ["records", "favors", "errors"].indexOf(cp_name) == -1 ) return false;
        if ( cp_name == "records" ) this.component = this.components.records;
        if ( cp_name == "favors" ) this.component = this.components.favors;
        if ( cp_name == "errors" ) this.component = this.components.errors;

        $(e.target).siblings().removeClass("li_select");
        $(e.target).addClass("li_select");

        this.forceUpdate();
    },
    render: function (){
        var self = this;
        var nav = { "records": "练习记录", "favors": "收藏本", "errors": "错题本" };
        return (
            <div className="pra_content">
                <ul className="title_list clearfix">
                    { Object.keys(nav).map(function (k, i){
                        if ( i == 0 ) var _ccclass = "li_select";
                        else var _ccclass = "";
                        return ( <li  className={_ccclass} data-component={k} onClick={self.switch_component}>{nav[k]}</li>);
                    }) }
                </ul>
                {this.component}
            </div>
        );
    }
});