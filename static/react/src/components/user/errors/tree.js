


if ( !window.components ) window.components = {};
if ( !window.components.user ) window.components.user = {};
if ( !window.components.user.errors ) window.components.user.errors = {};

window.components.user.errors.tree = React.createClass({
    displayName: "components.user.errors.tree",
    name: "我的错题记录",
    api: new apis.user.records.errors(),
    size: 20,
    page: 1,
    getInitialState: function (){
        // props: page / size / parent / root / indexbox
        return { "catalogs": [], "total": 0 };
    },
    componentDidMount: function (){
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
                if ( "total" in body != true ) body.total = 0;
            }catch(e){
                console.warn(e); return false;
            }
            self.replaceState(body);
        };
        this.api.fetch( "0", callback );
    },
    render: function (){
        var self = this;
        var tips = (<div className="pra_list f1" >你还没有错题数据哦 </div>);
        if ( this.state.catalogs.length != 0 ) tips = null;

        return (
            <div className="pra_main pra_main_action">
                <ul className="clearfix">
                    { tips }
                    { this.state.catalogs.map(function (catalog, index){
                        return (
                            <window.components.user.errors.leaf 
                                            key={"tree_of_errors_cid_"+catalog.cid+"_index_"+index+"_"}
                                            index={[index+1]}
                                            catalog={catalog} 
                                            root={self.props.root} 
                                            indexbox={self.props.indexbox}
                                            parent={self}  />
                        );
                    }) }
                </ul>
            </div>
        );
    }
});


window.components.user.errors.leaf = React.createClass({
    displayName: "components.user.errors.tree",
    name: "叶子",
    api: new apis.user.records.errors(),
    size: 20,
    page: 1,
    getInitialState: function (){
        // props: parent / root / catalog
        var catalog = this.props.catalog;
        if ( "catalogs" in catalog != true ) catalog.catalogs =  [];
        return catalog;
    },
    componentDidMount: function (){
        // this.fetch();
    },
    fetch: function (e, rid){
        var self = this;
        if ( self.props.index.length == 3 ) return null;

        var target = $(e.target);
        if ( $(e.target).hasClass("noa") ){
            // $(e.target).removeClass("noa").addClass("a");
        } else {
            $(e.target).removeClass("a").addClass("noa");
            self.setState({"catalogs": [] });
            return;
        }

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "catalogs" in body != true ) body.catalogs = [];
                // if ( "total" in body != true ) body.total = 0;
            }catch(ee){
                console.warn(ee); return false;
            }
            $(target).removeClass("noa").addClass("a");
            self.setState({"catalogs": body.catalogs});
        };
        this.api.fetch( this.state.cid, callback );
    },
    mk_error_test: function (){
        var self = this;
        loading.show("正在组卷 ... ");
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            }catch(e){
                loading.hide();
                console.warn(e); return;
            }
            loading.hide();
            self.props.indexbox.setState({
                    "content": <window.components.tests.answer.test 
                                            tid={body.tid} 
                                            root={self.props.indexbox}
                                            indexbox={self.props.indexbox} />
            });
        };
        var api = new apis.tests();
        api.init("errors", { "num" : 15, "duration" : 0, "cid": parseInt(this.state.cid) }, callback);
    },
    render: function (){
        var self = this;
        return (
            <li data-cid={this.state.cid}>
                <div className="tk_tab_list">
                    <div 
                        className="tk_tab_1 noa" 
                        onClick={self.fetch} 
                        dangerouslySetInnerHTML={{__html: [this.state.cname, "（共", this.state.totalNum, "道题）"].join("")}} />
                    <div className="tk_tab_5">
                        <a className="ti_a" onClick={self.mk_error_test}>错题重练</a>
                    </div>
                </div>
                <ul>
                    { this.state.catalogs.map(function (catalog, index){
                        if ( self.props.index.length == 3 ) return null;
                        return (
                            <window.components.user.errors.leaf 
                                            key={"tree_of_errors_cid_"+catalog.cid+"_index_"+index+"_"}
                                            index={self.props.index.concat(index+1)}
                                            catalog={catalog} 
                                            root={self.props.root} 
                                            indexbox={self.props.indexbox}
                                            parent={self}  />
                        );
                    }) }
                </ul>
            </li>
        );
    }
});
