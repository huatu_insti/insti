if ( !window.components ) window.components = {};
if ( !window.components.papers ) window.components.papers = {};

window.components.papers.create = React.createClass({
    displayName: "components.papers.create",
    name: "试卷创建",
    provinces: [],
    api: new apis.papers(),
    getInitialState: function (){
        window.paper_create = this;
        return {
             "pid": 0, "status": 0, "name": "", "inittime": time.time(),
             "updtime": time.time(), 
             "attrs": [
                {"name": "year", "value": "2015"},
                { "name": "area", "value": "3125"}
             ],
             "tombstone": 0,
             "struct": []
        };
    },
    componentDidMount: function (){
        this.getProvinces();
        if ( parseInt(this.props.pid) != 0 ) this.pull();
    },
    init: function (xmen){
        var self = this;
        if ( !xmen ) var xmen = function(){};
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                self.replaceState(JSON.parse(response.body));
                xmen(true);
            }catch(e){
                console.warn(e);
                xmen(false);
            }
        };
        this.api.init(this.state, callback);
    },
    pull: function (){
        if ( this.props.pid && parseInt(this.props.pid) > 1 ) {
            var pid = 1;
        } else {
            throw new Error("Error.");
        }
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                console.warn(e);return;
            }
        };
        this.api.pull(this.props.pid, callback);
    },
    getProvinces: function (){
        // /locs/provinces
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);return;
            }
            try{
                self.provinces = JSON.parse(response.body).locations;
                self.setState({"reload": time.time() });
            }catch(e){
                console.warn(e);return;
            }
        };
        var api = new apis.locations();
        api.fetch(0, callback);
        // api.provinces(callback);
    },
    setYear: function (e, rid){
        var attrs = this.state.attrs.concat();
        // var year = $('[data-reactid="#rid#"]'.replace("#rid#", rid)).find("option:selected").val();
        attrs[0] = { "name": "year", "value": e.target.value.toString()  };
        this.setState({"attrs": attrs} );
    },
    setArea: function (e, rid){
        var attrs = this.state.attrs.concat();
        // var area = $('[data-reactid="#rid#"]'.replace("#rid#", rid)).find("option:selected").val();
        var target = $(e.target).find("option:selected");
        var id = target.data("id");
        var name = target.data("name");
        
        attrs[1] = { "name": "area", "value": id+":"+name };
        this.setState({"attrs": attrs} );
    },
    setName: function (e, rid){
        this.setState({"name": e.target.value} );
    },
    edit: function (){
        // 录题
        var self = this;
        var callback = function (status){
            if ( status == true ){
                window.views.papers.edit(self.state.pid);
            } else {
                console.warn("试卷创建失败！"); return;
            }
        };
        if ( this.state.name.length < 1 ) {
            alert("试卷名还没有填写哦");return;
        }
        
        this.init(callback);
    },
    render: function (){
        var self = this;

        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "创建试卷"), 
                    React.createElement("ul", {className: "creatte_ul clearfix"}, 
                        React.createElement("li", {className: "cre_f1 create_on"}, "1.创建试卷"), 
                        React.createElement("li", {className: "cre_f1"}, "2.录入试卷"), 
                        React.createElement("li", {className: "cre_f1"}, "3.预览试卷"), 
                        React.createElement("li", {className: "cre_f1"}, "4.提交审核")
                    ), 
                    React.createElement("div", {className: "return_list", onClick: views.papers.fetch.bind(this, 1, 20, -9) }, React.createElement("a", null, "返回列表")), 
                    React.createElement("div", {className: "input_test", onClick: this.edit}, React.createElement("a", null, "录入试题"))
                ), 
                React.createElement("div", {className: "create_con"}, 
                    React.createElement("div", {className: "create_info"}, 
                        React.createElement("div", {className: "create_inp"}, 
                            React.createElement("div", {className: "papaer_name"}, "试卷名称"), 
                            React.createElement("input", {className: "test", type: "text", placeholder: "请输入试卷名称", onBlur: this.setName})
                        ), 
                        React.createElement("div", {className: "create_inp"}, 
                            React.createElement("div", {className: "papaer_name"}, "试卷年份"), 
                            React.createElement("select", {id: "select_area", onBlur: this.setYear}, 
                                React.createElement("option", {value: "0"}, "请选择试卷年份"), 
                                 range(2000, 2016).map(function(year, index){
                                    if ( parseInt(year) == parseInt(self.state.year) ){
                                        return React.createElement("option", {value: year, selected: "true"}, year);
                                    } else {
                                        return React.createElement("option", {value: year}, year);
                                    }
                                }) 
                            )
                        ), 
                        React.createElement("div", {className: "create_inp"}, 
                            React.createElement("div", {className: "papaer_name"}, "试卷地区"), 
                            React.createElement("select", {id: "select_area", onBlur: this.setArea}, 
                                React.createElement("option", {value: "0"}, "请选择试卷地区"), 
                                 this.provinces.map(function(province, index){
                                    if ( parseInt(province.id) == parseInt(self.state.area) ){
                                        return React.createElement("option", {value: province.id, "data-id": province.id, "data-name": province.name, selected: "true"}, province.name);
                                    } else {
                                        return React.createElement("option", {value: province.id, "data-id": province.id, "data-name": province.name}, province.name);
                                    }
                                }) 
                            )
                        )
                    )
                )
            )
        );
    }
});
