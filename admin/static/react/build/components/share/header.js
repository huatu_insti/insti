if ( !window.components ) window.components = {};
if ( !window.components.share ) window.components.share = {};
window.components.share.header = React.createClass({displayName: "header",
	   render:function(){
	   	    return     React.createElement("div", {className: "header"}, 
					        React.createElement("div", {className: "logo fl"}, 
					            React.createElement("img", {src: "static/images/pic_yuan.png"}), 
					            React.createElement("span", {onClick: "window.location.href='/';"}, "题库管理后台")
					        ), 
					        React.createElement("div", {className: "main-nav fl"}, 
					            React.createElement("div", {className: "show_sidebar fl"})
					        ), 
					        React.createElement("div", {className: "user"}, 
					            React.createElement("div", {className: "user-bar fr", onClick: "$(this).next().removeClass('none').toggle();"}, 
					                "admin", 
					                React.createElement("img", {src: "static/images/button_pullDown.png"})
					            ), 
					            React.createElement("div", {className: "user_info none"}, 
					                    React.createElement("div", {className: "modify_pass mod"}, React.createElement("a", {href: "tor.views.auth.chpasswd"}, "修改密码")), 
					                    React.createElement("div", {className: "exit mod"}, React.createElement("a", {href: "/logout"}, "退出"))
					            )
					        )
                       )
					           
					                
	   }
})					                  