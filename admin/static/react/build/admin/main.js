// Main JavaScript Document

if ( !window.views ) window.views = {};

if ( !window.views.clean ) {
    window.views.clean = function (){
        try{
            React.unmountComponentAtNode(document.getElementById('container'));
            $("#container").empty();
        }catch(e){
            throw new Error("unmount react element fail.");
        }
    };
}

if ( !window.views.papers ) window.views.papers = {};


views.papers.fetch = function (page, size, area){
    // 提取试卷列表
    window.views.clean();
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    var el = React.createElement(window.components.papers.list, { "page": page, "size": size } );
    React.render(el, document.getElementById("container") );
};
views.papers.init = function ( pid ){
    // 创建试卷
    if ( !pid || parseInt(pid) < 1 ) var pid = "0";
    window.views.clean();
    var el = React.createElement( window.components.papers.create, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.edit = function ( pid ){
    // 编辑试卷
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.paper, { "pid": pid, "tixu_card2": [] } );
    React.render(el, document.getElementById("container") );
    var initUI = function (){
        ;(function($, e){
            //编辑模式
            $("dl").on("click", '.paper_item_title', function(){
                var _this = $(this);
                $parent = _this.parent(),
                $toggle = $parent.find('.paper_item_toggle'),
                $del = $parent.find('.paper_item_del');
                $parent.find('.paper_drag_item').addClass('none');  
                $toggle.removeClass('paper_item_show').addClass('paper_item_hide').hide();
                $del.hide();
                $parent.next().show();
                _this.removeClass('title_h');
                _this.css('width','370px');
            });
            //编辑失去焦点
            $('dl').on('blur', '.paper_item_title', function(){
                var _this = $(this);
                $parent = _this.parent(),
                $toggle = $parent.find('.paper_item_toggle'),
                $del = $parent.find('.paper_item_del');
                _this.css('width','300px'); 
                $toggle.show();
                $del.show();
            });
            /****title****/
            $("dl").on("mouseover", '.paper_item_slide', function(){
                var _this = $(this),
                    $substance = _this.next(),
                    $toggle = _this.find('.paper_item_toggle'),
                    $drag = _this.find('.paper_drag_item');
                if ($substance.is(':hidden')) {
                    $toggle.addClass('paper_item_show');
                } else {
                    $toggle.addClass('paper_item_hide');
                }
                $drag.removeClass('none');  
                _this.addClass('paper_item_slide_hover').find('.paper_item_score input').addClass('paper_item_slide_hover');
                _this.find('.paper_item_slide_tit input').addClass('paper_item_slide_hover');
                $toggle.removeClass('none');    
                _this.find(".paper_item_del").removeClass('none');  
            });
            $("dl").on("mouseout", '.paper_item_slide', function(){
                var _this = $(this),
                    $toggle = _this.find('.paper_item_toggle'),
                    $drag = _this.find('.paper_drag_item');
                $drag.addClass('none'); 
                /*
                _this.css('backgroundColor','#fff');
                _this.find('.paper_item_score input').css('backgroundColor','#fff');
                */
                _this.removeClass('paper_item_slide_hover').find('.paper_item_score input').removeClass('paper_item_slide_hover');
                _this.find('.paper_item_slide_tit input').removeClass('paper_item_slide_hover');
                $toggle.addClass('none');   
                _this.find(".paper_item_del").addClass('none');
            });
            // to show
            $('dl').on('click','.paper_item_show',function(){
                var _this = $(this),
                    $substance = _this.parent().next();
                    /*$drag = _this.parent().find('.paper_drag_item');*/
                $substance.show();
                _this.removeClass('paper_item_show').addClass('paper_item_hide');
                //$drag.addClass('none');   
                _this.parent().find('.paper_item_title').removeClass('title_h');
            });
            // to hide
            $('dl').on('click','.paper_item_hide',function(){
                var _this = $(this),
                    $substance = _this.parent().next();
                    //$drag = _this.parent().find('.paper_drag_item');
                $substance.hide();
                _this.removeClass('paper_item_hide').addClass('paper_item_show');
                //$drag.removeClass('none');
                _this.parent().find('.paper_item_title').addClass('title_h');
            });
            //title,score
            $('dl').on('mouseover', '.paper_item_slide_tit input,.paper_item_title,.paper_item_score input,.paper_edit_div,.pape_attr_edit', function(){
                $(this).addClass('on');
            });
            $('dl').on('mouseout', '.paper_item_slide_tit input,.paper_item_title,.paper_item_score input,.paper_edit_div,.pape_attr_edit', function(){
                $(this).removeClass('on');
            });
            //选项可编辑区域
            $('dl').on('click', '.paper_edit_div', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_choice_del').hide();
                _this.css('width','415px');
            });
            $('dl').on('blur', '.paper_edit_div', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_choice_del').show();
                _this.css('width','350px');
            });
            //属性编辑区域
            $('dl').on('click', '.pape_attr_edit', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_attr_del').hide();
                _this.css('width','415px');
            });
            $('dl').on('blur', '.pape_attr_edit', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_attr_del').show();
                _this.css('width','350px');
            });
            /***choice***/
            $('dl').on('mouseover','.paper_choice',function(){
                var _this = $(this);
                _this.css('backgroundColor','#eee');
                _this.find('.paper_choice_del').removeClass('none');    
            });
            $('dl').on('mouseout','.paper_choice',function(){
                var _this = $(this);
                _this.css('backgroundColor','#fff');
                _this.find('.paper_choice_del').addClass('none');
            });
            /**attr**/
            $('dl').on('mouseover' ,'.pape_attr', function(){
                var _this = $(this);
                _this.css('backgroundColor','#eee');
                _this.find('.paper_attr_del').removeClass('none');
            });
            $('dl').on('mouseout' ,'.pape_attr', function(){
                var _this = $(this);
                _this.css('backgroundColor','#fff');
                _this.find('.paper_attr_del').addClass('none');
            });
        })(jQuery, window);
    };
    initUI();
};

views.papers.preview = function ( pid ) {
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.preview, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.review = function ( pid ) {
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.review, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.feedback = function (){

};


/**

    @   exams

**/
if ( !window.views ) window.views = {};
if ( !window.views.exams ) window.views.exams = {};

views.exams.fetch = function (page, size, area ){
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    window.views.clean();
    var el = React.createElement(window.components.exams.list, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};
views.exams.view = function (eid ){
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    window.views.clean();
    var el = React.createElement(window.components.exams.view, { "eid": eid } );
    React.render(el, document.getElementById('container'));
};



/**
    
    @   user && dev options

**/
if ( !window.views ) window.views = {};
if ( !window.views.system ) window.views.system = {};

if ( !window.views.system.developer ) window.views.system.developer = {};
window.views.system.developer.options = function (){

    window.views.clean();
    var page = 1;
    var size = 20;
    var el = React.createElement(window.components.developer.options, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};

if ( !window.views.system.users ) window.views.system.users = {};
window.views.system.users.fetch = function (){
    window.views.clean();
    var page = 1;
    var size = 20;
    var el = React.createElement(window.components.users.list, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};
window.views.system.users.add = function (){
    return false;  // 废弃
    var el = React.createElement(window.components.users.add, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};


/*
    
    @   catalogs

*/
if ( !window.views ) window.views = {};
if ( !window.views.catalog ) window.views.catalog = {};
// catalog - questions
if ( !window.views.catalog.questions ) window.views.catalog.questions = {};
window.views.catalog.questions.fetch = function (cid){
    // 提取该标签的下属子标签
    window.views.clean();
    if ( !cid || parseInt(cid) < 1 ) var cid = 0;
    var page = 1;
    var size = 20;
    var props = { "cid": cid, "page": page, "size": size};
    var el = React.createElement(window.components.catalogs.atoms.list, props );
    React.render(el, document.getElementById('container'));
};


// catalog - papers
if ( !window.views.catalog.papers ) window.views.catalog.papers = {};
window.views.catalog.papers.fetch = function (cid){
    // 提取该标签的下属子标签
    window.views.clean();
    if ( !cid || parseInt(cid) < 1 ) var cid = 0;
    var page = 1;
    var size = 20;
    var props = { "cid": cid, "page": page, "size": size};
    var el = React.createElement(window.components.catalogs.papers.list, props );
    React.render(el, document.getElementById('container'));
};


// catalog - exams
if ( !window.views.catalog.exams ) window.views.catalog.exams = {};
window.views.catalog.exams.fetch = function (cid){
    // 提取该标签的下属子标签
    window.views.clean();
    if ( !cid || parseInt(cid) < 1 ) var cid = 0;
    var page = 1;
    var size = 20;
    var props = { "cid": cid, "page": page, "size": size};
    var el = React.createElement(window.components.catalogs.exams.list, props );
    React.render(el, document.getElementById('container'));
};

/*
    

    
*/
