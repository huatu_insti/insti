
<?php
//require_once "HTTP/Request2.php";
require_once "HTTP/Request2.php";
//require_once "KVAPI.php";
Class Requests{

    /*
        关于HTTP请求方法资料请参考: https://zh.wikipedia.org/wiki/%E8%B6%85%E6%96%87%E6%9C%AC%E4%BC%A0%E8%BE%93%E5%8D%8F%E8%AE%AE#.E5.8D.8F.E8.AE.AE.E6.A6.82.E8.BF.B0
                                                                     http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html

        HTTP/1.1 请求方法: 

            #方法名称是区分大小写的。当某个请求所针对的资源不支持对应的请求方法的时候，服务器应当返回状态码405（Method Not Allowed），
            #当服务器不认识或者不支持对应的请求方法的时候，应当返回状态码501（Not Implemented）。
            #HTTP服务器至少应该实现GET和HEAD方法，其他方法都是可选的。
            #当然，所有的方法支持的实现都应当符合下述的方法各自的语义定义。
            #此外，除了上述方法，特定的HTTP服务器还能够扩展自定义的方法。
            #例如：PATCH（由RFC5789指定的方法）:用于将局部修改应用到资源。
            #
            #HTTP/1.1协议中共定义了八种方法（也叫“动作”）来以不同方式操作指定的资源

            OPTIONS：这个方法可使服务器传回该资源所支持的所有HTTP请求方法。用'*'来代替资源名称，向Web服务器发送OPTIONS请求，
                                   可以测试服务器功能是否正常运作。
            HEAD：与GET方法一样，都是向服务器发出指定资源的请求。
                            只不过服务器将不传回资源的本文部份。它的好处在于，使用这个方法可以在不必传输全部内容的情况下，
                            就可以获取其中“关于该资源的信息”（元信息或称元数据）。
            GET：向指定的资源发出“显示”请求。使用GET方法应该只用在读取数据，而不应当被用于产生“副作用”的操作中，
                        例如在Web Application中。其中一个原因是GET可能会被网络蜘蛛等随意访问。参见安全方法
            POST：向指定资源提交数据，请求服务器进行处理（例如提交表单或者上传文件）。
                           数据被包含在请求本文中。这个请求可能会创建新的资源或修改现有资源，或二者皆有。
            PUT：向指定资源位置上传其最新内容。
            DELETE：请求服务器删除Request-URI所标识的资源。
            TRACE：回显服务器收到的请求，主要用于测试或诊断。
            CONNECT：HTTP/1.1协议中预留给能够将连接改为管道方式的代理服务器。
                                    通常用于SSL加密服务器的链接（经由非加密的HTTP代理服务器）。
            PATCH（由RFC5789指定的方法）: 用于将局部修改应用到资源。

    */
    public static function options($options=array()){
        // HTTP OPTIONS Method.
    }
    public static function get($url, $options=array()){
        // HTTP GET Method.
        if ( !is_array($options) ) $options = array();
        if ( isset($options['params']) && is_array($options['params']) ){
            // Build URI.
            $query = http_build_query($params);
            $url = $url."?".$query;
        }

        $request = new HTTP_Request2($url, 'GET');
        $request = self::_process_options($request, $options);
        return self::_get_response($request);
    }
    public static function head($url, $options=array()){
        // HTTP HEAD Method.

    }
    public static function post($url, $data=array(), $options=array() ){
        // HTTP POST Method.
        // post data  encode way:  urlencode
        if ( !is_array($options) ) throw new Exception("options type error.");
        if ( !is_string($data) && !is_array($data) ) throw new Exception("post data type error.");
        // urlencode 函数 编码与 WWW 表单 POST 数据的编码方式是一样的，同时与 application/x-www-form-urlencoded 的媒体类型编码方式一样。
        // 参见: http://cn2.php.net/manual/zh/function.urlencode.php
        
        $request = new HTTP_Request2($url, 'POST');
        // request 类的方法 addPostParameter 最终会在http body 上面添加经过 urlencode 编码过的数据
        // 而在 PUT 方法当中, urlencode 编码并不是必须的
        $request->setBody( $data );  // Or $request->addPostParameter( $data );
        // 指定数据编码方式
        if ( is_array($data) ) {
            // $options['header'][] = array('Content-Type'=>'application/x-www-form-urlencoded;');
            $request->setHeader("Content-Type", "application/x-www-form-urlencoded; charset: utf8;");
            $data = http_build_query($data);
        } else {
            $request->setHeader("Content-Type", "application/json; charset: utf8;");
        }
        
        $request = self::_process_options($request, $options);
        return self::_get_response($request);
    }
    public static function put( $url, $data='', $options=[] ){
        // HTTP PUT Method.
        if ( !is_array($options) ) throw new Exception("options type error.");
        if ( !is_string($data) && !is_array($data) ) throw new Exception("post data type error.");
        // PUT 模式下对于 Body 数据默认使用 JSON 序列化, 而 POST 模式下 默认使用 urlencode 编码 ( 历史原因 )
        if ( is_array($data) ) $data = json_encode($data, true);
        $request = new HTTP_Request2($url, 'PUT');
        
        //$options['header'][] = array('Content-Type'=>'application/json; charset: utf8;');
   
        $request->setBody( $data );
        $request->setHeader("Content-Type", "application/json; charset: utf8;");
        $request = self::_process_options($request, $options);
        return self::_get_response($request);
    }
    public static function delete($url, $options=array() ){
        // HTTP DELETE Method.

    }
    public static function trace($url, $options=array() ){
        // HTTP TRACE Method.

    }
    public static function connect($url, $options=array() ){
        // HTTP CONNECT Method.

    }

    public static function patch($url, $data, $options=[] ){
        // HTTP PATCH Method.
         if ( !is_array($options) ) throw new Exception("options type error.");
        if ( !is_string($data) && !is_array($data) ) throw new Exception("post data type error.");
        // PATCH 模式下对于 Body 数据默认使用 JSON 序列化, 而 POST 模式下 默认使用 urlencode 编码 ( 历史原因 )
        if ( is_array($data) ) $data = json_encode($data, true);
        $request = new HTTP_Request2($url, 'PATCH');
        $request->setBody( $data );
        // $options['header'][] = array('Content-Type'=>'application/json;');
        $request->setHeader("Content-Type", "application/json; charset: utf8;");
        
        $request = self::_process_options($request, $options);
        return self::_get_response($request);
    }

    /*
        底层操作方法

    */
    public static function request($url, $options=array() ){
        return new RequestsBase($url, $options);
    }


    /*
        @ Response 处理
    */
    public static function _get_response($request){
        $response = $request->send();
        $code = $response->getStatus();                   // HTTP CODE
        $reason = $response->getReasonPhrase();  // HTTP MSG
        $header = $response->getHeader();
        $cookies = $response->getCookies();
        $body = $response->getBody();                     // ResponseText
        return array('code'=>$code, 'reason'=>$reason, 'header'=>$header, 'cookies'=>$cookies, 'body'=>$body);
    }
    public static function _process_options($request, $options){
        // 处理 可选参数
        if ( isset($options['header']) && is_array($options['header']) ){
            // 追加头信息
            // setHeader('Content-Type', 'application/json');
            foreach ( $options['header'] as $k=>$v ){
                $request->setHeader($k, $v);
            }
        }
        return $request;
    }

}






class SessionStore{
    /*
        @@    Session Store 存储机制

    */
    /*
    static $PATH_KT = "http://tiku.huatu.com/apis/casca/kvt/";
    static $PATH_KV = "http://tiku.huatu.com/apis/casca/kv/";
    static $PATH_KL = "http://tiku.huatu.com/apis/casca/log/";
    */
    static $PATH_KT = "";
    static $PATH_KV = "";
    static $PATH_KL = "";

    static $TABLE_NAME = 'Session';            // 表名

    public function __construct (){

    }
    public static function get($key){
        // 查询
        return KD::kv(self::$TABLE_NAME)->get($key);
    }

    /*

        key: String
        value: String
        ttl: Int                # Time To Live ( Seconds )
    */
    public static  function put($key, $value, $ttl=86400){
        // 存储  ttl = 24*60*60 = 86400
        if ( is_int($ttl) && $ttl >= 0 ){
            return KD::kv(self::$TABLE_NAME)->put($key, $value, $ttl);
        } else {
            return KD::kv(self::$TABLE_NAME)->put($key, $value);
        }
    }
    public static function delete($key){
        // 删除
        return KD::kv(self::$TABLE_NAME)->delete($key );
    }
    public static function keys(){
        // 返回 会话数据库当中 所有的会话 键
        return KD::kt(self::$TABLE_NAME)->keys();
    }
    public static function values(){
        // 返回 会话数据库当中 所有的会话 的数据
        // 数据量会相当庞大
        return KD::kt(self::$TABLE_NAME)->values();
    }

    public static function _table_exists(){
        return KD::kt(self::$TABLE_NAME)->exists();
    }
    public static function _table_init(){
        return KD::kt(self::$TABLE_NAME)->create();
    }

}

class Session {
    /*
        @   Session 类
               Session 与 用户表(user table) 统一
    */
    //static $EXPIRE_AGE = 604800;                  //7*24*60*60
    static $KEY = '';

    public function __construct ($key){
        //echo "I'm Session Class Init Function.\n";
        //$this->DB = new SessionStore();
        if ( !$key || !is_string($key) ) throw new Exception("Session Key Require.", 1);
        self::$KEY = $key;
    }
    public static function get($key=''){
        // 提取会话数据
        if ( $key == '' ) $key = self::$KEY;
        return SessionStore::get($key);
    }
    public static function keys($key=''){
        // 获取该会话数据中的所有键值
        if ( $key == '' ) $key = self::$KEY;
        $session = self::get($key);
        if ( json_decode($session, true) ) return array_keys($session);
        else return array();
    }
    public static function values($key=''){
        // 获取该会话数据中的所有键值
        if ( $key == '' ) $key = self::$KEY;
        $session = self::get($key);
        if ( json_decode($session, true) ) return array_values($session);
        else return array();
    }

    /*
        @@      删除会话数据提供了多个函数

    */
    public static function flush($key=''){
        // 删除当前 会话数据
        if ( $key == '' ) $key = self::$KEY;
        return SessionStore::delete($key);
    }
    public static function clear($key=''){
        // 删除当前 会话数据 (  快捷方式 )
        if ( $key == '' ) $key = self::$KEY;
        return self::flush($key);
    }
    /*
    public static function empty($key=''){
        // 删除当前 会话数据 (  快捷方式 )
        if ( $key == '' ) $key = self::$KEY;
        return self::flush($key);
    }
    */


    public static function make($uid='',  $options=array() ){
        // 为当前会话创建一个 会话唯一键.
        // options:  
        //       uname:      string
        //       nickname: string
        //       email:         string
        //       phone:        int
        //       
        if ( $uid == '' ) {
            $key = sha1( microtime().'_'.uniqid().'_'.time() );
        } else {
            if ( gettype($uid) != 'integer' ) throw new Exception("Error uid.", 1);
            $key = sha1( $uid . "_" . microtime().'_'.uniqid().'_'.time() );
        }
        // 对 key 进行存储
        if ( !is_array($options) ) $options = array();
        $options['uid'] = $uid;
        if ( count($options) > 1 ){
            // 验证 KEY
            $require_keys = array('uname,', 'nickname', 'password', 'email', 'phone');
            foreach ($require_keys as $_key ) {
                if ( !in_array($_key, $require_keys) ) {
                    throw new Exception("Key -> {$_key} Required.", 1);
                }
            }
        }
        $value = array( 'user'=>$options  );
        // 写入存储
        if ( $_status = SessionStore::put($key, json_encode($value), 15*24*60*60) ) return $key;
        else return False;
    }

    /*
            @@ 设置类

    */
    public function set_expiry($value){
        // 设置session过期时间
        // $value: seconds
        if ( !in_array(strtolower(gettype($value)), array('integer', 'null') ) ){
            return false;
        }
        if ( $value == 0 ){
            // session , expire when user's web browser is closed.

        } elseif ( $value == null ){
            // session reverts to using the global session expiry policy.
            $value = self::$EXPIRE_AGE;
            
        } elseif ( $value > 0 ){
            
        } elseif ( $value < 0 ){
            // terminal this session.
            return $this->flush();
        } else {
            // unknow.
            return false;
        }
    }
    
    public function get_expiry_age(){
        // 返回当前 session 的过期 秒数
        
    }
    public function get_expiry_date(){
        // 返回当前 session 的过期 日期

    }
    public static function encode(){
        // Cookie Session info encode.
        
    }
    public static function decode(){
        // 
        $ckey_length = 4;
        $key = md5($key ? $key : UC_KEY);
        $keya = md5(substr($key, 0, 16));
        $keyb = md5(substr($key, 16, 16));
        #$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';
        if ( isset($ckey_length) && !empty($ckey_length) ){
            // ENCODE
            $keyc = substr( md5( microtime() ), -$ckey_length);
            // DECODE
            $keyc = substr( $string, 0, $ckey_length );
            
        } else {
            $keyc = '';
        }

        $cryptkey = $keya.md5($keya.$keyc);
        $key_length = strlen($cryptkey);
        
        // DECODE
        
        
        // ENCODE
        $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
        
        $string_length = strlen($string);
        $result = '';
        $box = range(0, 255);
        $rndkey = array();
        for($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }
        for($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        if($operation == 'DECODE') {
            if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            return $keyc.str_replace('=', '', base64_encode($result));
        }
    }

}

/*
$a = new Session();
$a->clear('1234');
*/





// NOTE:
// $url = 'http://v.huatu.com/API/cps/tiku/tiku_lemon.php?' . http_build_query($params);


class UserDB_TK{
    // 用户数据库操作( 题库(vhuatu.v_qbank_user Users.,v_qbank_user) 
    static $DB_HOST = '192.168.100.18';   //  192.168.100.18     192.168.100.18      192.168.100.18
    static $DB_PORT = 3306;
    static $DB_USER = 'vhuatu';
    static $DB_PWD = 'vhuatu_2013';

    static $DB_NAME = 'Users';    // 兼容 数据库 vhuatu.v_qbank_user
    static $DB_CHARSET = "utf8";
    
    public function __construct(){
        if ( $_SERVER['HTTP_HOST'] == 'tiku.huatu.com' || $_SERVER['HTTP_HOST'] == 'zhuantiku.com'  
                || $_SERVER['HTTP_HOST'] == 'tiku.huatu.com' || $_SERVER['HTTP_HOST'] == '211.151.160.103'
                || $_SERVER['HTTP_HOST'] == '211.151.160.104' || $_SERVER['HTTP_HOST'] == '192.168.100.103'
                || $_SERVER['HTTP_HOST'] == '192.168.100.104' || $_SERVER['HTTP_HOST'] == '211.151.160.103'
                || $_SERVER['HTTP_HOST'] == '192.168.11.182'
        ){
            self::$DB_HOST = "192.168.100.18";
        } else {
            self::$DB_HOST = "192.168.100.18";
        }
        
        self::$DB_HOST = "192.168.100.18";
        $this->DB = new mysqli(self::$DB_HOST, self::$DB_USER, self::$DB_PWD, self::$DB_NAME, self::$DB_PORT );
        $this->DB->autocommit(FALSE);               // 关闭事务自动提交功能
        $this->DB->set_charset(self::$DB_CHARSET);
    }

}

class UserDB_HT{
    // 华图(UC) )
    static $DB_HOST = '123.103.79.90';  // 
    static $DB_PORT = 3309;
    static $DB_USER = 'v_ztk';
    static $DB_PWD = 'ztk86Ytul1ppy';
    static $DB_NAME = 'ucentermain';
    static $DB_CHARSET = "utf8";

    // 丑陋的 UC 参数   ( 有什么作用不知道 )
    static $UC_KEY = "xsifsktiku09283slous";   // uc_authcode 需要用到
    static $UC_API = "http://passport.huatu.com";
    static $UC_APPID = 29;
    static $UC_PPP = 20;
    // 附加接口添加用户接口地址
    // define('DATA_API_HOST', 'http://192.168.100.13');

    public function __construct (){
        $this->DB = new mysqli(self::$DB_HOST, self::$DB_USER, self::$DB_PWD, self::$DB_NAME, self::$DB_PORT );
        $this->DB->autocommit(FALSE);               // 关闭事务自动提交功能
        $this->DB->set_charset(self::$DB_CHARSET);
    }

}



class User{
    /*
        @@  用户 操作类
            
    */
    public static function get($opt){
		
        // 查询用户基础信息
        // $opt: array.
        // md5(pass_{$passwd})
        if ( !is_array($opt) ) return false;

        // 快捷方式
        if ( isset($opt['id']) && !empty($opt['id']) && is_int($opt['id']) ){
            $opt['uid'] = $opt['id'];
        }
        if ( isset($opt['uname']) && !empty($opt['uname']) && is_string($opt['uname']) ){
            $opt['username'] = $opt['uname'];
        }

        $sql_ht = "SELECT uid,username,password,salt,email,regip,regdate,appid FROM ucentermain.uc_members ";
        $sql_tk = "SELECT PUKEY,uname,FB1Z1,passwd,reg_mail,reg_phone FROM vhuatu.v_qbank_user ";
        if ( isset($opt['uid']) && !empty($opt['uid']) && is_int($opt['uid']) ){
            // 根据 用户 编号来 提取 用户基础信息
            $uid = $opt['id'];
            $sql_ht .= "WHERE uid={$uid}";
            $sql_tk .= "WHERE PUKEY={$uid}";
            #$ext_sql = "SELECT userid,username,phone,email,bd FROM ucentermain.common_user_bd WHERE username='{$email}'";
        } elseif ( isset($opt['username']) && !empty($opt['username']) && is_string($opt['username']) ){
            // 根据用户名来提取 用户基础信息
            $username = $opt['username'];
            $sql_ht .= "WHERE username='{$username}'";
            $sql_tk .= "WHERE uname='{$username}'";
        } elseif ( isset($opt['nickname']) && !empty($opt['nickname']) && is_string($opt['nickname']) ){
            // 根据用户昵称来提取用户基础信息
            // 考虑到非唯一性问题, 返回的可能将会是列表形式
            $nickname = $opt['nickname'];
            //$sql_ht .= "WHERE nickname='{$nickname}'";
            unset($sql_ht);  // 华图 UC 数据库不支持 昵称
            $sql_tk .= "WHERE FB1Z1='{$nickname}'";
        } elseif ( isset($opt['email']) && !empty($opt['email']) && is_string($opt['email']) ){
            // 根据电子邮件来登陆
            $email = $opt['email'];
            //$sql_ht .= "WHERE email='{$email}'";
            $ext_sql = "SELECT userid,username,phone,email,bd FROM ucentermain.common_user_bd WHERE email='{$email}'";
            #$sql_tk .= "WHERE uname='{$username}'";
        } elseif ( isset($opt['phone']) && !empty($opt['phone']) && (int)($opt['phone']) > 0 ){
            // 采用题库数据库
            $phone = (float)$opt['phone'];
            //$sql_tk .= "WHERE reg_phone='{$phone}'";
            $ext_sql = "SELECT userid,username,phone,email,bd FROM ucentermain.common_user_bd WHERE phone='{$phone}' ORDER BY id ASC LIMIT 1";
        } else {
            // unknow.
            return array('code'=>0, 'msg'=>'参数错误');
        }
        // 执行SQL查询
        $result = array();
        
        // 取消查询题库数据库
        if ( isset($opt['phonea']) && !empty($opt['phonea']) && is_int($opt['phonea']) ){
            // 题库 数据库 实例
            $DB_TK = new UserDB_TK();
            $DB_TK = $DB_TK->DB;
            if ( $DB_TK->connect_errno ){
                return array('code'=>0, 'msg'=>'网络繁忙', 'error'=>$DB_TK->connect_errno);
            }
            if ( isset($sql_tk) ){
                $_result = $DB_TK->query( $sql_tk );
                $result_tk = array();
                if ( $_result ){
                    foreach ($_result as $x){
                        array_push($result_tk, $x);
                    }
                } else $result_tk = false;
                unset($_result);
            }
            $DB_TK->close();
            if ( is_array($result) && is_array($result_tk) && isset($result_tk[0]['uname'])  ){
                #$result['nickname'] = $result_tk[0]['FB1Z1'];
                $uname = $result_tk[0]['uname'];
                $response = self::get(array('uname'=>$uname));
                if ( $response && is_array($response) && $response['code'] == 1 ){
                    $response = $response['data'];
                    $response['password.tk'] = $result_tk[0]['passwd'];    // UC enter password.
                    $response['nickname'] = $result_tk[0]['FB1Z1'];
                    //$response['phone'] = (int)$opt['phone'];
                    $result = $response;
                    unset($response);
                    unset($uname);
                    unset($result_tk);
                } else {
                    // 登录失败
                    return false;
                }
            } else return false;
        } else {
            // 华图  数据库 实例
            $DB_HT = new UserDB_HT();
            $DB_HT = $DB_HT->DB;
            if ( $DB_HT->connect_errno ){
                return array('code'=>0, 'msg'=>'网络繁忙', 'error'=>$DB_TK->connect_errno);
            }
            
            $result_ht = array();
            if ( isset($ext_sql) ){
                $_result = $DB_HT->query( $ext_sql );
                if ( $_result->num_rows > 0 ){
                    foreach ($_result as $x){
                        $result['phone'] = (float)$x['phone'];
                        $result['email'] = $x['email'];
                        $result['bd'] = $x['bd'];
                        $_uname = $x['username'];
                    }
                    $sql_ht .= "WHERE username='{$_uname}'";
                } else{
                    if($opt['username']){
                        $sql_ht .= "WHERE username='".$opt['username']."'";
                    }elseif($opt['email']){
                        $sql_ht .= "WHERE username='".$opt['email']."'";
                    }else{
                        unset($sql_ht);
                    }
                };
            }
            if ( isset($sql_ht) ){
                $_result = $DB_HT->query( $sql_ht );
                foreach ($_result as $x){
                    array_push($result_ht, $x);
                }
                unset($_result);
            }
            
            
            $result['id'] =  $result_ht[0]['uid'];
            $result['uid'] =  $result_ht[0]['uid'];
            $result['username'] =  $result_ht[0]['username'];
            $result['uname'] =  $result_ht[0]['username'];

            if (!isset($result['email']) || (int)explode($result['bd'], ",")[1]  != 1  ) $result['email'] =  $result_ht[0]['email'];
            $result['regip'] =  $result_ht[0]['regip'];
            $result['regdate'] =  $result_ht[0]['regdate'];
            $result['appid'] =  $result_ht[0]['appid'];           // UC app id.
            $result['salt'] = $result_ht[0]['salt'];                   // 密码盐
            $result['password.ht'] = $result_ht[0]['password'];    // UC enter password.
            
            // 当登陆方式为 用户名/用户编号时(非手机号/电子邮箱)，增加扩展信息查询
            if ( isset($sql_ht) && !isset($ext_sql) ){
                $uname = $result['uname'];
                $ext_sql = "SELECT userid,username,phone,email,bd FROM ucentermain.common_user_bd WHERE username='{$uname}'";
                
                $_result = $DB_HT->query( $ext_sql );
                if ( $_result->num_rows > 0 ){
                    foreach ($_result as $x){
                        $result['phone'] = (int)$x['phone'];
                        $result['email'] = $x['email'];
                        $result['bd'] = $x['bd'];
                    }
                }
                
            }
            $DB_HT->close();
        }
        
            /*
                Array
                (
                    [id] => 7828121
                    [uid] => 7828121
                    [username] => bbc1000
                    [uname] => bbc1000
                    [email] => hgjhgjhgj@5435435435.com
                    [phone] => 131
                    [regip] => 221.234.210.32
                    [regdate] => 1385443469
                    [appid] => 0
                    [password.ht] => 4cac87355740644d04e6346631063898
                    [nickname] => nick3
                    [password.tk] => b0c5b390077153d834b461dfcd7df51b
                )
            */
        if ( empty( $result['uid'] ) || (int)$result['uid'] < 1 ) {
            return array('code'=>0, 'msg'=>'查询失败', 'error'=>$result);
        }
        return array('code'=>1, 'msg'=>'用户查询成功', 'data'=>$result);
    }

    public static function post($opt){
        // 新增用户 (暂不支持 昵称 )

        # 必填参数
        if ( empty($opt['password']) ) return array('code'=>0, 'msg'=>'密码不能为空' );
        else $password = $opt['password'];
        if ( empty($opt['email']) && empty($opt['phone']) ) return array('code'=>0, 'msg'=>'手机或邮箱不能同时为空');
        else {
            // 手机号注册优先( 出于信息收集的重要性 )
            if ( isset($opt['phone']) && !empty($opt['phone']) ) $phone = $opt['phone'];
            elseif ( isset($opt['email']) && !empty($opt['email']) ) $email = $opt['email'];
        }

        # 可选参数
        // 应用 编号
        if ( isset($opt['appid']) && is_int($opt['appid']) ) $appid = $opt['appid'];
        else $appid = 29;
        // 注册地址
        if ( isset($opt['regip']) && is_int($opt['regip']) ) $regip = $opt['regip'];
        else {
            if ( isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']) ){
                $regip = $_SERVER['REMOTE_ADDR'];
            } else {
                $regip = '';
            }
        }
        // 注册日期
        if ( isset($opt['regdate']) && is_int($opt['regdate']) ) $regdate = $opt['regdate'];
        else $regdate = time();
        // 密码加盐算法
        if ( isset($opt['salt']) && is_int($opt['salt']) ) $salt = $opt['salt'];
        else $salt = substr(uniqid(rand()), -6);

        /*
        $DB_TK = new UserDB_TK();
        $DB_TK = $DB_TK->DB;
        // 帐号注册之前先验证手机号码或电子邮件以及用户名是否已经存在.
        $tk_query_sql = "SELECT * FROM vhuatu.v_qbank_user WHERE ";
        if ( isset($opt['phone']) ) {
            $phone = $opt['phone'];
            $tk_query_sql .= "reg_phone='{$phone}'";
        } else if ( isset($opt['email']) ) {
            $email = $opt['email'];
            $tk_query_sql .= "reg_mail='{$email}'";
        }
        $_result = $DB_TK->query( $tk_query_sql );
        if ( $_result->num_rows > 0 ){
            // 已经存在该用户
            return array('code'=>0, 'msg'=>'该手机号已经被注册过啦');
        } else {
            //echo "Not Exists.\n";
        }
        */
        
        $DB_HT = new UserDB_HT();
        $DB_HT = $DB_HT->DB;
        if ( $DB_HT->connect_errno ){
            return array('code'=>0, 'msg'=>'网络繁忙', 'error'=>$DB_TK->connect_errno);
        }
            
        if ( isset($opt['phone']) ) {
            $phone = (int)$opt['phone'];
            $sql = "SELECT userid,phone,email,bd FROM ucentermain.common_user_bd WHERE phone='{$phone}';";
        } else if ( isset($opt['email']) ) {
            $email = $opt['email'];
            $sql = "SELECT userid,phone,email,bd FROM ucentermain.common_user_bd WHERE email='{$email}';";
        }
        $_result = $DB_HT->query( $sql );
        if ( $_result->num_rows > 0 ){
            // 已经存在该用户
            return array('code'=>0, 'msg'=>'该手机号已经被注册过啦');
        }
        // 问题提示 ( 此处数据库并没有该字段，类似于密码保护之类的 密宝问题 
        // 此处并没有应用
        //$question_id = '';

        // 密码哈希计算次数，目前采取 一次 MD5哈希散列值
        // NOTE: 题库 密码无需加盐 (  $pwd = md5($password) )
        $password = md5($password.$salt);
        // $password = md5(md5($password).$salt);
        // $password = substr(md5($password),8,16);

        // 用户名生成，采用 httk_ 前缀 加上 最大 userid
        //$username = $opt['username'];
        /*
        $sql = "SELECT max(uid) FROM ucentermain.uc_members;";
        $_result = $DB_HT->query( $sql );
        $result = array();
        foreach ($_result as $_key=>$_value) {
            foreach ( $_value as $_k=>$_uid ){
                array_push( $result, (int)$_uid );
            }
        }
        */

        #$max_uid = $result[0];
        // $max_uid = number_format(dechex(microtime(true)),10, ".", "");
        $max_uid = uniqid();
        /*
        $ucs4 = bin2hex(mb_convert_encoding($string, 'ucs-4', 'utf-8'));  // 每个字符长度为8个字节（十六进制）
        $len = strlen($ucs4);
        $str_array = array();
        for ($i=0; $i<$len-1; $i +=8){
            $ucs4_str = substr($ucs4, $i, 8);
            #$utf8_str = mb_convert_encoding(hex2bin($ucs4_str), 'utf-8', 'ucs-4');
            #$str_array[] = $utf8_str;
            $str_array[] = base_convert($ucs4_str, 16, 10);
        }
        */
        $username = "httk_{$max_uid}";
        // unset($result);
        // unset($_result);
        // 锁表操作, 避免最大编号发生变化
        # UCenter
        #$sql_ht .= "LOCK TABLES ucentermain.uc_members READ;";
        #$sql_ht .= "LOCK TABLES ucentermain.uc_memberfields READ;";
        # Tiku ( vhuatu/Users )
        #$sql_tk .= "LOCK TABLES vhuatu.v_qbank_user READ;";
        #$sql_tk .= "LOCK TABLES Users.v_qbank_user READ;";

        #$DB_HT->query($sql_ht);
        #$DB_TK->query($sql_tk);

        // 写入该新增用户信息
        $_insert_time = microtime(true);  #dechex(microtime(true))
        $sql_ht = "INSERT INTO ucentermain.uc_members SET secques='', username='{$username}', password='{$password}', ";
        $sql_ht .= "email='{$email}', regip='{$regip}', regdate='{$regdate}', salt='{$salt}',appid={$appid}";
        
        if ( !isset($email) ) $email = '';

        $status = $DB_HT->query( $sql_ht );

        if ( $status ) $uid = $DB_HT->insert_id;
        else {
            #$_sql_log = 
            file_put_contents('/var/www/zhuantiku/Runtime/sql.log', $_insert_time."\t".$sql_ht."\t".(string)$status."\n", FILE_APPEND);
            return array('code'=>0, 'msg'=>'数据写入失败...');
        }
		
        //判断绑定情况(2015-3-7 韩钰修改)
        empty($phone) ? $p = 0 : $p = 1;
        empty($email) ? $e = 0 : $e = 1;
        $bd = $p.','.$e;
        
        $sql_ht2 = "INSERT INTO ucentermain.common_user_bd SET userid={$uid}, username='{$username}', phone='{$phone}', email='{$email}', bd='{$bd}'";
		
        $DB_HT->query( $sql_ht2 );

        unset($status );
        $sql = "INSERT INTO ucentermain.uc_memberfields SET uid='{$uid}' ";
        $DB_HT->query( $sql );


        // $password = md5("pass_{$password}");

        // $sql_tk = "INSERT INTO vhuatu.v_qbank_user SET uname='{$username}', passwd='{$password}', ";
        // $sql_tk .= "email='{$email}', regip='{$regip}', regdate='{$regdate}', salt='{$salt}',appid={$appid}";

        // $sql_tk2 = "INSERT INTO Users.v_qbank_user SET uname='{$username}', password='{$password}', ";
        // $sql_tk2 .= "reg_mail='{$email}', reg_ip='{$regip}', reg_phone='{$phone}'";

        // $DB_TK->query($sql_tk);
        // $DB_TK->query($sql_tk2);

        // 解锁表
        /*
        $sql = "UNLOCK TABLES ucentermain.uc_members READ;";
        $sql .= "UNLOCK TABLES ucentermain.uc_memberfields READ;";
        $DB_HT->query( $sql );
        */

        // 返回用户基础信息
        $user_info = array(
                'id' => $uid,
                'uid' => $uid,
                'username' => $username,
                'uname' => $username,         // 快捷方式 
                'nickname' => $nickname,
                'password.ht'  => $password,
                'password.tk' => str_replace($salt, '', $password),
                'salt' => $salt,
                'email' => $email,
                'phone' => $phone,
                'regip' => $regip,
                'regdate' => $regdate,
                'appid' => $appid,
            );
        return array('code'=>1, 'msg'=>'用户新增成功', 'data'=>$user_info);
    }

    public static function put ($keys, $opt){
        // 对用户执行 UPDATE 操作

        // 华图  数据库 实例
        $DB_HT = new UserDB_HT();
        $DB_HT = $DB_HT->DB;
        
        // 题库 数据库 实例
        $DB_TK = new UserDB_TK();
        $DB_TK = $DB_TK->DB;
        
        if ( !is_array($opt) ) return array('code'=>0, 'msg'=>'参数错误');

        if ( isset($opt['uname']) && !empty($opt['uname']) ) {
            // 处理快捷方式
            $opt['username'] = $opt['uname'];
            unset($opt['uname']);
        }

        // 检查并判断 $keys 类型
        if ( is_int($keys) ) {
            // 当 参数 keys 为整数时，keys 会被当作 用户唯一编号处理
            if ( (int)$keys <= 0  ) return array('code'=>0, 'msg'=>'用户编号不能小于0');
            else  $uid = (int)$keys;
        } elseif ( is_array($keys) ){
            // 当 参数 keys 为数组时，keys 可以使用用户名称，编号，昵称做定位处理
            if ( in_array('uid', array_keys($keys) ) && (int)$keys['uid'] > 0 ) $uid = (int)$keys['uid'];
            elseif ( in_array('username', array_keys($keys) ) && !empty($keys['username']) ) $username = $keys['username'];
            elseif ( in_array('nickname', array_keys($keys) ) && !empty($keys['nickname']) ) $nickname = $keys['nickname'];
            else return array('code'=>0, 'msg'=>'uid/uname/nickname Reuired..');
        } else {
            return array('code'=>0, 'msg'=>'参数错误');
        }

        // 检查 $opt 参数
        $uni_keys = array('username','nickname', 'password', 'salt', 'email', 'phone', 'regip', 'regdate', 'appid');
        foreach ( array_keys($opt) as $k ){
            if ( !array_key_exists($k, $uni_keys) ) {
                // 非法更新键
                return array('code'=>0, 'msg'=>'准备更新的字段非法');
            }
        }


        // 组装 SQL
        $sql_ht = "UPDATE ucentermain.uc_members  ";
        $sql_tk = "UPDATE Users.v_qbank_user  ";

        foreach ( $opt as $k=>$v ){
            if (is_string($v)) {
                $sql_ht .= "SET {$k}='{$v}' ";
                $sql_tk .= "SET {$k}='{$v}' ";
            } elseif (is_numeric($v)) {
                $sql_ht .= "SET {$k}={$v} ";
                $sql_tk .= "SET {$k}={$v} ";
            } elseif ( is_null($v) ) {
                $sql_ht .= "SET {$k}=NULL ";
                $sql_tk .= "SET {$k}=NULL ";
            }
            // else  PASS.
        }

        if ( isset($uid) )  {
            $sql_ht .= "WHERE uid={$uid}";
            $sql_tk .= "WHERE uid={$uid}";
        } elseif ( isset($username) ) {
            $sql_tk .= "WHERE uname='{$username}'";
            $sql_ht .= "WHERE username='{$username}'";
        } elseif ( isset($nickname) ) {
            $sql_tk .= "WHERE FB1Z1='{$nickname}'";
            unset($sql_ht);
        } else return array('code'=>0, 'msg'=>'参数错误');

        // 执行 SQL 查询
        if ( isset($sql_ht) && !empty($sql_ht) ){
            $status_ht = $DB_HT->query( $sql_ht );
        }
        //  判断执行 结果
        if ( isset($status_ht) && (bool)$status_ht != true ){
            return array('code'=>0, 'msg'=>'用户信息更新失败');
        }

        if ( isset($sql_tk) && !empty($sql_tk) ){
            $status_tk = $DB_TK->query( $sql_tk );
        }
        //  判断执行 结果
        if ( isset($status_tk) && (bool)$status_tk != true ){
            if ( !isset($nickname) ){
                return array('code'=>0, 'msg'=>'用户信息更新失败(UCenter 信息更新成功)');
            } else return array('code'=>0, 'msg'=>'用户信息更新失败');
        }

        return array('code'=>1, 'msg'=>'用户信息更新成功');
    }

    public static function delete($uid){
        // 删除用户 ( 基本用不到 )

        return array('code'=>0, 'msg'=>'Ooops...该功能暂未开放');
        // 华图  数据库 实例
        $DB_HT = new UserDB_HT();
        $DB_HT = $DB_HT->DB;
        
        // 题库 数据库 实例
        $DB_TK = new UserDB_TK();
        $DB_TK = $DB_TK->DB;

        if ( !is_int($uid) ||$uid <= 0 ) return array('code'=>0, 'msg'=>'用户编号须为大于零的整数');

        $sql = "DELETE FROM ucentermain.uc_members WHERE uid={$uid};";
        $sql .= "DELETE FROM ucentermain.uc_memberfields WHERE uid={$uid};";

        $status = $DB_HT->query( $sql );
        if ( $status ) return array('code'=>1, 'msg'=>'用户删除成功');
        else return array('code'=>1, 'msg'=>'用户删除失败');
    }

    /*
    // 需要同步登录的 站点信息
    $_CACHE['apps'] = array (
                                      10 =>
                                      array (
                                        'appid' => '10',
                                        'type' => 'OTHER',
                                        'name' => 'v.htexam.com',
                                        'url' => 'http://v.htexam.com/SSOClient',
                                        'ip' => '211.151.49.15',
                                        'viewprourl' => '',
                                        'apifilename' => 'uc.php',
                                        'charset' => '',
                                        'synlogin' => '1',
                                        'extra' =>
                                        array (
                                          'apppath' => '',
                                          'extraurl' =>
                                          array (
                                            0 => 'http://v.huatu.com/SSOClient',
                                          ),
                                        ),
                                        'recvnote' => '1',
                                      ),
                                    );
    
    $str = "action=synlogin&username={$username}&uid={$uid}&phone={$phone}&password={$password}";
    $str .= "&time={$time}&appid={$appid}&email={$email}&md5password={$md5_password}";
    $str = urlencode( uc_authcode( $str, "ENCODE", "UC_APP_ID:{$appid}" ) ) 
    // 生产 同步登录的 JS 请求
    $template = "<script type=\"text/javascript\" src=\"{$urlbase}/api/uc.php?time={$time}&appid={$appid}&code={$str}\" ></script>\n";
    
    // UC 加密函数
    function uc_authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
        $ckey_length = 4;
        $key = md5($key ? $key : UC_KEY);
        $keya = md5(substr($key, 0, 16));
        $keyb = md5(substr($key, 16, 16));
        $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';
        $cryptkey = $keya.md5($keya.$keyc);
        $key_length = strlen($cryptkey);
        $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
        $string_length = strlen($string);
        $result = '';
        $box = range(0, 255);
        $rndkey = array();
        for($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }
        for($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        if($operation == 'DECODE') {
            if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            return $keyc.str_replace('=', '', base64_encode($result));
        }
    }

    */

}



// 测试函数 ( 请勿调用 )
function test_GetUser(){
    // 提取用户信息 ( GET, 登录 )
    #$user_info = User::get(array('phone'=>15927687017));
    $_SERVER['HTTP_HOST'] = "127.0.0.1";
    $user_info = User::get(array('uname'=>"httk_55094f21"));
    print_r($user_info);
}
function test_PostUser(){
    // 新增用户 ( 注册 )
    $options = array('email'=>'hahahha@haha.net', 'password'=>789522);
    $user_info = User::post($options);
    print_r($user_info);
}
function test_PutUser(){
    // 更新用户信息 ( 昵称，密码 ，邮件)

}
function test_DeleteUser(){
    // 功能暂不开放

}

function testRequest(){

    $url = "http://tiku.huatu.com/apis/cascalog/logs";
    $table = 'fast_user_log'; // fast_user_log spec_user_log past_user_log mod_user_log
    $info = array('exam_id'=>99999);
    $ttl = null;
    $data = array('title'=>$table, 'con'=>json_encode($info), 'ttl'=>300 );
    $data = json_encode($data);
    $response = Requests::put($url, $data, array( 'header'=>array('user-agent'=>'IM/TEST', 'content-type'=>'application/json') ) );
    print_r($response);
    echo "\n";
}
//testRequest();
// test_GetUser();
//test_PostUser();

/*
import urllib2
opener = urllib2.build_opener(urllib2.HTTPHandler)
request = urllib2.Request('http://example.org', data='your_put_data')
request.add_header('Content-Type', 'your/contenttype')
request.get_method = lambda: 'PUT'
url = opener.open(request)

**/
