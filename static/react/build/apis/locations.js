

if ( !window.apis ) window.apis = {};

apis.locations = function (){
    if ( "locations" in this ) throw new Error("需要先实例化 apis.locations 类( 伪 Class )");
    this.name = "locations api";
    this.version = "0.1";
};

apis.locations.prototype.provinces = function (xmen){
    var url = "/instiApis/whipapp/locations/provinces";
    requests.get(url, {"async": true, "callback": xmen});
};

apis.locations.prototype.cities = function (id, xmen){
    var url = "/instiApis/whipapp/locations/cities/" + id;
    requests.get(url, {"async": true, "callback": xmen});
};
