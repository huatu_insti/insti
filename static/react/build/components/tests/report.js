
if ( !window.components ) window.components = {};
if ( !window.components.tests ) window.components.tests = {};
if ( !window.components.tests.report ) window.components.tests.report = {};

window.components.tests.report.test = React.createClass({
    displayName: "components.tests.report.test",
    name: "答题报告主框架",
    api: new apis.tests(), 
    components: {"rank": "", "report": "", "parse": ""},
    component: "",
    test_source: {'1': 'test', '2': 'catalog', '3': 'errors', "4": "exam"},
    getInitialState: function (){
        // root: indexbox
        // props: root / tid, tab
        /*
            "answers": {
                "1": {
                    "qid": 0,
                    "qans": "A",
                    "uans": "B",
                    "time": 1427852136
                }
            }
        */
        return {
            "secretid": 0,
            "eid": 0,
            "name": "",
            "struct": [],
            "answers": {},
            "status": 0,
            "stamp": 1, // 测试生成来源
            "duration": 0,
            "inittime": 1427781503,
            "updtime": time.time(),
            "tombstone": 0,
            "tid": this.props.tid
        };
    },
    componentDidMount: function (){
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                // status: 
                //      0 ->    答题中
                //      1 ->    交卷 
                //      2 ->    已出练习报告
                if ( parseInt(body.status) == 1 ) {
                    console.warn("试卷报告真正生成当中，请稍后在练习中心里面查看...");
                    setalert("试卷报告真正生成当中，请稍后在练习中心里面查看...");
                    // return false;
                } else if ( parseInt(body.status) == 0 ) {
                    setalert("你还没有交卷哦......");
                    console.warn("该测试状态不正确！ Test Status: " + body.status );
                    return false;
                }
                if ( "answers" in body != true ) body.answers = {};
            }catch(e){
                console.warn(e); return;
            }
            // if ( parseInt(body.status) != 1 ) {
            //     // 还未交卷，无法查看报告
            //     loading.show("该试卷还未交卷，无法查看报告～～");
            //     console.warn(":: 试卷还未交卷，无法查看报告～～");
            //     // window.location.href="/index.php?s=/exam/answer/testid/" + self.props.tid;
            //     // self.props.root.setState({"content": <window.components.tests.answer.test tid={self.props.tid} root={self.props.root} />});
            //     // loading.hide();
            //     // return false;
            // }
            self.answers = body.answers;
            if ( self.test_source[body.stamp] == 'exam' ){
                self.components.rank = React.createElement(window.components.tests.report.rank, {root: self, parent: self, test: body, tid: self.props.tid});
            } else {
                delete self.components['rank'];
            }
            
            self.components.report = React.createElement(window.components.tests.report.report, {root: self, parent: self, test: body, tid: self.props.tid, indexbox: self.props.indexbox});
            self.components.parse = React.createElement(window.components.tests.report.parse, {root: self, parent: self, test: body, tid: self.props.tid, indexbox: self.props.indexbox});
            self.component = self.components[Object.keys(self.components)[0]];
            if ( self.props.tab == "parse" ) {
                $('li[data-component="parse"]').siblings().removeClass("li_select");
                $('li[data-component="parse"]').addClass("li_select");
                self.component = self.components.parse;
            } else if ( self.props.tab == "report" ) {
                $('li[data-component="report"]').siblings().removeClass("li_select");
                $('li[data-component="report"]').addClass("li_select");
                self.component = self.components.report;
            } else {
                console.warn("unknow tab name...");
            }
            self.replaceState(body);
            self.forceUpdate();
        };
        this.api.pull(this.props.tid, callback);

    },
    switch_component: function (e, rid){
        var cp_name = $(e.target).data("component");
        if ( ["rank", "report", "parse"].indexOf(cp_name) == -1 ) return false;
        if ( cp_name == "rank" ) this.component = this.components.rank;
        if ( cp_name == "report" ) this.component = this.components.report;
        if ( cp_name == "parse" ) this.component = this.components.parse;

        $(e.target).siblings().removeClass("li_select");
        $(e.target).addClass("li_select");

        this.forceUpdate();
    },
    render: function (){
        var self = this;
        var nav = { "report": "答题报告", "parse": "题目解析" };
        if ( self.test_source[self.state.stamp] == 'exam' ) nav = { "rank": "成绩排名", "report": "答题报告", "parse": "题目解析" };

        return (
            React.createElement("div", {className: "report", "data-tid": this.state.tid}, 
                React.createElement("div", {className: "ans_top"}, 
                    React.createElement("div", {className: "ans_top_title", dangerouslySetInnerHTML: {__html: "练习名称："+this.state.name}}), 
                    React.createElement("span", null, "交卷时间：", time.ctime(this.state.updtime))
                ), 
              
                React.createElement("div", {className: "answer_con"}, 
                    React.createElement("div", {className: "ans_main"}, 
                        React.createElement("ul", {className: "ans_list clearfix"}, 
                             Object.keys(nav).map(function (k, i){
                                if ( i == 0 ) var _ccclass = "li_select";
                                else var _ccclass = "";
                                return (
                                    React.createElement("li", {className: _ccclass, "data-component": k, onClick: self.switch_component}, nav[k])
                                );
                            }) 
                        ), 
                        this.component
                    )
                )
            )
        );
    }
});

window.components.tests.report.rank = React.createClass({
    displayName: "components.tests.report.rank",
    name: "成绩排名",
    api: new apis.tests(),
    getInitialState: function (){
        return {};
    },
    componentDidMount: function (){
        
    },
    pull: function (){

    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "ans_main_type ans_score "}, 
                React.createElement("div", {className: "ans_score_top clearfix"}, 
                    React.createElement("div", {className: "ans_score_left"}, 
                        React.createElement("span", null, "您的得分"), 
                        React.createElement("div", {className: "ans_score_num"}, React.createElement("span", null, "53.8"), React.createElement("span", null, "分")), 
                        React.createElement("span", null, "本卷最高得分："), React.createElement("span", null, "87.5"), React.createElement("span", null, "分"), 
                        React.createElement("div", {className: "ans_score_info"}, 
                            "您共作答试题：", React.createElement("span", null, "120道"), "  " + ' ' +
                            "排名：", React.createElement("span", null, "150名"), "  " + ' ' +
                            "总作答人数：", React.createElement("span", null, "202人")
                        )
                    ), 
                    React.createElement("div", {className: "ans_score_right"}, 
                        React.createElement("div", {className: "ans_score_ranking"}, 
                            React.createElement("div", {className: "person_list ans_ranking_person_01"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_02"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_03"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_04"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_05"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_06"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_07"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_08"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_09"}), 
                            React.createElement("div", {className: "person_list ans_ranking_person_10"})
                        ), 
                         React.createElement("div", {className: "ans_score_info"}, 
                            "击败同类考生：", 
                            React.createElement("span", null, "19"), 
                            React.createElement("span", null, "%"), 
                            "  "
                        )
                    )
                ), 
                React.createElement("span", null, "接下来您是不是想"), 
                React.createElement("div", {className: "ans_train"}, React.createElement("a", {href: "javascript:;"}, "强化训练"))
            )
        );
    }
});


/**
    测试报告当中的 标签树 （包括 涉及知识点 树 以及 错题当中的 涉及知识点的标签树）

**/
window.components.tests.report.tree = React.createClass({
    displayName: "components.tests.report.tree",
    name: "标签树",
    api: new apis.tests(),
    getInitialState: function (){
        // props: tid, parent, cat
        return { "catalogs": [] };
    },
    componentDidMount: function (){
        this.fetch();
    },
    fetch: function (){
        // 拉取顶层标签树
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            } catch(e) {
                console.warn("fetch catalogs fail."); return;
            }
            self.replaceState(body);
        };
        this.api.tree( this.props.cat, this.props.tid, "0", callback );
    },
    render: function (){
        var self = this;
        if ( this.props.cat == "all" ) {
            return (
                React.createElement("div", {className: "ans_main_01"}, 
                    React.createElement("h3", null, "本次练习", this.props.parent.state.total.atoms, "道"), 
                    React.createElement("div", {className: "point_01 "}, 
                        React.createElement("div", {className: "tk_answer_list fr_01"}, 
                           React.createElement("div", {className: "tk_answer_list_top"}, 
                                React.createElement("div", {className: "tk_tab_1"}, "考点"), 
                                React.createElement("div", {className: "tk_tab_2"}, "答题对数"), 
                                React.createElement("div", {className: "tk_tab_3"}, "题目总数"), 
                                React.createElement("div", {className: "tk_tab_4"}, "正确率"), 
                                React.createElement("div", {className: "tk_tab_5"}, "强化训练")
                            ), 
                            React.createElement("div", {className: "tk_answer_list_bottom tk_have_tab_action"}, 
                                React.createElement("ul", {className: "clearfix"}, 
                                     this.state.catalogs.map(function (catalog, index){
                                        /*
                                            // catalog
                                                {
                                                    cid: 258,
                                                    cname: "基础知识部分",
                                                    info: {
                                                        hit: 3,
                                                        total: 10,
                                                        rate: 0.35,
                                                        diff: "+3"  ||  "-2",
                                                    }
                                                }
                                        */
                                        return (
                                            React.createElement(window.components.tests.report.leaf, {
                                                parent: self, 
                                                indexbox: self.props.indexbox, 
                                                root: self, 
                                                index: [index+1], 
                                                catalog: catalog})
                                        );
                                    }) 
                                )
                            )
                        )
                    )
                )
            );
        } else if ( this.props.cat == "error" ) {
            return (
                React.createElement("div", {className: "ans_main_01"}, 
                    React.createElement("h3", null, "错题", this.props.parent.state.total.errors, "道"), 
                    React.createElement("div", {className: "point_01 "}, 
                        React.createElement("div", {className: "tk_answer_list fr_01"}, 
                           React.createElement("div", {className: "tk_answer_list_top"}, 
                                React.createElement("div", {className: "tk_tab_1"}, "模块"), 
                                React.createElement("div", {className: "tk_tab_2"}, "历史错题"), 
                                React.createElement("div", {className: "tk_tab_3 col-3"}, "新增/减少情况"), 
                                React.createElement("div", {className: "tk_tab_4 col-4"}, "总计"), 
                                React.createElement("div", {className: "tk_tab_5"}, "错题训练")
                            ), 
                            React.createElement("div", {className: "tk_answer_list_bottom tk_have_tab_action"}, 
                                React.createElement("ul", {className: "clearfix"}, 
                                     this.state.catalogs.map(function (catalog, index){
                                        return (
                                            React.createElement(window.components.tests.report.leaf, {
                                                parent: self, 
                                                indexbox: self.props.indexbox, 
                                                root: self, 
                                                index: [index+1], 
                                                catalog: catalog})
                                        );
                                    }) 
                                )
                            )
                        )
                    )
                )
            );
        }
    }
});

window.components.tests.report.leaf = React.createClass({
    displayName: "components.tests.report.leaf",
    name: "标签树-叶子",
    api: new apis.tests(),
    getInitialState: function (){
        // props: catalog / parent / root 
        /*
            // catalog
                {
                    cid: 258,
                    cname: "基础知识部分",
                    info: {
                        hit: 3,       // 答对题数 || 总计(错题数量, 包含以往错题数量)
                        total: 10,  // 题目总数 || 历史错题
                        rate: 0.35, // 正确率 || 错误率
                        diff: "+3"  ||  "-2",
                    }
                }
        */
        var catalog = this.props.catalog;
        catalog['children'] = [];
        return catalog;
    },
    componentDidMount: function (){
        // 初始化样式
        // $(".tk_tab_list .tk_tab_5").hover(
        //     function (){
        //         $(this).find("div").show();
        //     },
        //     function (){
        //         $(this).find("div").hide();
        //     }
        // );

    },
    fetch: function (e, rid){
        // 拉取该节点的下属节点信息
        var self = this;
        var target = $(e.target);
        if ( $(e.target).hasClass("noa") ){
            // $(e.target).removeClass("noa").addClass("a");
        } else {
            $(e.target).removeClass("a").addClass("noa");
            self.setState({"children": [] });
            return;
        }

        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
            } catch(e) {
                console.warn("fetch catalogs fail."); return;
            }
            $(target).removeClass("noa").addClass("a");
            self.setState({"children": body.catalogs});
        };
        this.api.tree( this.props.root.props.cat, this.props.root.props.tid, this.state.cid, callback );
    },
    mk_test: function (source, cid){
        var self = this; 
        if ( parseInt(cid) < 1 ) return ;
        loading.show('正在组卷');
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                loading.hide();
                console.warn(response); return;
            }
            try{

                var body = JSON.parse(response.body);
              
            }catch(e){
                loading.hide();
                console.warn(e); return;
            }
            loading.hide();

            self.props.root.props.indexbox.setState({"content": React.createElement(window.components.tests.answer.test, {tid: body.tid, root: self.props.indexbox})});
        };
        if ( source == "errors" ) this.api.init("errors", { "num" : 15, "duration" : 0, "cid": parseInt(cid) }, callback);
        else if ( source == "catalogs" ) this.api.init("catalog", {"num" : 20, "duration" : 0 ,"cid": parseInt(cid) }, callback);
        else console.warn(":: 组卷失败……");
    },
    render: function (){
        var self = this;
        // <div className="tk_tab_1 col-1">常识判断</div>  className: noa

        if ( this.props.root.props.cat == "error" ) {
            var diff_class_name;
            var diff;
            if ( parseInt(this.state.add) > 0 ) {
                // diff_class_name = {"background": "rgba(255, 0, 0, 0.44)"};
                diff_class_name = {"background": "white"};
                diff = "+" + this.state.add;
            } else if ( parseInt(this.state.add) < 0 ) {
                diff_class_name = {"background": "#E8F5CE"};
                diff = this.state.add.toString();
            } else if ( parseInt(this.state.add) == 0 ) {
                diff_class_name = {"background": "white"};
                diff = this.state.add.toString();
            }

            return (
                React.createElement("li", null, 
                    React.createElement("div", {className: "tk_tab_list"}, 
                        React.createElement("div", {className: "tk_tab_1 col-1"}, this.state.cname), 
                        React.createElement("div", {className: "tk_tab_2"}, this.state.errors), 
                        React.createElement("div", {className: "tk_tab_3 col-3 col_item", style: diff_class_name}, diff), 
                        React.createElement("div", {className: "tk_tab_4 col-4"}, this.state.total), 
                        React.createElement("div", {className: "tk_tab_5"}, 
                            React.createElement("div", null, 
                                React.createElement("a", {onClick: self.mk_test.bind(self, "errors", this.state.cid)}, "错题重练")
                            )
                        )
                    ), 
                    React.createElement("ul", {className: "clearfix"}, 
                         this.state.children.map(function (catalog, index){
                            return (
                                React.createElement(window.components.tests.report.leaf, {
                                    parent: self, 
                                    indexbox: self.props.indexbox, 
                                    root: self.props.root, 
                                    index: self.props.index.concat(index+1), 
                                    catalog: catalog})
                            );
                        }) 
                    )
                )
            );
        } else if ( this.props.root.props.cat == "all" ) {
            return (
                React.createElement("li", null, 
                    React.createElement("div", {className: "tk_tab_list"}, 
                        React.createElement("div", {className: "tk_tab_1 noa", onClick: self.fetch}, this.state.cname), 
                        React.createElement("div", {className: "tk_tab_2"}, this.state.rightNum), 
                        React.createElement("div", {className: "tk_tab_3"}, this.state.totalNum), 
                        React.createElement("div", {className: "tk_tab_4"}, Math.round(this.state.rightRate*100*100)/100+"%"), 
                        React.createElement("div", {className: "tk_tab_5"}, 
                            React.createElement("div", null, 
                                React.createElement("a", {onClick: self.mk_test.bind(self, "catalogs", this.state.cid)}, "强化训练")
                            )
                        )
                    ), 
                    React.createElement("ul", {className: "clearfix"}, 
                         this.state.children.map(function (catalog, index){
                            return (
                                React.createElement(window.components.tests.report.leaf, {
                                    parent: self, 
                                    indexbox: self.props.indexbox, 
                                    root: self.props.root, 
                                    index: self.props.index.concat(index+1), 
                                    catalog: catalog})
                            );
                        }) 
                    )
                )
            );
        }


    }
});


window.components.tests.report.report = React.createClass({
    displayName: "components.tests.report.report",
    name: "答题报告",
    api: new apis.tests(),
    getInitialState: function (){
        // props: indexbox, root
        return { 
            "total":  {
                "right": 0,
                "atoms": 0,
                "errors": 0
            }
        };
    },
    componentDidMount: function (){
        this.total();
        // 初始化样式

    },
    pull: function (){

    },
    total: function (){
        // 计算试卷试题总量
        var self = this;
        
        var atoms = 0;
        var errors = 0;
        var right = 0;

        var compute_total = function (elem){
            if ( "struct" in elem != true )  elem.struct = [];
            atoms += elem.struct.length;
            var i = 0;
            for (i=0; i<elem.struct.length; i++ ) {
                if ( "struct" in elem.struct[i] == true) {
                    compute_total(elem.struct[i]);
                }
            }
        };
        compute_total(this.props.root.state);

        Object.keys(this.props.root.state.answers).map(function (qid, index){
            var answer = self.props.root.state.answers[qid];
            if ( answer.uans == answer.qans ) right += 1;
        });
        errors = atoms - right;
        this.setState({"total": {"errors": errors, "right": right, "atoms": atoms} });
    },
    compute_atoms: function (){
        var self = this;
        var atoms = [];
        var answers = this.props.root.state.answers;

        var parse_question = function (question){
            if ( question.qid in answers ){
                if ( answers[question.qid].uans == answers[question.qid].qans ) question['is_right'] = true;
                else question['is_right'] = false;
            } else question['is_right'] = false; // 未答题
            atoms.push(question);
        };
        var parse_composite = function (composite){
            if ( composite.cqid in self.elements.composites ) {
                // tixu_card.push(self.elements.composites[parseInt(composite.cqid)] );
            }
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };
        var i = 0;
        for (i=0; i<this.props.root.state.struct.length; i++ ) {
            if ( "qid" in this.props.root.state.struct[i] == true) {
                parse_question(this.props.root.state.struct[i]);
            } else if ( "cqid" in this.props.root.state.struct[i] == true) {
                parse_composite(this.props.root.state.struct[i]);
            } 
        }
        return atoms;
    },
    jump: function (index){
        var self = this;
        var atoms = this.compute_atoms();

        self.props.root.components.parse = React.createElement(window.components.tests.report.parse, {root: self.props.root, parent: self.props.root, test: self.props.root.state, tid: self.props.tid, jump: atoms[index]['qid']});
        self.props.root.component = self.props.root.components.parse;

        $(".ans_list li").siblings().removeClass("li_select");
        $('.ans_list li[data-component="parse"]').addClass("li_select");
        self.props.root.forceUpdate();
    },
    render: function (){
        var self = this;
        var atoms = this.compute_atoms();
        return (
            React.createElement("div", {className: "ans_main_type ans_report"}, 
                React.createElement("div", {className: "ans_main_01"}, 
                    React.createElement("h3", null, "本次练习", this.state.total.atoms, "道"), 
                    React.createElement("div", {className: "ans_result"}, 
                        React.createElement("div", {className: "ans_main_left", 
                                dangerouslySetInnerHTML: {__html: "答对题数<br /><br />\n" + this.state.total.right}}), 
                        React.createElement("div", {className: "ans_main_right"}, 
                            React.createElement("table", {className: "sheetTable", cellSpacing: "0"}, 
                                React.createElement("tbody", null, 
                                    range(Math.ceil(atoms.length/20)).map(function (row, index){
                                        return (
                                            React.createElement("tr", null, 
                                                 atoms.slice(index*20, 20).map(function(atom, index2){
                                                    if ( atom.is_right == true ) {
                                                        return (
                                                            React.createElement("td", {className: "green"}, 
                                                                React.createElement("a", {href: "javascript:;", onClick: self.jump.bind(self, index*20+index2)}, index*20+(index2+1))
                                                            )
                                                        );
                                                    } else {
                                                        return (
                                                            React.createElement("td", null, 
                                                                React.createElement("a", {href: "javascript:;", onClick: self.jump.bind(self, index*20+index2)}, index*20+(index2+1))
                                                            )
                                                        );
                                                    }
                                                }) 
                                            )
                                        );
                                    }) 
                                )
                            )
                        )
                    )
                ), 

                React.createElement(window.components.tests.report.tree, {tid: this.props.root.state.tid, parent: this, cat: "all", indexbox: this.props.indexbox}), 
                React.createElement(window.components.tests.report.tree, {tid: this.props.root.state.tid, parent: this, cat: "error", indexbox: this.props.indexbox})

            )
        );
/*
                <div className="ans_sp0">
                    你的正确率还可以再提高一点点，送你一本绝密资料吧！
                    <a href="javascript:;" className="act_get">点击领取</a>
                </div>
                <div className="ans_sp1">
                    公考不再难过关，小伙伴们快围观；华图名师齐上阵，周周更新播不停。
                    <a href="javascript:;" className="act_look">瞧一瞧</a>
                </div>

*/
    }
});

window.components.tests.report.parse = React.createClass({
    displayName: "components.tests.report.parse",
    name: "题目解析",
    api: new apis.tests(),
    elements: { "composites": {}, "questions": {} },
    jobs: 0,
    getInitialState: function (){
        return this.props.test;
    },
    componentDidMount: function (){

    },
    pull: function (){

    },
    jump: function (){
        if ( !this.props.jump ) return false;
        var qid = this.props.jump;
        var target = $('.element[data-qid="#QID#"]'.replace("#QID#", qid) );
        $("body, html").scrollTop( target.offset().top-55 );
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "ans_main_type ans_jiexi elements"}, 
                 this.props.test.struct.map(function (elem, index){
                    if ( "cqid" in elem == true ) {
                            return (
                                React.createElement(window.components.tests.report.parse_composite, {
                                    cqid: elem.cqid, 
                                    test: self.props.test, 
                                    root: self, 
                                    parent: self, 
                                    index: [index+1]})
                            );
                        } else if ( "qid" in elem == true ) {
                            return (
                                React.createElement(window.components.tests.report.parse_question, {
                                    qid: elem.qid, 
                                    test: self.props.test, 
                                    root: self, 
                                    parent: self, 
                                    index: [index+1]})
                            );
                        } else {
                            return ( React.createElement("div", {className: "error"}, "Unknow") );
                        }
                }) 
            )
        );
    }
});


window.components.tests.report.parse_question = React.createClass({
    displayName: "components.tests.report.parse_question",
    name: "单选题",
    tixu: 1,
    tixu2: "1",
    api: new apis.questions(),
    isShow: false,
    getInitialState: function (){
        this.tixu = this.props.index.join("-");
        this.tixu2 = this.props.tixu;
        return {
            "content": "",
            "choices": [],
            "attrs": [],
            "ans": "",
            "ansnum": 1,
            "score": 0,
            "tombstone": 0,
            "updtime": 1427183577,
            "inittime": 1427183577,
            "isMark": false,
            "qid": this.props.qid
        };
    },
    componentWillMount: function (){
        this.props.root.jobs += 1;
    },
    componentDidMount: function (){
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "content" in body != true ) body.content = "";
                if ( "attrs" in body != true ) body.attrs = [];
                if ( "choices" in body != true ) body.choices = [];
            }catch(e){
                console.warn(e); return;
            }
            if ( parseInt(body.ansnum) > 1 ) self.name = "多选题";
            if ( self.props.root.jobs > 0 ) self.props.root.jobs--;
            if ( self.props.root.jobs == 0 ){
                self.props.root.jump();
            }
            body.isMark = false;
            self.replaceState(body);
            self.isMark();   // 同步收藏状态
        };
        this.api.pull(this.props.qid, callback);
    },
    addMark: function (){
        var self = this;
        var api = new window.apis.user.records.favors();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);return;
            }
            self.setState({"isMark": true});
        };
        api.append([{"qid": this.state.qid, "time": time.time()}], callback);
    },
    removeMark: function (){
        var self = this;
        var api = new window.apis.user.records.favors();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response);return;
            }
            self.setState({"isMark": false});
        };
        api.remove([{"qid": this.state.qid, "time": time.time()}], callback);
    },
    updateMarkStatus: function (){
        // 更新 收藏状态
        var self = this;
        if ( self.state.isMark == false ) {
            self.addMark();
        } else {
            self.removeMark();
        }
    },
    isMark: function (){
        var self = this;
        var api = new window.apis.user.records.favors();
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                self.setState({"isMark": false});
                // console.warn(response);
                return;
            }
            self.setState({"isMark": true});
        };
        api.hit( this.state.qid, callback );
    },
    showJiexi: function (){
        if ( this.isShow == false ) {
            this.isShow = true;
            this.forceUpdate();
        } else {
            this.isShow = false;
            this.forceUpdate();
        }
    },
    render: function (){
        var self = this;
        var _class;
        if ( this.isShow == false ) _class = "none";
        else _class = "";
        // 判断试题对错
        var answers = this.props.root.props.root.answers;
        
        var isright = "回答正确";
        if ( this.props.qid in answers == true && answers[this.props.qid].uans.length > 0 ){
            var answer = answers[this.props.qid];
            range(answer.qans.length).map(function (i){
                if ( answer.uans.indexOf(answer.qans[i]) == -1 ) isright = "回答错误";
            });
        } else {
            // 未作答
            var answer = {"uans": "未作答"};
            isright = "回答错误";
        }
        var m_class;
        if ( self.state.isMark == true ) {
            m_class = "jiexi_icon_01_active";
        } else {
            m_class = "jiexi_icon_01";
        }
        return (
            React.createElement("div", {className: "ans_jiexi_model element", "data-qid": this.state.qid, "data-tixu": this.tixu}, 
                React.createElement("div", {className: "jiexi_model clearfix"}, 
                    React.createElement("div", null, this.tixu), 
                     React.createElement("div", {className: "content", dangerouslySetInnerHTML: {__html: "（" + this.name + "）" + this.state.content}})
                ), 
                React.createElement("div", {className: "jiexi_choice"}, 
                    React.createElement("ul", null, 
                         self.state.choices.map(function (choice, index){
                            if ( choice == "{%NULL%}" || choice == "{%NULL%} " ) return undefined;
                            return (
                                React.createElement("li", {key: "question_"+self.state.qid+"_choice_"+index+"_"}, 
                                    React.createElement("div", {className: "value", dangerouslySetInnerHTML: {__html: chr(65+index) + ". " + choice}})
                                )
                            );
                        }) 
                    )
                ), 
                React.createElement("div", {className: "jiexi_info"}, 
                    React.createElement("div", {className: "jiexi_info_top"}, 
                        React.createElement("div", {className: "jiexi_info_left fl"}, 
                            React.createElement("span", {onClick: this.showJiexi}, "展开解析"), 
                            React.createElement("a", {onClick: this.showJiexi}, React.createElement("img", {src: "static/images/jiexi_down.png"}))
                        ), 
                        React.createElement("div", {className: "jiex_info_right fr"}, 
                            React.createElement("ul", {className: "clearfix"}, 
                                React.createElement("li", {className: m_class, onClick: self.updateMarkStatus}, "收藏本题"), 
                                React.createElement("li", {className: "jiexi_icon_02", style: {"display": "none"}}, "名师讲"), 
                                React.createElement("li", {className: "jiexi_icon_03", style: {"display": "none"}}, "想听老师讲"), 
                                React.createElement("li", {className: "jiexi_icon_04", style: {"display": "none"}}, "纠错")
                            )
                        )
                    ), 
                    
                    React.createElement("div", {className: "jiexi_info_div " + _class}, 
                        React.createElement("div", {className: "jiexi_info_detail"}, 
                            "正确答案是", React.createElement("span", {className: "sp_true"}, this.state.ans), "，" + ' ' +
                            "你的答案是", React.createElement("span", {className: "sp_false"}, answer.uans), "。", 
                            isright
                        ), 
                         self.state.attrs.map(function (attr, index){
                            if ( attr.name == "抽题" ) return null;
                            if ( attr.value == undefined || attr.value == null || attr.value == "" ) return null;
                            if ( attr.name == "地区" || attr.name == "知识点" ) {
                                var names = attr.value.split(",").map(function (kv, i){
                                    return kv.split(":")[1];
                                }).join(",");
                                attr.value = names;
                            }
                                return (
                                    React.createElement("div", {key: "question_"+self.state.qid+"_attr_"+index+"_", className: "jiexi_info_come clearfix"}, 
                                        React.createElement("div", {className: "jiexi_info_come_le key"}, attr.name), 
                                        React.createElement("div", {className: "value", dangerouslySetInnerHTML: {__html: attr.value.toString()}})
                                    )
                                );
                        }), 
                        React.createElement("div", {className: "jiexi_info_come clearfix", style: {"display": "none"}}, 
                            React.createElement("div", {className: "jiexi_info_come_top"}, 
                                React.createElement("div", {className: "jiexi_info_come_left"}, "笔记"), 
                                React.createElement("div", {className: "jiexi_edit"}, "编辑笔记")
                            ), 
                            React.createElement("div", {className: "jiexi_info_come_bottom none"}, 
                                React.createElement("textarea", null), 
                                React.createElement("span", {className: "info_span"}, "还可以输入500个字符"), 
                                React.createElement("a", {href: "javascript:;", className: "info_come_reacord"}, "记录")
                            )
                        )
                    )
                )
            )
        );
    }
});

window.components.tests.report.parse_composite = React.createClass({
    displayName: "components.tests.report.parse_composite",
    name: "复合题",
    tixu: 1,
    tixu2: "1",
    api: new apis.composites(),
    getInitialState: function (){
        this.tixu = this.props.index.join("-");
        this.tixu2 = this.props.tixu;
        return {
            "content": "",
            "struct": [],
            "tombstone": 0,
            "updtime": 1427185277,
            "inittime": 1427185277,
            "cqid": this.props.cqid
        };
    },
    componentWillMount: function (){
        this.props.root.jobs += 1;
    },
    componentDidMount: function (){
        this.pull();
        // this.props.root.elements.composites[body.cqid] = {"element": body, "component": this}
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            }
            try{
                var body = JSON.parse(response.body);
                if ( "content" in body != true ) body.content = "";
                if ( "struct" in body != true ) body.struct = [];
            }catch(e){
                console.warn(e); return;
            }
            if ( parseInt(body.ansnum) > 1 ) self.name = "多选题";
            self.props.root.elements.composites[body.cqid] = {"element": body, "component": self}
            if ( self.props.root.jobs > 0 ) self.props.root.jobs--;
            if ( self.props.root.jobs == 0 ){
                self.props.root.jump();
            }
            self.replaceState(body);
        };
        this.api.pull(this.props.cqid, callback);
    },
    render: function (){
        var self = this;
        return (
            React.createElement("div", {className: "ans_jiexi_composite element", "data-cqid": this.state.cqid, "data-tixu": this.tixu}, 
                React.createElement("div", {className: "ans_jiexi_composite_content clearfix"}, 
                    React.createElement("div", {className: "ti_composites_content_fl fl"}, this.name), 
                    React.createElement("div", {className: "ti_composites_content_fr fl"}, 
                        "阅读下列材料，回答下面的题目。", 
                                React.createElement("br", null), 
                                React.createElement("p", {dangerouslySetInnerHTML: {__html: this.state.content}}), 
                                React.createElement("img", {src: "static/images/test.png"})
                    )
                ), 
                React.createElement("div", {className: "elements"}, 
                     this.state.struct.map(function (elem, index){
                        if ( "cqid" in elem == true ) {
                            return (
                                React.createElement(window.components.tests.report.parse_composite, {
                                    cqid: elem.cqid, 
                                    test: self.props.test, 
                                    root: self.props.root, 
                                    parent: self, 
                                    index: self.props.index.concat(index+1)})
                            );
                        } else if ( "qid" in elem == true ) {
                            return (
                                React.createElement(window.components.tests.report.parse_question, {
                                    qid: elem.qid, 
                                    test: self.props.test, 
                                    root: self.props.root, 
                                    parent: self, 
                                    index: self.props.index.concat(index+1)})
                            );
                        } else {
                            return ( React.createElement("div", {className: "error"}, "Unknow") );
                        }
                    }) 
                )
            )
        );
    }
});



