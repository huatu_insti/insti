
if ( !window.components ) window.components = {};
if ( !window.components.share ) window.components.share = {};

window.components.share.footer = React.createClass({
    displayName: "components.share.footer",
    name: "页脚",
    //api: new apis.tests(),
    getInitialState: function (){
        return {};
    },
    pull: function (){
        
    },
    componentDidMount: function (){
        
    },
    render: function (){
        var self = this;
		/*
			<div className="flinks">
                            <a target="_blank" href="#">网站地图</a> | 
                            <a target="_blank" href="#">活动公告</a> | 
                            <a target="_blank" href="#">公考社区</a> | 
                            <a target="_blank" href="#">联系我们</a> 
                         </div>
		*/
        return (
            React.createElement("div", {className: "ti_footer"}, 
                React.createElement("div", {className: "foot"}, 
                    React.createElement("div", {className: "foot_left"}
                       
                    ), 
                    React.createElement("div", {className: "foot_right"}, 
                        React.createElement("div", {class: "flinks", "data-reactid": ".2.0.1.0"}), 
                         React.createElement("div", {className: "copyright"}, 
                            "武汉迈酷奇科技有限公司 鄂ICP备120109号-4"  
                         ), 
                         React.createElement("div", null, 
                           "友情链接： ", React.createElement("a", {href: "http://www.mstudy.me", target: "_blank", 
                                         style: {"color":"black"}}, "麦学习")
                         )
                         
                    )
                )
            )
        );
    }
});
