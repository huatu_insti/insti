if ( !window.components ) window.components = {};
if ( !window.components.exams ) window.components.exams = {};

window.components.exams.view =  React.createClass({
    displayName: "ExamsView",
    name: "考试预览",
    api: new window.apis.exams(),
    questions: {},
    composites: {},
    // elements: { "element": null, "component": null, "tixu": "0", "index": [1] },

    getInitialState: function (){
        if ( !this.props.eid || parseInt(this.props.eid) < 1 ) throw new Error("window.components.exams.list react init error.");
        return { "name": "", "eid": this.props.eid, "struct": [], "attrs": [] };
    },
    componentDidMount: function (){
        window.exams_preview = this;
        this.pull();
    },
    pull: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                self.replaceState(JSON.parse(response.body));
            }catch(e){
                console.warn(e); return false;
            }

        };
        this.api.pull(this.props.eid, callback);
    },
    total: function (t){
        var self = this;
        if ( t == "elements" ) {
            return Object.keys(self.composites).length + Object.keys(self.questions).length;
        } else if ( t == "score" ) {
            var score = 0;
            Object.keys(self.questions).map(function(qid, index){
                score += parseFloat(self.questions[qid].element.score);
            });
            return score;
        }
    },
    mk_tixu_card: function (){
        // 题序卡
        var self = this;
        var tixu_card = [];

        var parse_question = function (question){
            if ( parseInt(question.qid) in self.questions ) {
                tixu_card.push(self.questions[parseInt(question.qid)] );
            }
        };
        var parse_composite = function (composite){
            if ( composite.cqid in self.composites ) {
                tixu_card.push(self.composites[parseInt(composite.cqid)] );
            }
            if ( "struct" in composite != true ) composite.struct = [];
            var _i=0;
            for (_i=0; _i<composite.struct.length; _i++ ) {
                if ( "qid" in composite.struct[_i] == true) {
                    parse_question(composite.struct[_i]);
                } else if ( "cqid" in composite.struct[_i] == true) {
                    parse_composite(composite.struct[_i]);
                } 
            }

        };

        var i = 0;
        for (i=0; i<this.state.struct.length; i++ ) {
            if ( "qid" in this.state.struct[i] == true) {
                parse_question(this.state.struct[i]);
            } else if ( "cqid" in this.state.struct[i] == true) {
                parse_composite(this.state.struct[i]);
            } 
        }
        return tixu_card;
    },
    render: function (){
        // 渲染模板
        var self = this;
        var tixu_card = this.mk_tixu_card();
        tixu_card = [];
        /*
            {
                "name":"p1",
                "struct":[
                    {"qid":554},{"qid":555},{"cqid":129,"struct":[]}
                ],
                "attrs":[
                    {"name":"year","value":"2002"},{"name":"area","value":"21"}
                ],
                "uppertime":0,
                "lowertime":0,
                "duration":7200,
                "tombstone":0,
                "inittime":1427426836,
                "updtime":1427426836,
                "eid":29
            }
        */
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "create_head"}, 
                    React.createElement("div", {className: "record_paper"}, "试卷预览"), 
                    React.createElement("ul", {className: "clearfix"}, 
                        React.createElement("li", {className: "create_off"}, "文案")
                    ), 
                    React.createElement("div", {className: "return_list"}, "文案"), 
                    React.createElement("div", {className: "input_test"}, "文案")
                ), 
                React.createElement("div", {className: "pre_title"}, 
                    React.createElement("h3", {dangerouslySetInnerHTML: {__html: "试卷名称：" + this.state.name}}), 
                    React.createElement("span", {dangerouslySetInnerHTML: {__html: "试卷总分：" + this.total("score")}}), 
                    React.createElement("span", {dangerouslySetInnerHTML: {__html: "试题题量：" + this.total("elements")}})
                ), 
                React.createElement("div", {className: "pre_type"}, 
                    React.createElement("ul", {className: "clearfix"}, 
                         tixu_card.map(function(obj, index){
                            if ( "qid" in obj.element ) {
                                return (
                                    React.createElement("li", {
                                        key: "tixucard_q_" + obj.element.qid, 
                                        "data-qid": obj.element.qid, 
                                        "data-tixu": obj.tixu}, 
                                        React.createElement("a", {href: "javascript:;"}, obj.tixu)
                                    )
                                );
                            } else if ( "cqid" in obj.element ) {
                                return (
                                    React.createElement("li", {
                                        key: "tixucard_c_" + obj.element.cqid, 
                                        "data-cqid": obj.element.cqid, 
                                        "data-tixu": obj.tixu}, 
                                        React.createElement("a", {href: "javascript:;"}, obj.tixu)
                                    )
                                );
                            }
                        }) 
                    )
                ), 
                React.createElement("div", {className: "elements"}, 
                     this.state.struct.map(function(elem, index){
                        if ( "qid" in elem ){
                            return React.createElement(window.components.questions.preview, {
                                        key: "preview_question_" + elem.qid.toString(), 
                                        qid: elem.qid.toString(), 
                                        paper: self, 
                                        index: [index+1]})
                        } else if ( "cqid" in elem ){
                            return React.createElement(window.components.composites.preview, {
                                        key: "preview_composite_" + elem.cqid.toString(), 
                                        cqid: elem.cqid.toString(), 
                                        paper: self, 
                                        index: [index+1]})
                        } else {
                            console.warn("unknow element type.");
                            return React.createElement("div", {className: "element"}, "Sync Element Fail.");
                        }
                    }) 
                )
            )

            );
    }

});
