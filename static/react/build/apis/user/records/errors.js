
if ( !window.apis ) window.apis = {};
if ( !window.apis.user ) window.apis.user = {};
if ( !window.apis.user.records ) window.apis.user.records = {};

apis.user.records.errors = function (){
    if ( "errors" in this ) throw new Error("需要先实例化 apis.user.records.errors 类( 伪 Class )");
    this.name = "errors api";
    this.version = "0.1";
};
apis.user.records.errors.prototype.fetch_old = function (opts, xmen){
    // 获取用用户错题记录
    if (!opts ) var opts = { "size": 20, "page": 1 };
    if ( !opts.size ) opts.size = 20;
    if ( !opts.page ) opts.page = 1;
    if ( !xmen ) var xmen = function (){};
    
    var url = "/instiApis/whipapp/user/errors";
    requests.get(url, {"async": true, "callback": xmen});
};
apis.user.records.errors.prototype.fetch = function (node, xmen){
    // 获取用用户错题记录
    if ( parseInt(node) < 0 ) return false;
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipapp/user/errors/catalog/"+parseInt(node).toString();
    requests.get(url, {"async": true, "callback": xmen});
};

apis.user.records.errors.prototype.remove = function (errors, xmen){
    // 清空用用户错题记录
    if ( !xmen ) var xmen = function (){};
    
    var url = "/instiApis/whipapp/user/errors";
    if ( Object.keys(errors).length == 0 ) {
        // 清空用户所有错题记录
        requests.METHOD_DELETE(url, {"async": true, "callback": xmen});
    } else {
        // 删除某个错题记录
        // errors: {"action": "REMOVE","errors": [{"qid": 5,"qans": "A","uans": "B","time": 1427882333}]}
        requests.put(url, {"action": "REMOVE", "errors": errors}, {"async": true, "callback": xmen});
    }
};

apis.user.records.errors.prototype.append = function (errors, xmen){
    // 新增用户收藏记录
    if ( !xmen ) var xmen = function (){};
    // errors: {"action": "ADD","errors": [{"qid": 5,"qans": "A","uans": "B","time": 1427882333}] }
    var url = "/instiApis/whipapp/user/errors";
    requests.put(url, {"action": "ADD", "errors": errors }, {"async": true, "callback": xmen});
};

apis.user.records.errors.prototype.rank = function (opts, xmen){
    // 获取用用户的错题重练推荐排名
    if (!opts ) var opts = { "size": 20, "page": 1 };
    if ( !opts.size ) opts.size = 20;
    if ( !opts.page ) opts.page = 1;
    if ( !xmen ) var xmen = function (){};
    
    var url = "/instiApis/whipapp/user/errors/rank";
    requests.get(url, {"async": true, "callback": xmen});
};