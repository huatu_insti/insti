

if ( !window.components ) window.components = {};
if ( !window.components.exams ) window.components.exams = {};

window.components.exams.list =  React.createClass({
    displayName: "ExamsList",
    name: "考试列表",
    exams: [],           // 单条试卷信息栏
    page: 1,
    area: -9,
    size: 20,
    api: new window.apis.exams(),
    getInitialState: function (){
        if ( !this.props.page || !this.props.size ) throw new Error("window.components.exams.list react init error.");
        if ( parseInt(this.props.page) < 1 || parseInt(this.props.size<1) ) throw new Error("window.components.exams.list react init error.");
        return { "exams": [], "count": 0 };
    },
    componentDidMount: function (){
        this.page = parseInt(this.props.page);
        this.size = parseInt(this.props.size);
        this.fetch();
    },
    fetch: function (){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return false;
            }
            try{
                var body = JSON.parse(response.body); 
                if ( "exams" in body != true ) body.exams = [];
                if ( "count" in body != true ) body.count = 0;
            }catch(e){
                console.warn(e);  return false;
            }
            self.replaceState(body);
        };
        this.api.fetch(this.page, this.size, callback);
    },
    remove: function (eid){
        var self = this;
        var callback = function (response){
            if ( response.type == 'progress' ) return 'loading ...';
            if ( response.code != 200 ) {
                console.warn(response); return;
            } else {
                $("tr[data-eid='#eid#']".replace("#eid#", eid)).remove();
            }
        };
        // callback({"type": "hehe", "code": 500});
        $("<div class=\"confirm_exam_del\">"
            +"<div class=\"message\">确定撤出考试吗?</div>"
            +"<div class=\"add_btn\">"
                +" <a class=\"add_canal\">取消<\/a>"
                +" <a class=\"add_new\">确定<\/a>"
            +"</div></div>").dialog({
               title:"撤出考试",
               close:true,
               modal:true,
               width:"400px"
        })
        $(".confirm_exam_del").find(".add_canal").on("click",function(){ $(".confirm_exam_del").dialog("close")})
                              .end().find(".add_new").on("click",function(){ 
                                     self.api.remove(eid, callback);$(".confirm_exam_del").dialog("close");})

       
        
    },
    onSetPage: function (page, e){
        var self = this;

        if ( ! page || (parseInt(page) < 1  && page != "goto" ) ) return false;
        if ( page == "goto" ) page = parseInt($(e.target).val());
        if ( parseInt(page) == parseInt(this.page) ) return true;
        if ( parseInt(page) > 0 && parseInt(page) < Math.ceil(this.state.count/this.size)+1 ){
            this.page = parseInt(page);
            this.fetch();
        }
    },
    previewExam:function(eid){
         
         var self = this;  
         self.props.root.setState({"content":<window.components.exams.view eid={eid} root={self.props.root}/>})
    },
    render: function (){
        // 渲染模板
        var self = this;
        /*
        count: 1,
        exams: [
            0: {
                eid: 2,
                inittime: 1424446662,
                lowertime: 1424446662,
                name: "test",
                tombstone: 0,
                updtime: 1424446662
            }
        ]
        */
        
        return (
            <div>
                <div className="create_head">
                    <div className="record_paper">试卷列表</div>
                </div>

                <div className="paper_sort_main">
                    <div className="paper_sort_title" style={{"display": "none"}}>
                        <span>试卷名称</span>
                        <input type="text" placeholder="请输入试卷名称" className="inp_search" />
                        <a className="sort_search" href="javascript:;">搜索</a>
                    </div>
                    <table className="tab_01" bordercolor="#ddd" border="1px" align="left" cellSpacing="0" cellPadding="0" width="100%">
                       <thead>
                            <tr>
                                <th>编号</th>
                                <th>试卷名称</th>
                                <th>考试类型</th>
                                <th>科目</th>
                                <th>标签</th>
                                <th>添加时间</th>
                                <th>编辑</th>
                           </tr>
                        </thead>
                        <tbody id="tid" align="center">
                            { this.state.exams.map(function (exam, index){
                                return (
                                    <tr data-eid={exam.eid} key={exam.eid}>
                                        <td>{exam.eid}</td>
                                        <td>{exam.name}</td>
                                        <td>事业单位</td>
                                        <td>公共基础知识</td>
                                        <td>未知</td>
                                        <td>{time.ctime(exam.inittime)}</td>
                                        <td className="sort_operate">
                                            <span className="btn btn-default"><a onClick={self.previewExam.bind(self,exam.eid)} style={{"marginRight":"10px"}}>查看</a></span>
                                            <span className="btn btn-default"><a onClick={self.remove.bind(this, exam.eid) }>撤出考试</a></span>
                                        </td>
                                    </tr>
                                );
                            }) }
                            
                            <tr key={"exams_list_nav___"}>
                                <td colSpan="7">
                                    <div className="page_left"><span>共{this.state.count}条记录，每页{this.size}条，共{Math.ceil(this.state.count/this.size)}页</span></div>
                                    <div className="page_right">
                                        <span className="page_sp">
                                            跳转至第&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="text" className="page_num"  
                                                        placeholder={this.page+1} 
                                                        onChange={this.onSetPage.bind(self, "goto") } />页，
                                            页数&nbsp;{this.page}&nbsp;/&nbsp;{Math.ceil(this.state.count/this.size)}
                                        </span>
                                        <a className="page_top" onClick={this.onSetPage.bind(self, this.page-1) }>&lt;</a>
                                        <a className="page_down" onClick={this.onSetPage.bind(self, this.page+1) }>&gt;</a>
                                    </div>
                                </td>
                            </tr>
                       </tbody>
                  </table>
                </div>
            </div>
            );
    }

});