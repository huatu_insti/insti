
if ( !window.apis ) window.apis = {};
if ( !window.apis.catalogs ) window.apis.catalogs = {};

apis.catalogs.atoms = function (){
    if ( "atoms" in this || "atoms" in this ) throw new Error("需要先实例化 apis.catalogs.atoms 类( 伪 Class )");
    this.name = "catalogs.atoms api";
    this.version = "0.1";
};

/*
    @            cid: 标签编号
            hookid: 父级标签

*/
apis.catalogs.atoms.prototype.root = function (cid, xmen){
    // 提取试卷标签的 根标签
    if ( !xmen ) var xmen = function (){};
    if ( !cid ) var cid = 0;
    if ( parseInt(cid) == 0 ) var url = "/instiApis/whipapp/catalogs/questions";
    else var url = "/instiApis/whipapp/catalogs/questions/" + cid + "/children";
    requests.get(url, {'"async': true, "callback": xmen});
};
apis.catalogs.atoms.prototype.fetch = function (cid, xmen){
    // 提取该标签的 子标签
    if ( !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    if ( parseInt(cid) == 0 ) var url = "/instiApis/whipapp/catalogs/questions";
    else var url = "/instiApis/whipapp/catalogs/questions/" + cid + "/children";
    requests.get(url, {'"async': true, "callback": xmen});
};

apis.catalogs.atoms.prototype.pull = function (cid, xmen){
    if ( !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipapp/catalogs/questions/" + cid ;
    requests.get(url, {'"async': true, "callback": xmen});
};
apis.catalogs.atoms.prototype.push = function (catalog, xmen){
    if ( !catalog ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    // catalog format: {"name" : "言言语理解", "hookid" : 1}
    // Note: 这里的 hookid: String
    var url = "/instiApis/whipapp/catalogs/questions";
    requests.put(url, catalog, {'"async': true, "callback": xmen});
};
apis.catalogs.atoms.prototype.update = function (cid, catalog, xmen){
    if ( !catalog || !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    // catalog format: {"name" : "言言语理解", "hookid" : 1}
    // Note: Hookid: Int
    var url = "/instiApis/whipapp/catalogs/questions/" + cid;
    requests.patch(url, catalog, {'"async': true, "callback": xmen});
};
apis.catalogs.atoms.prototype.bind_elems = function (cid, catalog, elems, xmen){
    if ( !catalog || !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    /*
        {
        "name": "常识与判断",
        "hookid": 0,
        "elems": [
            {"id": 2,"name": "hhh"}
        ],
        "updtime": 1427348837,
        "inittime": 1427347782,
        "tombstone": 0,
        "cid": 6
        }
    */
    var url = "/instiApis/whipapp/catalogs/questions/" + cid + "/elems";
    requests.patch(url, catalog, {'"async': true, "callback": xmen});
};
apis.catalogs.atoms.prototype.remove = function (cid, xmen){
    if ( !cid ) throw new Error("参数错误.");
    if ( !xmen ) var xmen = function (){};
    var url = "/instiApis/whipapp/catalogs/questions/" + cid ;
    requests.METHOD_DELETE(url, {'"async': true, "callback": xmen});
};
