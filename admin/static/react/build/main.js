// Main JavaScript Document

if ( !window.views ) window.views = {};

if ( !window.views.clean ) {
    window.views.clean = function (){
        try{
            React.unmountComponentAtNode(document.getElementById('container'));
            $("#container").empty();
        }catch(e){
            throw new Error("unmount react element fail.");
        }
    };
}

if ( !window.views.papers ) window.views.papers = {};


views.papers.fetch = function (page, size, area){
    // 提取试卷列表
    window.views.clean();
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    var el = React.createElement(window.components.papers.list, { "page": page, "size": size } );
    React.render(el, document.getElementById("container") );
};
views.papers.init = function ( pid ){
    // 创建试卷
    if ( !pid || parseInt(pid) < 1 ) var pid = "0";
    window.views.clean();
    var el = React.createElement( window.components.papers.create, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.edit = function ( pid ){
    // 编辑试卷
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.paper, { "pid": pid, "tixu_card2": [] } );
    React.render(el, document.getElementById("container") );
    var initUI = function (){
        ;(function($, e){
            //编辑模式
            $("dl").on("click", '.paper_item_title', function(){
                var _this = $(this);
                $parent = _this.parent(),
                $toggle = $parent.find('.paper_item_toggle'),
                $del = $parent.find('.paper_item_del');
                $parent.find('.paper_drag_item').addClass('none');  
                $toggle.removeClass('paper_item_show').addClass('paper_item_hide').hide();
                $del.hide();
                $parent.next().show();
                _this.removeClass('title_h');
                _this.css('width','370px');
            });
            //编辑失去焦点
            $('dl').on('blur', '.paper_item_title', function(){
                var _this = $(this);
                $parent = _this.parent(),
                $toggle = $parent.find('.paper_item_toggle'),
                $del = $parent.find('.paper_item_del');
                _this.css('width','300px'); 
                $toggle.show();
                $del.show();
            });
            /****title****/
            $("dl").on("mouseover", '.paper_item_slide', function(){
                var _this = $(this),
                    $substance = _this.next(),
                    $toggle = _this.find('.paper_item_toggle'),
                    $drag = _this.find('.paper_drag_item');
                if ($substance.is(':hidden')) {
                    $toggle.addClass('paper_item_show');
                } else {
                    $toggle.addClass('paper_item_hide');
                }
                $drag.removeClass('none');  
                _this.addClass('paper_item_slide_hover').find('.paper_item_score input').addClass('paper_item_slide_hover');
                _this.find('.paper_item_slide_tit input').addClass('paper_item_slide_hover');
                $toggle.removeClass('none');    
                _this.find(".paper_item_del").removeClass('none');  
            });
            $("dl").on("mouseout", '.paper_item_slide', function(){
                var _this = $(this),
                    $toggle = _this.find('.paper_item_toggle'),
                    $drag = _this.find('.paper_drag_item');
                $drag.addClass('none'); 
                /*
                _this.css('backgroundColor','#fff');
                _this.find('.paper_item_score input').css('backgroundColor','#fff');
                */
                _this.removeClass('paper_item_slide_hover').find('.paper_item_score input').removeClass('paper_item_slide_hover');
                _this.find('.paper_item_slide_tit input').removeClass('paper_item_slide_hover');
                $toggle.addClass('none');   
                _this.find(".paper_item_del").addClass('none');
            });
            // to show
            $('dl').on('click','.paper_item_show',function(){
                var _this = $(this),
                    $substance = _this.parent().next();
                    /*$drag = _this.parent().find('.paper_drag_item');*/
                $substance.show();
                _this.removeClass('paper_item_show').addClass('paper_item_hide');
                //$drag.addClass('none');   
                _this.parent().find('.paper_item_title').removeClass('title_h');
            });
            // to hide
            $('dl').on('click','.paper_item_hide',function(){
                var _this = $(this),
                    $substance = _this.parent().next();
                    //$drag = _this.parent().find('.paper_drag_item');
                $substance.hide();
                _this.removeClass('paper_item_hide').addClass('paper_item_show');
                //$drag.removeClass('none');
                _this.parent().find('.paper_item_title').addClass('title_h');
            });
            //title,score
            $('dl').on('mouseover', '.paper_item_slide_tit input,.paper_item_title,.paper_item_score input,.paper_edit_div,.pape_attr_edit', function(){
                $(this).addClass('on');
            });
            $('dl').on('mouseout', '.paper_item_slide_tit input,.paper_item_title,.paper_item_score input,.paper_edit_div,.pape_attr_edit', function(){
                $(this).removeClass('on');
            });
            //选项可编辑区域
            $('dl').on('click', '.paper_edit_div', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_choice_del').hide();
                _this.css('width','415px');
            });
            $('dl').on('blur', '.paper_edit_div', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_choice_del').show();
                _this.css('width','350px');
            });
            //属性编辑区域
            $('dl').on('click', '.pape_attr_edit', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_attr_del').hide();
                _this.css('width','415px');
            });
            $('dl').on('blur', '.pape_attr_edit', function(){
                var _this = $(this);
                $parent = _this.parent();
                $parent.find('.paper_attr_del').show();
                _this.css('width','350px');
            });
            /***choice***/
            $('dl').on('mouseover','.paper_choice',function(){
                var _this = $(this);
                _this.css('backgroundColor','#eee');
                _this.find('.paper_choice_del').removeClass('none');    
            });
            $('dl').on('mouseout','.paper_choice',function(){
                var _this = $(this);
                _this.css('backgroundColor','#fff');
                _this.find('.paper_choice_del').addClass('none');
            });
            /**attr**/
            $('dl').on('mouseover' ,'.pape_attr', function(){
                var _this = $(this);
                _this.css('backgroundColor','#eee');
                _this.find('.paper_attr_del').removeClass('none');
            });
            $('dl').on('mouseout' ,'.pape_attr', function(){
                var _this = $(this);
                _this.css('backgroundColor','#fff');
                _this.find('.paper_attr_del').addClass('none');
            });
        })(jQuery, window);
    };
    initUI();
};

views.papers.preview = function ( pid ) {
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.preview, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.review = function ( pid ) {
    if ( !pid || parseInt(pid) < 1 ) return "pid error.";
    window.views.clean();
    var el = React.createElement( window.components.papers.review, { "pid": pid } );
    React.render(el, document.getElementById("container") );
};

views.papers.feedback = function (){

};


/**

    @   exams

**/
if ( !window.views ) window.views = {};
if ( !window.views.exams ) window.views.exams = {};

views.exams.fetch = function (page, size, area ){
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    window.views.clean();
    var el = React.createElement(window.components.exams.list, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};
views.exams.view = function (eid ){
    if ( !page ) var page = 1;
    if ( !area ) var area = -9;
    if ( !size ) var size = 20;
    window.views.clean();
    var el = React.createElement(window.components.exams.view, { "eid": eid } );
    React.render(el, document.getElementById('container'));
};



/**
    
    @   user && dev options

**/
if ( !window.views ) window.views = {};
if ( !window.views.system ) window.views.system = {};

if ( !window.views.system.developer ) window.views.system.developer = {};
window.views.system.developer.options = function (){

    window.views.clean();
    var page = 1;
    var size = 20;
    var el = React.createElement(window.components.developer.options, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};

if ( !window.views.system.users ) window.views.system.users = {};
window.views.system.users.fetch = function (){
    window.views.clean();
    var page = 1;
    var size = 20;
    var el = React.createElement(window.components.users.list, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};
window.views.system.users.add = function (){
    return false;  // 废弃
    var el = React.createElement(window.components.users.add, { "page": page, "size": size} );
    React.render(el, document.getElementById('container'));
};


/*
    
    @   catalogs

*/
if ( !window.views ) window.views = {};
if ( !window.views.catalog ) window.views.catalog = {};
// catalog - questions
if ( !window.views.catalog.questions ) window.views.catalog.questions = {};
window.views.catalog.questions.fetch = function (cid){
    // 提取该标签的下属子标签
    window.views.clean();
    if ( !cid || parseInt(cid) < 1 ) var cid = 0;
    var page = 1;
    var size = 20;
    var props = { "cid": cid, "page": page, "size": size};
    var el = React.createElement(window.components.catalogs.atoms.list, props );
    React.render(el, document.getElementById('container'));
};


// catalog - papers
if ( !window.views.catalog.papers ) window.views.catalog.papers = {};
window.views.catalog.papers.fetch = function (cid){
    // 提取该标签的下属子标签
    window.views.clean();
    if ( !cid || parseInt(cid) < 1 ) var cid = 0;
    var page = 1;
    var size = 20;
    var props = { "cid": cid, "page": page, "size": size};
    var el = React.createElement(window.components.catalogs.papers.list, props );
    React.render(el, document.getElementById('container'));
};


// catalog - exams
if ( !window.views.catalog.exams ) window.views.catalog.exams = {};
window.views.catalog.exams.fetch = function (cid){
    // 提取该标签的下属子标签
    window.views.clean();
    if ( !cid || parseInt(cid) < 1 ) var cid = 0;
    var page = 1;
    var size = 20;
    var props = { "cid": cid, "page": page, "size": size};
    var el = React.createElement(window.components.catalogs.exams.list, props );
    React.render(el, document.getElementById('container'));
};

window.App = React.createClass({displayName: "App",
  getInitialState: function  () {
    return {
      "route": window.location.hash.substr(1),
      // "component": el
    };
  },
  componentDidMount: function () {
    var self = this;
    var onHashChange = function (e){
        var component = null;
        console.log( "HashChange: route " + window.location.hash.substr(1) );
        self.setState( { "route": window.location.hash.substr(1) } ); // , "component": component
    };
    window.addEventListener('hashchange', onHashChange);
  },
  views: function (){
        var self = this;
        check_login();

        var view = {"component": null, "props": {} };

        var route = self.state.route;
        if ( "auth" in window != true || "name" in window.auth != true ) {
            if ( self.state.route != "signin" && self.state.route != "signup" && self.state.route != "findpwd" ) {
                window.location.assign("#signin");
                route = "signin";
            }
        }

        if ( route.length < 1 ) route = "index";

        switch ( route ) {
            // case 'index': view.component = window.components.index.index; view.props = {}; break;
            
            case 'index': view.component = window.components.share.index; view.props = {}; break;
            case 'signin': view.component = window.components.auth.sign; view.props = {"action": "signin"}; break;
            case 'signup': view.component = window.components.auth.sign; view.props = {"action": "signup"}; break;
            case 'resetpwd': view.component = window.components.auth.sign; view.props = {"action": "resetpwd"}; break;
            case 'findpwd': view.component = window.components.auth.sign; view.props = {"action": "findpwd"}; break;
            case 'setting': view.component = window.components.user.settings.setting; view.props = {"component": window.components.user.settings.setTestArea}; break;
            case 'setting_test_area': 
                        view.component = window.components.user.settings.setting;
                        view.props = {"component": window.components.user.settings.setTestArea };
                        break;
            case 'setting_ques_area':
                        view.component = window.components.user.settings.setting;
                        view.props = {"component": window.components.user.settings.setQuestionAreaAndYear};
                        break;
            default:
                        view.component = window.components.error;
                        view.props = {"code": "404", "msg": "页面不存在"};
                        break;
        };
        if ( view.component == null ) return null;
        return React.createElement(view.component, view.props );
  },
  render: function () {
    var self = this;
    return (
        React.createElement("div", null, 
             self.views() 
        )
    );
  }
});


function check_login (){
    //115.29.211.139/.test(window.location.href)
    if ( true ) { console.log("check cookie");
        document.cookie.split("; ").map(function (cookie, index){
            try{
                key = cookie.split('=')[0];
                value = cookie.split("=")[1];
                if ( key == 'insti_admin' ) {
                    var uinfo = JSON.parse(decodeURIComponent(value));
                    window.auth = { "token": uinfo.token,"name":uinfo.uname};
                   
                  
                }
                    
                    
                                        
                                          
                                      
                 
            } catch (e){ 
                console.log(e);
                window.location.assign("#signin");
                return;
            }
        });
    } else {
        window.auth = {};
        window.auth.token = "65794a68634842705a4349364e6977696157357064485270625755694f6a45304e5441334e54457a4d7a5173496d563463476c795a576c75496a6f784e4455784d4445774e544d3066513d3d2e6b424e4e32474734534b2b5354482f7a36306d743838594158466b3d";
        // window.auth.token = "65794a68634842705a4349364e6977696332566a636d5630615751694f6a45774e7977696157357064485270625755694f6a45304d7a51334d4445774d545573496d563463476c795a576c75496a6f784e444d304f5459774d6a453166513d3d2e7754495864306155674a395a5776595a6b72364c7742564c5947493d";
        window.auth.name = "测试用户";
        window.auth.years = [2014, 2015];
        window.auth.areas = [420100, 420200];
    }
//http://sy.tiku.huatu.com/instiApis/whipbase/papers/papers?page=0&size=20&token=65794a68634842705a4349364e6977696157357064485270625755694f6a45304e4459344e6a67774d444d73496d563463476c795a576c75496a6f784e4451334d5449334d6a417a66513d3d2e62776a41353439734746724a527971705654776368614542362b303d
}